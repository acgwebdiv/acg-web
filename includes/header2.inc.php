	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/includes/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/includes/css/publicServer.css" type="text/css" media="screen" />
	<link rel="shortcut icon" href="/includes/images/favicon.png" type="image/png" />
	<link rel="icon" href="/includes/images/favicon.png" type="image/png" />
	<link rel="apple-touch-icon" href="/includes/images/favicon_app.png"/>
	<link rel="apple-touch-icon" sizes="72x72" href="/includes/images/favicon_app.png"/>
	<link rel="apple-touch-icon" sizes="114x114" href="/includes/images/favicon_app.png"/>
	<link rel="apple-touch-icon" sizes="152x152" href="/includes/images/favicon_app.png"/>
</head>

<body>
	<?php

    session_start();

    if(filter_has_var(INPUT_GET, "lang")) {
        
        $ac_lang = filter_input(INPUT_GET, "lang");

        // register the session and set the cookie
        $_SESSION['lang'] = $lang;
        setcookie('lang', $lang, time() + (3600 * 24 * 30));
        
    } else if(isSet($_SESSION['lang'])) {
        
        $ac_lang = $_SESSION['lang'];
        
    } else if(filter_has_var(INPUT_COOKIE, "lang")) {
        
        $ac_lang = filter_input(INPUT_COOKIE, "lang");
        
    } else {
        
        $ac_lang = 'en';
    }

    // switch ($ac_lang) {
    //   case 'en':
    //   $lang_file = 'lang.en.php';
    //   break;

    // //   case 'de':
    // //   $lang_file = 'lang.de.php';
    // //   break;

    // //   case 'pl':
    // //   $lang_file = 'lang.pl.php';
    // //   break;

    //   default:
    //   $lang_file = 'lang.en.php';
    // }
    // Setting $lang to "en" until international version is finished
    $ac_lang = 'en';
    
    include('header2.inc.lang.'.$ac_lang.'.php');

    ?>
    
    <!--<div id="languageButtons">-->
    <!--    <form action="<?php echo htmlentities($_SERVER['PHP_SELF']);?>">-->
    <!--    <ul id="languageList">-->
    <!--        <li><button type="submit" name="lang" value="en"><image src="/includes/language/flag_en.png" alt="EN"></li>-->
    <!--        <li><button type="submit" name="lang" value="de"><image src="/includes/language/flag_de.png" alt="DE"></li>-->
    <!--        <li><button type="submit" name="lang" value="pl"><image src="/includes/language/flag_pl.png" alt="PL"></li>-->
    <!--    </ul>-->
    <!--    </form>-->
    <!--</div>-->
	<div id="banner"></div>

<div class="contentWrapper">
    
    <ul class="navigationList">
    	<!-- <li class="seperator"></li> -->
    	<li><a href="/index.php"><?php echo $lang['MAIN_MENU_HOME'];?></a></li>
		<!-- <li class="seperator"></li> -->
		<li class="dropdown">
		    <a href="/about-us.php"><?php echo $lang['MAIN_MENU_ABOUTUS'];?></a>
		    <div class="dropdown-content">
		        <a href="/squadrons/squadrons.php"><?php echo $lang['MAIN_MENU_SQUADRONS'];?></a>
		        <a href="/events.php"><?php echo $lang['MAIN_MENU_EVENTS'];?></a>
		    </div>
		</li>
		<li class="dropdown">
		    <a href="/server/server.php"><?php echo $lang['MAIN_MENU_SERVER'];?></a>
		    <div class="dropdown-content">
		        <a href="/server/server.php#IL2" style="text-align: left"><?php echo $lang['IL2'];?></a>
		        <div>
		            <a href="/server/clod_documentation.php" style="font-size: 16px;"><?php echo $lang['IL2-DOC'];?></a>
		            <a href="/server/clod_mission_briefings.php" style="font-size: 16px;"><?php echo $lang['IL2-MISSIONS'];?></a>
		            <a href="/radar/radar.php" style="font-size: 16px;"><?php echo $lang['IL2-RADAR'];?></a>
		            <a href="/publicserver/pilotList.php" style="font-size: 16px;"><?php echo $lang['IL2-STATS'];?></a>
		        </div>
		        <a href="/server/server.php#DCS"><?php echo $lang['DCS'];?></a>
		        <div>
		            <a href="/server/dcs_mission_briefings.php" style="font-size: 16px;"><?php echo $lang['DCS-MISSIONS'];?></a>
		        </div>
		    </div>
		</li>
		<!-- <li class="seperator"></li> -->
		<li><a href="/forum/index.php"><?php echo $lang['MAIN_MENU_FORUM'];?></a></li>
		<!-- <li class="seperator"></li> -->
		<li><a href="/teamspeak.php"><?php echo $lang['MAIN_MENU_TEAMSPEAK'];?></a></li>
		<!-- <li class="seperator"></li> -->
		<li><a href="/flight_school/flight_school.php"><?php echo $lang['MAIN_MENU_FLIGHTSCHOOL'];?></a></li>
		<!-- <li class="seperator"></li> -->
		<li><a href="/donate.php"><?php echo $lang['MAIN_MENU_DONATE'];?></a></li>
	</ul>

		<div class="content">
