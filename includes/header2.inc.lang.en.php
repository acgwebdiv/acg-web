<?php

/* 
------------------
Language: English
------------------
*/

//header2.inc.php

$lang = array();
 
$lang['MAIN_MENU_HOME'] = 'Home';
$lang['MAIN_MENU_ABOUTUS'] = 'About ACG';
$lang['MAIN_MENU_FORUM'] = 'Forum';
$lang['MAIN_MENU_SQUADRONS'] = 'Squadrons';
$lang['MAIN_MENU_SERVER'] = 'Public Server';
$lang['IL2'] = 'IL-2 Cliffs of Dover';
$lang['IL2-DOC'] = '-Documentation';
$lang['IL2-MISSIONS'] = '-Missions';
$lang['IL2-RADAR'] = '-Radar';
$lang['IL2-STATS'] = '-Pilot statistics';
$lang['DCS'] = 'DCS Server';
$lang['DCS-MISSIONS'] = '-Missions';
$lang['MAIN_MENU_RADAR'] = 'Radar';
$lang['MAIN_MENU_TEAMSPEAK'] = 'TeamSpeak';
$lang['MAIN_MENU_FLIGHTSCHOOL'] = 'Flight School';
$lang['MAIN_MENU_EVENTS'] = 'Events';
$lang['MAIN_MENU_DONATE'] = 'Donate';

