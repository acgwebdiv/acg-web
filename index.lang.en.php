<?php

/* 
------------------
Language: English
------------------
*/

//index.php

$lang['INDEX_PAGE_HEADER'] = "Welcome to Air Combat Group";

$lang['INDEX_PAGE_P1'] = "Air Combat Group consists of both Allied and Axis squadrons
    under a single operational umbrella flying with full realism settings. We are
    an English and German speaking group located in Europe and the Americas and 
    meet on our own Teamspeak 3 server. Our pilots are interested in the history 
    of World War II air combat on all fronts, seeking immersive air combat in the 
    theatres of the time through historical campaign and other online activities. 
    By supporting both sides of the conflict we can create parity in theatre and 
    achieve common ground across the group, ultimately we seek to mirror the numbers
    flying on each side and support increasingly large campaigns.";

$lang['INDEX_PAGE_P2'] = "Our objective is to provide an unpressurised, non-competitive
    and friendly environment in which to enjoy combat operations, whichever side 
    a pilot prefers, and all standards are welcome regardless of experience. The
    squadrons within the group are historically based and the missions we fly are
    selected for their historical accuracy whether on our public server or in campaign.
    Group and squadron objectives are to re-enact the history ahead of competition,
    to attempt to be a part of being there, to learn about some of the experiences
    that were faced by the young pilots who fought for their countries in World 
    War II and to honour them by understanding more about what they faced.";

$lang['INDEX_PAGE_P3'] ="If this sounds of interest to you we are welcoming to new
    pilots of any standard or experience, even if you've never flown before - all
    you need is a decent PC with IL-2 Sturmovik: Cliffs of Dover or IL-2 Sturmovik: 
    Battle of Stalingrad (depending on the campaign being run) and a headset/microphone. 
    Just hop onto our TS3 comms at anytime, weekends are best, and join in the fun.";