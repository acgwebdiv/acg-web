<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | Stab/JG26</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>Stab/JG26</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/stabjg26.png" alt="Stab/JG26 Logo">
		
		<h2>History</h2>
		<div class="contentText">
			<p>
				Jagdgeschwader (JG) 26 "Schlageter" was known to the Allied forces in World War II as "The Abbeville Boys". The unit crest of a black gothic 'S' on a white shield was created in reflection of its involvement in the re-occupation of the Rhineland on March 7, 1936 (in violation of the Treaty of Versailles and the Locarno Pact). The locals had adopted the unit and renamed it after a local nationalist hero Albert Leo Schlageter. Leo, a World War I veteran, had been shot by the French in 1923 for attempting to destroy railway tracks taking coal from the Rhineland back to France as part of the war reparations imposed on Germany in the Treaty of Versailles. France's political policy did not allow any coal to be sold in Germany, even if they wanted to pay for it, despite a great shortage of coal during this time and the hardship the policy brought to the German people.
			</p>
		</div>

		<h2>Stab/JG26 in the ACG</h2>
		<div class="contentText">
			<p>
				The Stab is based on Audembert in the Pas-de-Calais-Nord territory, a short flight from South East England and the RAF. 
			</p>
		</div>

		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 11;
                            include("./rosterDisplayLWStab.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>