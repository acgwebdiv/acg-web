<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | 6./ZG766</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>6./ZG76</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/6zg76.png" alt="6./ZG76 Logo">
		
		<h2>History</h2>
		<div class="contentText">
		<p>
		Zerst&ouml;rergeschwader 76 was formed on 1 May 1939 with the I. Gruppe and II. Gruppe without a Geschwaderstab. The II. Gruppe was initially equipped with the Messerschmitt Bf 109D and was known as Jagdgruppe 176 but converted to the Bf-110 heavy fighter in March 1940 to take part in the Battle for France. Led by Oblt. Heinz Nacke, Staffelkapit&auml;n of 6./ZG76, gained initial success on 12 May 1940, when he shot down a Morane-Saulnier M.S.406 fighter. He added a further eight victories during the campaign.  II. Gruppe were based at Abbeville during the Battle of Britain flying escort missions for Luftwaffe bomber raids however suffered heavy losses, some 98 aircrew were killed, missing of POW. 6./ZG76 were withdrawn from France in 1941 to Jever in Germany with II. Gruppe prior to re-designation to III. Gruppe of Nachtjagdgeschwader 3 in November that year.  In August 1943 6./ZG76 was reformed from 3./NAGr.6 at Wertheim for Defence of the Reich operations intercepting the American daytime bomber raids but again suffered heavy losses to the American escort P-47 Thunderbolts and P-51 Mustangs. 6./ staffel converted to the new Me-410 in July 1944. II./ZG76 were one of the last two of the day Zerst&ouml;rergruppen operational and were stationed in Czechoslovakia and then East Prussia by Autumn of 1944. Although the unit was meant to convert to the FW-190 as II./JG76, in reality the pilots were distributed among numerous existing fighter units by the end of 1944.
		</p>
		<p>
		Unlike many Luftwaffe units, 6./ staffel did not have a crest but rather identified themselves with the superlative and aggressive shark's mouth livery on the noses of their Bf-110 heavy fighters and in ACG we have created a staffel crest for the unit in our own design.
		</p>
		</div>

		<h2>6./ZG76 in the ACG</h2>
		<div class="contentText">
		This is all you really need to know about this totally cool bunch of fellas
		<p>
		<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/26IRAch5PaM" frameborder="0" allowfullscreen></iframe>		
		</p>
		</div>

		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 9;
                            include("./rosterDisplayLW.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>