<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | 5./JG26</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>5./JG26</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/5jg26.png" alt="5./JG26 Logo">
		
		<h2>History</h2>
		<div class="contentText">
			<p>
				Jagdgeschwader (JG) 26 "Schlageter" was known to the Allied forces in World War II as "The Abbeville Boys". The unit crest of a black gothic 'S' on a white shield was created in reflection of its involvement in the re-occupation of the Rhineland on March 7, 1936 (in violation of the Treaty of Versailles and the Locarno Pact). The locals had adopted the unit and renamed it after a local nationalist hero Albert Leo Schlageter. Leo, a World War I veteran, had been shot by the French in 1923 for attempting to destroy railway tracks taking coal from the Rhineland back to France as part of the war reparations imposed on Germany in the Treaty of Versailles. France's political policy did not allow any coal to be sold in Germany, even if they wanted to pay for it, despite a great shortage of coal during this time and the hardship the policy brought to the German people.
			</p>
			<p>
				5./JG26 belonged to the second Gruppe within the Jagdgeschwader 26 (II./JG26) and they were the first to choose their unit emblem.  The cartoon raven is based on the popular 19th century picture story by comic artist Wilhelm Busch which tells a black comedy tale of a clumsy, unlucky raven named Hans Huckebein, in rhyme.  The raven was chosen by Oblt. Herwig Knueppel, Condor Legion veteran of the Geschwader.
			</p>
			<p>
				5./JG26 were active during the Battle of France as the Wermacht swept to power and operated right on the front line at the Pas-De-Calais throughout the Battle of Britain, predominantly on fighter sweeps and bomber escort missions.
				After the "Battle of Britain" JG26 was left to defend in the West with JG2 "Richthofen", where they remained throughout the whole war. II./JG26 picked Abbeville in Northern France as their main base to operate from while JG2 operated south of Le Harve as their responsibility to defend along that section of the French coast.
			</p>
			<p>
				Due to the quality of leadership, attention giving to training replacement pilots and the professionalism shown by these Luftwaffe pilots the Allied pilots came to respect the "Abbeville Boys". Any yellow nosed Messerschmitt or Focke-Wulf 190 (II./JG26 were the first units which were equipped with the Fw 190) ever seen was reported as being flown by JG26 and they were regarded as having some of the best pilots in the Luftwaffe throughout the War.
			</p>	
		</div>

		<h2>5./JG26 in the ACG</h2>
		<div class="contentText">
			<p>
				English speaking axis pilots based in European timezones are to fly the latest Messerschmitt Bf109E fighter with some of the finest pilots in II./JG26.
			</p>
			<p>
				The II. Gruppe is based on Marquise in the Pas-de-Calais-Nord territory a short flight from South East England and the RAF. 5./JG26 are strengthening into a fighting machine which the Tommies struggle to deal with as the battle swings toward Germany.
			</p>
			<p>
				Your primary role will be that of close escort to our bomber crews although JG26 are best at longer range free sweeps across enemy territory hunting down the RAF.
			</p>
		</div>

		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 6;
                            include("./rosterDisplayLW.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>