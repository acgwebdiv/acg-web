<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.1</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.1</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no1.png" alt="No.1 Logo">
		
		<h2>History</h2>
		<div class="contentText">
			
				<!--History of No.1-->
		</div>

		<h2>No.1 Squadron in the ACG</h2>
		<div class="contentText">
			<!--<p>-->
			<!--	Like their sister squadrons within Tangmere wing, No.1 operate -->
			<!--	the Hawker Hurricane fighter and is an english speaking squadron. -->
			<!--	As with all ACG squadrons for general online flight you will not -->
			<!--	be restricted to this type, or even the RAF, but you will fly the -->
			<!--	choice of your commander during official campaigns or upon his request.-->
			<!--</p>-->
			<!--<p>-->
			<!--	Your primary task will be to hunt down and destroy enemy bombers -->
			<!--	whilst maintaining vigilance as they usually come escorted by the -->
			<!--	Bf109 single engined fighter and Bf110 heavy fighter. Jerry often -->
			<!--	has a go, but as you will find, the Hurricane gives as good as it -->
			<!--	gets.-->
			<!--</p>-->
			<!--<p>Throughout the war No.1 used the Squadron code "JX".</p>-->
		</div>

		<div class="roster">
                    <h2>Roster</h2>
			<?php 
                $sqn = 18;
                include("./rosterDisplayRAF.php"); 
           	?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>