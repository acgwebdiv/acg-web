<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.111</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.111</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no111.png" alt="No.111 Logo">

		<h2>History</h2>
		<div class="contentText">
			<p>
				No.111 Squadron was formed at Deir-el-Belah in Palestine on 1 August 1917 as a fighter squadron to support the Army in its offensive against the Turks in Palestine and Syria. Initially it used a variety of types but standardised on Nieuports and SE5As at the beginning of 1918, the latter becoming its standard equipment in July. In October the campaign came to an end and the squadron was withdrawn to Egypt where it re-equipped with Bristol Fighters in February 1919. On 1 February 1920, No.111 was renumbered 14 Squadron.  The crest was designed to include this action with two swords in saltire a cross potent quadrat charged with three seaxes fesswise in pale. The cross in the badge signifies Palestine, its World War One area of operation, the seaxes signify Essex its base and the swords London which it defended.  The motto "Adstantes" translates to "Standing to" to signal the squadrons preparedness.
			</p>
			<p>
				On 1 October 1923, No.111 reformed at Duxford as a fighter squadron, initially with one flight of Grebes. On 1 April 1924, a second flight of Snipes was added and in January 1925, a third flight with Siskins, which became standard equipment soon afterwards. In January 1931, Bulldogs were received and were replaced in May 1936 with Gauntlets. No.111 became the RAF's first Hurricane squadron in January 1938 and was engaged in defensive duties during the first months of World War Two. During the German invasion of France in May 1940, the squadron operated across the Channel, occasionally using French Airfields, followed by a short but busy period providing air cover for the evacuation fleet at Dunkirk. During the first half of the Battle of Britain it took part in the defence of south-east England, being withdrawn in September to refit in Scotland.
			</p>
			<p>
				The squadron replaced its Hurricanes with Supermarine Spitfires in April 1941 and in November relocated to RAF Gibraltar for support of Operation Torch for air support in North Africa. In a similar role it moved to Malta in June 1943 to support the invasion of Sicily. No.111 moved through Italy with the advancing Allied ground forces and remained there until the end of the war, after which it moved to Austria. The squadron disbanded in May 1947. 269 aircraft were claimed shot down, making the squadron one of the top RAF scorers for the war.
			</p>	
		</div>

		<h2>No.111 Squadron in the ACG</h2>
		<div class="contentText">
			<p>	
				English speaking Hawker Hurricane pilots are posted to No.111 Squadron. For general online flight you will not be restricted to the Hurricane, or even the RAF, but you will fly the choice of your commander during official campaigns or upon his request.
			</p>
			<p>
				Just as in real life No.111 are part of the famous Kenley wing, just to the south of London, so you will need to become familiar with its location and the region geography.  RAF Croydon, slightly north of Kenley is the usual operating aerodrome for No.111.</p>
			</p>
			
			<p>Throughout the war No.111 used the Squadron code "JU".</p>
		</div>
		
		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 10;
                            include("./rosterDisplayRAF.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>