<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.302</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.302</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no302.png" alt="No.302 Logo">
		
		<h2>History</h2>
		<div class="contentText">
                    <p>
                        In the early summer of 1940 there were 443 Pilots in the UK who had escaped both the Nazi occupation of their homeland and the fall of France (where some had flown under French colours).  A good many had already joined the Royal Air Force Volunteer Reserve and were flying in standard RAF squadrons as RAF pilots.  However, an agreement between the Polish Government in exile and the UK government allowed for purely Polish units to be created, albeit under British command and control.  The first of these was 302 Squadron.
                     </p>
                     <p>
                        302 Squadron was stood up on the 10 July 1940 under the command of Squadron Leader W.A.J. Satchell with British officers posted in as temporary flight commanders, engineering officer and intelligence officer.  The first polish officers, P/O Lapka and P/O Pilch arrived to RAF Leconfield from No. 6 OTU on the 17 July.  It was to fly Hurricanes. 
                     </p>
                     <p>
                        The remainder of July was spent assimilating new pilots and ground crew and very intense training, the latter necessary to enable the squadron to fit into Fighter Command's system. One of No. 302 Sqn's pilots was F/ Lf Wladyslaw Gnys, the first allied pilot to down a german aircraft in World War 2 (he shot down two Do-17 on 1st September 1939, around 07:00 AM.) August rolled around and 302 became part of 12 Group's operation strength. Their cherry was lost on 20 August 1940. 
                     </p>
                     <p>
                         <i>"Green Section, B Flight, sighted e/a at 1910 flying at 3,000 feet East between Hull and Spurn Point.  Section attacked and the e/a crashed into the sea 6 miles S.W. of withernsea.  4 Prisoners taken.  This was the Squadron's first engagement with the enemy"  (from 302 Operations Record Book) </i>
                     </p>
                     <p>
                        On 12 September 302 started to move South to Duxford into 11 Group and the "Frontline".   They became operational there on the 15th and made their mark immediately.  On that day they claimed eleven enemy aircraft destroyed and another five probably destroyed.  Not without loss though - F/lt Chopik was killed in action.
                     </p>
                     <p>
                        302 was launched.  After sterling work in the Battle of Britain they then went on to perform a number of roles including pure fighter, bomber escort and  fighter bomber in Spitfires and Mustangs.  They flew nearly 11,000 operational sorties and lost, killed and missing, 33 pilots. However, they destroyed 47 enemy aircraft with another 25 probables plus rather a lot of ground targets wrecked.
                     </p>
		</div>

		<h2>No.302 Squadron in the ACG</h2>
		<div class="contentText">
                        <p>
				Like their sister squadrons within Tangmere wing, No.302 operate the Hawker Hurricane fighter and is a polish speaking squadron. As with all ACG squadrons for general online flight you will not be restricted to this type, or even the RAF, but you will fly the choice of your commander during official campaigns or upon his request.
			</p>
			<p>
				Your primary task will be to hunt down and destroy enemy bombers whilst maintaining vigilance as they usually come escorted by the Bf109 single engined fighter and Bf110 heavy fighter. Jerry often has a go, but as you will find, the Hurricane gives as good as it gets.
			</p>
			<p>Throughout the war No.302 used the Squadron code "WX".</p>
                     
		</div>

		<div class="roster">
                    <h2>Roster</h2>
                    <?php 
                        $sqn = 15;
                        include("./rosterDisplayRAF.php"); 
                    ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");