<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.32</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.32</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no32.png" alt="No.32 Logo">
		
		<h2>History</h2>
		<div class="contentText">
			<p>
				No.32 "The Royal" Squadron was formed on 12 January 1916 at Netheravon and moved to France as a fighter squadron in May 1916. Equipped with DH2s it flew patrols over the Western Front for a year before beginning to re-equip with DH5s. These in turn began to be replaced by SE5As in December 1917 which were flown for the rest of the war on fighter and ground attack missions.  The squadron crest is a hunting horn stringed, symbolising the squadron's ability to hunt the enemy down, the motto "Adeste Comites" translates to "Rally round, comrades" alluding to the team mentality of the squadron. In March 1919, the squadron returned to the UK as a cadre and disbanded on 29 December 1919.
			</p>
			<p>
				No.32 reformed on 1 April 1923 at Kenley as a single flight of Snipe fighters. A second flight was formed on 10 December 1923 and a third brought the squadron up to strength on 1 June 1924. Grebes were received at the end of 1924 and were replaced by Gamecocks two years later. They were then equipped in succession with Siskins, Bulldogs and Gauntlets, finally receiving Hurricanes in October 1938 and these were flown on defensive patrols when World War Two broke out.
			</p>
			<p>
				In May 1940, the squadron flew patrols over northern France and took part in the defence of south-east England during the opening weeks of the Battle of Britain before moving to northern England at the end of August 1940.
			</p>
			<p>
				The Squadron's Hurricanes saw little action throughout 1941, but did attempt, unsuccessfully, to escort the Fairey Swordfish biplanes of 825 Naval Air Squadron during their doomed attempt to stop the German warships Scharnhorst, Gneisenau and Prinz Eugen during the Channel Dash on 12 February 1942, and then carried out a number of night intruder operations before being deployed overseas.  Following Operation Torch in North Africa in December 1942, No.32 Squadron was deployed with its Hurricanes to Algeria, converting to the Supermarine Spitfire by July 1943.  Later operations included a deployment to Greece where the squadron took part in the Greek Civil War from September 1944 to February 1945.
			</p>
		</div>

		<h2>No.32 Squadron in the ACG</h2>
		<div class="contentText">
			<p>
				Like their sister squadron within Biggin Hill wing, No.32 operate the Hawker Hurricane fighter and is an English speaking squadron. As with all ACG squadrons for general online flight you will not be restricted to this type, or even the RAF, but you will fly the choice of your commander during official campaigns or upon his request.
			</p>
			<p>
				As RAF Biggin Hill is a large sector aerodrome there is room to share with the Spitfires of No.610 for your duration whilst the Battle of Britain rages so you will need to become familiar with the local landmarks.
			</p>
			<p>
				Your primary task will be to hunt down and destroy enemy bombers whilst maintaining vigilance as they usually come escorted by the Bf109 single engined fighter and Bf110 heavy fighter. Jerry often has a go, but as you will find, the Hurricane gives as good as it gets.
			</p>
			<p>Throughout the war No.32 used the Squadron code "GZ".</p>
		</div>

		<div class="roster">
                    <h2>Roster</h2>
			<?php 
                            $sqn = 14;
                            include("./rosterDisplayRAF.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>