<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.610</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.610</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no610.png" alt="No.610 Logo">

		<h2>History</h2>
		<div class="contentText">
			<p>
				No.610 "County of Chester" Squadron was formed at Hooton Park on 10 February 1936 as a day bomber unit of the Auxiliary Air Force. Initially equipped with Harts, they began flying training in May 1936 and in May 1938 re-equipped with Hinds.  The squadron crest, a garb, a wheatsheaf, was chosen as such charges appear in the armorial bearings of the City of Chester as do the colours of red and blue.  The motto, Alifero Tollitur Axe Ceres, translates as "Ceres (the Goddess of corn) rising in a winged car (a chariot)" is a quote from Ovid. 
			</p>
			<p>
				On 1 January 1939, the squadron was redesignated a fighter unit but retained their Hinds in anticipation of the arrival of Defiants. On the outbreak of war they received Hurricanes, but by the end of September 1939 had exchanged these for Spitfires.
			</p>
			<p>
				Operational on 21 October 1939 in time for the German offensive in May 1940, No.610 moved to Biggin Hill and helped to provide fighter cover for the Dunkirk evacuation beaches and shipping, then taking part in the greater Battle of Britain. After sustaining severe casualties the squadron moved to RAF Acklington for rest and recuperation at the end of August. 
			</p>
			<p>
				In 1941, the squadron moved south to RAF Tangmere where they were one of Douglas Bader's three Spitfire squadrons of the Tangmere wing. 610 Squadron remained based in the UK until 1945, when it moved to the continent to provide fighter cover as the allies entered Germany. No.610 Squadron was disbanded before the end of the war at RAF Warmwell on 3 March 1945.
			</p>
		</div>

		<h2>No.610 Squadron in the ACG</h2>
		<div class="contentText">
			<p>
				Hero Spitfire pilots will be posted to No.610 Squadron to fly the Supermarine creation and fear of the Luftwaffe.  No.610 operate out of RAF Biggin Hill with sister squadrons No.32 and No.501 who complete the wing. For general online flight you will not be restricted to the Spitfire, or even the RAF, but you will fly the choice of your commander during official campaigns or upon his request. 
			</p>
			<p>
				As a main sector station on the front line for the Battle of Britain you will need to become familiar with the local landmarks if the fight is to be won.
			</p>
			<p>
				Your primary task will be to hunt down and destroy enemy fighters,  No.610 are known to always be ready and keen to hand the enemy a swift black eye.
			</p>
			<p>Throughout the war No.610 used the Squadron code "DW".</p>
		</div>
		
		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 12;
                            include("./rosterDisplayRAF.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>