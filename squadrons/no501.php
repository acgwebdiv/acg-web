<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.501</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.501</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no501.png" alt="No.501 Logo">

		<h2>History</h2>
		<div class="contentText">
			<p>
				No.501 "County of Gloucester" Squadron was the fourteenth of the twenty-one flying units in the Royal Auxiliary Air Force, the volunteer reserve part of the British Royal Air Force and took the White Boar for their crest, a noble symbol of King Richard III, Duke of Gloucester.  The motto "Nil Time" literally means "Fear Nothing".
			</p>
			<p>
				The squadron won seven battle honours, flying Hurricane, Spitfire and Tempest fighter aircraft during World War II, and was one of the most heavily engaged units in RAF Fighter Command. In particular, the Squadron saw extensive action during the Battle of France and Battle of Britain.
			</p>
			<p>
				Formed at Filton in June 1929 as a Special Reserve unit it was not until 1939 and several types later that No.501 was redesignated a fighter squadron and received the Hurricane MkI. The squadron flew defensive patrols until the German attack on France in May 1940, when it moved across the Channel to provide fighter cover for the BEF, retiring to Brittany and returning to the UK when France surrendered.
			</p>
			<p>
				No.501 was based in southern England throughout the Battle of Britain and began fighter sweeps over France early in 1941. In April 1941 they converted to Spitfires, flying sweeps until July 1944 before converted to Tempests which were used to catch V1 flying bombs over south-east England.
			</p>
		</div>

		<h2>No.501 Squadron in the ACG</h2>
		<div class="contentText">
			<p>
				Should you be based in the European timezones and wish to challenge yourself, or just have a love for the unsung hero of the RAF during the Battle of Britain, then you will be posted to No.501 "County of Gloucester" Squadron to fly the Hawker Hurricane fighter.
				For general online flight you will not be restricted to the Hurricane, or even the RAF, but you will fly the choice of your commander during official campaigns or upon his request. 
			</p>
			<p>
				Just as in real life No.501 operate out of Gravesend aerodrome just to the east of London and RAF Kenley, the main sector station, for most of the Battle of Britain so you will need to become familiar with the local landmarks. 
			</p>
			<p>
				Your primary task will be to hunt down and destroy enemy bombers so a knowledge of enemy types will be required. No.501 are not afraid of enemy fighters however and are happy to attack and mix with Jerry, much to his displeasure.
			</p>
			<p>Throughout the war No.501 used the Squadron code "SD".</p>
		</div>
		
		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 2;
                            include("./rosterDisplayRAF.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>