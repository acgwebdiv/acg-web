<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.43</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.43</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no43.png" alt="No.43 Logo">
		
		<h2>History</h2>
		<div class="contentText">

		</div>

		<h2>No.43 Squadron in the ACG</h2>
		<div class="contentText">

		</div>

		<div class="roster">
                    <h2>Roster</h2>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>