<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | Squadrons</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>Squadrons</h1>

		<div class="contentText">
		
			<!-- Roayal Air Force -->
			<table class="squadronTable" border="0">
				<tr>
					<th><img src="../includes/images/navigation/raf_small.png"></th>
					<th>Royal Air Force</th>
					<th><img src="../includes/images/navigation/raf_small.png"></th>
				</tr>
				<tr>
					<td><a title="No.64" href="/squadrons/no64.php"><img src="../includes/images/logos/no64.png"><br>No.64</a></td>
					<td><a title="No.501" href="/squadrons/no501.php"><img src="../includes/images/logos/no501.png"><br>No.501</a></td>
					<td><a title="No.615" href="/squadrons/no615.php"><img src="../includes/images/logos/no615.png"><br>No.615</a></td>
				</tr>

				<tr>
					<td><a title="No.32" href="/squadrons/no32.php"><img src="../includes/images/logos/no32.png"><br>No.32</a></td>
					<td><a title="No.111" href="/squadrons/no111.php"><img src="../includes/images/logos/no111.png"><br>No.111</a></td>
					<td><a title="No.610" href="/squadrons/no610.php"><img src="../includes/images/logos/no610.png"><br>No.610</a></td>
				</tr>

			</table>

			<br/><br/>

			<!-- Luftwaffe -->
			<table class="squadronTable" border="0">
				<tr>
					<th><img src="../includes/images/navigation/luftwaffe_small.png"></th>
					<th>Luftwaffe</th>
					<th><img src="../includes/images/navigation/luftwaffe_small.png"></th>
				</tr>
				<tr>
					<td><a title="Stab/JG26" href="/squadrons/stabjg26.php"><img src="../includes/images/logos/stabjg26.png"><br>Stab/JG26</a></td>
					<td></td>
					<td><a title="6./ZG76" href="/squadrons/6zg76.php"><img src="../includes/images/logos/6zg76.png"><br>6./ZG76</a></td>
				</tr>

				<tr>
					<td><a title="4./JG26" href="/squadrons/4jg26.php"><img src="../includes/images/logos/4jg26.png"><br>4./JG26</a></td>
					<td><a title="5./JG26" href="/squadrons/5jg26.php"><img src="../includes/images/logos/5jg26.png"><br>5./JG26</a></td>
					<td><a title="6./JG26" href="/squadrons/6jg26.php"><img src="../includes/images/logos/6jg26.png"><br>6./JG26</a></td>
				</tr>

				<tr>
					<td><a title="7./JG26" href="/squadrons/7jg26.php"><img src="../includes/images/logos/7jg26.png"><br>7./JG26</a></td>
					<td><a title="8./JG26" href="/squadrons/8jg26.php"><img src="../includes/images/logos/8jg26.png"><br>8./JG26</a></td>
					<td><a title="9./JG26" href="/squadrons/9jg26.php"><img src="../includes/images/logos/9jg26.png"><br>9./JG26</a></td>
				</tr>
			</table>

	</div>

<?php
	include("../includes/footer.inc.php");
?>