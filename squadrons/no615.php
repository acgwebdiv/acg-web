<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.615</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.615</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no615.png" alt="No.615 Logo">
		
		<h2>History</h2>
		<div class="contentText">
			<p>
				No.615 "County of Surrey" Squadron was formed in June 1937 at Kenley as an army co-operation unit of the Auxiliary Air Force and their crest displays the oak sprig representing the Weald of Surrey. The motto "Conjunctis Viribus" translates as "By our united force".
			</p>
			<p>
				They became a fighter squadron at the end of 1938 after receiving Gauntlet fighters. These were replaced by Gladiators in May 1939, which the squadron took to France in November 1939 as part of the Air Component of the BEF. In April 1940 they converted to Hurricanes in the field but only completed this task after pulling back to England as France collapsed.
			</p>
			<p>
				 No.615 took part in the opening stages of the Battle of Britain, rested in between and returned in October, operating mainly from RAF Kenley. In February 1941 they began taking part in sweeps over France for escort and ground attack missions before leaving for India in March 1942. The squadron assembled at Jessore and moved to the Burma front to fly ground-attack and defensive sorties. In May 1943 they re-equiped with Spitfires and fought on until being withdrawn for defensive patrols around Calcutta in August 1944, only resuming the  offensive operations in 1945.
			</p>
			<p>
				After victory in Europe No.615 was officially disbanded but No.135 Squadron at Vizagapatam was immediately renumbered No.615 Squadron. Equipped with Thunderbolts, it was training for the invasion of Malaya until being disbanded in September 1945.
			</p>
		</div>

		<h2>No.615 Squadron in the ACG</h2>
		<div class="contentText">
			<p>
				Like their sister squadron at RAF Kenley, No.615 operate the Hawker Hurricane fighter, and likewise communicates in English in the European timezone. As with all ACG squadrons for general online flight you will not be restricted to this type, or even the RAF, but you will fly the choice of your commander during official campaigns or upon his request.
			</p>
			<p>
				As RAF Kenley is a large sector aerodrome thee is room to share with No.64 for your duration whilst the Battle of Britain rages so you will need to become familiar with the local landmarks.
			</p>
			<p>
				Your primary task will be to hunt down and destroy enemy bombers whilst maintaining vigilance as they usually come escorted by the Bf109. Jerry often has a go, but as you will find, the Hurricane gives as good as it gets.
			</p>
			<p>Throughout the war No.615 used the Squadron code "KW".</p>
		</div>

		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 4;
                            include("./rosterDisplayRAF.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>