<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | No.64</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
		<h1>No.64</h1>
		
		<img class="squadronBadge" src="../includes/images/logos/no64.png" alt="No.64 Logo">

		<h2>History</h2>
		<div class="contentText">
			<p>
				No.64 Squadron was formed at Sedgeford in August 1916 as a training unit but in October 1917 the squadron moved to the Western Front for fighter patrol and ground attack duties for the rest of World War I. In February 1919 it returned to the UK and disbanded on 31 December 1919. No.64 was reformed at Heliopolis in 1936 from D Flights in No.6 and No.208 squadrons and it is for this time spent in Egypt during the Abyssinian crisis which provides the Scarab beetle for their squadron crest.  The motto "Tenax Propositi" means "Firm of purpose" indicating the seriousness with which No.64 squadron pilots approach their duties.
			</p>
			<p>
				On the outbreak of World War II the squadron was engaged in patrols off the East Coast in Blenheims providing fighter defence for the Home Fleet before a conversion to Spitfires took place in time for the squadron to cover the evacuation from Dunkirk. Immediately afterwards, No.64 operated from RAF Kenley playing a key role during the Battle of Britain beating off the German attackers. After a rest period they took part in offensive sweeps over northern France, progressing to attacks such as Overlord and Market Garden until late 1944. They ended the war in the Mustang III long range fighter and escorting RAF bombers into the heart of the Reich.
			</p>
		</div>

		<h2>No.64 Squadron in the ACG</h2>
		<div class="contentText">
			<p>	
				Should you be based in the European timezones and have the desire to fly the Supermarine Spitfire fighter then you will be posted to No.64 Squadron. For general online flight you will not be restricted to the Spitfire, or even the RAF, but you will fly the choice of your commander during official campaigns or upon his request.
			</p>
			<p>
				Just as in real life No.64 operate from Kenley aerodrome, just to the south of London, so you will need to become familiar with its location and the region geography.</p>
			</p>
			<p>
				Your primary task will be to engage fighters and provide cover and escort for other RAF types in accordance with mission orders. Note that the Hun do not like the Spitfire and will be very respectful and knowledgeable of its qualities so be careful not to play into his hands.
			</p>
			<p>Throughout the war No.64 used the Squadron code "SH".</p>
		</div>
		
		<div class="roster">
			<h2>Roster</h2>
			<?php 
                            $sqn = 3;
                            include("./rosterDisplayRAF.php"); 
                        ?>
		</div>
		
<?php
	include("../includes/footer.inc.php");
?>