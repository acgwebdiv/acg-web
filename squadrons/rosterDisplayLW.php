<?php
include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/db_connect.php');
include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/characterDBFunctions.php');

$dbx = getDBx();

$sql = "SELECT roster.positionID, roster.squadronID, roster.memberID, ".
       "acgmembers.callsign, hangar.image AS aircraftImage, hangar.designation ".
       "FROM roster ".
       "LEFT JOIN acgmembers ON roster.memberID = acgmembers.id ".
       "LEFT JOIN hangar ON hangar.memberID = acgmembers.id ".
       "WHERE roster.squadronID = $sqn ORDER BY positionID ASC";
$result = mysqli_query($dbx, $sql);
$geschwaderArray = array();
while($row = mysqli_fetch_assoc($result)) {
    
    $schwarmInfo = getSchwarmInfo($row["positionID"]);
    $schwarm = $schwarmInfo[0];
    $schwarmPos = $schwarmInfo[1];
    $role = $schwarmInfo[2];
    
    $geschwaderArray[$schwarm][$schwarmPos]["role"] = $role;
    $geschwaderArray[$schwarm][$schwarmPos]["link"] = "aircombatgroup.co.uk/acg-pam/memberDetails.php?m_id=".$row["memberID"];
    $geschwaderArray[$schwarm][$schwarmPos]["callsign"] = $row["callsign"];
    $geschwaderArray[$schwarm][$schwarmPos]["acImage"] = $row["aircraftImage"];
    $geschwaderArray[$schwarm][$schwarmPos]["designation"] = $row["designation"];
    
    $rankID = getMostRecentRankID($row["memberID"], $dbx);
    $sql = "SELECT name, image FROM ranks WHERE id = $rankID";
    $querry = mysqli_query($dbx, $sql);
    $rank_result = mysqli_fetch_assoc($querry);
    
    $geschwaderArray[$schwarm][$schwarmPos]["rankName"] = $rank_result["name"];
    $geschwaderArray[$schwarm][$schwarmPos]["raImage"] = $rank_result["image"];
}
function getSchwarmInfo($position){
    
    
    switch ($position) {
        case 1:
            return array(1, 1, 'Staffelführer');
        case 2:
            return array(1, 2, 'Katschmarek');
        case 3:
            return array(1, 3, 'Rottenführer');
        case 4:
            return array(1, 4, 'Katschmarek');
        case 5:
            return array(2, 1, 'Schwarmführer');
        case 6:
            return array(2, 2, 'Katschmarek');
        case 7:
            return array(2, 3, 'Rottenführer');
        case 8:
            return array(2, 4, 'Katschmarek');
        case 9:
            return array(3, 1, 'Schwarmführer');
        case 10:
            return array(3, 2, 'Katschmarek');
        case 11:
            return array(3, 3, 'Rottenführer');
        case 12:
            return array(3, 4, 'Katschmarek');
        case 13:
            return array(4, 1, 'Schwarmführer');
        case 14:
            return array(4, 2, 'Katschmarek');
        case 15:
            return array(4, 3, 'Rottenführer');
        case 16:
            return array(4, 4, 'Katschmarek');
        }
}

?>
<table class="rosterTable" border="1">
    <tr class="rosterTableHeader">
        <th colspan="3">Pilot</th>
        <th colspan="2" rowspan="4">Aircraft</th>
    <tr>
    <tr class="rosterTableHeader">
        <th>Rank</th>
        <th>Callsign</th>
        <th>Role</th>
    <tr>
    <?php
    
    for($n = 1; $n < 5; $n++) {
        
        if(array_key_exists($n, $geschwaderArray)){
            
            $memberArray = $geschwaderArray[$n];
            if(count($memberArray)>0) {
    
    ?>
    <tr class="rosterTableSubHeader">
        <td colspan="5"><?php echo $n.".Schwarm";?></td>
    </tr>   
    <?php
    
                foreach ($memberArray as $memberPos => $memberInfo) {
                    $rankImage = "../acg-pam/imgsource/LW-ranks/".$memberInfo["raImage"];
                    if(!empty($memberInfo["acImage"])){
                        $acftImage = "../includes/images/aircraft/".$memberInfo["acImage"].".png";
                        $acftImageSmall = "../includes/images/aircraft/".$memberInfo["acImage"]."_small.png";
                    }
    
    ?>
    
    <tr>
        <td><img src="<?php echo $rankImage; ?>" title="<?php echo $memberInfo["rankName"]; ?>" alt="<?php echo $memberInfo["rankName"]; ?>"></td>
        <td class="rosterTablePilot"><?php echo $memberInfo["callsign"]; ?></td>
        <td><?php echo(htmlentities($memberInfo["role"])); ?></td>
        <td class="rosterTableAircraft">
            <?php if(!empty($memberInfo["acImage"])){ ?>
                <a href="<?php echo $acftImage;?>"> <img src="<?php echo $acftImageSmall;?>"></a>   
            <?php } else { echo $memberInfo["designation"];} ?>
        </td>
    </tr>
    
    <?php    
                }
            }
        }
    }    
//Get all aircraft still in hangar
$sql = "SELECT memberID, designation, image FROM hangar ".
       "WHERE squadronID = $sqn AND memberID <= 0";
$result = mysqli_query($dbx, $sql);
if(mysqli_num_rows($result) > 0){
?>
    <tr class="rosterTableSubHeader">
        <td colspan="5">Aeroplanes In Hanger</td>
    </tr>
    <?php while($row = mysqli_fetch_assoc($result)){ 
        if($row["memberID"] == -1){
            $callsign = "Reserved";
        } else {
            $callsign = "";
        }
        
        if(!empty($row["image"])){
            $acftImage = "../includes/images/aircraft/".$row["image"].".png";
            $acftImageSmall = "../includes/images/aircraft/".$row["image"]."_small.png";
        }
    ?>
    <tr>
        <td></td>
        <td class="rosterTablePilot"><?php echo $callsign; ?></td>
        <td></td>
        <td class="rosterTableAircraft">
            <?php if(!empty($row["image"])){ ?>
                <a href="<?php echo $acftImage;?>"> <img src="<?php echo $acftImageSmall;?>"></a>   
            <?php } else { echo $row["designation"];} ?>
        </td>
    </tr>
<?php 
    
    }
}
    
?>
</table>