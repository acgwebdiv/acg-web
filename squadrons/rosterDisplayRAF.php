<?php
include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/db_connect.php');

$dbx = getDBx();

$sql = "SELECT acgmembers.id, acgmembers.callsign, ranks.value, ".
       "ranks.image AS ranksImage, ranks.name AS rName, acgmembers.status, currentsqu2.squadronid, ".
       "hangar.designation, hangar.image AS aircraftImage, ".
       "currentsts2.statusID ".
       "FROM acgmembers ".
       "LEFT JOIN ".
	        "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	    "JOIN ".
     		    "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
            "ON (currentsts.memberID, currentsts.sdate) = ".
            "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
        "ON currentsts2.memberID = acgmembers.id ".
       "LEFT JOIN hangar ON acgmembers.id = hangar.memberID ". 
       "LEFT JOIN ".
            "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
            "JOIN ".
                "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                "FROM transfers GROUP BY memberid) AS currentsqu ".
            "ON (currentsqu.memberid, currentsqu.tdate) = ".
            "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
       "ON currentsqu2.memberid = acgmembers.id ".
       "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
        "LEFT JOIN ".
            "(SELECT promotions.memberid, promotions.value, promotions.date FROM promotions ".
            "JOIN ".
                "(SELECT promotions.memberid, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                "FROM promotions GROUP BY memberid) AS currentrank ".
            "ON (currentrank.memberid, currentrank.pdate) = ".
            "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
       "ON currentrank2.memberid = acgmembers.id ".
       "LEFT JOIN ranks ON (currentrank2.value, squadrons.faction) = (ranks.value, ranks.faction) ".
       "WHERE currentsts2.statusID = 1 ".
       "AND (currentsqu2.squadronid = $sqn OR squadrons.acg_unit = $sqn) ".
       "ORDER BY value DESC, joiningdate ASC";
    // echo $sql;
$result = mysqli_query($dbx, $sql);
$memberArray = array();

$sql = "SELECT acgmembers.id, acgmembers.callsign, ranks.value, ".
       "ranks.image AS ranksImage, ranks.name AS rName, acgmembers.status, currentsqu2.squadronid, ".
       "hangar.designation, hangar.image AS aircraftImage, ".
       "currentsts2.statusID ".
       "FROM acgmembers ".
       "LEFT JOIN ".
	        "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	    "JOIN ".
     		    "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
            "ON (currentsts.memberID, currentsts.sdate) = ".
            "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
        "ON currentsts2.memberID = acgmembers.id ".
       "LEFT JOIN hangar ON acgmembers.id = hangar.memberID ". 
       "LEFT JOIN ".
            "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
            "JOIN ".
                "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                "FROM transfers GROUP BY memberid) AS currentsqu ".
            "ON (currentsqu.memberid, currentsqu.tdate) = ".
            "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
       "ON currentsqu2.memberid = acgmembers.id ".
       "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
        "LEFT JOIN ".
            "(SELECT promotions.memberid, promotions.value, promotions.date FROM promotions ".
            "JOIN ".
                "(SELECT promotions.memberid, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                "FROM promotions GROUP BY memberid) AS currentrank ".
            "ON (currentrank.memberid, currentrank.pdate) = ".
            "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
       "ON currentrank2.memberid = acgmembers.id ".
       "LEFT JOIN ranks ON (currentrank2.value, squadrons.faction) = (ranks.value, ranks.faction) ".
       "WHERE currentsts2.statusID = 4 ".
       "AND (currentsqu2.squadronid = $sqn OR squadrons.acg_unit = $sqn) ".
       "ORDER BY value DESC, joiningdate ASC";
    // echo $sql;
$mresult = mysqli_query($dbx, $sql);

//switch ($sqn) {
//    
//    case 2:
//        $imageStr = "no501_tbp";
//        break;
//    case 3:
//        $imageStr = "no64_tbp";
//        break;
//    case 4:
//        $imageStr = "no615_tbp";
//        break;
//    case 10:
//        $imageStr = "no111_tbp";
//        break;
//    case 12:
//        $imageStr = null;
//        break;
//}
//
//$memberArray[2]["link"] = "aircombatgroup.co.uk/acg-pam/memberDetails.php?m_id=2";
//$memberArray[2]["rankName"] = "Wing Commander";
//$memberArray[2]["raImage"] = "RankWingCommander.png";
//$memberArray[2]["callsign"] = "Osprey";
//$memberArray[2]["designation"] = "P for Pip";
//if(!is_null($imageStr)){
//    $memberArray[2]["acImage"] = $imageStr;
//} else {
//    $memberArray[2]["acImage"] = null;
//}

while($row = mysqli_fetch_assoc($result)) {
    $memberId = $row["id"];
    $memberArray[$memberId]["status"] = $row["status"];
    $memberArray[$memberId]["link"] = "aircombatgroup.co.uk/acg-pam/memberDetails.php?m_id=".$row["id"];
    $memberArray[$memberId]["rankName"] = $row["rName"];
    $memberArray[$memberId]["raImage"] = $row["ranksImage"];
    $memberArray[$memberId]["callsign"] = $row["callsign"];
    $memberArray[$memberId]["designation"] = $row["designation"];
    $memberArray[$memberId]["acImage"] = $row["aircraftImage"];
}
while($row = mysqli_fetch_assoc($mresult)) {
    $memberId = $row["id"];
    $memberArray[$memberId]["status"] = $row["status"];
    $memberArray[$memberId]["link"] = "aircombatgroup.co.uk/acg-pam/memberDetails.php?m_id=".$row["id"];
    $memberArray[$memberId]["rankName"] = $row["rName"];
    $memberArray[$memberId]["raImage"] = $row["ranksImage"];
    $memberArray[$memberId]["callsign"] = $row["callsign"];
    $memberArray[$memberId]["designation"] = $row["designation"];
    $memberArray[$memberId]["acImage"] = $row["aircraftImage"];
}

?>
<table class="rosterTable" border="1">
    <tr class="rosterTableHeader">
        <th colspan="3">Pilot</th>
        <th colspan="2" rowspan="4">Aircraft</th>
    <tr>
    <tr class="rosterTableHeader">
        <th>Rank</th>
        <th>Callsign</th>
        <th>Designation</th>
    <tr>
    <?php
    foreach ($memberArray as $memberID => $memberInfo) {
        
        $rankImage = "../acg-pam/imgsource/RAF-ranks/".$memberInfo["raImage"];
        if(!empty($memberInfo["acImage"])){
            $acftImage = "../includes/images/aircraft/".$memberInfo["acImage"].".png";
            $acftImageSmall = "../includes/images/aircraft/".$memberInfo["acImage"]."_small.png";
        }
    ?>
    <tr>
        <?php if($memberInfo["status"]=="1"){ ?>
            <td><img src="<?php echo $rankImage; ?>" title="<?php echo $memberInfo["rankName"]; ?>" alt="<?php echo $memberInfo["rankName"]; ?>"></td>
            <td class="rosterTablePilot"><?php echo $memberInfo["callsign"]; ?></td>
        <?php } else { ?>
            <td class="rosterTablePilot" colspan="2"><?php echo $memberInfo["callsign"]; ?></td>
        <?php } ?>
        <td><?php echo $memberInfo["designation"]; ?></td>
        <td class="rosterTableAircraft">
            <?php if(!empty($memberInfo["acImage"])){ ?>
                <a href="<?php echo $acftImage;?>"> <img src="<?php echo $acftImageSmall;?>"></a>   
            <?php } else { echo $memberInfo["designation"];} ?>
        </td>
    </tr>
    <?php 
        }    
//Get all aircraft still in hangar
$sql = "SELECT memberID, designation, image FROM hangar ".
       "WHERE squadronID = $sqn AND memberID <= 0";
$result = mysqli_query($dbx, $sql);
if(mysqli_num_rows($result) > 0){
?>
    <tr class="rosterTableSubHeader">
        <td colspan="5">Aeroplanes In Hanger</td>
    </tr>
    <?php while($row = mysqli_fetch_assoc($result)){ 
        if($row["memberID"] == -1){
            $callsign = "Reserved";
        } else {
            $callsign = "";
        }
        
        if(!empty($row["image"])){
            $acftImage = "../includes/images/aircraft/".$row["image"].".png";
            $acftImageSmall = "../includes/images/aircraft/".$row["image"]."_small.png";
        }
    ?>
    <tr>
        <td></td>
        <td class="rosterTablePilot"><?php echo $callsign; ?></td>
        <td><?php echo $row["designation"]; ?></td>
        <td class="rosterTableAircraft">
            <?php if(!empty($row["image"])){ ?>
                <a href="<?php echo $acftImage;?>"> <img src="<?php echo $acftImageSmall;?>"></a>   
            <?php } else { echo $row["designation"];} ?>
        </td>
    </tr>
<?php
    }
}
?>
</table>

