<?php
	include("includes/header1.inc.php");
?>
	<title>Air Combat Group | Events</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("includes/header2.inc.php");
	include('events.lang.'.$ac_lang.'.php');
?>
				<h1><?php echo $lang['EVENTS_HEADER']; ?></h1>

				<div class="contentText">
					<img src="includes/images/events/events_header.png">

					<p><?php echo $lang['EVENTS_P1']; ?></p>

				</div>
				<h2><?php echo $lang['EVENTS_HEADER2']; ?></br>
				<?php echo $lang['EVENTS_HEADER3']; ?></h2>
				<div class="contentText">
					
					<table class="eventTable" border="1" >
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T1']; ?></td>
							<td><?php echo $lang['EVENTS_T2']; ?></td>						
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T3']; ?></td>
							<td><?php echo $lang['EVENTS_T4']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T5']; ?></td>
							<td><?php echo $lang['EVENTS_T6']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T7']; ?></td>
							<td><?php echo $lang['EVENTS_T8']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T9']; ?></td>
							<td><a href="teamspeak.php"><?php echo $lang['EVENTS_T10']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T11']; ?></td>
							<td><?php echo $lang['EVENTS_T12']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T13']; ?></td>
							<td><?php echo $lang['EVENTS_T14']; ?></td>
						</tr>							
						<tr>
							<td colspan="2" align="center"><iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/Vzuu_dWhrO8?rel=0" frameborder="0" allowfullscreen></iframe></td>
						</tr>						
					</table>

				</div>
				
				<h2><?php echo $lang['EVENTS_HEADER4']; ?></h2>
				<div class="contentText">
					
					<table class="eventTable" border="1" >
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T15']; ?></td>
							<td><?php echo $lang['EVENTS_T16']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T17']; ?></td>
							<td><?php echo $lang['EVENTS_T18']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T19']; ?></td>
							<td><?php echo $lang['EVENTS_T20']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T21']; ?></td>
							<td><?php echo $lang['EVENTS_T22']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T23']; ?></td>
							<td><?php echo $lang['EVENTS_T24']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T25']; ?></td>
							<td><?php echo $lang['EVENTS_T26']; ?></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><img src="includes/images/events/training.jpg"></td>
						</tr>						
					</table>

				</div>		

				<h2><?php echo $lang['EVENTS_HEADER5']; ?></h2>
				<div class="contentText">
					
					<table class="eventTable" border="1" >
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T27']; ?></td>
							<td><?php echo $lang['EVENTS_T28']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T29']; ?></td>
							<td><?php echo $lang['EVENTS_T30']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T31']; ?></td>
							<td><?php echo $lang['EVENTS_T32']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T33']; ?></td>
							<td><?php echo $lang['EVENTS_T34']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T35']; ?></td>
							<td><?php echo $lang['EVENTS_T36']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T37']; ?></td>
							<td><?php echo $lang['EVENTS_T38']; ?></td>
						</tr>						
						<tr>
							<td colspan="2" align="center"><img src="includes/images/events/DCS.jpg"></td>
						</tr>						
					</table>

				</div>	
				
				<h2><?php echo $lang['EVENTS_HEADER6']; ?></h2>
				<div class="contentText">
					
					<table class="eventTable" border="1" >
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T39']; ?></td>
							<td><?php echo $lang['EVENTS_T40']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T41']; ?></td>
							<td><?php echo $lang['EVENTS_T42']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T43']; ?></td>
							<td><?php echo $lang['EVENTS_T44']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T45']; ?></td>
							<td><?php echo $lang['EVENTS_T46']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T47']; ?></td>
							<td><?php echo $lang['EVENTS_T48']; ?></td>
						</tr>
						<tr>
							<td class="eventTableFirstCol"><?php echo $lang['EVENTS_T49']; ?></td>
							<td><?php echo $lang['EVENTS_T50']; ?></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><img src="includes/images/events/bays.jpg"></td>
						</tr>						
					</table>

				</div>
				
<?php	
	include("includes/footer.inc.php");
?>