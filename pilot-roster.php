<?php
	include("includes/header1.inc.php");
	include_once(dirname(__FILE__).'/acg-pam/includes/db_connect.php');
    include_once(dirname(__FILE__).'/acg-pam/includes/functions.php');
	
	if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $dbx = getDBx();
	
	$sql = "SELECT acgmembers.id, acgmembers.callsign, acgmembers.status, ".
	       "characterstatus.status AS cstatus, ".
	       "COUNT(reports.id) AS nsorties ".
	       "FROM acgmembers ".
		   "LEFT JOIN careercharacters ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN characterstatus ON careercharacters.characterstatus = characterstatus.id ".
           "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
           "LEFT JOIN missions ON reports.missionID = missions.id ". 
           "WHERE careercharacters.characterstatus <> 3 AND reports.accepted = 1 ".
           "GROUP BY acgmembers.id ORDER BY nsorties DESC ";
    // echo $sql;
    $result = mysqli_query($dbx, $sql);
    $n_ID = mysqli_num_rows($result);
    $m_array = mysqli_fetch_all($result);
    
    $n_entries = 20;
    $n_pages = ceil($n_ID / $n_entries);
    $start_from = ($page - 1)*$n_entries;
    $stop_at = min(($page - 1)*$n_entries + $n_entries, count($m_array));
    
    $currentMaxSorties = $m_array[0][4];
    $currentRank = 1;
    $m_array[0][5] = $currentRank;
    for($n = 1; $n < count($m_array); $n++){
        if($m_array[$n][4] < $currentMaxSorties) {
            $currentRank++;
            $currentMaxSorties = $m_array[$n][4];
        } 
        $m_array[$n][5] = $currentRank;
    }

    $members_array = array();
    for($n = $start_from; $n < $stop_at; $n++){
    
    	$m_id = $m_array[$n][0];
        $value_array = array();
        $value_array["id"] = $m_id;
        echo "id: ".$m_id." ".$m_array[$n][1];
        $value_array["callsign"] = $m_array[$n][1];
        $value_array["status"] = $m_array[$n][2];
        $value_array["sorties"] = $m_array[$n][4];
        $value_array["rankSorties"] = $m_array[$n][5];

        $sql = "SELECT COUNT(careercharacters.id) AS ncharacters FROM careercharacters ".
           "WHERE personifiedBy =  $m_id";
        $fresult = mysqli_query($dbx, $sql);
        $frow = mysqli_fetch_assoc($fresult);
        $value_array["ncharacters"] = $frow["ncharacters"];

    	$sql = "SELECT squadrons.faction, squadrons.name, UNIX_TIMESTAMP(transfers.transferDate) AS tspdate FROM transfers ".
           "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "WHERE memberid = $m_id ".
           "ORDER BY tspdate DESC LIMIT 1";
        $fresult = mysqli_query($dbx, $sql);
        $frow = mysqli_fetch_assoc($fresult);
        $faction = $frow["faction"];
        $value_array["faction"] = $frow["faction"];
        $value_array["squadron"] = $frow["name"];
        
        $sql = "SELECT ranks.abreviation, ranks.value, promotions.date, UNIX_TIMESTAMP(promotions.date) AS tspdate, ".
           "ranks.image ". 
           "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE memberid = $m_id ORDER BY tspdate DESC";
        $presult = mysqli_query($dbx, $sql);
        $prow = mysqli_fetch_assoc($presult);
        $value_array["rank"] = $prow["abreviation"];
        $value_array["rankImage"] = $prow["image"];

        $sql = "SELECT claimstatusraf.status, SUM(1-claimsraf.shared*0.5) AS points ".
           "FROM claimsraf LEFT JOIN reports ON claimsraf.reportid = reports.id ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
           "WHERE acgmembers.id = $m_id AND reports.accepted=1 AND claimsraf.accepted = 1 ".
           "AND careercharacters.characterstatus <> 3 ".
           "GROUP BY claimsraf.enemystatus";
        $rafvictoriesresult = mysqli_query($dbx, $sql);
        $value_array["rafvictories"] = $rafvictoriesresult;

        $sql = "SELECT COUNT(claimslw.id), claimslw.confirmed ".
           "FROM claimslw LEFT JOIN reports ON claimslw.reportid = reports.id ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id = $m_id AND reports.accepted=1 AND claimslw.accepted = 1 ".
           "AND careercharacters.characterstatus <> 3 ".
           "GROUP BY claimslw.confirmed";
        $lwvictoriesresult = mysqli_query($dbx, $sql);
        $value_array["lwvictories"] = $lwvictoriesresult;

        $members_array[] = $value_array;
    }
?>
	<title>Air Combat Group | Pilot Roster</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("includes/header2.inc.php");
?>
	<h1>Pilot Roster</h1>
		<div class="contentText">
		    <p>This table shows all active (white background) and former (black-background)
		    ACG-pilots. The pilots are ranked by a Survival-Score, which is the 
		    sum of sorties of the pilot's surviving career characters (characters 
		    with status active, POW or dissmissed).</p>
		    <p>Click on any pilot name to look at his member details in the ACG-Mission
		    and Pilot Database.</p>
			<div>
    			<table style="width: 100%; border-collapse: collapse;">
        			<thead style="font-size: large;">
            			<tr>
            				<th>Rank:</th>
            				<th></th>
			                <th colspan="2" >Name:</th>
			                <th>Unit:</th>
	            		</tr>
	        		</thead>
		        <?php
                    for($n = 0; $n < count($members_array); $n++){
                        $m_id = $members_array[$n]["id"];
				        $link = "acg-pam/memberDetails.php?m_id=".$m_id;
				        $status = $members_array[$n]["status"];
				        if($status == 1){
				            $styleStr = "background-color: rgba(255, 255, 255, 0.2);";
				        } else {
				            $styleStr = "background-color: rgba(00, 00, 00, 0.2);";
				        }
				        
				        
		        ?>
		        <tbody>
		            <tr style="<?php echo $styleStr; ?>">
		            	<td rowspan="2" style="text-align: center; font-size: xx-large;">
		            	    <?php echo $members_array[$n]["rankSorties"];?>
		            	</td>
		            	<td rowspan="2" style="">
		            	    <?php if($members_array[$n]["faction"] == "RAF"){ ?>
					        <!--<img src="acg-pam/imgsource/RAF-ranks/RAFUniform.png">-->
					        <img src="acg-pam/imgsource/RAF-ranks/<?php echo $members_array[$n]["rankImage"]; ?>">
					        <?php } else if($members_array[$n]["faction"] == "LW"){ ?>
					        <!--<img src="acg-pam/imgsource/LW-ranks/LWUniform.png">-->
					        <img src="acg-pam/imgsource/LW-ranks/<?php echo $members_array[$n]["rankImage"]; ?>">
        					<?php } ?>
        				</td>
		                <td colspan="2" style="">
		                    <b><a href="<?php echo($link);?>"><?php echo($members_array[$n]["rank"]
		                    ." ".$members_array[$n]["callsign"]);?></a></b>
            			</td>
		                <td rowspan="2" style="text-align: center;">
		                    <?php echo $members_array[$n]["squadron"];?>
		                </td>
		            </tr>
		            <tr style="<?php echo $styleStr; ?>">
		                <td style="font-size: small;">
		                   Survival-Score: <?php echo $members_array[$n]["sorties"];?><br>
		                   Characters: <?php echo $members_array[$n]["ncharacters"];?><br>
                            <?php
                                $separator = "";
                                while($row = mysqli_fetch_array($members_array[$n]["rafvictories"])){
                                    echo($separator.$row[0].": ".$row[1]);
                                    $separator = " - ";
                                }
                                
                                while($row = mysqli_fetch_array($members_array[$n]["lwvictories"])){
                                    $claimStatus = ($row[1])?"Confirmed: ":"Unconfirmed: ";
                                    echo($separator.$claimStatus.$row[0]);
                                    $separator = " - ";
                                }
                            ?>
                            
		                </td>
		                <td style="height: 90px;">
		                    <img src="acg-pam/includes/awardStripesFactory.php?m_id_only_survived_small=<?php echo $m_id; ?>">
		                </td>
		            </tr>
		            <tr>
		                <!--spaceholder-->
		                <td style="height: 2px;"></td>
		            </tr>
		        </tbody>
        
       			 <?php } ?>
			    </table>
			</div>
		<div class="pageSelect">
		    <?php  createPageSelect($n_pages, $page, "pilot-roster.php?"); ?>
		</div>
	</div>
<?php
	include("includes/footer.inc.php");
?>