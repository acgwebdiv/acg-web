<?php
	include("includes/header1.inc.php");
?>
	<title>Air Combat Group | TeamSpeak</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
	<link rel="stylesheet" type="text/css" href="tsstatus/tsstatus.css" />
<script type="text/javascript" src="tsstatus/tsstatus.js"></script>
<?php
	include("includes/header2.inc.php");
?>

				<h1>Join us on Teamspeak!</h1>
		<?php
				require_once("tsstatus/tsstatus.php");
				$tsstatus = new TSStatus("85.236.100.27", 20274);
				$tsstatus->useServerPort(16317);
				$tsstatus->imagePath = "/tsstatus/img/";
				$tsstatus->timeout = 2;
				$tsstatus->setCache(5);
				$tsstatus->hideEmptyChannels = false;
				$tsstatus->hideParentChannels = false;
				$tsstatus->showNicknameBox = false;
				$tsstatus->showPasswordBox = false;
				echo $tsstatus->render();
?> 	
				<div class="contentText">
					<p>
						If you are interested in flying with others on comms don't be shy, jump into our public 125 slot TeamSpeak 3 server.
						We welcome like-minded flyers who like the immersion of squadron flying.
					</p>
					<p>
						Teamspeak is a free installation available <a href="http://www.teamspeak.com/?page=downloads"><u>here</u></a>
					</p>					
					<p>Once installed set up a Bookmark to Air Combat Group using the following connection details and join in.</p>
				
					<table id="tsDetails">
						<tr>
							<td>Address:</td>
							<td>85.236.100.27:16317</td>
						</tr>
						<tr>
							<td>Password:</td>						
							<td>No Password Set</td>
						</tr>
					</table>


					<p>ACG uses "Push-To-Talk" in as an anti-spam measure, you can set a Push-To-Talk key binding in the Settings>Options>Capture menu, then just use Teamspeak like a Walkie-Talkie.</p>
				  <img src="../includes/images/teamspeak/PTT.png" style="width:500px;" border="0">
					<p>Should you experience any problems please post in our forums and we'll be happy to help you out.</p>

				</div>



<?php	
	include("includes/footer.inc.php");
?>