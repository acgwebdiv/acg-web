<?php

/* 
------------------
Language: English
------------------
*/

//events.php

$lang['EVENTS_HEADER'] = "Events";

$lang['EVENTS_P1'] = "Are you looking for some real and team based flying action, 
    apart from your daily dogfights on public servers? Then you should participate 
    in our events!";

$lang['EVENTS_HEADER2'] = "OUR PREMIER EVENT !!!!!";
$lang['EVENTS_HEADER3'] = "Sunday Historical Campaign";

$lang['EVENTS_T1'] = "Description:";
$lang['EVENTS_T2'] = "Historical Fall of France, Battle of Britain, Battle of Moscow  
    Campaigns (with more to come!) - The most immersive wartime pilot experience in IL-2 Sturmovik: Cliffs of 
    Dover and IL-2 Sturmovik: Battle of Stalingrad! This is mission flying in a re-enactment
    style for ACG pilots and those who would like to try the experience before considering membership. If you would 
    like to feel the roar of full squadron/staffel takeoffs, Fighter Command operations, 
    scrambles, formation flight in large numbers to the enemy and have a huge scrap 
    high up then come into our Teamspeak and fly with us. We welcome new pilots, 
    no experience necessary as training and help will be provided.";
$lang['EVENTS_T3'] = "Time:";
$lang['EVENTS_T4'] = "Meet Sunday at 19:00 UK";
$lang['EVENTS_T5'] = "Mission Duration:";
$lang['EVENTS_T6'] = "One mission per week of approx. 80 minutes";
$lang['EVENTS_T7'] = "Server:";
$lang['EVENTS_T8'] = "ACG Server in the Lobby";
$lang['EVENTS_T9'] = "TeamSpeak:";
$lang['EVENTS_T10'] = "<a href='teamspeak.php'>ACG TeamSpeak (85.236.100.27:16317)</a> 
    is compulsory";
$lang['EVENTS_T11'] = "Mission Briefings:";
$lang['EVENTS_T12'] = "Check the briefing in the <a href='http://www.aircombatgroup.co.uk/acg-pam/'>Mission and Pilot Database (PAM)</a>. Follow orders from 
    your flight commander.";
$lang['EVENTS_T13'] = "Flight Logs:";
$lang['EVENTS_T14'] = "Take a look at our flight reports from the campaign in the <a href='http://www.aircombatgroup.co.uk/acg-pam/'>PAM</a>";
    
$lang['EVENTS_HEADER4'] = "ACG Training - OPEN TO ALLCOMERS";

$lang['EVENTS_T15'] = "Description:";
$lang['EVENTS_T16'] = "The ACG holds 3 squadron specific training sessions (Tuesdays, Wednesdays and Thursdays) during the week which are open
    to the public to join in. Please just join and introduce yourself.";
$lang['EVENTS_T17'] = "Time:";
$lang['EVENTS_T18'] = "1900 UK";
$lang['EVENTS_T19'] = "Mission Duration:";
$lang['EVENTS_T20'] = "As long as you want to, normally a couple of hours.";
$lang['EVENTS_T21'] = "Server:";
$lang['EVENTS_T22'] = "ACG Public IL-2 Sturmovik: Battle of Stalingrad server <a href='http://www.aircombatgroup.co.uk/server/clod.php'><u>here</u></a>";
$lang['EVENTS_T23'] = "TeamSpeak:";
$lang['EVENTS_T24'] = "<a href='teamspeak.php'>ACG TeamSpeak (85.236.100.27:16317)</a>";
$lang['EVENTS_T25'] = "Mission Information:";
$lang['EVENTS_T26'] = "Training is squadron based or mixed. 
    Non-ACG pilots are welcome regardless of standard, you may post in advance in 
    our <u><a href='http://www.aircombatgroup.co.uk/forum/viewtopic.php?f=3&t=4580'>Reception 
    training request thread</a></u> for one on one help, but it's not required. 
    We have lots of members willing to help any level under no obligation whatsoever.";

$lang['EVENTS_HEADER5'] = "Every Night: DCS";

$lang['EVENTS_T27'] = "Description:";
$lang['EVENTS_T28'] = "Our chaps fly all sorts on DCS so drop in anytime and get involved. 
    It may be a Cold War jets mission, WWII with the Mustang vs Dora or perhaps 
    modern conflict Taliban busting in the A-10.";
$lang['EVENTS_T29'] = "Time:";
$lang['EVENTS_T30'] = "Drop by Teamspeak, most busy around 1930GMT";
$lang['EVENTS_T31'] = "Mission Duration:";
$lang['EVENTS_T32'] = "Whatever you want.";
$lang['EVENTS_T33'] = "Server:";
$lang['EVENTS_T34'] = "ACG Public DCS server <a href='http://www.aircombatgroup.co.uk/server/dcs.php'><u>here</u></a>";
$lang['EVENTS_T35'] = "TeamSpeak:";
$lang['EVENTS_T36'] = "<a href='teamspeak.php'>ACG TeamSpeak (85.236.100.27:16317) 
    DCS channels.</a>";
$lang['EVENTS_T37'] = "Information:";
$lang['EVENTS_T38'] = "Please view our <u><a href='http://www.aircombatgroup.co.uk/forum/viewforum.php?f=57'>DCS 
    forums</a></u> for more information and to get involved.</a>";
    
$lang['EVENTS_HEADER6'] = "Saturday night BLITZ";

$lang['EVENTS_T39'] = "Description:";
$lang['EVENTS_T40'] = "BLITZ at your Six is a six week 'campaign style' campaign that will follow a story and plot. 
    This will be a 'human' dynamic set of missions, requiring low overhead from a staffing perspective 
    and hopefully a lot of fun, but more importantly, a very different feel to the campaign mission.";
$lang['EVENTS_T41'] = "Time:";
$lang['EVENTS_T42'] = "Saturdays - 6pm UK";
$lang['EVENTS_T43'] = "Mission Duration:";
$lang['EVENTS_T44'] = "As long as it takes, normally a couple of hours.";
$lang['EVENTS_T45'] = "Server:";
$lang['EVENTS_T46'] = "ACG Public server <a href='http://www.aircombatgroup.co.uk/server/clod.php'><u>here</u></a>";
$lang['EVENTS_T47'] = "TeamSpeak:";
$lang['EVENTS_T48'] = "<a href='teamspeak.php'>ACG TeamSpeak (85.236.100.27:16317)</a>";
$lang['EVENTS_T49'] = "Mission Information:";
$lang['EVENTS_T50'] = "Please view our <u><a href='https://aircombatgroup.co.uk/forum/viewtopic.php?f=5&t=9583'>IL-2 Sturmovik: Cliffs of Dover Blitz Forum</a></u>
    for more information to join the action.";
    