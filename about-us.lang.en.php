<?php

include($_SERVER["DOCUMENT_ROOT"].'/acg-pam/includes/db_connect.php');
$dbx = getDBx();

//Loading Osprey's and Pitti's callsign and squadron
    $sql = "SELECT acgmembers.callsign, squadrons.name FROM acgmembers ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
           "WHERE acgmembers.id = 2 OR acgmembers.id = 36 OR acgmembers.id = 64 ". 
           "OR acgmembers.id = 1 OR acgmembers.id = 13 ORDER BY acgmembers.id";
    $result = mysqli_query($dbx, $sql);
    $hc_result = mysqli_fetch_all($result);
    $ospreyStr = $hc_result[1][1]."_".$hc_result[1][0];
    $pittiStr = $hc_result[3][1]."_".$hc_result[3][0];
    $daveStr = $hc_result[4][1]."_".$hc_result[4][0];
    $thaineStr = $hc_result[0][1]."_".$hc_result[0][0];
    $bonkinStr = $hc_result[2][1]."_".$hc_result[2][0];

/* 
------------------
Language: English
------------------
*/

//about-us.php

$lang['ABOUT_US_HEADER'] = "About Us";

$lang['ABOUT_US_P1'] = "We are a very laid back bunch of guys, we mix Allied and
    Axis as friends and our aim is immersion above everything else, right down to
    our friendly inter-squadron rivalry.";
    
$lang['ABOUT_US_P2'] = "ACG are not hung up on ranks, calling each other &lsquo;Sir&rsquo;
    nor jumping through hoops in order to climb a virtual ladder which has little
    bearing on real life. Our ranking system is a uniform and only has relevance 
    in providing pilots online with a guide to who should be in charge of any group
    at any given time, and even then it is pretty laid back. The system is based
    on what somebody brings to the group and demotion occurs as easily as promotion.
    Why should a new pilot who has a lot of experience and skill have to start at
    the bottom&#63;  Why should somebody who is always involved or contributing 
    the group sit on a low rank because somebody else signed up earlier&#63;";

$lang['ABOUT_US_HEADER2'] = "Are you interested in joining us?";

$lang['ABOUT_US_P3'] = "If you've come here to just take a look at our site and 
    forums then you are welcome, you will find that you don't even need to register
    to post in the Reception forums so go ahead, please do, you are welcome to join
    in the conversations. For the other forums just register and join in the chat.";

$lang['ABOUT_US_P4'] = "This is the blunt and truthful bit. To become an ACG pilot 
    you must join in our Teamspeak and fly with us, the address is 85.236.100.27:16317
    but more info <u><a href='http://www.aircombatgroup.co.uk/teamspeak.php'>here</a></u>
    if you need it.";

$lang['ABOUT_US_P5'] = "We do not care for flying experience a jot,  what matters
    is that you make a commitment for the longer term and in the right spirit. As
    a member your attendance is recorded by your attendance in the weekly campaign
    combined with the after action flight report written into the forums (just flying
    the campaign does not count). This is not a big flying commitment at all and 
    the rest of your time is up to you however ideally you should be the type who 
    flies 2 or 3 times a week and when you do so you join our Teamspeak channel.";

$lang['ABOUT_US_P6'] = "We would also like you to keep abreast of what is going on
    in ACG so a regular forum visit and furnishing us with an email address which
    you actually use is important. Finally, out of respect, should you wish to leave
    then notify your ACG commander or make a posting in the forums - we really do 
    not want you if you are going to just disappear after a month of regular activity, 
    seriously, if you can't commit then ACG is not for you";

$lang['ABOUT_US_P7'] = "Join us if you are";

$lang['ABOUT_US_LI1'] = "interested in immersing yourself in the history of flight 
    combat ('being there') and not so interested in 'winning'";

$lang['ABOUT_US_LI2'] = "want to fly in a large group of like-minded people on historical
    missions";

$lang['ABOUT_US_LI3'] = "are a relaxed and helpful person with a good sense of humour";

$lang['ABOUT_US_LI4'] = "going to be around to fly with us. No large unannounced 
    on/off breaks or going AWOL.";

$lang['ABOUT_US_P8'] = "Your commitment to ACG";

$lang['ABOUT_US_LI5'] = "Make every effort to be a regular pilot for our Sunday campaign, 
    @1900GMT (most Sundays)";

$lang['ABOUT_US_LI6'] = "Write your flight report within 1 week of that flight";

$lang['ABOUT_US_LI7'] = "Join Teamspeak when you fly";

$lang['ABOUT_US_LI8'] = "Notify command via PM/Forum/email if you decide to leave us, 
    or for any extended period of absence";

$lang['ABOUT_US_P9'] = "If you wish to know more then of course ask in the forums, 
    or better still, come and give us a try - remember, there is no standard to reach 
    to join ACG, we will help you into the sky with us.";

$lang['ABOUT_US_HEADER3'] = "Joining ACG as a full member";

$lang['ABOUT_US_P10'] = "You cannot just decide to join by changing your ID, you must 
    be approved by at least the Squadron Leader or Staffelkapitan of the relevant 
    unit.  This is simple, just ask to join and they will advise accordingly but 
    normally you will have been asked about your commitment to ACG first!";

$lang['ABOUT_US_P11'] = "You must be registered and furnish the founder, $ospreyStr,
    with an email address which you use. By default this will be taken from your 
    forum registration. You will need to edit your Steam ID and Teamspeak ID, and
    add $ospreyStr and your unit leader as Steam friends, in the case of the Luftwaffe
    you must also add $pittiStr. You will have also been offered an aircraft 
    tail number from those available for your unit and there is a skin available 
    for you, please ask your commander.";

$lang['ABOUT_US_P12'] = "New members may request ACG signatures from the forum thread. 
    Personal noseart is permitted on your aircraft but it must be approved by your 
    Squadron Leader or Staffelkapitan before you use it in campaign. To be acceptable 
    it must look like it is from the period, so nothing modern. You must not change 
    your skin in other ways, for example adding kill marks, because each change 
    to a skin causes a new download to occur for all players.";

$lang['ABOUT_US_HEADER4'] = "About what we do in video";

$lang['ABOUT_US_P13'] = "$daveStr gives a pilots eye viewpoint of being an ACG pilot 
    in this video";

$lang['ABOUT_US_P14'] = "Looking deeper, $thaineStr has compiled this epic documentary-drama 
    of a FULL mission. This cinema starts at the very beginning of an ACG Campaign 
    evening showing both factions in action, the briefing, our Sector Control system, 
    the orders via our historically based command systems, the build up and it culminates 
    by drilling down into a part of the dogfight action as it unfolded for the pilots 
    involved. Filmed from the participants eye view this really gives the vision 
    of ACG in action, unscripted, as pilots filled with anticipation and their fear 
    of the mission ahead fought for their virtual lives.  It's 1hr 21m long, get 
    yourself a drink and put your feet up, watch in stages if you don't have the 
    time, but please do watch it all!";

$lang['ABOUT_US_HEADER5'] = "How we track our campaign progress";

$lang['ABOUT_US_P15'] = "$bonkinStr explains the details of our campaign diary 
    database touched upon in the previous video";

$lang['ABOUT_US_HEADER6'] = "Uniform";

$lang['ABOUT_US_P16'] = "The only formality is the mask for your callsign which 
    is Squadron_Callsign, for example '$ospreyStr'. We do not allow numbers, 
    unpronounceable words or special characters and recommend one or two syllables. 
    If somebody else already has your aircraft number or callsign it's first come 
    first served. Aside this 'uniform' the rest is all about enjoying yourself.";

$lang['ABOUT_US_HEADER7'] = "Our ranking system explained";

$lang['ABOUT_US_P17'] = "All new pilots come in at the lowest grade &#40;<i>Sergeant</i> 
    in the RAF, <i>Serzhant</i> in the VVS and <i>Gefreiter</i> in the Luftwaffe&#41; and all ranks are decided 
    and approved by the commanding officer of the unit. This could be by recommendation 
    from lower ranking pilots but will usually results from a commander observation 
    over time. They will stay at the lowest grade until they show that they are 
    active with the group and all this involves is getting involved and being a 
    regular pilot. They do not have to be online all the time but always come into 
    Teamspeak when flying to join up with squadron pals, weekly would be fine. Only 
    if the pilot is clearly advanced then he may be directly promoted to Officer 
    rank and avoid all of the NCO ranks. This is purely by decision of top brass/high 
    command where all of the advanced qualities of an experienced virtual pilot 
    are demonstrated. Always consider that ACG is an historical group and we try 
    to maintain an historical balance in the ranking system so if someone is above 
    you and doing their job you may find your route blocked, it may take extra efforts! 
    If you are rank bothered then please don't be, it's just a part of our immersion, 
    besides, officers are expected to do extra stuff, or they are demoted. Promotion 
    is based on: ";

$lang['ABOUT_US_LI9'] = "Squadron Involvement";
$lang['ABOUT_US_LI10'] = "Campaigns";
$lang['ABOUT_US_LI11'] = "Regular online flight";
$lang['ABOUT_US_LI12'] = "Flying Improvement";
$lang['ABOUT_US_LI13'] = "Training (attendance & completion)";
$lang['ABOUT_US_LI14'] = "Skill (takeoff, landings, navigation & formation)";
$lang['ABOUT_US_LI15'] = "Combat tactics & K/D ratio";
$lang['ABOUT_US_LI16'] = "Shooting accuracy";
$lang['ABOUT_US_LI17'] = "Flight leadership";
$lang['ABOUT_US_LI18'] = "Communication";
$lang['ABOUT_US_LI19'] = "Contribution";
$lang['ABOUT_US_LI20'] = "Website (design, articles & graphics)";
$lang['ABOUT_US_LI21'] = "Server (coding & mapping)";
$lang['ABOUT_US_LI22'] = "Training (provision & instruction)";
$lang['ABOUT_US_LI23'] = "Recruitment";

$lang['ABOUT_US_P18'] = "See also the complete <a href='/acg-ranks.php'>list of 
    ACG ranks</a>.";

$lang['ABOUT_US_HEADER8'] = "Demotion";

$lang['ABOUT_US_P19'] = "Such a harsh word, don&rsquo;t take it like that here. 
    In some other squadrons the situation can occur where somebody attains a high 
    rank and then disappears from the group for long periods. When they return they 
    keep their rank and officialdom, which pisses other people off. In ACG you may 
    lose your grade for going AWOL &#45; don&rsquo;t expect to keep it if you&rsquo;re 
    not going to be about. This keeps the system fresh and ensures that the people 
    contributing to the group are provided with the responsibility &#45; this goes 
    all the way to the top. Pilots who go missing will be contacted by email just 
    to find out what they are up to, the consequences ranging from absolutely nothing 
    to removal from the group &#45; but we do like to keep our pilots!";
