<?php

	if (!isset($_POST['s'])) {
		$side = "RAF";
	} else {
		$side = $_POST['s'];
	}

	$bg = 'https://dl.dropboxusercontent.com/s/9f30sptezjek3y4/LW.png';
	$json = 'https://dl.dropboxusercontent.com/s/z40btrfpbfvkwdo/LW.json';
	$altAl = 'Math.round(parseInt(ht)/100) / 10.0;';
	$back = '#FB0020';
	$fore = '#000000';
	$frnd = '#2000FB';
	$stackfore = '#DCDCDC';
	$spdLimit = 400;
	$spdUnits = "km/h";

	if ($side == "RAF") {
		$bg = 'https://dl.dropboxusercontent.com/s/7yxnvanbe5twsay/RAF.png';
		$json = 'https://dl.dropboxusercontent.com/s/u8xw1emprgkctgd/RAF.json';
		$altAl = 'Math.round(parseInt(ht)/1000);';
		$back = '#2000FB';
		$fore = '#DCDCDC';
		$frnd = '#FB0020';
		$stackfore = '#DCDCDC';
		$spdLimit = 250;
		$spdUnits = "mph";
		
	}
?>
<HTML> 
	<HEAD>
		<?php
			include("../includes/header_meta_data.inc.html");
		?>
		<title>Air Combat Group | Radar</title>
		<link rel="stylesheet" href="../includes/css/radarmap.css" type="text/css" media="screen" />
	</HEAD>
	
	<BODY onload = 'startTimer();'>
		<script type='text/javascript'>
			var canvas;
			var ctx;
			var JDta;
			var background;
			var width = 1050;
			var height = 900;
			var xOff = 1;
			var yOff = 1;
			var xSize = 30;
			var ySize = 30;
			var mouseIsDown = false;

			function mouseDown(canvas, evt) {
				mouseIsDown = true;
			}

			function mouseUp(canvas, evt) {
				mouseIsDown = false
			}

			function getMousePos(canvas, evt) {
				var rect = canvas.getBoundingClientRect();
				return {
				  x: evt.clientX - rect.left,
				  y: evt.clientY - rect.top
				};
			}

			function getTouchPos(canvas, evt) {
				var rect = canvas.getBoundingClientRect();
				return {
				  x: evt.targetTouches[0].pageX - rect.left,
				  y: evt.targetTouches[0].pageY - rect.top
				};
			}

			function handleMouse(e) {
				var pos = getMousePos(canvas, e);
				if (pos.x <= width && pos.x >0) {
					if (pos.y <= height && pos.y >0) {
						if (JDta.PublicCampaign == "Public") {
							drawStack(pos.x, pos.y);
						}
						gridFromPtr(pos.x, pos.y);
					}
				}
			}

			function handleTouch(e) {
				var pos = getTouchPos(canvas, e);
				if (pos.x <= width && pos.x >0) {
					if (pos.y <= height && pos.y >0) {
						if (JDta.PublicCampaign == "Public") {
							drawStack(pos.x, pos.y);
						}
						gridFromPtr(pos.x, pos.y);
					}
				}
			}

			function inCircle(x1,y1,x2,y2,r) {
				var deltaX = x1 - x2;
				deltaX = deltaX * deltaX;
				var deltaY = y1 - y2;
				deltaY = deltaY * deltaY;
				var dist = Math.sqrt((deltaX + deltaY));
				var retVal = false;
				if (dist <= r) {
					retVal = true;
				}
				return retVal;
			}

			function gridFromPtr(mx,my) {
				var Xstart = (mx - ((mx - xOff) % xSize));
				var Ystart = (my - ((my - yOff) % ySize));
				var X = parseInt(Xstart / xSize)
				var Y = parseInt(Ystart / ySize)
				var xGridRef = ""
				if (X < 26) {
					xGridRef = "A" + String.fromCharCode(65 + X);
				} else {
					xGridRef = "B" + String.fromCharCode(39 + X);
				}
				var yGridRef = (30 - Y);
				document.getElementById('mapgrid').innerHTML = xGridRef + "," + yGridRef;
			}

			function drawToken(context,X,Y,rad)
			{
				var cnv = 0.017453292519943;
				//BACK
				context.beginPath();
				context.arc(X,Y, rad, 0, Math.PI*2, true);
				context.closePath();
				context.fill();
				//HDG
				var hdg = JDta.RaidReports[i].Heading - 90;
				var hyp = (rad * 2) - 6;
				var adj1 = parseInt(Math.cos(cnv * hdg) * rad);
				var opp1 = parseInt(Math.sin(cnv * hdg) * rad);
				var adj2 = parseInt(Math.cos(cnv * hdg) * hyp);
				var opp2 = parseInt(Math.sin(cnv * hdg) * hyp);
				context.moveTo(X + adj1, Y + opp1);
				context.lineTo(X + adj2, Y + opp2);
				context.stroke();
				//CNTR-LINE
				context.moveTo(X - rad, Y);
				context.lineTo(X + rad, Y);
				context.lineWidth = 2;
				context.stroke();
			}

			function drawStack(mx,my) {
				if (JDta.PublicCampaign == "Public") {
					var stack = document.getElementById('stack');
					var stx = stack.getContext("2d");
					var oForm = document.forms['postform'];
					var of = oForm.elements['f'];
					if (!document.getElementById('sticky').checked) {
						stx.clearRect(0, 0, canvas.width, canvas.height);
					}
					stx.lineWidth = 2;
					stx.font = "8pt Arial";
					offy = parseInt(stx.font);
					var j = 0;
					var X = 35;
					for (k = 0; k < JDta.RaidReports.length; k++) { 
						stx.fillStyle = '<?php echo $back?>';
						stx.strokeStyle = '<?php echo $stackfore?>';
						i = JDta.RaidReports.length - k - 1;
						var size = parseInt(JDta.RaidReports[i].CleanNum);
						var plotX = parseInt(JDta.RaidReports[i].X);
						var plotY = parseInt(JDta.RaidReports[i].Y);
						var diam = Math.min(size,12)+26;
						var rad = (diam/2) + stx.lineWidth;
						var Y = 30 + (j * 50);
						var proceed = true;
						if (of.value == 'Filtered') {
							proceed = false;
							if (size > 2) {
								proceed = true;
							}
						}
						if (document.getElementById('sticky').checked) {
							if (mouseIsDown == true) {
								proceed = false;
							}
						}
						var ofInterest = false;
						ofInterest = inCircle(mx,my,plotX,plotY,rad);
						if (proceed == true && ofInterest == true) {
							if (document.getElementById('sticky').checked) {
								if (j ==0) {
								stx.clearRect(0, 0, canvas.width, canvas.height);
								}
							}
							j++;
							drawToken(stx,X,Y,rad);
							//TXT
							stx.fillStyle = '<?php echo $stackfore?>';
							var ac = JDta.RaidReports[i].NumberOfAircraft;
							ac = ac.replace("A/C","");
							var offx = stx.measureText(ac).width / 2;
							stx.fillText(ac,X - offx,Y + offy + 2);
							var ht = JDta.RaidReports[i].Altitude;
							ht = <?php echo $altAl ?>
							offx = stx.measureText(ht).width / 2;
							stx.fillText(ht,X - offx,Y - 3);
							var at = JDta.RaidReports[i].AircraftType;
							var db = JDta.RaidReports[i].DetectedBy;
							var spd = parseInt(JDta.RaidReports[i].Speed);
							var sec = JDta.RaidReports[i].MapGrid;
							var alt = JDta.RaidReports[i].Altitude;
							hdg = JDta.RaidReports[i].Heading;
							if (spd > <?php echo $spdLimit?>) {
								spd = '><?php echo $spdLimit?>'+'<?php echo $spdUnits?>'  
							} else {
								spd = spd + '<?php echo $spdUnits?>'
							}
							var line1 = sec + " (" + alt + ", " + hdg + "�)";
							var line2 = at;
							var line3 = db + " (" + spd + ")";
							boxX = Math.max(stx.measureText(line1).width,stx.measureText(line2).width);
							boxX = Math.max(stx.measureText(line3).width,boxX) + 6;
							boxY = 3 * (offy + 6);
							XX = X + 30;
							YY = Y - 20;
							stx.beginPath();
							stx.rect(XX, YY, boxX, boxY);
							stx.fillStyle = '#F4DEB5';
							stx.fill();
							stx.strokeStyle = '#000000';
							stx.stroke();
							stx.fillStyle = '#000000';
							offx = (boxX - stx.measureText(line1).width) / 2;
							stx.fillText(line1,XX + offx,YY + offy + 4);
							offx = (boxX - stx.measureText(line2).width) / 2;
							stx.fillText(line2,XX + offx,YY + 2 * (offy + 4));
							offx = (boxX - stx.measureText(line3).width) / 2;
							stx.fillText(line3,XX + offx,YY + 3 * (offy + 4));	
						}
					}
				}
			}

			function startTimer() {
				// init background 
				background = new Image();
				background.src = '<?php echo $bg?>';
				//init canvas
				canvas = document.getElementById('map');
				ctx = canvas.getContext("2d");
				canvas.addEventListener('mousemove', handleMouse, false);
				canvas.addEventListener('touchstart', handleTouch, false);
				canvas.addEventListener("mousedown", mouseDown, false);
				document.body.addEventListener("mouseup", mouseUp, false);
				ctx.imageSmoothingEnabled = true;
				timedUpdateImage();
			}

			function countdown() {
				var seconds_left = 60;
				var interval = setInterval(function() {
					document.getElementById('tick').innerHTML = --seconds_left;
					if (seconds_left <= 0)
					{
						clearInterval(interval);
						timedUpdateImage();
					}
				}, 1000);
			}

			function timedUpdateImage() {
				pullJSON();
			}

			function pullJSON(){
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function() {
					if (xhr.readyState == XMLHttpRequest.DONE) {
						JDta = JSON.parse(xhr.responseText);
						//Draw Image with *new* JSON
						var stack = document.getElementById('stack');
						var stx = stack.getContext("2d");
						stx.clearRect(0, 0, canvas.width, canvas.height);
						renderImage();
						countdown();
					}
				}
				xhr.open('GET', '<?php echo $json?>', true);
				xhr.send(null);
			}

			function prepopdrop() {
				var select = document.getElementById("friendTracker");
				var chosen = "None";
				if (select.selectedIndex != -1) {
					chosen = select.options[select.selectedIndex].value;
				}
				select.options.length = 0;
				var pilot = -1;
				var el = document.createElement("option");
				el.textContent = "None";
				el.value = "None";
				select.appendChild(el);	
				for (i = 0; i < JDta.FriendPilots.length; i++) { 
					var el = document.createElement("option");
					el.textContent = JDta.FriendPilots[i].Id;
					el.value = JDta.FriendPilots[i].Id;
					if (JDta.FriendPilots[i].IsAI == "False") {
						if (el.value == chosen) {
							el.selected = true;
							pilot = i;
						}
						select.appendChild(el);
					}
				}
				return pilot;
			}

			function renderImage() {
				//BG
				ctx.drawImage(background,0,0);
				//INFOBOX
				var acft = JDta.TotalAircraft;
				var hmns = JDta.ActivePlayers;
				var gmtm = JDta.MissionTime;
				var srtm = JDta.ServerTime;
				var line1 = "Total A/C in game: " + acft + " (" + hmns + " human)";
				var line2 = "Server Local Time: " + srtm;
				var line3 = "In-Game Time: " + gmtm;
				ctx.font = "12pt Arial";
				var offy = parseInt(ctx.font);
				var boxX = Math.max(ctx.measureText(line1).width,ctx.measureText(line2).width);
				boxX = Math.max(ctx.measureText(line3).width,parseInt(boxX)) + 6;
				var boxY = 3 * (offy + 10) - 4;
				var XX = 10;
				var YY = 10;
				ctx.beginPath();
				ctx.rect(XX, YY, boxX, boxY);
				ctx.fillStyle = '#F4DEB5';
				ctx.fill();
				ctx.strokeStyle = '#000000';
				ctx.stroke();
				ctx.fillStyle = '#000000';
				ctx.fillText(line1,13,27);
				ctx.fillText(line2,13,47);
				ctx.fillText(line3,13,67);
				if (JDta.PublicCampaign == "Campaign") {
					var line = "ACG Campaign is active - plots are disabled.";
					ctx.font = "bold 35pt Arial";
					var offy = parseInt(ctx.font);
					ctx.fillStyle = '#000000';
					ctx.fillText(line,20,200);
				} else {
					pilot = prepopdrop();
					//Raids
					var oForm = document.forms['postform'];
					var of = oForm.elements['f'];
					var od = oForm.elements['d'];
					ctx.font = "8pt Arial";
					offy = parseInt(ctx.font);
					for (i = 0; i < JDta.RaidReports.length; i++) { 
						ctx.fillStyle = '<?php echo $back?>';
						ctx.strokeStyle = '<?php echo $fore?>';
						ctx.lineWidth = 2;
						var X = parseInt(JDta.RaidReports[i].X);
						var Y = parseInt(JDta.RaidReports[i].Y);
						var size = parseInt(JDta.RaidReports[i].CleanNum);
						var proceed = true;
						if (of.value == 'Filtered') {
							proceed = false;
							if (size > 2) {
								proceed = true;
							}
						}
						if (proceed == true) {
							var diam = Math.min(size,12)+26;
							var rad = diam/2;
							drawToken(ctx,X,Y,rad)
							//TXT
							ctx.fillStyle = '<?php echo $fore?>';
							var ac = JDta.RaidReports[i].NumberOfAircraft;
							ac = ac.replace("A/C","");
							var offx = ctx.measureText(ac).width / 2;
							ctx.fillText(ac,X - offx,Y + offy + 2);
							var ht = JDta.RaidReports[i].Altitude;
							ht = <?php echo $altAl ?>
							offx = ctx.measureText(ht).width / 2;
							ctx.fillText(ht,X - offx,Y - 3);
							if (od.value == 'Verbose') {
								var at = JDta.RaidReports[i].AircraftType;
								var db = JDta.RaidReports[i].DetectedBy;
								boxX = Math.max(ctx.measureText(at).width,ctx.measureText(db).width) + 6;
								boxY = 2 * (offy + 6);
								XX = X-(boxX / 2.0);
								YY = Y + offy + 6;
								ctx.beginPath();
								ctx.rect(XX, YY, boxX, boxY);
								ctx.fillStyle = '#F4DEB5';
								ctx.fill();
								ctx.strokeStyle = '#000000';
								ctx.stroke();
								ctx.fillStyle = '#000000';
								offx = ctx.measureText(at).width / 2;
								ctx.fillText(at,X - offx,YY + (boxY / 2) - 2);
								offx = ctx.measureText(db).width / 2;
								ctx.fillText(db,X - offx,YY + (boxY) - 4);
							}
						}
					}
					//Friends - need to be last to be on top.
					if (pilot > -1) {
						var PX = parseInt(JDta.FriendPilots[pilot].IL2X);
						var PY = parseInt(JDta.FriendPilots[pilot].IL2Y);
						ctx.fillStyle = '<?php echo $frnd?>';
						ctx.strokeStyle = '<?php echo $fore?>';
						ctx.lineWidth = 2;							
						ctx.beginPath();
						ctx.arc(PX,PY, 8, 0, Math.PI*2, true);
						ctx.closePath();
						ctx.fill();
						ctx.stroke();
					}
				}
			}

			function toggle(butt,el,op1,op2) {
				var oForm = document.forms['postform'];
				var oe = oForm.elements[el];
				if (oe.value == op1) {
					oe.value = op2;
				} else {
					oe.value = op1;
				}
				if (butt == 'Side') {
					oForm.submit();
				} else {
					document.getElementById(butt).value = oe.value;
					renderImage();
				}
			}

			function ddHandle() {
				document.getElementById('friendTracker').disabled = true;
				renderImage();
				var seconds_left = 10;
				var interval = setInterval(function() {
					--seconds_left;
					if (seconds_left <= 0)
					{
						clearInterval(interval);
						var select = document.getElementById("friendTracker");
						select.selectedIndex = 0;
						renderImage();
						document.getElementById('friendTracker').disabled = false;
					}
				}, 1000);
			}

			function gcv() {
				var win = window.open('https://www.dropbox.com/sh/qr3np0djvvrk0vu/AAAX9JNJDkX7B3GKwT17u0j2a?dl=0', '_blank');
				win.focus();
			}

			function acg() {
				window.open('http://www.aircombatgroup.co.uk/', '_self');
			}			
		</script>	
		<form  action='<?php echo $_SERVER[PHP_SELF]?>' method='post' autocomplete='off' name='postform'>
				<table border=0 cellpadding=4>
				<tr>
					<td>Controller:</td>
					<td><input type='button' id='Side' value='<?php echo $side?>' class='button' onclick="toggle('Side','s','RAF','LW')"></td>
					<td></td>
					<td>Raid detail:</td>
					<td><input type='button' id='Detail' value='Short' class='button' onclick="toggle('Detail','d','Short','Verbose')"></td>
					<td></td>
					<td>Size:</td>
					<td><input type='button' id='Filter' value='Unfiltered' class='button' onclick="toggle('Filter','f','Filtered','Unfiltered')"></td>
					<td></td>
					<td>Show:</td>
					<td><select id = 'friendTracker' onchange='ddHandle()'></select></td>
					<td></td>
					<td>Keep Last Stack:</td>
					<td><input type='checkbox' id='sticky'></td>
				</tr>			
				</table>
				<input name='s' class='input' type='hidden' value='<?php echo $side?>'>
				<input name='d' class='input' type='hidden' value='Short'>	
				<input name='f' class='input' type='hidden' value='Unfiltered'>
		</form>
		
		<div class='imageSub' style='width: 1250px;'> 
			<table border = 0>
			<tr>
			<td>
			<canvas id='map' width='1050' height='900'> Your browser does not support the HTML5 canvas tag.</canvas>
			</td><td>
			<canvas id='stack' width='200' height='900'> Your browser does not support the HTML5 canvas tag.</canvas>
			</td>
			</tr>
			</table>
			<div class='blackbg'></div>
			<div class='tick' id='tick'></div>
			<div class='yellowbg'></div>
			<div class='mapgrid' id='mapgrid'></div>			
		</div>
		<table border=0>
			<tr>
				<td><input type='button' value='ACG Homepage' class='button' onclick='acg()'></td>
				<td><input type='button' value='Latest GCV tracks' class='button' onclick='gcv()'></td>
			<tr>
		</table>
		<div id='preload' style='display:none'>
			<img src='<?php echo $bg?>' width='1' height='1'/>
		</div>		
	</BODY>
</HTML>
