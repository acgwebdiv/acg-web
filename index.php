<?php
	include("includes/header1.inc.php");
?>
	<title>Air Combat Group | Home Page</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("includes/header2.inc.php");
	include('index.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['INDEX_PAGE_HEADER'];?></h1>

		<div class="contentText">
			<p><?php echo $lang['INDEX_PAGE_P1'];?></p>
			<p><?php echo $lang['INDEX_PAGE_P2'];?></p>
			<p><?php echo $lang['INDEX_PAGE_P3'];?></p>
		</div>
		<br>
		<iframe width="920" height="518" src="//www.youtube-nocookie.com/embed/v6u4JxgE2S4?rel=0" frameborder="0" allowfullscreen></iframe>
		<br>
		<br>

		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.5";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<div class="fb-page" data-href="https://www.facebook.com/AirCombatGroup/" data-tabs="timeline" data-width="500" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
			<div class="fb-xfbml-parse-ignore">
				<blockquote cite="https://www.facebook.com/AirCombatGroup/">
					<a href="https://www.facebook.com/AirCombatGroup/">Air Combat Group</a>
				</blockquote>
			</div>
		</div>

		
<?php
	include("includes/footer.inc.php");
?>