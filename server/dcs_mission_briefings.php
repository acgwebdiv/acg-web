<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | DCS WW2 - Public Server - Mission Briefings</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
				<h1>DCS WW2 - Public Server</h1>

				<h2>Mission Briefings</h2>

				<h3>Eclipse</h3>
				<img src="../includes/images/mission_briefings/dcs_eclipse.png">
<!--
				<h3>Operation Whale Eater</h3>
				<img src="../includes/images/mission_briefings/dcs_operation_whale_eater.png">

				<h3>Blood and Iron</h3>
				<img src="../includes/images/mission_briefings/dcs_blood_and_iron.png">

				<h3>Im Stahlgewitter</h3>
				<img src="../includes/images/mission_briefings/dcs_im_stahlgewitter.png">
-->
				<br><br>
				<div class="contentTextParagraphHeader">
					<p>
						You can find further mission details and the server rules in the server/mission description in game.
					</p>
				</div>
				
<?php	
	include("../includes/footer.inc.php");
?>