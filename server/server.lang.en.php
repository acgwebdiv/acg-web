<?php

/* 
------------------
Language: English
------------------
*/

//index.php

$lang['SERVER_PAGE_HEADER'] = "Public Servers";

$lang['SERVER_PAGE_P1'] = "Air Combat Group operates public servers with historically ".
	"based missions and dogfight missions. Our server machine is located in a modern ".
	"data center in Strasbourg. It has a huge bandwidth and a strong multicore CPU ".
	"to give you the best possible online flight sim experience.";

$lang['SERVER_PAGE_HEADER_IL2'] = "IL-2 Cliffs of Dover";
$lang['SERVER_PAGE_INFO_IL2_1'] = "ACG's IL-2 Cliffs of Dover server features 5 hour ".
	"battles containing multiple dynamic missions with their own objectives which ".
	"will be different each time you fly. To determine the battle winner there are ".
	"points scored for striking objectives with weight of bombs, ships sunk and ".
	"aircraft downed and this is announced at battle end.";

$lang['SERVER_PAGE_INFO_IL2_2'] = "Flight data from the public server is recorded and ".
	"stored in a database. The data is used to create pilot statistics and to drive ".
	"ACG's unique character career system.";
	
$lang['SERVER_PAGE_HEADER_IL3'] = "ACG Pointers to Success";

$lang['SERVER_PAGE_HEADER_ILL1'] = "Always consider the mission. War effort points ".
	"are accrued by carrying out mission objectives and preventing the enemy from ".
	"the same. ";
$lang['SERVER_PAGE_HEADER_ILL2'] = "Always consider the mission. War effort points ".
	"are accrued by carrying out mission objectives and preventing the enemy from ".
	"the same. ";
$lang['SERVER_PAGE_HEADER_ILL3'] = "Treat your pilot life as if it were your own. ".
	"This is not kamikaze. Death is costly to you and to your army, it is better to be ".
	"a PoW than risk drowning.";
$lang['SERVER_PAGE_HEADER_ILL4'] = "Try not to lose your aircraft to the enemy. ".
	"If you are damaged and can escape, then escape. If your machine is striken ".
	"then you life matters more.";
$lang['SERVER_PAGE_HEADER_ILL5'] = "4. Assets matter. Strike targets or help your ".
	"forces strike targets by escort. Try to destroy enemy bombers before they reach ".
	"target rather than on the return.";
$lang['SERVER_PAGE_HEADER_ILL6'] = "Use the RDF feature to help win the battle.";


$lang['SERVER_PAGE_HEADER_IL2_1'] = "Documentation";
$lang['SERVER_PAGE_INFO_IL2_3'] = "These pages intend to provide you with the necessary ".
    "information on our public server such that you might get the most enjoyment ".
    "out of it. Look here to find information on how to connect, how to use server ".
    "commands and how the all the processes driving the server missions, pilot statistics ".
    "and radar work.";

$lang['SERVER_PAGE_HEADER_IL2_2'] = "Missions";
$lang['SERVER_PAGE_INFO_IL2_4'] = "Air Combat Group strive to re-create a Battle ".
	"of Britain experience. We use squadrons placed at historical and strategic ".
	"locations with AI and objectives geared to the real targets of the Summer of ".
	"1940. Want to fly a bomber? No problem! All targets for the AI bombers are ".
	"also valid targets for human pilots to score some points as long as the task ".
	"is listed in the briefing.";
		

$lang['SERVER_PAGE_HEADER_IL2_3'] = "Radar";
$lang['SERVER_PAGE_INFO_IL2_5'] = "You want to know where all the action is currently ".
	"happening on the server? You always dreamed of being a ground controller? Follow ".
	"this way and guide your flights towards the enemy with ACG's public server ".
	"Radar from Donkeyworks.";

$lang['SERVER_PAGE_HEADER_IL2_4'] = "Pilot statistics";
$lang['SERVER_PAGE_INFO_IL2_6'] = "ACG members are required to file After Action ".
	"Reports and a character system has been developed to deepen the immersion ".
	"during our virtual sorties. The public server is designed with the same goals ".
	"in mind, such that all pilots flying on it might get an experience close to ".
	"ACG's Sunday campaign. You will, however, not be required to file After Action ".
	"Reports. The character system on the public server is fully automatic and the ".
	"only thing required from you is to fly and enjoy it.";

$lang['SERVER_PAGE_HEADER_DCS'] = "DCS Series";
$lang['SERVER_PAGE_INFO_DCS_1'] = "Our DCS WW2 server features a dogfight mission ".
	"with a duration of about 6 hours set in different weather settings. The missions ".
	"are designed to provide fast action for everyone. If the player numbers are ".
	"low, AI fighters will spawn in and join the fight. In addition to hat we added ".
	"convoys for air to ground / air to sea engagements.";

$lang['SERVER_PAGE_HEADER_DCS_1'] = "How to connect";
$lang['SERVER_PAGE_INFO_DCS_2'] = "Start your game and enter the Multiplayer menu, ".
	"then look out for the <b>ACG WW2 Server</b> in the server list. To join simply ".
	"double click on the name or select and press 'Join', no password is required.";
$lang['SERVER_PAGE_INFO_DCS_3'] = "Direct Connect (IP:Port): 85.93.89.102:10308";
$lang['SERVER_PAGE_INFO_DCS_4'] = "The server is runnning the latest stable version ".
	"of the simulator as long as we didn't announce something different ".
	"<a title='ACG Forum - DCS WW2 Public Server Thread' href='/forum/viewtopic.php?f=77&t=5428.'>here</a>";
$lang['SERVER_PAGE_HEADER_DCS_2'] = "Mission Briefing";
$lang['SERVER_PAGE_INFO_DCS_5'] = "Get your briefing here.";
