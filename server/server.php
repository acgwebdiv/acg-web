<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | Public Server</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
	include('server.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['SERVER_PAGE_HEADER'];?></h1>
				
		<div class="contentText">
		
			<p class="contentTextParagraphHeader">
				<?php echo $lang['SERVER_PAGE_P1'];?>
			</p>
		</div>
		
		<div class="contentText" id="IL2">
			<h2 class="serverHeader">
				<img src="../includes/images/navigation/clod.png">
			</h2>
			<p><?php echo $lang['SERVER_PAGE_INFO_IL2_1'];?></p>
			<p><?php echo $lang['SERVER_PAGE_INFO_IL2_2'];?></p>
			<p style="font-size: 22px; font-weight: bold; color: #ffffff">
				<?php echo $lang['SERVER_PAGE_HEADER_IL3'];?></p>
			<ol>
				<li><?php echo $lang['SERVER_PAGE_HEADER_ILL1'];?></li>
				<li><?php echo $lang['SERVER_PAGE_HEADER_ILL2'];?></li>
				<li><?php echo $lang['SERVER_PAGE_HEADER_ILL3'];?></li>
				<li><?php echo $lang['SERVER_PAGE_HEADER_ILL4'];?></li>
				<li><?php echo $lang['SERVER_PAGE_HEADER_ILL5'];?></li>
				<li><?php echo $lang['SERVER_PAGE_HEADER_ILL6'];?></li>
			</ol>
			<div class="serverSubNavigation">
				<ul>
					<li>
						<a href="/server/clod_documentation.php">
						<img src="../includes/images/navigation/PublicServerDoc.jpg">
						<?php echo $lang['SERVER_PAGE_HEADER_IL2_1'];?>
						<p><?php echo $lang['SERVER_PAGE_INFO_IL2_3'];?></p>
						</a>
					</li>
					<li>
						<a href="/server/clod_mission_briefings.php">
						<img src="../includes/images/navigation/PublicServerBrief.jpg">
						<?php echo $lang['SERVER_PAGE_HEADER_IL2_2'];?>
						<p><?php echo $lang['SERVER_PAGE_INFO_IL2_4'];?></p>
						</a>
					</li>
					<li>
						<a href="/radar/radar.php">
						<img src="../includes/images/navigation/radar.jpg">
						<?php echo $lang['SERVER_PAGE_HEADER_IL2_3'];?>
						<p><?php echo $lang['SERVER_PAGE_INFO_IL2_5'];?></p>
						</a>
					</li>
					<li>
						<a href="../publicserver/pilotList.php">
						<img src="../includes/images/navigation/PublicServerPilotStats.jpg">
						<?php echo $lang['SERVER_PAGE_HEADER_IL2_4'];?>
						<p><?php echo $lang['SERVER_PAGE_INFO_IL2_6'];?></p>
						</a>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="contentText" id="DCS">
			<h2 class="serverHeader">
				<img src="../includes/images/navigation/dcs.png">
			</h2>
			<p><?php echo $lang['SERVER_PAGE_INFO_DCS_1'];?></p>
			<p style="font-size: 22px; font-weight: bold; color: #ffffff">
				<?php echo $lang['SERVER_PAGE_HEADER_DCS_1'];?></p>
			<p><?php echo $lang['SERVER_PAGE_INFO_DCS_2'];?></p>
			<p><?php echo $lang['SERVER_PAGE_INFO_DCS_3'];?></p>
			<p><?php echo $lang['SERVER_PAGE_INFO_DCS_4'];?></p>
			<div class="serverSubNavigation">
				<ul>
					<li>
						<a href="/server/dcs_mission_briefings.php">
						<img src="../includes/images/navigation/dcs_eclipse.png">
						<?php echo $lang['SERVER_PAGE_HEADER_DCS_2'];?>
						<p><?php echo $lang['SERVER_PAGE_INFO_DCS_5'];?></p>
						</a>
					</li>
				</ul>
			</div>
		</div>
<?php
	include("../includes/footer.inc.php");
?>