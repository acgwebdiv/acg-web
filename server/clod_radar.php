<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | CloD - Public Server - Fighter Command</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
				<h1>IL-2 Cliffs of Dover - Public Server</h1>

				<h2>Fighter Command : RDF (Range and Direction Finding)</h2>
				<div class="contentText">
					<p>Our server features a unique realism facility which replicates the Fighter Command response defence system of combined RDF and Observer Corps. This system is available to online players through the in game menu and advises pilots of enemy raids of three aeroplanes or greater together with an interception vector for engagement. The system is also available to Luftwaffe pilots for rendezvous with bomber/fighter groups for escort.  We have included this non-historical feature in order that Luftwaffe pilots may group more effectively or are more likely to escort their raids.  During the Battle of Britain, although Germany had radar systems in place they were only used as an attack device, in particular for finding British shipping convoys as they made their Channel runs.</p>

					<p>Fighter Command has been coded by ACG pilot No.501_Moggel with the consultation and assistance of other ACG members.</p>
					<p>Features:<br>
					�	Accurate detection heights and ranges for the British Chain Home and Chain Home Low systems of the Battle of Britain.<br>
					�	Detection and grouping of human and AI fighters and bombers.<br>
					�	Filter Room simulation which compiles relevant information prior to report to pilot.<br>
					�	Filter Room four minute delay between detection and report (leads to "Fog of War" situations as E/A change course and altitude).<br>
					�	Priority raids based on individual position and calculated pilot interception solution.<br>
					�	Observer Corps simulation once behind CHL position.<br>
					�	Observer Corps tracking bomber groups only as per historical behaviour.<br>
					�	Fighter Command reporting of enemy calculated geographical position, height, heading (once available) and approximate size.<br>
					�	Interception time and vector provided to the pilot when interception is possible.<br>
					�	Automatic grouping and splitting of enemy aircraft by the system.<br>
					�	"Lost" raids.  If a tracked group moves out of range of the system it is reported as lost up to four minutes later.<br>
					�	IFF installed (Identification Friend or Foe). There will be no �Battle of Barking Creek�, the system will disregard friendly aircraft.<br>
					�	Refreshable menu request.<br>
					�	Bonus "Luftflotte" rendezvous feature for Luftwaffe pilots to rendezvous with groups.</p>
					
					<p>You can now concentrate on the mission and forget the big orange HUD messages or chatbar. There is no need to just guess that the fastest way to find action is to just fly low around Manston and Hawkinge waiting for Bf109's to turn up. Historical missions on the ACG server now allow the RAF to intercept any raid within range, in your time, and the Luftwaffe pilots will not wish to circle a base whilst their bomber fleet is intercepted and massacred. Bf109 pilots will be able to immerse themselves into their true historical �freie jagd� advanced fighter sweep and close escort duties and await interception by the RAF in squadron size. Now it is up to the RAF to select their attack position in advance, re-enacting the grandiose battles from over 70 years ago.</p>
	
					<p>The current version has the following limitations which may be updated with future releases:<br>
						�	The RDF system is presently not destroyable.<br>
						�   The system does not use the -10 degrees magnetic variation of the time.</p>
									
				</div>
				
				<h2>Using Fighter Command / Luftflotte</h2>
				<div class="contentText">
					<p>The entire system is available to each pilot via the in game menu, invoked by pressing 'TAB'. You may wish to reposition other windows for clarity since the menu cannot be moved in game.</p>
					<p>Next select option �4. Mission� to open Fighter Command.</p>
					<p><img src="../includes/images/rdf/RDF01.png"</p>
					<p>From the options select �1. Fighter Command� and an RDF report will be provided to you detailing any detected raids at the time. Option 2 is a stub for future enhancement of the system if required.</p>
					<p><img src="../includes/images/rdf/RDF02.png"</p>
					<p>You can see below the raids reported and these are ordered according to your own geographical position with the suggested priority from Fighter Command to you listed first, another pilot at another position may be given a different order. In this case the first raid is very close since there is no interception vector supplied, so keep looking!</p>
					<p><center><img src="../includes/images/rdf/RDF04.png" style="width:920px; height:170px; border="0"></center></p>					
					<p>This example contains a position of 2 separate raids detailing their position, height, heading and number.  In this case if I fly on 80 degrees to 10,000ft I should see them heading North East in just over 1 minute, since I am flying from the West I would expect them to appear at about 2 O'Clock. The second raid is a lower priority based on the time it is expected for me to intercept. Usually this simply is further from me or is heading in a similar direction to me therefore catching it would take longer. These are calculated based on fixed cruising speeds for British fighters in level flight so you may need to allow for climbing or descent.</p>
					<p><center><img src="../includes/images/rdf/RDF03.png"style="width:920px; height:149px; border="0"></center></p>
				
				</div>
				
				<h2>Fighter Command in Action</h2>
				<div class="contentText">
				<p>This short video demonstrates an RAF scramble and interception using the Fighter Command RDF system.</p>
				<center><iframe width="640" height="360" src="http://www.youtube.com/embed/GuOfLJ4bLn8?feature=player_detailpage" frameborder="0" allowfullscreen></iframe></center><br>
				
				<p>Best Practice:<br>
				�	Join ACG Teamspeak 3 voice comms @ 85.236.100.27:16317 (no password).<br>
				�	Set your compass and directional indicator prior to take off for effective interception. <a href="http://www.youtube.com/watch?v=CTRC0sY67Pg"><u>Watch this video for basic setup</u></a><br>
				�	Stick together as a group and assign a flight leader to lead the interception.<br>
				�	Don't be afraid to talk in comms! We are welcoming and encourage communication.<br>
				�	Join one of the Air Combat Group squadrons and get involved! We are recruiting both RAF and Luftwaffe pilots.<br>				
				
				<p>If you need any help or have further questions please post in our forums or ask an ACG member on Teamspeak.</p>
				<p>Happy hunting!</p>
				</div>
<?php
	include("../includes/footer.inc.php");
?>