<?php
	include("../includes/header1.inc.php");
	
	
?>
	<title>Air Combat Group | Public Server</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
	include('clod_documentation.lang.'.$ac_lang.'.php');
?>
	<h1><?php echo $lang['SERVER_DOC_HEADER'];?></h1>
			
	<div class="contentText">
		<p><?php echo $lang['SERVER_DOC_P1'];?></p>
		<p><?php echo $lang['SERVER_DOC_P2'];?></p>
		<p><?php echo $lang['SERVER_DOC_P3'];?></p>
	</div>
	<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_M'];?></h2>
		<p><?php echo $lang['SERVER_DOC_MP1'];?></p>
	</div>
		<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_M1'];?></h2>
		<p><?php echo $lang['SERVER_DOC_MP2'];?></p>
	</div>
	<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_R'];?></h2>
		<p><?php echo $lang['SERVER_DOC_R1'];?></p>
		<center><iframe width="640" height="360" 
		src="//www.youtube.com/embed/GuOfLJ4bLn8?feature=player_detailpage" 
		frameborder="0" allowfullscreen></iframe></center>
		<p><?php echo $lang['SERVER_DOC_R2'];?></p>
	</div>
	<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_C1'];?></h2>
		<p><?php echo $lang['SERVER_DOC_C1'];?></p>
		<p><?php echo $lang['SERVER_DOC_C2'];?></p>
	</div>
	<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_C2'];?></h2>
		<p><?php echo $lang['SERVER_DOC_C3'];?></p>
		<p><?php echo $lang['SERVER_DOC_C4'];?></p>
		<p><?php echo $lang['SERVER_DOC_C5'];?></p>
		<p><?php echo $lang['SERVER_DOC_C6'];?></p>
		<p><?php echo $lang['SERVER_DOC_C7'];?></p>
	</div>
	<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_C3'];?></h2>
		<p><?php echo $lang['SERVER_DOC_C8'];?></p>
		<p><?php echo $lang['SERVER_DOC_C9'];?></p>
		<p><?php echo $lang['SERVER_DOC_C10'];?></p>
	</div>
		<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_C4'];?></h2>
		<p><?php echo $lang['SERVER_DOC_C11'];?></p>
		<p><?php echo $lang['SERVER_DOC_C12'];?></p>
		<p><?php echo $lang['SERVER_DOC_C13'];?></p>
	</div>
	</div>
		<div class="contentText">
		<h2><?php echo $lang['SERVER_DOC_HEADER_C5'];?></h2>
		<p><?php echo $lang['SERVER_DOC_C14'];?></p>
		<p><?php echo $lang['SERVER_DOC_C15'];?></p>
	</div>
<?php
	include("../includes/footer.inc.php");
?>

