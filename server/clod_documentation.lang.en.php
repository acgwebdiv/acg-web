<?php

/* 
------------------
Language: English
------------------
*/

//index.php

$lang['SERVER_DOC_HEADER'] = "ACG's IL-2 Cliffs of Dover server documentation";

$lang['SERVER_DOC_P1'] = "Thank you for choosing to fly with Air Combat Group on ".
    "our public campaign and career server. The following text should give you all ".
    "the information you need to make the most for your sorties and gain the most ".
    "for your own pilot and the war effort. ";

$lang['SERVER_DOC_P2'] = "ACG's primary goal has always been to re-enact the history ".
	"ahead of competition, to attempt to be a part of being there, to learn about ".
	"some of the experiences that were faced by the young pilots who fought for ".
	"their countries in World War II and to honour them by understanding more about ".
	"what they faced. ACG use historically based missions in our Sunday campaign, ".
	"participating members are required to file After Action Reports (AARs) and a ".
	"character system has been developed to achieve this goal <a title='ACG Pilot ".
	"and Mission Database' href='/acg-pam/index.php'>here</a>.";

$lang['SERVER_DOC_P3'] = "The public server is designed with the same goals in mind, ".
	"such that all pilots get an experience close to ACG's sunday campaign. You will, ".
	"however, not be required to file After Action Reports. The whole system on ".
	"the public server is fully automatic and the only thing required from you is ".
	"to fly and enjoy it. Fly like your life is the life of your pilot, fly for ".
	"the greater good of the war effort and you will be rewarded. All the detail ".
	"follows.";
	
$lang['SERVER_DOC_HEADER_M'] = "Missions";

$lang['SERVER_DOC_MP1'] = "Our missions are historically based and have timed objectives ".
    "whilst the mission is alive, each making a contribution to an overall score ".
    "for the duration of the server up-time cycle of 4 hours. They consist of a ".
    "variety of types with most attacks being executed by AI aircraft on set target ".
    "areas. There are also opportunities for ship strikes and menace attacks. It ".
    "is up to you if you wish to join the attack directly with bombs or guns, or ".
    "help make the mission a success by escort duty, but be aware that missions ".
    "matter and you will be rewarded by the automated system for the success of ".
    "your army. You can enjoy our missions at any time because whenever the server ".
    "has fewer than 15 human players the system will autospawn AI fighters.";
    
$lang['SERVER_DOC_HEADER_M1'] = "Mission cycling";

$lang['SERVER_DOC_MP2'] = "The system runs a main mission for 4 hours during which ".
	"points are accrued for bombs on target and destroyed assets in air, sea and ".
	"land when the sub missions are carried out. Each sub mission has a life, for ".
	"example if there is a bomber raid on Manston then any bomb falling on Manston ".
	"during the life of that mission will count toward the overall score, but at ".
	"other times it will not. There are exceptions to this such as for striking ".
	"ships running the gauntlet through the Channel. Since destruction matters it ".
	"is important to see that your army suffers minimal losses and that the enemy ".
	"attacks are thwarted before the target is reached. The server supports polygon ".
	"target areas in order that bombs only need to fall inside to count, so you ".
	"are not required to destroy an asset directly in order to gain points. Should ".
	"you wish to join a formation you may select an aircraft from particular home ".
	"bases and either fly in formation with the AI attackers, or make your own way ".
	"in, and just like the AI your bombs will count should they strike home.";
	
$lang['SERVER_DOC_HEADER_R'] = "Radar";

$lang['SERVER_DOC_R1'] = "ACG offers 2 RDF (Radar) systems within the server. Firstly, ".
	"through the in game menu system an RAF pilot can be provided with bearings ".
	"onto the nearest enemy raid from your own position. And as a Luftwaffe pilot ".
	"the same system will offer a bearing to your own bomber group so that you can ".
	"rendezvous for escort duty or to join the formation for the attack.";
	
$lang['SERVER_DOC_R2'] = "The second option for RDF is using our purpose built radar ".
	"web page <a href='/radar/radar.php'>here</a>. This system provides a slightly ".
	"delayed map of the battle area and aircraft within it. You can see all assets ".
	"from either side including type, number, position, heading and altitude. The ".
	"position information is as per the 'Sector Clock' of the time which tells the ".
	"viewer how old the position is. There are other features, please use it an ".
	"improve your mission experience.";

$lang['SERVER_DOC_HEADER_C1'] = "Stats and Character System";

$lang['SERVER_DOC_C1'] = "Stats will be collected about your sorties as soon as ".
	"you start to fly on the public server. The collected stats will be processed ".
	"to visualize your virtual career; a career in which you can see the progress ".
	"of your virtual characters as they contribute to the success of missions, be ".
	"awarded medals and gain promotions.";

$lang['SERVER_DOC_C2'] = "To ACG members, the character system is part of their ".
	"weekly flying routine. However for newcomers some general concepts need to ".
	"be explained first.";

$lang['SERVER_DOC_HEADER_C2'] = "Difference between pilots and characters";

$lang['SERVER_DOC_C3'] = "All pilots that fly on the public are identified by their ".
	"Steam name. All information on events on the public server will be contributed ".
	"to the same pilot, as long as he/she flies under the same name on the public ".
	"server.";

$lang['SERVER_DOC_C4'] = "Note: Should a pilot fly under the name 'Hans' on the ".
	"public server, all information on takeoffs, victories etc.. will be contributed ".
	"to that name. Should the pilot consider to change his/her Steam name and fly under ".
	"'Fritz' for the next sorties, the new information will be contributed to the ".
	"new name. Like that, the pilot will have two pilots, 'Hans' and 'Fritz', each ".
	"with their own stats.";
	
$lang['SERVER_DOC_C5'] = "Characters on the public server are somewhat like characters ".
	"in a role playing game. They will be created for you with your first sortie ".
	"and collect stats as long as they survive their sorties and are not captured. ".
	"Each pilot will have one active character for each faction he/she flies for ".
	"(Royal Air Force or Luftwaffe). The character is set to inactive should a sortie ".
	"end with the virtual death or capture of the character, a new character will ".
	"be created for the following sorties.";

$lang['SERVER_DOC_C6'] = "Characters get a first- and familiy name, randomly drawn ".
	"from a pool of the most popular names of 1940. The name of the corresponding ".
	"pilot will be put in parentheses between the characters names on our stats ".
	"pages to identify the pilot he belongs to. For example, a character with the ".
	"name Hans 'Thaine' Mayer would indicate a character of pilot 'Thaine'.";

$lang['SERVER_DOC_C7'] = "Pay also attention that some events will be contributed ".
	"to the pilot, while others are contributed to characters.";

$lang['SERVER_DOC_HEADER_C3'] = "Pilot and character update cycle";

$lang['SERVER_DOC_C8'] = "The server stores all sorts of data about every sortie ".
	"on a database. A procedure to update a pilot's characters, each character's ".
	"awards, and the pilot's rank is triggered once the pilot leaves the server. ".
	"All newly stored sorties of the pilot are attributed to the active character ".
	"of the pilot. Sorties have to be finished regularly though. Sorties that were ".
	"abandoned in air or terminated through a disconnect are hereby ignored.";
	
$lang['SERVER_DOC_C9'] = "The active character will be set to inactive should a ".
	"sortie end with the capture or death of the pilot, and a new character will ".
	"be created for the next sorties. The criteria for awarding medals and decorations ".
	"are finally checked and possibly issued for the active character, as well as ".
	"for each character that was captured or killed during the last sorties.";

$lang['SERVER_DOC_C10'] = "Finally possible promotions or demotions are issued for ".
	"the pilot based on the last promotion/demotion date, current rank level and ".
	"number of sorties flown on the public server over the past period.";

$lang['SERVER_DOC_HEADER_C4'] = "Ranks";

$lang['SERVER_DOC_C11'] = "Each pilot on the public server has a certain rank level ".
	"between 1 and 14 that corresponds to a specific rank depending on the faction. ".
	"A description of each rank and it's correlation to rank levels will be added ".
	"to this page in short time. Rank levels 1 to 7 correspond ".
	"to noncommissioned officers (NCO's), while rank levels 8-14 correspond to commissioned ".
	"officers (CO's). The pilots travel up and down through the rank levels by promotions ".
	"and demotions, one rank level a time.";
	
$lang['SERVER_DOC_C12'] = "The amount of time between each promotion and the criteria ".
	"depend on the rank level. Promotions to rank levels 1 to 7 can be issued every ".
	"14 days, while promotions to levels 8 to 14 can be issued every 31 days. The ".
	"deciding criteria for each promotion is the amount of sorties by the pilot ".
	"since the last promotion. The necessary amount for each rank level can be seen ".
	"in the table below. A demotion is issued should once a pilot is not flying ".
	"on the server for longer than 31 days.";

$lang['SERVER_DOC_C13'] = "Note that rank levels are assigned to the pilot. However, ".
	"the rank levels will reflect directly on the characters ranks. That means should ".
	"a pilot be on rank level 7, his active RAF character will hold the rank of ".
	"Sergeant, while his Luftwaffe character will hold the rank of Unterfeldwebel. ".
	"Should the pilot be promoted on rank level to level 8, his active characters ".
	"will be promoted to ranks Flight Sergeant and Feldwebel accordingly. In case ".
	"a character is captured or killed, he will hold his last rank and each newly ".
	"created character will be assigned the rank corresponding to the current rank ".
	"level of the pilot. Characters of the RAF always hold at least the rank of ".
	"Sergeant.";
	
$lang['SERVER_DOC_HEADER_C5'] = "Awards";

$lang['SERVER_DOC_C14'] = "Characters might be awarded medals and decorations depending ".
	"on their performance. The awards are depended on the number of successful returns, ".
	"victories or the combination of both. They are in some cases as well depended ".
	"on the rank of the character. The award images were created by ACG-member Robo ".
	"and the criteria correspond to those used in the ACG campaign. The criteria ".
	"are based on real life award and decoration criteria but have been tweaked ".
	"in order to achieve the approximate historical distribution throughout the ".
	"whole pool of pilots. This tweaking is based on data from the ACG campaign ".
	"and might therefore not fit the public server. However, sufficient data from ".
	"the public server is not yet available to fit the criteria to the public server ".
	"pilot pool. Further adjustments might be needed in the future.";
	
$lang['SERVER_DOC_C15'] = "Note that awards and decorations are assigned to the ".
	"characters, not the pilot. They can be seen on each pilot's character list ".
	"and the character profiles. Awarded medals and decorations of surviving characters, ".
	"active or POW, are displayed as well on the profile of the corresponding pilot.";