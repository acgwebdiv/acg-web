<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | CloD - Public Server - Public Server - Commands</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
				<h1>IL-2 Cliffs of Dover - Public Server</h1>

				<h2>Server Commands</h2>
				<div class="contentText">
					<p>
						The server will supply regular messages to you about the current mission status and tasks.
						However, to retrieve more detailed information you can use the following commands in the chat console.
					</p>
					<p>
						<b><i>&ltscore</i></b> : displays the current battle score<br>
						<b><i>&lttl</i></b> : displays the time left before the battle ends<br>
						<b><i>&ltbriefing</i></b> : displays the current mission information<br>
					</p>
				</div>
				
<?php
	include("../includes/footer.inc.php");
?>