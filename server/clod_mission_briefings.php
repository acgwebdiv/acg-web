<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | CloD - Public Server - Mission Briefings</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
				<h1>IL-2 Cliffs of Dover - Public Server</h1>

				<h2>Mission Briefings</h2>
				<div class="contentText">

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Bf 110 attack on Manston airfield.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>35 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>The radar picked up enemy formations in the area of Dunkerque. It is presumed that the enemy attacks from Eastern direction! Use the RDF to get the latest positions of the enemy.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>8 Bf 110s are on a low level attack mission on Manston Airfield, support them! Rendezvous point is Dunkerque, T+15 minutes at 500m. Waypoint over the sea is BC/BD 25/26 T+21 minutes at 1500m. Planned arrival at target is T+30 minutes.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td>Area of Manston airfield. All military targets. Be careful to avoid the residential towns adjacent to the target.</td>
							</tr>	
							<tr>
								<td colspan="2" align="center"><img src="../includes/images/mission_briefings/IL2_Manston01.jpg"></td>
							</tr>	
							
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Blenheim Attack on Boulogne harbour.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>40 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>A group of Blenheims is about to attack the harbour of Boulogne. Rendezvous points are Eastchurch at T+10 minutes and Dungeness at T+22 minutes at about 9000ft. Estimated time at the target is T+38 minutes.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>An enemy bomber formation has been spotted on the way to France. Patrol over the channel and coast to find them!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td>Area of Boulogne harbour. Avoid the residential area and the main town to the East and concentrate your efforts on the industrial factories and storage facilities around the docks.</td>
							</tr>		
							<tr>
								<td colspan="2" align="center"><img src="../includes/images/mission_briefings/IL2_Boulogne01.jpg"></td>
							</tr>								
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Ju-88s attacking Short factory at Rochester.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>50 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Our long range radar has picked up some scatter over the French coast. Please use the RDF system to spot and intercept incoming raids.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Ju-88s of KG76 are heading towards Rochester to bomb the Short aircraft factory based on the river mouth and the industry in the neighbouring town which provides supplies to it. Escort them to their target and back again. Rendezvous points are Dieppe at T+12 minutes or in map grid AS17 at T+28 minutes at an altitude of 5000m. Expected arrival at target is T+37 minutes.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Blenheim Attack on Cap Gris-Nez Radar.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>32 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Some Blenheims are on a low level attack on the German radar installations at Cap Gris-Nez (AZ20.8). Rendezvous with them over Eastchurch at T+12 minutes or Folkestone at T+20 minutes and protect them on their way to and from the target. The Blenheims are flying at about 1600ft and will reach Cap Gris-Nez at T+26 minutes. Good luck!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Perform a free hunt over the Channel in the Area of Cap Gris-Nez, Calais, Dover, Folkestone.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Do17 &amp; Ju88 attack on Lympne.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>45 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Recon reports that Jerry will try and attack on RAF Lympne (AV23.3). We expect a medium bomber attack that arrives around T+20 minutes to T+30 minutes. Order for all fighters, protect Lympne airfield.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Our bomber group will attack the airfield of Lympne (AV23.3) in a combined low/high level attack. Sadly the massive planned fighter cover is cancelled because of miscommunication. Emergency Order for all BF109 and BF110, protect our Do17s and Ju88s. Approximate arrival time at Cap Gris-Nez (AZ20.9) is T+12 minutes. RAF Lympne is HDG 300 from rendezvous with arrival time in the target area at T+25 minutes. Hurry up guys, without you, our bombers will be in deep trouble!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Furball over Dover.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>30 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Gentlemen, we have spotted an enemy force leaving France on the way to to Dover. This is the day you are trained for, we expect a massive fighter cover. Go to Dover to defend England!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>We want to get as many RAF Fighters in the air as possible, to give them a bloody nose with our 109s. To reach this goal we will attack the radar station of Dover. At T+10 minutes our strike force will leave France near Wissant/Coquelles BB21 in 4000m. Be ready to fight the RAF over Dover. Horrido!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>He111 attack on Dover radar and Folkestone harbour.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>30 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Enemy bombers are coming in from the East at about 16000ft. We except them to make their attack somewhere between Deal and Dungeness. Use the RDF to get the latest status.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Support a group of Heinkels in their attack on the Dover radar station and Folkestone harbour. They took off in Belgium and approach their targets from the East at an altitude of 5300m.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Wellingtons attack Saint-Omer factories.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>45 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>A group of Wellingtons is on the way to attack factories at Saint-Omer. They are flying at 13000ft. Escort them to their target! Rendezvous points are Eastchurch T+5 minutes and Deal T+15 minutes. Estimated arrival at target is T+30 minutes.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>We received reports that enemy bomber formations have been spotted near Southend. Their heading is South-East. Intercept them!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Do17 Attack on Hawkinge.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>30 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Gentlemen, We believe that Jerry will attack us somewhere around Folkstone, Hawkinge, Lympe in AW23. Attention, we expect a massive fighter cover. Go to Folkstone and cover the area. Good luck!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Kameraden, Our target for today, the airfield of Hawkinge. At T+15 minutes our strike force, Do 17 "Bleistifte", flying at 1000m covered by 109´s in 3000m, will leave France near Calais/Coquelles BB21. Arrivial time at target is T+25 minutes. Horrido!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Free hunt over Canterbury.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>40 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Enemy fighters have been spotted in the area of Canterbury. Go there and get them!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Your task is to perform a free hunt over England in the area of Canterbury. Engage all enemies you can find. Good hunting!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>He111 attack Southend-on-Sea.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>40 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Gentlemen, a group of Heavy Bombers were spotted near Dunkerque heading North-West. We expect them to attack somewhere in the Thames estuary. Use the RDF to get their latest position information. Good luck.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Gentlemen, at T+6 minutes a group of He111 will arrive at Dunkerque at 5000m altitude. There heading will be North West to map grid BA27.7 which will be reached at T+20 minutes and from there West to Southend-on-Sea. The target is the train station of Southend-on-Sea which will be reached at T+30 minutes. Protect our M&ouml;belwagen!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Blenheim attack on the train bridges of Bourbourg, near Dunkerque.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>32 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Gentlemen, our target for today: the train bridges of Bourbourg, near Dunkerque in BE22.2. Our Blenheims coming from BE30 and are heading South to the target which will be reached at T+18 minutes. After the attack was made they carry on a bit more South to BE20.1 at T+22 minutes where they turn West to leave France with heading 300 in BB21.4 at T+30 minutes. Find the Blenheims and protect them!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Gentlemen, our Navy recon spotted a group of Blenheims way out North of Dunkerque in BE30, heading 180.  The target area is unknown, we guess, they try to cut our supply lines somewhere around Dunkerque. Go there and protect any kind of target like the harbour, train station or the railroad bridges. Good hunting!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>He111 attack Tilbury Docks.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>60 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>A massive enemy bomber force is building up in the area of Dunkerque. It is expected that they will attack targets in the Thames estuary like they did recently. Use the RDF to get the latest status.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Support II./KG27 with their He111s in an attack on the Tilbury Docks (AQ27.3). Rendezvous with them over Dunkerque at T+17 minutes at 4000m. They will fly towards Manston T+34 minutes and then head West to Tilbury T+49 minutes. Hals- und Beinbruch!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>JaBo attack on Ashford.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>60 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Patrol over the South and East coast of Kent and intercept any enemy you can find.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Perform a JaBo (fighter bomber) attack against the city of Ashford. There is no specific target, just bring as much terror over the whole city as you can until T+60 minutes.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Ju-88s attacking Lympne &amp; Hawkinge.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>45 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Our long range radar has picked up some scatter over the French coast. Please use the RDF system to spot and intercept incoming raids.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Two groups of Ju-88s heading towards Lympne and Hawkinge. Rendezvous at 5000m over Dieppe at T+12 minutes or 30km West of Boulogne at T+27 minutes. Arrival time at target is T+37 minutes. Protect the bombers from incoming interceptors and ensure their safe way home.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Blenheim Attack on Calais.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>32 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>12 Blenheims are on their way to attack Calais. Meet up with them over Canterbury at T+10 minutes or at T+14 minutes over Deal. Altitude is 14000ft. They'll head east from deal and then straight down South to Calais which will be reached at T+28 minutes.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>English bombers attacked Dunkerque yesterday. We expect that they will return today to hit another city in this area. Stay over the Channel and look out for incoming bombers!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Stuka attack on Dover harbour.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>40 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Our long range radar has picked up some scatter over the French coast. Please use the RDF system to spot and intercept incoming raids.</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>20 Stukas are attacking Dover harbour. Rendezvous is over Calais at T+15 minutes at altitude 3300m. The expected arrival time at the target is T+29 minutes.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>		

					<p>
						<table class="eventTable" border="1" >
							<tr>
								<td class="eventTableFirstCol">Title:</td>
								<td>Wellingtons attack Dunkerque.</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Duration:</td>
								<td>35 minutes</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Allied briefing:</td>
								<td>Gentlemen, our target for today: the cargo train station of Dunkerque in BE22.5. Our Wellington Bombers approaching the target from North with a direct heading towards South. Their waypoints are: - BE27 at T+10 minutes - target at T+20 minutes - turning point BD18 T+25 minutes - leaving France with heading 300 in BB21 T+35 minutes.  Find the Wellingtons and protect them!</td></td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Axis briefing:</td>
								<td>Kameraden! Wellington bombers are coming from the North towards Dunkerque. We expect them to arrive there between T+20 minutes and T+25 minutes. Go to the area of Dunkerque and hunt the bombers, watch out for escort fighters! Good luck!</td>
							</tr>
							<tr>
								<td class="eventTableFirstCol">Target information:</td>
								<td></td>
							</tr>											
						</table>
					</p>

				</div>
			
				
<?php	
	include("../includes/footer.inc.php");
?>