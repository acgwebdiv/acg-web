<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | DCS - Public Server</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
				<h1>DCS WW2 - Public Server</h1>
				
				<div class="contentText">
					<p>
						Our DCS WW2 server features a dogfight mission with a duration of about 6 hours set in different weather settings. The missions are designed to provide fast action for everyone. 
						If the player numbers are low, AI fighters will spawn in and join the fight. In addition to hat we added convoys for air to ground / air to sea engagements.
					</p>
				</div>
				
				<h2>How to connect</h2>
				<div class="contentText">
					<p>
						Start your game and enter the Multiplayer menu, then look out for the <b>ACG WW2 Server</b> in the server list.
						To join simply double click on the name or select and press 'Join', no password is required.
						<br><br>
						Direct Connect (IP:Port): 85.93.89.102:10308
						<br><br>
						The server is runnning the latest stable version of the simulator as long as we didn�t announce something different <a title="ACG Forum - DCS WW2 Public Server Thread" href="/forum/viewtopic.php?f=77&t=5428.">here<a>.
					</p>
				</div>

				<h2>More Information</h2>
				<div class="contentSubMenu">
						<a title="Mission Briefings" href="/server/dcs_mission_briefings.php">� Mission Briefings<a><br>
				</div>
				
<?php
	include("../includes/footer.inc.php");
?>