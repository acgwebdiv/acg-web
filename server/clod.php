<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | Public Server</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
?>
				<h1>IL-2 Sturmovik: Cliffs of Dover Blitz - Public Server</h1>
				
				<div class="contentText">
					<p>
						The server features 5 hour battles containing multiple dynamic missions with their own objectives which will be different each time you fly.
						To determine the battle winner there are points scored for striking objectives with weight of bombs, ships sunk and aircraft downed and this is announced at battle end.
					</p>
				</div>
				
				<h2>How to connect</h2>
				<div class="contentText">
					<p>
						Start your game and enter the Multiplayer menu, then select 'Client' to bring up the server lists and look for the <i>ACG Public Server</i>.
						To join simply double click on the name or select and press 'Join Server', no password is required.
						<br><br>
						Direct Connect (IP:Port): 85.93.89.102:27015
						<!--<a href="steam://connect/85.93.89.102:27015" class="postlink" rel="nofollow"><img src="" border="0" alt="" />click</a>-->
						<br><br>
						<b>Note: Blitz Edition is required to join the server!</b>
					</p>
				</div>
				
				<h2>Mission philosophy</h2>
				<div class="contentText">
					<p>
						The Air Combat Group strives to re-create a Battle of Britain experience. We use squadrons placed at historical and strategic locations with AI and objectives geared to the real targets of the Summer of 1940.  
						Extensive briefings (r-click the map in game) are available detailing your objectives and you may choose to be guided on your mission by our exclusive <i>Fighter Command / Luftflotte</i> systems
						which provides interception and rendezvous information.
						<br/><br/>
						Want to fly a bomber? No problem! All targets for the AI bombers are also valid targets for human pilots to score some points as long as the task is listed in the briefing.
					</p>
				</div>

				<h2>Radar</h2>
				<div class="contentText">
					<p>
						You want to know where all the action is currently happening on the server?<br>
						You always dreamed of being a ground controller?
					</p>
				</div>
				<div class="contentSubMenu">
						<a title="Radar" href="/radar/radar.php">Check out our live radar!<a><br>
				</div>

				<h2>More Information</h2>
				<div class="contentSubMenu">
						<a title="Mission Briefings" href="/server/clod_mission_briefings.php">� Mission Briefings<a><br>
						<a title="Fighter Command / Luftflotte System" href="/server/clod_radar.php">� Fighter Command / Luftflotte System<a><br>
						<a title="Server Commands" href="/server/clod_server_commands.php">� Server Commands<a><br>
				</div>
				
				<h1>IL-2 Sturmovik: Battle of Stalingrad - Public Server</h1>
				
				<div class="contentText">
					<p>
						The server features battles containing multiple dynamic missions with their own objectives.
					</p>
				</div>
				
				<h2>How to connect</h2>
				<div class="contentText">
					<p>
						Start your game and enter the Multiplayer, click on Dogfights to bring up the server list and look for the <i>Air Combat Group - Public Server</i>.
						To join simply double click on the name or select and press 'Join Server', no password is required.
						<br><br>
					</p>
				</div>
				
				<h2>Mission philosophy</h2>
				<div class="contentText">
					<p>
						The Air Combat Group strives to re-create an authentic pilot experience experience. We use squadrons placed at historical and strategic locations with objectives geared to the real targets of the Russian Front.
						<br/><br/>
					</p>
				</div>
				
				<h2>More Information</h2>
				<div class="contentSubMenu">
						<a title="Mission Briefings" href="/server/clod_mission_briefings.php">� Mission Briefings<a><br>
				</div>
<?php
	include("../includes/footer.inc.php");
?>