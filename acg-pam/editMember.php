<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    //Accessing data for member from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")) {
        
        $edit_id = filter_input(INPUT_GET, "m_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT username, callsign, status, admin, mapViewer, missionBuilder ".
               "FROM acgmembers WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $membername = $result["username"];
        $callsign = $result["callsign"];
        $status = $result["status"];
        $admin = $result["admin"];
        $mapViewer = $result["mapViewer"];
        $missionBuilder = $result["missionBuilder"];
        
        // Accessing membership status data from database for editing.
        $sql = "SELECT memberstatuslog.id, memberstatus.status, memberstatuslog.date, memberstatuslog.comment, ".
               "UNIX_TIMESTAMP(memberstatuslog.date) AS mspdate ".
               "FROM memberstatuslog LEFT JOIN memberstatus ON memberstatuslog.statusID = memberstatus.id ".
               "WHERE memberID = $edit_id ORDER BY mspdate DESC";
        // echo $sql;
        $su_query = mysqli_query($dbx, $sql);

        $sql = "SELECT memberstatus.id, memberstatus.status FROM memberstatus";
        $su_result = mysqli_query($dbx, $sql);
        
        // Accessing squadron and transfer data from database for editing.
        $sql = "SELECT transfers.id, squadrons.name, transfers.transferdate, UNIX_TIMESTAMP(transferdate) AS tstdate ".
                "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
                "WHERE memberid = $edit_id ORDER BY tstdate DESC";
        $t_query = mysqli_query($dbx, $sql);
        
        $sql = "SELECT * FROM squadrons";
        $s_query = mysqli_query($dbx, $sql);
        
        // Access character data from database for editing.
        $sql = "SELECT careercharacters.id, careercharacters.firstname, careercharacters.lastname, ".
               "characterstatus.status, careercharacters.characterstatus FROM careercharacters ".
               "LEFT JOIN characterstatus ON characterstatus.id = careercharacters.characterstatus ". 
               " WHERE personifiedby = $edit_id";
        $c_query = mysqli_query($dbx, $sql);
        
        $sql = "SELECT id, status FROM characterstatus";
        $cs_query = mysqli_query($dbx, $sql);
        $cs_array = mysqli_fetch_all($cs_query, MYSQLI_NUM);
                
        $sql = "SELECT squadrons.faction, UNIX_TIMESTAMP(transfers.transferdate) AS tstdate ".
               "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
               "WHERE memberid = $edit_id ORDER BY tstdate DESC";
        $cf_query = mysqli_query($dbx, $sql);
        $squadron_check = mysqli_num_rows($cf_query);
        if($squadron_check > 0){

            $cf_result = mysqli_fetch_assoc($cf_query);
            $faction = $cf_result["faction"];
        }
        
        // Accessing rank and promotion data from database for editing.
        $sql = "SELECT promotions.id, ranks.name, promotions.date, promotions.comment, ".
               "UNIX_TIMESTAMP(promotions.date) AS tspdate ".
               "FROM promotions LEFT JOIN ranks ON (promotions.value, ranks.faction) = (ranks.value, '$faction') ".
               "WHERE memberID = $edit_id ORDER BY tspdate DESC";
//        echo $sql;
        $p_query = mysqli_query($dbx, $sql);

        $sql = "SELECT ranks.value, ranks.name FROM ranks WHERE faction = '$faction' ORDER BY value";
        $r_result = mysqli_query($dbx, $sql);
    } 
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/memberAdminLogic.js"></script>
<script src="jscript/transferMemberLogic.js"></script>
<script src="jscript/promoteMemberLogic.js"></script>
<script src="jscript/characterLogic.js"></script>
<script type="text/javascript">

//function transfer(){
//    transferMember();
//    
//}

window.onload = function(){
    gebid("membername").addEventListener("change", checkDBforMembername, false);
    gebid("callsign").addEventListener("change", checkDBforCallsign, false);
    gebid("changeMembername").addEventListener("click", changeMembername, false);
    gebid("changeCallsign").addEventListener("click", changeCallsign, false);
    gebid("changeAdmin").addEventListener("click", changeAdmin, false);
    gebid("deleteMember").addEventListener("click", deleteMember, false);
    gebid("statusupdateBtn").addEventListener("click", updateStatus, false);
    gebid("transferBtn").addEventListener("click", transferMember, false);
    gebid("promoteBtn").addEventListener("click", promoteMember, false);
//         gebid("submitBtn").addEventListener("click", createCharacter, false);
    gebid("characterBtn").addEventListener("click", createCharacter, false);
};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-120.1</p>

<h3>Edit member ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<form id="editMember" onsubmit="return false;" >
    <div class="middlePageStandard">
        <b>Member name:</b>
        <input type="text" id="membername" name="membername" value="<?php echo $membername; ?>">
        <button id="changeMembername">Save Change</button>
        <span id="mnamestatus" ></span>
        <p>This has to be the same name as used in the ACG-Forums.
        Allowed symbols are letters, numbers, whitespace and hyphen.</p>
    </div>

    <div class="middlePageStandard">
        <b>Member callsign:</b>
        <input type="text" id="callsign" name="callsign" value="<?php echo $callsign; ?>">
        <button id="changeCallsign">Save Change</button>
        <span id="callsignstatus" ></span>
        <p>Shorter name for usage in After Action Reports. It should be
        the same callsign as used ingame. Allowed symbols are letters
        and numbers.</p>
    </div>

    <div class="middlePageStandard">
        <b>Member permissions:</b><br>
        <input id="admin" name="admin" type="checkbox" <?php if($admin) {echo "checked";} ?> >
        Admin: <br>
        <input id="missionBuilder" name="missionBuilder" type="checkbox" <?php if($missionBuilder) {echo "checked";} ?> >
        Mission builder: <br>
        <input id="mapViewer" name="mapViewer" type="checkbox" <?php if($mapViewer) {echo "checked";} ?> >
        Map viewer: <br>
        <br>
        <button id="changeAdmin">Save Change</button>
        <span id="adminStatus" ></span>
        <p>Member permissions grant the member rights to add/edit/edit members, missions,
        etc. Map viewer permissions grant the member rights to view the interactive raid
        map for campaign events. Mission builder permissions grant the members rights to
        view and edit the raid map.</p>
    </div>

    <div class="middlePageStandard">
        <p><button id="deleteMember">Delete</button> Deletes member from database.
        Only members that did not attend a single mission should be deleted from
        the database for consistency.</p>
    </div>
</form>

<hr>
<h3>Membership status:</h3>
<form id="memberStatus" onsubmit="return false;" >
    <div class="middlePageStandard">
    <p>Update the status of the member. The current date will be set as the update
    date if the date-fields are left empty.</p>
    <p><b>Note:</b> Please add the estimated date of return in the comments when setting
    members to "Relieved from Duty".</p>
    </div>
    
    <div>
    <b>Update status to:</b>
        <select id="status" name="status">
            <?php
                while($row = mysqli_fetch_assoc($su_result)){
            ?>
            <option value="<?php echo $row["id"];?>"><?php echo $row["status"];?></option>
            <?php } ?>    
        </select>
    </div>

    <div class="middlePageStandard">
        <b>Date (YYYY-MM-DD):</b> 
        <input type="text" id="sudateY" name="sudateY" size="4" maxlength="4">-
        <input type="text" id="sudateM" name="sudateM" size="2" maxlength="2">-
        <input type="text" id="sudateD" name="sudateD" size="2" maxlength="2">
        <p>If left empty the current date will be set.</p>
    </div>
    
    <div class ="middlePageStandard">
        <b>Status update comment:</b><br>
        <textarea id="sucomment" rows="2" cols="50" maxlength="200"></textarea>
    </div>
    
    <div class="middlePageStandard">
        <button id="statusupdateBtn">Update status</button>
        <span id="statusUpdateStatus">&nbsp;</span>
    </div>
</form>


<div class="middlePageStandard">
    <h3>Status updates:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>Status:</th>
            <th>Date:</th>
            <th>Comment:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($su_query)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["status"];?></td>
            <td><?php echo $row["date"];?></td>
            <td><?php echo $row["comment"];?></td>
            <td><button onclick="deleteStatusUpdate(<?php echo $row['id']; ?>)">Delete</button></td>
        </tr>
        <?php } ?>
    </table>
</div>  

<hr>
<h3>Transfer member:</h3>
<form id="transferMember" onsubmit="return false;" >
    <div class="middlePageStandard">
    <p>Transfer member to another unit. The current date will be set
    as the transfer date if the date-fields are left empty. For historical reasons
    it is advised to dismiss any active character first before transferring a 
    member to a different faction. A new character should be created afterwards.
    This will ensure that the new character bears the name fitting the new 
    units faction.</p>
    </div>
    
    <div>
    <b>Transfer to unit:</b>
        <select id="squadron" name="squadron">
            <?php
                while($row = mysqli_fetch_assoc($s_query)){
            ?>
            <option value="<?php echo $row["id"];?>"><?php echo $row["faction"]." - ".$row["name"];?></option>
            <?php } ?>   
        </select>
    </div>

    <div class="middlePageStandard">
        <b>Transfer date (YYYY-MM-DD):</b> 
        <input type="text" id="tdateY" name="tdateY" size="4" maxlength="4">-
        <input type="text" id="tdateM" name="tdateM" size="2" maxlength="2">-
        <input type="text" id="tdateD" name="tdateD" size="2" maxlength="2">
        <p>If left empty the current date will be set.</p>
    </div>
    
    <div class="middlePageStandard">
        <button id="transferBtn">Transfer member</button>
        <span id="transferStatus">&nbsp;</span>
    </div>
    
</form>

<div class="middlePageStandard">
    <h3>Transfers:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>To unit:</th>
            <th>Date:</th>                        
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($t_query)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["name"];?></td>
            <td><?php echo $row["transferdate"];?></td>
            <td><button onclick="deleteTransfer(<?php echo $row['id']; ?>)">Delete</button></td>
        </tr>
        <?php } ?>
    </table>
</div>

<hr>
<h3>Promote/Demote member:</h3>
<form id="promoteMember" onsubmit="return false;" >
    <div class="middlePageStandard">
    <p>Promote/demote member. The current date will be set as the promotion/demotion
    date if the date-fields are left empty.</p>
    </div>
    
    <div>
    <b>Promote/demote to rank:</b>
        <select id="rank" name="rank">
            <?php
                while($row = mysqli_fetch_assoc($r_result)){
            ?>
            <option value="<?php echo $row["value"];?>"><?php echo $row["name"];?></option>
            <?php } ?>    
        </select>
    </div>

    <div class="middlePageStandard">
        <b>Promotion/demotion date (YYYY-MM-DD):</b> 
        <input type="text" id="pdateY" name="pdateY" size="4" maxlength="4">-
        <input type="text" id="pdateM" name="pdateM" size="2" maxlength="2">-
        <input type="text" id="pdateD" name="pdateD" size="2" maxlength="2">
        <p>If left empty the current date will be set.</p>
    </div>
    
    <div class ="middlePageStandard">
        <b>Promotion/demotion comment:</b><br>
        <textarea id="pcomment" rows="2" cols="50" maxlength="200"></textarea>
    </div>
    
    <div class="middlePageStandard">
        <button id="promoteBtn">Promote/Demote member</button>
        <span id="promoteStatus">&nbsp;</span>
    </div>
</form>


<div class="middlePageStandard">
    <h3>Promotions/Demotions:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>To rank:</th>
            <th>Date:</th>
            <th>Comment:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($p_query)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["name"];?></td>
            <td><?php echo $row["date"];?></td>
            <td><?php echo $row["comment"];?></td>
            <td><button onclick="deletePromotion(<?php echo $row['id']; ?>)">Delete</button></td>
        </tr>
        <?php } ?>
    </table>
</div>          
<?php include(dirname(__FILE__).'/footer.php');