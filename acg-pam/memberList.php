<?php
    include(dirname(__FILE__).'/header0.php');
    
    // Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "index")) {
        $indexLetter = filter_input(INPUT_GET, "index").'%';
    } else {
        $indexLetter = '%';
    }
    $indexClause = "callsign LIKE '$indexLetter' ";
    $indexPost = "index=$indexLetter";
    if(filter_has_var(INPUT_GET, "sqn")) {
        $sqn = filter_input(INPUT_GET, "sqn");
        $sqnClause = "AND squadrons.id = $sqn ";
        $sqnPost = "sqn=$sqn";
    } else {
        $sqnClause = "";
        $sqnPost = "";
    }
    if(filter_has_var(INPUT_GET, "filter")) {
        $filter = filter_input(INPUT_GET, "filter");
    } else {
        $filter = "callsign";
    }
    $filterClause = "ORDER BY $filter ASC ";
    $filterPost = "filter=$filter";
    
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
        $pagePost = "page=$page";
    } else {
        $page = 1;
        $pagePost = "";
    }
    $n_entries = 18;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT acgmembers.id, acgmembers.callsign, ranks.abreviation, ".
           "ranks.value, firststs2.date AS joiningdate, acgmembers.dischargedate, ".
           "memberstatus.status AS status, currentsts2.date AS lastStatus, ".
           "squadrons.name FROM acgmembers ".
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MIN(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS firststs ".
                "ON (firststs.memberID, firststs.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS firststs2 ".
            "ON firststs2.memberID = acgmembers.id ".
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
            "ON currentsts2.memberID = acgmembers.id ".
           "LEFT JOIN memberstatus ON currentsts2.statusID = memberstatus.id ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
            "LEFT JOIN ".
                "(SELECT promotions.memberid, promotions.value FROM promotions ".
                "JOIN ".
                    "(SELECT promotions.memberid, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                    "FROM promotions GROUP BY memberid) AS currentrank ".
                "ON (currentrank.memberid, currentrank.pdate) = ".
                "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
           "ON currentrank2.memberid = acgmembers.id ".
           "LEFT JOIN ranks ON (currentrank2.value, squadrons.faction) = (ranks.value, ranks.faction) ".
           "WHERE $indexClause $sqnClause $filterClause LIMIT $start_from, $n_entries";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(acgmembers.id) FROM acgmembers ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
           "WHERE $indexClause $sqnClause";
//    echo $sql;
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-001.1</p>
<h3>ACG-Members:</h3>
<div>
    <p>These pages list all registered ACG members with their corresponding names, 
    callsigns, joining- and eventually discharge dates, their status and their 
    current squadron. Click on any member to access a detailed profile.</p>
</div>
<div class="indexSelect">
    <?php createPageIndex($indexLetter, "memberList.php?".$sqnPost."&"); ?>
</div>
<div>
    <table>
        <thead>
            <tr>
                <?php $filterLink = "memberList.php?".$sqnPost."&".$indexPost."&".$pagePost; ?>
                <th><a href="<?php echo($filterLink."&filter=value");?>">Rank:</a></th>
                <th><a href="<?php echo($filterLink."&filter=callsign");?>">Callsign:</a></th>
                <th><a href="<?php echo($filterLink."&filter=joiningdate");?>">Joining Date:</a></th>
                <th><a href="<?php echo($filterLink."&filter=lastStatus");?>">Last status change:</a></th>
                <th><a href="<?php echo($filterLink."&filter=status");?>">Current Status:</a></th>
                <th>Current Squadron/Staffel:</th>
            </tr>
        </thead>
        <?php
            while($row = mysqli_fetch_assoc($result)) {
                $link = "memberDetails.php?m_id=".$row["id"];
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($link);?>"><?php echo $row["abreviation"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["callsign"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["joiningdate"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["lastStatus"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["status"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["name"];?></a></td>
            </tr>    
        </tbody>
        
        <?php } ?>
    </table>
</div>
<div class="pageSelect">
    <?php  createPageSelect($n_pages, $page, "memberList.php?".$sqnPost."&".$indexPost."&"); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');