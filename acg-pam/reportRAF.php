<?php
    include(dirname(__FILE__).'/header0.php');
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    $dbx = getDBx();
    
    if(filter_has_var(INPUT_GET, "r_id")){
        $report_id = filter_input(INPUT_GET, "r_id");
    } else {
        header("location: missionList.php");
        exit();
    }
       
    $user_id = $_SESSION["userID"];
    //Load this report
    
    $sql = "SELECT squadrons.name AS s_name, squadrons.code AS s_code, ".
           "flightsraf.name AS flight, sectionraf.name AS section, sectionpos.name AS sectionpos, ".
           "careercharacters.firstname, careercharacters.lastname, acgmembers.callsign, ".
           "acgmembers.id AS m_id, missions.histdate, missionstatus, missions.realDate, ".
           "reports.aerodrome, reports.markings, reports.synopsis, reports.missionid, ".
           "aeroplanes.name AS aeroplane, ".
           "reportdetailsraf.serialno ,".
           "pilotstatus.status AS pilotstatus, aeroplanestatus.status AS aeroplanestatus, ".
           "reports.accepted ".
           "FROM reports ".
           "LEFT JOIN squadrons ON reports.squadronid = squadrons.id ".
           "LEFT JOIN reportdetailsraf ON reportdetailsraf.reportid = reports.id ".
           "LEFT JOIN flightsraf ON flightsraf.id = reportdetailsraf.flight ".
           "LEFT JOIN sectionraf ON sectionraf.id = reportdetailsraf.section ". 
           "LEFT JOIN sectionpos ON sectionpos.id = reportdetailsraf.sectionpos ".
           "LEFT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN missions ON reports.missionid = missions.id ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = reports.aeroplane ".
           "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
           "LEFT JOIN aeroplanestatus ON aeroplanestatus.id = reports.aeroplanestatus ".
           "WHERE reports.id = $report_id";
    
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>0) {
        
        $r_result = mysqli_fetch_assoc($query);
        
        $squadron_name = $r_result["s_name"];
        $squ_code = $r_result["s_code"];
        
        $flight = $r_result["flight"];
        $section = $r_result["section"];
        $sectionPos = $r_result["sectionpos"];
        
        $firstName = $r_result["firstname"];
        $lastName = $r_result["lastname"];
        $callsign = $r_result["callsign"];
        $m_id = $r_result["m_id"];
        $m_link = "memberDetails.php?m_id=".$m_id;
        
        $mission_hdate = date("d M Y H:i", strtotime($r_result["histdate"]));
        $mi_realDate = $r_result["realDate"];
        $mi_id = $r_result["missionid"];
        $mi_status = $r_result["missionstatus"];
        $m_faction = getFaction($_SESSION["userID"], $dbx);
        
        $aerodrome = $r_result["aerodrome"];
        $aeroplane = $r_result["aeroplane"];
        $markings = $r_result["markings"];
        $serialNo = $r_result["serialno"];
        $synopsis = parseImageTags($r_result["synopsis"]);
        
        
        $pilotStatus = $r_result["pilotstatus"];
        $aeroplaneStatus = $r_result["aeroplanestatus"];
        
        $accepted = $r_result["accepted"];
        
    } else {
       
        //TODO: REDIRECT SCRIPT!
//        header("location: missionList.php");
//        exit();
    }
       
    // Load claims from database and add them to the form
    $sql = "SELECT aeroplanes.name, claimsraf.id, claimstatusraf.status, claimsraf.shared, ".
           "claimsraf.description, claimsraf.accepted ". 
           "FROM claimsraf ".
           "RIGHT JOIN reports ON reports.id = claimsraf.reportid ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = claimsraf.aeroplane ".
           "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
           "WHERE reports.id = $report_id";
    $claim_result = mysqli_query($dbx, $sql);

    // Load ground claims from database and add them to the form
    $sql = "SELECT groundtargets.name, claimsground.id, claimsground.amount, ".
           "claimsground.description, claimsground.accepted ". 
           "FROM claimsground ".
           "RIGHT JOIN reports ON reports.id = claimsground.reportid ".
           "LEFT JOIN groundtargets ON groundtargets.id = claimsground.object ".
           "WHERE reports.id = $report_id";
//    echo $sql;
    $groundclaim_result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT ranks.abreviation ".
           "FROM ranks ".
           "RIGHT JOIN promotions ON (ranks.faction, ranks.value) = ('RAF', promotions.value) ".
           "JOIN ".
                "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS lastPromotionDates ".
           "ON (lastPromotionDates.memberid, lastPromotionDates.pdate) = 
                (promotions.memberid, UNIX_TIMESTAMP(promotions.date)) ".
           "WHERE promotions.memberid = $m_id";
    $rank_querry = mysqli_query($dbx, $sql);
    $rank_result = mysqli_fetch_assoc($rank_querry);
    $rank = $rank_result["abreviation"];
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script src="jscript/reportAdminLogic.js"></script>
 
<?php include(dirname(__FILE__).'/missionMenu.php'); ?> 
<p class="form_id">ACG-PAM/300-540.1</p>
<h3>After Action Report:</h3>
<div>
    <span class="AARSpanLeft">Squadron:</span>
    <span class="AARSpanRight"><?php echo($squadron_name); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Squadron code:</span>
    <span class="AARSpanRight"><?php echo($squ_code); ?></span>
</div>

<div>
    <span class="AARSpanFlightLeft">Flight/Section/Pos:</span>
    <span class="AARSpanFlightRight">
        <?php echo($flight." / ".$section." / ".$sectionPos); ?>
    </span>
</div> 

<div>
    <span class="AARSpanLeft">Name:</span>
    <span class="AARSpanRight"><a href="<?php echo($m_link);?>"><?php echo($rank." ".$firstName." '".$callsign."' ".$lastName); ?></a></span>
</div>

<div>
    <span class="AARSpanLeft">Date:</span>
    <span class="AARSpanRight"><?php echo($mission_hdate); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Base:</span>
    <span class="AARSpanRight"><?php echo($aerodrome); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Type:</span>
    <span class="AARSpanRight"><?php echo($aeroplane); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Markings:</span>
    <span class="AARSpanRight"><?php echo($markings); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Serial Nr.:</span>
    <span class="AARSpanRight"><?php echo($serialNo); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Synopsis:</span><br>
    <p id="synopsis"><?php echo(nl2br($synopsis)) ?></p>
</div>

<div id="claims">
    <hr>
    Claims:<br>
    <table>
<?php
    $isAdmin = false;
    if(isset($_SESSION["admin"])){
        if($_SESSION["admin"]){
            $isAdmin = true;
        }
    }
    while($r_row = mysqli_fetch_assoc($claim_result)) {
        
        $claim_id = $r_row["id"];
        $claim_accepted = $r_row["accepted"];
        if(!is_null($claim_id)){
?>
    <tr >
        <td><?php echo($r_row["name"]);?></td>
        <td><?php echo($r_row["status"]);?></td>
        <td><?php echo($r_row["shared"]? "Shared" : "");?></td>
        
        <td>
<?php 
        if($isAdmin){
            
            if($claim_accepted == -1){
?>
            <button id="acceptStatusChange" onclick="changeClaimAccepted(<?php echo('0, '.$claim_id);?>, 'RAF')">
            Revoke rejection</button>            
<?php 
            } else if($claim_accepted == 0){
?>
            <button id="rejectStatusChange" onclick="changeClaimAccepted(<?php echo('-1, '.$claim_id);?>, 'RAF')">
            Reject claim</button>
            <button id="acceptStatusChange" onclick="changeClaimAccepted(<?php echo('1, '.$claim_id);?>, 'RAF')">
            Accept claim</button> 
<?php                        
            } else if($claim_accepted == 1){
?>
            <button id="acceptStatusChange" onclick="changeClaimAccepted(<?php echo('0, '.$claim_id);?>, 'RAF')">
            Revoke acceptance</button> 
<?php                
            }
        } else {
            
            if($claim_accepted == -1){
                echo "Rejected";
            } else if($claim_accepted == 1){
                echo "Approved";
            }
        }
?>
        </td>
    </tr>
    <tr>
        <td colspan="4"><?php echo($r_row["description"]);?></td>
    </tr>
<?php
        }
    }
?>
    </table>
</div>
<div id="claims">Ground claims:<br>
    <hr>
    <table>
    <?php 
        while($r_row = mysqli_fetch_assoc($groundclaim_result)) {
        
        $claim_id = $r_row["id"];
        $claim_accepted = $r_row["accepted"];
        if(!is_null($claim_id)){
    ?>
    <tr>
        <td><?php echo($r_row["name"]);?></td>
        <td><?php echo("Amount: ".$r_row["amount"]);?></td>
        
        <td><?php if($isAdmin){ ?>
            <button id="acceptStatusChange" onclick="changeClaimAccepted(<?php echo($claim_accepted.', '.$claim_id);?>, 'ground')">
            <?php if($claim_accepted) {echo "Revoke Acceptance";} else {echo "Accept";} ?></button>
            <?php } else { ?>
            <?php if($claim_accepted) {echo "Approved";} ?>
        <?php } ?>
        </td>
    </tr>
    <tr>
        <td colspan="3"><?php echo($r_row["description"]);?></td>
    </tr>
<?php
        }
    }
?>
    </table>
</div>
<div>
    <hr>
    <span class="AARSpanLeft">Pilot status:</span>
    <span class="AARSpanRight"><?php echo($pilotStatus); ?></span>
</div>

<div>
    <span class="AARSpanLeft">Aircraft status:</span>
    <span class="AARSpanRight"><?php echo($aeroplaneStatus); ?></span>
    <hr>
</div>

<div><?php include(dirname(__FILE__).'/reportAdminForms.php');?></div>


<?php include(dirname(__FILE__).'/footer.php');