<script type='text/javascript' src="jscript/flexcroll.js"></script>
</head>
<body id="missionMenuBody">
    <div class="mainContainer">
        <div class="pageTop">
            <?php
            include(dirname(__FILE__).'/primMenu.php'); 
            include(dirname(__FILE__).'/userMenu.php');
            ?>
            <div class="secMenu">
                <ul>
                    <li><a href="missionDetails.php?m_id=<?php echo($mi_id); ?>">Overview</a></li>
                    <?php if($mi_status == 3 || $m_faction === "RAF"){ ?>
                        <li><a href="missionBriefing.php?m_id=<?php echo($mi_id); ?>&f=RAF">Briefing RAF</a></li>
                    <?php } ?>
                    <?php if($mi_status == 3 || $m_faction === "LW"){ ?>
                        <li><a href="missionBriefing.php?m_id=<?php echo($mi_id); ?>&f=LW">Briefing LW</a></li>
                    <?php } ?>
                    <?php if($mi_status == 3 || $m_faction === "VVS"){ ?>
                        <li><a href="missionBriefing.php?m_id=<?php echo($mi_id); ?>&f=VVS">Briefing VVS</a></li>
                    <?php } ?>
                    <?php if($mi_status == 2 && $_SESSION["username"] !== "Visitor") { ?>
                    <li><button onclick="editReport(<?php echo($mi_id.", ".$_SESSION["userID"]); ?>)">Write/Edit report</button></li>
                    <?php } ?>
                    <?php if($mi_status == 3) { ?>
                    <li><a href="missionDebriefDetails.php?m_id=<?php echo($mi_id); ?>">Details</a></li>
                    <?php } ?>
                    <?php if($mi_status >= 2 ) { ?>
                    <li><a href="missionClaims.php?m_id=<?php echo($mi_id); ?>">Claims</a></li>
                    <?php } ?>
                    <?php if($mi_status >= 2 ) { ?>
                    <li><a href="fortuneTellerBoX.php?m_id=<?php echo($mi_id); ?>">Check your fate</a></li>
                    <?php } ?>
                </ul>
                <?php
                    $sql = "SELECT DISTINCT squadrons.id, squadrons.name, squadrons.faction FROM squadrons ".
                           "RIGHT JOIN reports ON reports.squadronid = squadrons.id ".
                           "WHERE reports.missionid = $mi_id ORDER BY squadrons.faction DESC, squadrons.rank ASC";
                    $smenu_result = mysqli_query($dbx, $sql);
                    $link = "missionReports.php?m_id=$mi_id&";
                    include './squadronSubMenu.php';
                ?>
            </div>
        </div>
        <div class="pageMiddle">