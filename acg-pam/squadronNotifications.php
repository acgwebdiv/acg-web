<?php
    include(dirname(__FILE__).'/header0.php');
    include(dirname(__FILE__).'/includes/decorationsChecks.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    if(filter_has_var(INPUT_GET, "sqn")) {
        $sqn = filter_input(INPUT_GET, "sqn");
    }
    
    $dbx = getDBx();
    $sql = "SELECT squadrons.name FROM squadrons WHERE squadrons.id = $sqn";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $sqn_name = $row['name'];    
    
    $sql = "SELECT acgmembers.id, currentsqu2.squadronid ".
           "FROM acgmembers ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
           "WHERE currentsqu2.squadronid = $sqn";
    //echo $sql;
    $result = mysqli_query($dbx, $sql);
    $memberArray = array();
    while($row = mysqli_fetch_assoc($result)) {
        $memberId = $row["id"];        
//        checkDecorationsForMember($memberId, $dbx);
    }



    $sql = "SELECT reports.id, reports.missionID, careercharacters.firstName, ".
           "careercharacters.lastName, acgmembers.callsign, reports.type, reports.dateSubmitted ".
           "FROM reports ".
           "LEFT JOIN careercharacters ON reports.authorID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "WHERE reports.accepted = 0 AND reports.squadronID = $sqn"; 
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT acgmembers.id, decorations.availableSince, awards.name, careercharacters.firstName, ".
           "careercharacters.lastName, acgmembers.callsign, currentsqu2.squadronid ".
           "FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "LEFT JOIN careercharacters ON decorations.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "WHERE decorations.awarded = 0 AND currentsqu2.squadronid = $sqn ".
           "ORDER BY acgmembers.id ASC, decorations.availableSince ASC";
    $d_result = mysqli_query($dbx, $sql);
    
    $sql = "CALL `BoX_PAM_BlacklistDetail`($sqn)";
    $result = mysqli_query($dbx, $sql);

?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/notificationLogic.js"></script>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/squadronMenu.php'); ?> 
<p class="form_id">ACG-PAM/500-100.1</p>
<h3>To the commanding officer and adjutant of <?php echo $sqn_name; ?>:</h3>

<div>
    <h3>After Action Reports:</h3>

    <p>The following pilots are missing After Action Reports.</p>
<?php 
    if(mysqli_num_rows($result)>0){ ?>

    
    <table>
        <tr>
            <th>Name:</th>
            <th>Mission:</th>
            <th>Flew under:</th>
            <th style="width: 100px;"></th>
        </tr>
        <?php
            $member_id = "";
            while($row = mysqli_fetch_assoc($result)) {
                if(is_null($row["accepted"])) {
                    if($member_id != $row["id"]){
                        $member_id = $row["id"];
        ?>
        <tr>
            <td><?php echo $row["callsign"]; echo is_null($row["accepted"]);?></td>
            <td><?php echo $row["missionName"];?></td>
            <td><?php echo $row["boxname"];?></td>
            <td><a href="<?php echo($link);?>"><?php echo $cause;?></a></td>
        </tr>
        <?php 
                    } else {
        ?>
        <tr>
            <td></td>
            <td><?php echo $row["name"];?></td>
            <td><?php echo $row["GCPlayer"];?></td>
            <td><a href="<?php echo($link);?>"><?php echo $cause;?></a></td>
        </tr>
        <?php  
                    }
                }
            } 
        ?>
    </table>
    <br>
        <button id="acceptStatusChange" onclick="sendAARReminder(<?php echo($sqn);?>)">
        Send reminder</button>
    <?php
    }
    ?>
    

    <p>The following AAR's need approval by the commanding officer or squadron/Staffel adjutant.</p>
    <ul>
<?php
    while($row = mysqli_fetch_assoc($result)){
        if($row['type']==1){
            $link = "reportRAF.php?r_id=".$row["id"];  
        } else if($row['type']==2){
            $link = "reportLW.php?r_id=".$row["id"];
        }
        $str = "Mission ".$row['missionID'].": AAR-".$row['type']."-".$row['id']." by ".$row['firstName'].
               " '".$row['callsign']."' ".$row['lastName'].", submitted ".$row['dateSubmitted'].".";
?>
        <li><a href="<?php echo($link);?>"><?php echo($str);?></a></li>
<?php
    }
?>
    </ul>
</div>
<?php if(mysqli_num_rows($d_result)>0){ ?>
<div>
    <h3>Decoration recommendations:</h3>
    <p>The following pilots where recommended for decorations. Approval of the commanding officers 
    or squadron/Staffel adjutant is needed for awarding the decoration.</p>
    <ul>
<?php
    while($row = mysqli_fetch_assoc($d_result)){
        $link = "editCharacter.php?m_id=".$row["id"];
        $name = $row['name'];
        switch($row['name']){
            case "Mentioned in Dispatches":
                $name = "mentioning in dispatches";
                break;
            case "Mentioned in Wehrmachtsbericht":
                $name = "mentioning in Wehrmachtsbericht";
                break;
            default :
                $name = $row["name"];
                break;
        }
        $str = $row['availableSince'].": ".$row['firstName']." '".$row['callsign']."' ".$row['lastName']." for ".
               $name.".";
?>
        <li><a href="<?php echo($link);?>"><?php echo($str);?></a></li>
<?php
    }
?>
    </ul>
</div>
<?php } ?>
<?php include(dirname(__FILE__).'/footer.php');