<?php
    include(dirname(__FILE__).'/header0.php');
    
    // Check if existing user otherwise redirect
    if(!isset($_SESSION["userID"])){
        header("location: message.php?msg=You have no rights to access this page.");
        exit();
    }
    $user_id = $_SESSION["userID"];
    
    // Setting up indices for spreading content over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 10;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT id, message FROM notifications ".
           "WHERE receiver=$user_id ORDER BY id ASC LIMIT $start_from, $n_entries";   
    $notification_result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM notifications WHERE receiver=$user_id";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>

<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/ajax.js"></script>
<script src="jscript/notificationLogic.js"></script>
<?php include(dirname(__FILE__).'/mainMenu.php'); ?>
        
<p class="form_id">ACG-PAM/500-100.1</p>
<div>
    <h3>Notifications to <?php echo $_SESSION['callsign']; ?>:</h3>
    <?php
        while($row = mysqli_fetch_assoc($notification_result)) { 
    ?>
    <div>
        <button id="deleteBtn" onclick="deleteNotification(<?php echo $row["id"];?>)">X</button>
         <?php echo(htmlspecialchars_decode($row["message"]));?>
    <hr>
    </div>
    <?php } ?>
</div>

<div>
    <?php
        if($n_pages > 1){
           for($i = 1; $i <= $n_pages; $i++) {
                echo " <a href='notifications.php?page=".$i."' >".$i."</a>";
            } 
        }
    ?>
</div>



<?php include(dirname(__FILE__).'/footer.php');

