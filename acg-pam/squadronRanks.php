<?php
    include(dirname(__FILE__).'/header0.php');
    include(dirname(__FILE__).'/squadronHeader.php');
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">
    
    function editMember(id){
        window.location = "editMember.php?m_id="+id;
    }

</script>
<?php include(dirname(__FILE__).'/squadronMenu.php'); ?> 
<p class="form_id">ACG-PAM/500-200.1</p>
<h3><?php echo $sqn_name; ?>-Members:</h3>
<div>
    <p>This page lists all registered ACG members for <?php echo $sqn_name; ?> with their
    ranks, callsigns, joining-dates, length of service and date of last promotion. Click on any 
    member to access a detailed profile.</p>
</div>
<div>
    <p><b>Active members:</b></p>
    <table>
        <thead>
            <tr>
                <th>Rank:</th>
                <th>Callsign:</th>
                <th>Joining Date:</th>
                <th>Length of service:</th>
                <th>Last Promotion:</th>
            </tr>
        </thead>
        <?php
        foreach ($memberArray as $memberID => $memberInfo) {
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["rankAbreviation"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["callsign"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["joiningdate"];?></a></td>
                <td><?php echo lengthOfServiceByMemberID($memberID, $dbx);?></td>
                <td><?php echo $memberInfo["promotionDate"];?></td>
                <td><button onclick="editMember(<?php echo $memberID; ?>)">EDIT</button></td>
            </tr>    
        </tbody>

        <?php } ?>
    </table>
</div>
<div>
    <?php if(count($memberArrayRNR)>0){ ?>
    <p><b>Members on leave:</b></p>
    <table>
        <thead>
            <tr>
                <th>Rank:</th>
                <th>Callsign:</th>
                <th>Joining Date:</th>
                <th>Length of service:</th>
                <th>On leave since:</th>
            </tr>
        </thead>
        <?php
        foreach ($memberArrayRNR as $memberID => $memberInfo) {
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["rankAbreviation"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["callsign"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["joiningdate"];?></a></td>
                <td><?php echo lengthOfServiceByMemberID($memberID, $dbx);?></td>
                <td><?php echo lengthOfServiceByDates($memberInfo["lastStatus"],null);?></td>
                <td><button onclick="editMember(<?php echo $memberID; ?>)">EDIT</button></td>
            </tr>    
        </tbody>

        <?php } ?>
    </table>
    <?php } ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');