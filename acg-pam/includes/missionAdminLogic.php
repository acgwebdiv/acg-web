<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/functions.php');
session_start();
$dbx = getDBx();
if(filter_has_var(INPUT_POST, "n")) {
    $n = filter_input(INPUT_POST, "n");
    $c = filter_input(INPUT_POST, "c");
    $rDate = mktime(0, 0, 0, filter_input(INPUT_POST, "rm"),
        filter_input(INPUT_POST, "rd"), filter_input(INPUT_POST, "ry"));
    $hDate = mktime(filter_input(INPUT_POST, "hh"), filter_input(INPUT_POST, "hmm"), 0, 
            filter_input(INPUT_POST, "hm"), filter_input(INPUT_POST, "hd"), filter_input(INPUT_POST, "hy"));

    $sql = "INSERT INTO missions (name, campaignid, realdate, histdate, missionstatus)"
        ." VALUES ('$n', '$c', '".date("Y-m-d", $rDate)."', '".date("Y-m-d H:i", $hDate)."', 1)";
    if(mysqli_query($dbx, $sql)) {
        echo "Mission created";
        exit();
    } else {
        echo "Error during database manipulation. ".$sql;
        exit();
    }
    exit();
}

if(filter_has_var(INPUT_POST, "changeMissionName")) {
    $edit_id = $_SESSION["edit_id"];
    $n = filter_input(INPUT_POST, "changeMissionName");
    if($n != ""){
        if(changeMission("name", $n, $edit_id)) {
          
            echo "Name changed.";
            exit();
        } else {
            echo "Error during database manipulation.";
            exit();
        }  
        
    } else {
        if(changeMissionToNull("name", $edit_id)) {
            echo "Name changed.";
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    }
}

if(filter_has_var(INPUT_POST, "changeMissionCampaign")) {
    $edit_id = $_SESSION["edit_id"];
    $c = filter_input(INPUT_POST, "changeMissionCampaign");
    if(changeMission("campaignid", $c, $edit_id)) {
        echo "Campaign changed.";
        exit();
    } else
    {
        echo "Error during database manipulation.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "ry")) {
    $edit_id = $_SESSION["edit_id"];
    $check_Date = mktime(0, 0, 0, filter_input(INPUT_POST, "m"),
            $d = filter_input(INPUT_POST, "d"), filter_input(INPUT_POST, "ry"));
    if(changeMission("realdate", date("Y-m-d", $check_Date), $edit_id)) {
        echo "Real date changed.";
        exit();
    } else {
        echo "Error during database manipulation.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "hy")) {
    $edit_id = $_SESSION["edit_id"];
    $check_Date = mktime(filter_input(INPUT_POST, "h"), filter_input(INPUT_POST, "mm"), 0,
            filter_input(INPUT_POST, "m"), filter_input(INPUT_POST, "d"), filter_input(INPUT_POST, "hy"));
    if(changeMission("histdate", date("Y-m-d H:i", $check_Date), $edit_id)) {
        echo "Historic date changed.";
        exit();
    } else {
        echo "Error during database manipulation.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "changeMissionStatus")) {
    $edit_id = $_SESSION["edit_id"];
    $s = filter_input(INPUT_POST, "changeMissionStatus");
    if(changeMission("missionstatus", $s, $edit_id)) {
        echo "Status changed.";
        exit();
    } else
    {
        echo "Error during database manipulation.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "resetMission")){
    $edit_id = $_SESSION["edit_id"];
    $sql = "DELETE FROM GC_loggedmissionattendance WHERE MissionID='$edit_id'";
    if(mysqli_query($dbx, $sql)) {
        echo "reset";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "deleteMission")){
    $edit_id = $_SESSION["edit_id"];
    $sql = "DELETE FROM missions WHERE id='$edit_id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}

//if(filter_has_var(INPUT_POST, "saveMissionBriefing")) {
//    $edit_id = $_SESSION["edit_id"];
//    $s = filter_input(INPUT_POST, "saveMissionBriefing", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
//    $f = filter_input(INPUT_POST, "faction");
//    if(changeMission("briefing".$f, $s, $edit_id)) {
//        echo "Briefing saved.";
//        exit();
//    } else {
//        echo "Error during database manipulation.";
//        exit();
//    }
//}

if(filter_has_var(INPUT_POST, "saveMissionBriefing")) {
    
    $mi_id = filter_input(INPUT_POST, "saveMissionBriefing");
    $faction = filter_input(INPUT_POST, "faction");
    $type = filter_input(INPUT_POST, "type");
        
    $sql = "SELECT id FROM briefings WHERE missionID = $mi_id AND faction = '$faction'";

    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>0){
        $result = mysqli_fetch_assoc($query);
        $briefingID = $result["id"];
        
        //Check for text parts to store
        if(isset($_POST["text1"])){
            $text = filter_input(INPUT_POST, "text1", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
            $storeStr = "text1='".$text."'";
        }
        for($n = 2; $n <= 12; $n++){
            $postStr = "text".$n;
            if(isset($_POST[$postStr])){
                $text = filter_input(INPUT_POST, $postStr, FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
                $storeStr = $storeStr.", ".$postStr."='".$text."'";
            }
        }
        $sql = "UPDATE briefings SET type='$type',".$storeStr." WHERE id='$briefingID'";
        $result = mysqli_query($dbx, $sql);
        
    } else {
        
        //Check for text parts to store
        if(isset($_POST["text1"])){
            $nameStr = "(missionID, faction, type, text1";
            $text = filter_input(INPUT_POST, "text1", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
            $storeStr = "('$mi_id', '$faction', '$type', '$text' ";
        }
        for($n = 2; $n <= 12; $n++){
            $postStr = "text".$n;
            if(isset($_POST[$postStr])){
                $nameStr = $nameStr.", ".$postStr;
                $text = filter_input(INPUT_POST, $postStr, FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
                $storeStr = $storeStr.", '".$text."'";
            }
        }
        $nameStr = $nameStr.")";
        $storeStr = $storeStr.")";
        
        $sql = "INSERT INTO briefings ".$nameStr." VALUES ".$storeStr;
        $result = mysqli_query($dbx, $sql);
    }
    if($result) {
        echo "Briefing saved.";
        exit();
    } else {
        echo "Error during database manipulation. ".$sql;
        exit();
    }
}

function changeMission($field, $value, $edit_id){

    global $dbx;
    $sql = "UPDATE missions SET $field='$value' WHERE id='$edit_id'";
    return mysqli_query($dbx, $sql);
}

function changeMissionToNull($field, $edit_id){

    global $dbx;
    $sql = "UPDATE missions SET $field=NULL WHERE id='$edit_id'";
    return mysqli_query($dbx, $sql);
}
