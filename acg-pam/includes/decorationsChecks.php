<?php

function checkDecorationsForMember($memberID, $dbx){
    
    $sql = "SELECT careercharacters.id FROM careercharacters WHERE personifiedBy = $memberID";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        
        checkDecorations($row["id"], $dbx);
    }    
}

function checkDecorations($characterID, $dbx){
    
    include_once(dirname(__FILE__).'/characterDBFunctions.php');
    $faction = getCharacterFaction($characterID, $dbx);
    $sql = "SELECT MAX(reports.missionID) AS mxmid, MIN(reports.missionID) AS mnmid ".
           "FROM reports WHERE reports.authorid = $characterID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $mxmid = $row["mxmid"];
    $mnmid = $row["mnmid"];
    
    if($faction == "RAF"){
        for($missionID = $mnmid; $missionID <= $mxmid; $missionID++){
            checkRAFDecorations($characterID, $missionID);
        }
    } else if($faction == "LW"){
        for($missionID = $mnmid; $missionID <= $mxmid; $missionID++){
            checkLWDecorations($characterID, $missionID);
        }
    } else if($faction == "VVS"){
        for($missionID = $mnmid; $missionID <= $mxmid; $missionID++){
            checkVVSDecorations($characterID, $missionID);
        }
    }
    
    
}

function checkRAFDecorations($characterID, $missionID){
    
    include_once(dirname(__FILE__).'/characterDBFunctions.php');
        
    $dbx = getDBx();
    //Get date of mission
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $missionDate = $row["realDate"];


    //Get all decorations of the character
    $sql = "SELECT awards.abreviation, decorations.id, decorations.awarded FROM awards ".
           "LEFT JOIN decorations ON decorations.awardID = awards.id ".
           "WHERE decorations.characterID = $characterID";
    $result = mysqli_query($dbx, $sql);
    $decorationsArray = array();
    $awardedArray = array();
    while($row = mysqli_fetch_array($result)){
        $decorationsArray[] = $row[0];
        $awardedArray[$row[0]]["id"] = $row[1];
        $awardedArray[$row[0]]["awarded"] = $row[2];
    }
    
//    echo var_dump($awardedArray);
    
    //Get all awards
    $sql = "SELECT id, abreviation FROM awards ".
           "WHERE faction = 'RAF'";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_array($result)){
        $awardArray["$row[1]"] = $row[0];
    }
    
    //Get stats for character of non-Battle of Britain missions
    $sql = "SELECT COUNT(reports.id) AS sorties, SUM(reports.pilotStatus = 1) AS pilotOK, ".
           "SUM(reports.pilotStatus = 2) AS pilotWND, SUM(reports.aeroplaneStatus = 3) AS aeroLST ".
           "FROM reports LEFT JOIN missions ON reports.missionID = missions.id ".
           "WHERE reports.authorID = $characterID AND reports.accepted = 1 AND missions.id <= $missionID ".
           "AND (histDate < '1940-06-10 00:00:00' OR histDate > '1940-10-31 23:59:59')";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $sortiesNonBOB = $row["sorties"];
    $aeroLostNonBOB = $row["aeroLST"];
    $pilotOKNonBOB = $row["pilotOK"];
    $pilotWoundedNonBOB = $row["pilotWND"];
    
    //Get stats for character of Battle of Britain missions
    $sql = "SELECT COUNT(reports.id) AS sorties, SUM(reports.pilotStatus = 1) AS pilotOK, ".
           "SUM(reports.pilotStatus = 2) AS pilotWND, SUM(reports.aeroplaneStatus = 3) AS aeroLST ".
           "FROM reports LEFT JOIN missions ON reports.missionID = missions.id ".
           "WHERE reports.authorID = $characterID AND reports.accepted = 1 AND missions.id <= $missionID ".
           "AND (histDate >= '1940-06-10 00:00:00' AND histDate <= '1940-10-31 23:59:59')";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    
    $sortiesBOB = $row["sorties"];
    $aeroLostBOB = $row["aeroLST"];
    $pilotOKBOB = $row["pilotOK"];
    $pilotWoundedBOB = $row["pilotWND"];
    
    $sorties = $sortiesNonBOB + $sortiesBOB;
    $aeroLost = $aeroLostNonBOB + $aeroLostBOB;
    $pilotOK = $pilotOKNonBOB + $pilotOKBOB;
    $pilotWounded = $pilotWoundedNonBOB + $pilotWoundedBOB;
    
    $sql = "SELECT careercharacters.personifiedBy FROM careercharacters ".
           "WHERE careercharacters.id = $characterID";
    $querry = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($querry);
    $memberID = $result["personifiedBy"];
    $rankValue = getRankValueAtMission($memberID, $missionID, $dbx);
    
    $sql = "SELECT SUM(destrtable.pointsdestr) AS destr FROM reports ".
           "LEFT JOIN (SELECT claimsraf.reportID, (1-claimsraf.shared*0.5) AS pointsdestr ".
           "FROM claimsraf WHERE enemystatus = 1 AND claimsraf.accepted = 1) AS destrtable ON destrtable.reportID = reports.id ".
           "WHERE reports.accepted = 1  AND authorID = $characterID AND reports.missionID <= $missionID";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $destroyed = $row["destr"];
    
    $sql = "SELECT SUM(probtable.pointsprob) AS prob FROM reports ".
           "LEFT JOIN (SELECT claimsraf.reportID, (1-claimsraf.shared*0.5) AS pointsprob ".
           "FROM claimsraf WHERE enemystatus = 2 AND claimsraf.accepted = 1) AS probtable ON probtable.reportID = reports.id ".
           "WHERE reports.accepted = 1 AND authorID = $characterID AND reports.missionID <= $missionID";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $probable = $row["prob"];
    
    $sql = "SELECT COUNT(dmgtable.pointsdmg) AS dmg FROM reports ".
           "LEFT JOIN (SELECT claimsraf.reportID, 1 AS pointsdmg ".
           "FROM claimsraf WHERE enemystatus = 3 AND claimsraf.accepted = 1) AS dmgtable ON dmgtable.reportID = reports.id ".
           "WHERE reports.accepted = 1 AND authorID = $characterID AND reports.missionID <= $missionID";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $damaged = $row["dmg"];
    
    $succesfulReturnsNonBOB = $pilotOKNonBOB + $pilotWoundedNonBOB;
    $succesfulReturnsBOB = $pilotOKBOB + $pilotWoundedBOB;
    $succesfulReturns = $pilotOK + $pilotWounded;
    $points = $destroyed*10 + $probable*5 + $damaged*2;
    if($sorties > 0){
        $rtbRatio = 1 - $aeroLost/$sorties;
    } else {
        $rtbRatio = 0;
    }
    
//    echo ("Mission: ".$missionID." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." <br>");
    
    //Check for RAF Pilot Brevet
    $medalAbr = "AB";
    $criteria = $succesfulReturns > 1;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    //Victory Medal
    $medalAbr = "VM";
    $criteria = $succesfulReturnsNonBOB > 3;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //Victory Medal with Battle of Britain Clasp
    $medalAbr = "VM";
    $criteria = $succesfulReturnsBOB > 3;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //Victory Medal with Battle of Britain Clasp
    $medalAbr = "VM";
    $criteria = $succesfulReturns > 3;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Mentioned in Dispatches
    $medalAbr = "MiD";
    $criteria = $destroyed > 4;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Distinguished Flying Medal
    $medalAbr = "DFM";
    $criteria = ($destroyed > 7 | $points > 119) & $rankValue < 8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Distinguished Flying Medal with Bar
    $medalAbr = "DFM*";
    $criteriaA = $destroyed > 15;
    $criteriaB = $points > 199;
    $criteria = ($criteriaA | $criteriaB) & $rankValue < 8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Distinguished Flying Cross
    $medalAbr = "DFC";
    $criteria = ($destroyed > 7 | $points > 119) & $rankValue >= 8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Distinguished Flying Cross with Bar
    $medalAbr = "DFC*";
    $criteria = ($destroyed > 15 | $points > 199) & $rankValue >= 8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Distinguished Flying Cross with two Bars
    $medalAbr = "DFC**";
    $criteria = $points > 499 & $rankValue >= 8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Distinguished Service Order
    $medalAbr = "DSO";
    $criteria = $succesfulReturns > 9 & $destroyed > 6 & $rtbRatio >= 0.6 & $rankValue >= 8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Distinguished Service Order with Bar
    $medalAbr = "DSO*";
    $criteria = $succesfulReturns > 19 & $destroyed > 14 & $rtbRatio >= 0.85 & $rankValue >= 8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

}

function checkLWDecorations($characterID, $missionID){
    
    include_once(dirname(__FILE__).'/characterDBFunctions.php');
    
    $dbx = getDBx();
    //Get date of mission
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $missionDate = $row["realDate"];


    //Get all decorations of the character
    $sql = "SELECT awards.abreviation, decorations.id, decorations.awarded FROM awards ".
           "LEFT JOIN decorations ON decorations.awardID = awards.id ".
           "WHERE decorations.characterID = $characterID";
    $result = mysqli_query($dbx, $sql);
    $decorationsArray = array();
    $awardedArray = array();
    while($row = mysqli_fetch_array($result)){
        $decorationsArray[] = $row[0];
        $awardedArray[$row[0]]["id"] = $row[1];
        $awardedArray[$row[0]]["awarded"] = $row[2];
    }
    
    //Get all awards
    $sql = "SELECT id, abreviation FROM awards ".
           "WHERE faction = 'LW'";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_array($result)){
        $awardArray["$row[1]"] = $row[0];
    }
    
    //Get stats for character
    $sql = "SELECT reports.authorID, COUNT(reports.id) AS sorties, SUM(reports.pilotStatus = 1) AS pilotOK, ".
           "SUM(reports.pilotStatus = 2) AS pilotWND, SUM(reports.aeroplaneStatus = 3) AS aeroLST ".
           "FROM reports LEFT JOIN missions ON reports.missionID = missions.id ".
           "WHERE reports.authorID = $characterID AND reports.accepted = 1 AND missions.id <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $sorties = $row["sorties"];
    $aeroLost = $row["aeroLST"];
    $pilotOK = $row["pilotOK"];
    $pilotWounded = $row["pilotWND"];
    
    $sql = "SELECT SUM(claimslw.confirmed=1) AS conf, SUM(claimslw.confirmed=0) AS unconf ". 
           "FROM claimslw LEFT JOIN reports ON claimslw.reportid = reports.id ".
           "WHERE authorID = $characterID AND reports.accepted=1 AND claimslw.accepted = 1 ".
           "AND reports.missionID <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $conf = $row["conf"];
    $unconf = $row["unconf"];
    
    $sql = "SELECT COUNT(DISTINCT reports.id) AS swgv ".
           "FROM reports ".
           "LEFT JOIN claimsground ON reports.id = claimsground.reportID ".
           "WHERE authorID = $characterID AND reports.accepted=1 AND claimsground.accepted = 1 ".
           "AND reports.missionID <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $sortiesWithGroundVictories = $row["swgv"];
    
    $succesfulReturns = $pilotOK + $pilotWounded;
    $points = $conf + 0.5*$unconf;
    if($succesfulReturns > 0){
        $rtbRatio = 1 - $aeroLost/$succesfulReturns;
    } else {
        $rtbRatio = 0;
    }
    
    //Check for Flugzeugführer und Beobachterabzeichen
    $medalAbr = "FBA";
    $criteria = $succesfulReturns > 1;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Check for Verwundetenabzeichen in schwarz
    $medalAbr = "VA II";
    $criteria = $pilotWounded == 1;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Check for Verwundetenabzeichen in silber
    $medalAbr = "VA I";
    $criteria = $pilotWounded > 1;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Mentioned in Wehrmachtsbericht
    $medalAbr = "WB";
    $criteria = $succesfulReturns > 9 & $rtbRatio >= 0.8;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Eisernes Kreuz 2. Klasse
    $medalAbr = "EK II";
    $criteriaA = $conf > 2;
    $criteriaB = $pilotWounded > 1;
    $criteriaC = $succesfulReturns > 9 & $rtbRatio >= 0.8;
    $criteria = ($criteriaA | $criteriaB | $criteriaC);
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Eisernes Kreuz 1. Klasse
    $medalAbr = "EK I";
    $criteria = $conf > 5;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Ritterkreuz des Eisernen Kreuzes
    $medalAbr = "RK II";
    $criteria = $points > 14;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Ritterkreuz des Eisernen Kreuzes mit Eichenlaub
    $medalAbr = "RK I";
    $criteria = $points > 19;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Ehrenpokal der Luftwaffe
    $medalAbr = "EP";
    $criteria = $points > 24;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

    
    //Check for Flugzeugführer und Beobachterabzeichen in Gold mit Brillianten
    $medalAbr = "FBAgd";
    $criteria = $succesfulReturns > 29 & $conf > 19;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //Frontflugspange für Kampf- und Sturzkampfflieger in Bronze
    $medalAbr = "FFS-KSKb";
    $criteria = $sortiesWithGroundVictories > 2;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
                        
    //Frontflugspange für Kampf- und Sturzkampfflieger in Silber
    $medalAbr = "FFS-KSKs";
    $criteria = $sortiesWithGroundVictories > 5;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //Frontflugspange für Kampf- und Sturzkampfflieger in Gold
    $medalAbr = "FFS-KSKg";
    $criteria = $sortiesWithGroundVictories > 11;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);

}

function checkVVSDecorations($characterID, $missionID){
    
    include_once(dirname(__FILE__).'/characterDBFunctions.php');
    
    $dbx = getDBx();
    //Get date of mission
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $missionDate = $row["realDate"];


    //Get all decorations of the character
    $sql = "SELECT awards.abreviation, decorations.id, decorations.awarded FROM awards ".
           "LEFT JOIN decorations ON decorations.awardID = awards.id ".
           "WHERE decorations.characterID = $characterID";
    $result = mysqli_query($dbx, $sql);
    $decorationsArray = array();
    $awardedArray = array();
    while($row = mysqli_fetch_array($result)){
        $decorationsArray[] = $row[0];
        $awardedArray[$row[0]]["id"] = $row[1];
        $awardedArray[$row[0]]["awarded"] = $row[2];
    }
    
    //Get all awards
    $sql = "SELECT id, abreviation FROM awards ".
           "WHERE faction = 'VVS'";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_array($result)){
        $awardArray["$row[1]"] = $row[0];
    }
    
    //Get stats for character
    $sql = "SELECT reports.authorID, COUNT(reports.id) AS sorties, SUM(reports.pilotStatus = 1) AS pilotOK, ".
           "SUM(reports.pilotStatus = 2) AS pilotWND, SUM(reports.aeroplaneStatus = 3) AS aeroLST ".
           "FROM reports LEFT JOIN missions ON reports.missionID = missions.id ".
           "WHERE reports.authorID = $characterID AND reports.accepted = 1 AND missions.id <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $sorties = $row["sorties"];
    $aeroLost = $row["aeroLST"];
    $pilotOK = $row["pilotOK"];
    $pilotWounded = $row["pilotWND"];
    
    $sql = "SELECT SUM(claimsvvs.confirmed=1) AS conf, SUM(claimsvvs.confirmed=0) AS unconf ". 
           "FROM claimsvvs LEFT JOIN reports ON claimsvvs.reportid = reports.id ".
           "WHERE authorID = $characterID AND reports.accepted=1 AND claimsvvs.accepted = 1 ".
           "AND claimsvvs.groupClaim = 0 AND reports.missionID <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $conf = $row["conf"];
    $unconf = $row["unconf"];
    
    $sql = "SELECT SUM(vvs_group_claims.confirmed=1) AS conf, SUM(vvs_group_claims.confirmed=0) AS unconf ".
           "FROM vvs_group_claims ".
           "LEFT JOIN reports ON (vvs_group_claims.squadronID, vvs_group_claims.missionID) = (reports.squadronID, reports.missionID) ".
           "WHERE authorID = $characterID AND reports.accepted=1 AND vvs_group_claims.accepted = 1 ".
           "AND reports.missionID <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $gconf = $row["conf"];
    $gunconf = $row["unconf"];
    
    $sql = "SELECT MAX(x.conf) AS maxConf, MAX(x.unconf) AS maxUnconf ".
           "FROM ( ".
           "SELECT SUM(claimsvvs.confirmed=1) AS conf, SUM(claimsvvs.confirmed=0) AS unconf ".
           "FROM claimsvvs LEFT JOIN reports ON claimsvvs.reportid = reports.id ".
           "WHERE authorID = $characterID AND reports.accepted=1 AND claimsvvs.accepted = 1 ". 
           "AND claimsvvs.groupClaim = 0 AND reports.missionID <= $missionID ".
           "GROUP BY missionID ) AS x ";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $maxConfPerSortie = $row["maxConf"];
    $maxUnconfPerSortie = $row["maxUnconf"];
    
    $succesfulReturns = $pilotOK + $pilotWounded;

    $sql = "SELECT IFNULL(MAX(criteriaclaims),0) AS criteria FROM merit_stats ".
           "WHERE authorID = $characterID AND missionID <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $maxConfPerSortieAllSurvive = $row["criteria"];
    
    //Aviation Badge
    $medalAbr = "AB_VVS";
    $criteria = $succesfulReturns > 1;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //Order of the Red Banner
    $medalAbr = "OOTRB_VVS";
    $criteria = $conf > 2;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
                        
    //Order of the Red Star
    $medalAbr = "OOTRS_VVS";
    $criteria = $conf > 5;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //Medal "For Courage"
    $medalAbr = "MFC_VVS";
    $criteria = $maxConfPerSortie > 2;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
                        
    //Medal "For Battle Merit"
    $medalAbr = "FBM_VVS";
    $criteria = $maxConfPerSortieAllSurvive > 6;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
                        
    //Order of Lenin
    $medalAbr = "OOL_VVS";
    $criteria = $conf & $gconf > 9 & $sorties > 20;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
                        
    //Aircraft Kill Bonus
    $medalAbr = "AKB_VVS";
    $criteria = $conf > 0;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
                        
    //5 Combat Sortie Bonus
    $medalAbr = "5CSB_VVS";
    $criteria = $succesfulReturns > 4;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //15 Combat Sortie Bonus
    $medalAbr = "15CSB_VVS";
    $criteria = $succesfulReturns > 14;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //25 Combat Sortie Bonus
    $medalAbr = "25CSB_VVS";
    $criteria = $succesfulReturns > 24;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //40 Combat Sortie Bonus
    $medalAbr = "40CSB_VVS";
    $criteria = $succesfulReturns > 39;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
    
    //Medal "For the Defence of Moscow"
    $medalAbr = "MFTDOM_VVS";
    $criteria = $sorties > 14;
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx);
                        
    
}

function addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, $awardArray,
                              $characterID, $missionDate, $dbx){
    
    if(!in_array($medalAbr, $decorationsArray)){
        
        if($criteria){
            $awardID = $awardArray[$medalAbr];
            $sql = "INSERT INTO decorations (characterID, awardID, date, awarded, availableSince) VALUES ".
                   "($characterID, $awardID, '$missionDate', false, '$missionDate')";
            mysqli_query($dbx, $sql); 
        }
    } else if(!$awardedArray[$medalAbr]["awarded"]){

        if(!$criteria){
            $decorationID = $awardedArray[$medalAbr]["id"];
            $sql = "DELETE FROM decorations WHERE id=$decorationID";
            mysqli_query($dbx, $sql);
        }
    }
}
