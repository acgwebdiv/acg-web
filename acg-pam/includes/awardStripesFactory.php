<?php
//header("Content-type: image/png");
include_once(dirname(__FILE__).'/db_connect.php');
$dbx = getDBx();
if(filter_has_var(INPUT_GET, "c_id_big")){
    
    $characterID = filter_input(INPUT_GET, "c_id_big");

    $sql = "SELECT awards.abreviation, awards.image, decorations.date, awards.faction FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "WHERE decorations.characterID = $characterID AND decorations.awarded = 1 ".
           "ORDER BY date ASC" ;
    $querry = mysqli_query($dbx, $sql);
    
//    $oImage = imagecreatefrompng("../imgsource/MedalDFC.png");
    
    $imageArray = array();
    $oImageWidth = 0;
    $oImageHeight = 0;
    while($row = mysqli_fetch_assoc($querry)){
       
        $imageStr = $row["image"];
        $medalAbr = $row["abreviation"];
        if($medalAbr == "AB" | $medalAbr == "FBA" | $medalAbr == "FBAgd" | $medalAbr == "AB_VVS"){
            continue;
        }
        $image = imagecreatefrompng("../imgsource/medals-big/".$imageStr);
        if($medalAbr == "DFM*"){
            $medalAbr = "DFM";
        } else if($medalAbr == "DFC*"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DFC**"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DSO*"){
            $medalAbr = "DSO";
        } else if($medalAbr == "RK I"){
            $medalAbr = "RK II";
        } else {
            $oImageWidth += imagesx($image);   
        }
        $oImageHeight = max(array($oImageHeight, imagesy($image)));
        $imageArray[$medalAbr] = $image;

    }
    
    if(count($imageArray)>0){
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor($oImageWidth, $oImageHeight);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);

        $dst_x = 0;
        foreach($imageArray as $image) {

            $dst_y = ($oImageHeight - imagesy($image))/2;
            imagecopy($oImage, $image, $dst_x, $dst_y, 0, 0, imagesx($image), imagesy($image));
            $dst_x += imagesx($image);
        }
 
    } else {
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor(1, 1);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);
    }
    
    imagepng($oImage);
    imagedestroy($oImage);
}

if(filter_has_var(INPUT_GET, "c_id_small")){
    
    $characterID = filter_input(INPUT_GET, "c_id_small");
    
    $sql = "SELECT awards.abreviation, awards.image, decorations.date FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "WHERE decorations.characterID = $characterID AND decorations.awarded = 1 ".
           "ORDER BY date ASC" ;
    $querry = mysqli_query($dbx, $sql);
    
//    $oImage = imagecreatefrompng("../imgsource/MedalDFC.png");
    
    $imageArray = array();
    $oImageWidth = 0;
    $oImageHeight = 0;
    while($row = mysqli_fetch_assoc($querry)){
       
        $imageStr = $row["image"];
        $medalAbr = $row["abreviation"];
        if($medalAbr == "AB" | $medalAbr == "FBA" | $medalAbr == "FBAgd" | $medalAbr == "AB_VVS"){
            continue;
        }
        $image = imagecreatefrompng("../imgsource/medals-small/".$imageStr);
        if($medalAbr == "DFM*"){
            $medalAbr = "DFM";
        } else if($medalAbr == "DFC*"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DFC**"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DSO*"){
            $medalAbr = "DSO";
        } else if($medalAbr == "RK I"){
            $medalAbr = "RK II";
        } else {
            $oImageWidth += imagesx($image);   
        }
        $oImageHeight = max(array($oImageHeight, imagesy($image)));
        $imageArray[$medalAbr] = $image;

    }
    
    if(count($imageArray)>0){
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor($oImageWidth, $oImageHeight);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);

        $dst_x = 0;
        foreach($imageArray as $image) {

            $dst_y = ($oImageHeight - imagesy($image))/2;
            imagecopy($oImage, $image, $dst_x, $dst_y, 0, 0, imagesx($image), imagesy($image));
            $dst_x += imagesx($image);
        }
 
    } else {
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor(90, 90);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);
        
    }
    
    imagepng($oImage);
    imagedestroy($oImage);
}

if(filter_has_var(INPUT_GET, "m_id_big")){
    
    $memberID = filter_input(INPUT_GET, "m_id_big");
    $faction = filter_input(INPUT_GET, "f");
    
    $sql = "SELECT awards.abreviation, awards.image, decorations.date FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "LEFT JOIN careercharacters ON decorations.characterID = careercharacters.id ". 
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "WHERE acgmembers.id = $memberID AND decorations.awarded = 1 ".
           "AND awards.faction = $faction ".
           "ORDER BY date ASC" ;
    $querry = mysqli_query($dbx, $sql);
    
//    $oImage = imagecreatefrompng("../imgsource/MedalDFC.png");
    
    $imageArray = array();
    $oImageWidth = 0;
    $oImageHeight = 0;
    while($row = mysqli_fetch_assoc($querry)){
       
        $imageStr = $row["image"];
        $medalAbr = $row["abreviation"];
        if($medalAbr == "AB" | $medalAbr == "FBA" | $medalAbr == "FBAgd" | $medalAbr == "AB_VVS"){
            continue;
        }
        if(array_key_exists($medalAbr, $imageArray)){
            continue;
        }
        $image = imagecreatefrompng("../imgsource/medals-big/".$imageStr);
        if($medalAbr == "DFM*"){
            $medalAbr = "DFM";
        } else if($medalAbr == "DFC*"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DFC**"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DSO*"){
            $medalAbr = "DSO";
        } else if($medalAbr == "RK I"){
            $medalAbr = "RK II";
        } else {
            $oImageWidth += imagesx($image);   
        }
        $oImageHeight = max(array($oImageHeight, imagesy($image)));
        $imageArray[$medalAbr] = $image;

    }
    
    if(count($imageArray)>0){
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor($oImageWidth, $oImageHeight);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);

        $dst_x = 0;
        foreach($imageArray as $image) {

            $dst_y = ($oImageHeight - imagesy($image))/2;
            imagecopy($oImage, $image, $dst_x, $dst_y, 0, 0, imagesx($image), imagesy($image));
            $dst_x += imagesx($image);
        }
 
    } else {
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor(1, 1);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);
        
    }
    
    imagepng($oImage);
    imagedestroy($oImage);
}

if(filter_has_var(INPUT_GET, "m_id_small")){
    
    $memberID = filter_input(INPUT_GET, "m_id_small");
    
    $sql = "SELECT awards.abreviation, awards.image, decorations.date FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "LEFT JOIN careercharacters ON decorations.characterID = careercharacters.id ". 
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "WHERE acgmembers.id = $memberID AND decorations.awarded = 1 ".
           "ORDER BY date ASC" ;
    $querry = mysqli_query($dbx, $sql);
    
//    $oImage = imagecreatefrompng("../imgsource/MedalDFC.png");
    
    $imageArray = array();
    $oImageWidth = 0;
    $oImageHeight = 0;
    while($row = mysqli_fetch_assoc($querry)){
       
        $imageStr = $row["image"];
        $medalAbr = $row["abreviation"];
        if($medalAbr == "AB" | $medalAbr == "FBA" | $medalAbr == "FBAgd"  | $medalAbr == "AB_VVS"){
            continue;
        }
        if(array_key_exists($medalAbr, $imageArray)){
            continue;
        }
        $image = imagecreatefrompng("../imgsource/medals-small/".$imageStr);
        if($medalAbr == "DFM*"){
            $medalAbr = "DFM";
        } else if($medalAbr == "DFC*"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DFC**"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DSO*"){
            $medalAbr = "DSO";
        } else if($medalAbr == "RK I"){
            $medalAbr = "RK II";
        } else {
            $oImageWidth += imagesx($image);   
        }
        $oImageHeight = max(array($oImageHeight, imagesy($image)));
        $imageArray[$medalAbr] = $image;

    }
    
    if(count($imageArray)>0){
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor($oImageWidth, $oImageHeight);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);

        $dst_x = 0;
        foreach($imageArray as $image) {

            $dst_y = ($oImageHeight - imagesy($image))/2;
            imagecopy($oImage, $image, $dst_x, $dst_y, 0, 0, imagesx($image), imagesy($image));
            $dst_x += imagesx($image);
        }
 
    } else {
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor(90, 90);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);
        
    }
    
    imagepng($oImage);
    imagedestroy($oImage);
}

if(filter_has_var(INPUT_GET, "m_id_only_survived_small")){
    
    $memberID = filter_input(INPUT_GET, "m_id_only_survived_small");
    
    $sql = "SELECT awards.abreviation, awards.image, decorations.date FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "LEFT JOIN careercharacters ON decorations.characterID = careercharacters.id ". 
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "WHERE acgmembers.id = $memberID AND decorations.awarded = 1 ".
           "AND careercharacters.characterStatus <> 3 ".
           "ORDER BY date ASC" ;
    $querry = mysqli_query($dbx, $sql);
    
//    $oImage = imagecreatefrompng("../imgsource/MedalDFC.png");
    
    $imageArray = array();
    $oImageWidth = 0;
    $oImageHeight = 0;
    while($row = mysqli_fetch_assoc($querry)){
       
        $imageStr = $row["image"];
        $medalAbr = $row["abreviation"];
        if($medalAbr == "AB" | $medalAbr == "FBA" | $medalAbr == "FBAgd"  | $medalAbr == "AB_VVS"){
            continue;
        }
        if(array_key_exists($medalAbr, $imageArray)){
            continue;
        }
        $image = imagecreatefrompng("../imgsource/medals-small/".$imageStr);
        if($medalAbr == "DFM*"){
            $medalAbr = "DFM";
        } else if($medalAbr == "DFC*"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DFC**"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DSO*"){
            $medalAbr = "DSO";
        } else if($medalAbr == "RK I"){
            $medalAbr = "RK II";
        } else {
            $oImageWidth += imagesx($image);   
        }
        $oImageHeight = max(array($oImageHeight, imagesy($image)));
        $imageArray[$medalAbr] = $image;

    }
    
    if(count($imageArray)>0){
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor($oImageWidth, $oImageHeight);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);

        $dst_x = 0;
        foreach($imageArray as $image) {

            $dst_y = ($oImageHeight - imagesy($image))/2;
            imagecopy($oImage, $image, $dst_x, $dst_y, 0, 0, imagesx($image), imagesy($image));
            $dst_x += imagesx($image);
        }
 
    } else {
        // Create transparent image to draw on.
        $oImage = imagecreatetruecolor(90, 90);
        imagesavealpha($oImage, true);
        $trans_colour = imagecolorallocatealpha($oImage, 0, 0, 0, 127);
        imagefill($oImage, 0, 0, $trans_colour);
        
    }
    
    imagepng($oImage);
    imagedestroy($oImage);
}