<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/functions.php');
include_once(dirname(__FILE__).'/characterDBFunctions.php');

$dbx = getDBx();

if(filter_has_var(INPUT_POST, "rollDice")) {
    
    $status = filter_input(INPUT_POST, "rollDice");
    $mission_id = filter_input(INPUT_POST, "missionID");
    $member_id = filter_input(INPUT_POST, "memberID");
    $faction = getFaction($member_id, $dbx);
    $die_roll = rand(0,100);

    if($faction==="LW"){

        // Luftwaffe
        switch ($status) {
            // Return From Enemy Land Territory Chance: 2%
            // Return From Neutral Land Territory Chance: 85%
            // Pow Escape Chance: 1%
            case 1: //Crashlanded in England
                if($die_roll <= 2 ){
                    $returnText = "You were able to escape enemy territory without being captured.";
                } else if($die_roll <= 3) {
                    $returnText = "You were captured by the enemy, but escaped the POW camp.";
                } else {
                    $returnText = "You were captured by the enemy.";
                }
                break;
            case 2: //Ditched in the channel within 10km from English coast (Enemy)
            // Friendly ASR Chance Enemy Territory: 15%
            // Enemy ASR Chance Enemy Territory: 10%
            // Pow Escape Chance: 1%
                if($die_roll <= 15 ){
                    $returnText = "You were rescued by friendly ASR.";
                } else if($die_roll <= 25) {
                    $returnText = "You were rescued by enemy ASR and captured.";
                } else if($die_roll <= 26) {
                    $returnText = "You were rescued by enemy ASR and captured, but escaped the POW camp.";
                } else {
                    $returnText = "You drowned.";
                }
                break;
            case 3: //Ditched in the channel further than 10km from coast (Neutral)
            // Friendly ASR Chance Neutral Territory: 50%
            // Enemy ASR Chance Neutral Territory: 5%
            // Pow Escape Chance: 1%
                if($die_roll <= 50 ){
                    $returnText = "You were rescued by friendly ASR.";
                } else if($die_roll <= 55) {
                    $returnText = "You were rescued by enemy ASR and captured.";
                } else if($die_roll <= 56) {
                    $returnText = "You were rescued by enemy ASR and captured, but escaped the POW camp.";
                } else {
                    $returnText = "You drowned.";
                }
                break;
            case 4: //Ditched in the channel within 10km from French coast (Friendly)
            // Friendly ASR Chance Friendly Territory: 90%
            // Enemy ASR Chance Friendly Territory: 0%
            // Pow Escape Chance: 1%
                if($die_roll <= 90 ){
                    $returnText = "You were rescued by friendly ASR.";
                } else {
                    $returnText = "You drowned.";
                }
                break;
        }
            

    }
    else if($faction==="RAF"){

        // RAF
        switch ($status) {
            // Return From Enemy Land Territory Chance: 7%
            // Return From Neutral Land Territory Chance: 85%
            // Pow Escape Chance: 2%
            case 1: //Crashlanded in France
                if($die_roll <= 7 ){
                    $returnText = "You were able to escape enemy territory without being captured.";
                } else if($die_roll <= 9) {
                    $returnText = "You were captured by the enemy, but escaped the POW camp.";
                } else {
                    $returnText = "You were captured by the enemy.";
                }
                break;
            case 2: //Ditched in the channel within 10km from French coast (Enemy)
            // Friendly ASR Chance Enemy Territory: 8%
            // Enemy ASR Chance Enemy Territory: 10%
            // Pow Escape Chance: 2%
                if($die_roll <= 8 ){
                    $returnText = "You were rescued by friendly ASR.";
                } else if($die_roll <= 18) {
                    $returnText = "You were rescued by enemy ASR and captured.";
                } else if($die_roll <= 20) {
                    $returnText = "You were rescued by enemy ASR and captured, but escaped the POW camp.";
                } else {
                    $returnText = "You drowned.";
                }
                break;
            case 3: //Ditched in the channel further than 10km from coast (Neutral)
            // Friendly ASR Chance Neutral Territory: 50%
            // Enemy ASR Chance Neutral Territory: 5%
            // Pow Escape Chance: 2%
                if($die_roll <= 50 ){
                    $returnText = "You were rescued by friendly ASR.";
                } else if($die_roll <= 55) {
                    $returnText = "You were rescued by enemy ASR and captured.";
                } else if($die_roll <= 57) {
                    $returnText = "You were rescued by enemy ASR and captured, but escaped the POW camp.";
                } else {
                    $returnText = "You drowned.";
                }
                break;
            case 4: //Ditched in the channel within 10km from English coast (Friendly)
            // Friendly ASR Chance Friendly Territory: 90%
            // Enemy ASR Chance Friendly Territory: 0%
            // Pow Escape Chance: 2%
                if($die_roll <= 90 ){
                    $returnText = "You were rescued by friendly ASR.";
                } else {
                    $returnText = "You drowned.";
                }
                break;
        
        }
       
    }

    $sql = "INSERT INTO fortuneTellerLog(memberID, missionID, fate, location, faction) VALUES ".
           "($member_id, $mission_id, '$returnText', $status, '$faction')";
    if(mysqli_query($dbx, $sql)) {
        echo $returnText;
        exit();
    } else {
        echo "Script error. Please try again.";
        exit();
    }
}