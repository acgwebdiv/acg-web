<?php
include_once(dirname(__FILE__).'/db_connect.php');
session_start();
$dbx = getDBx();
if(filter_has_var(INPUT_POST, "addSquadron")) {
    $n = filter_input(INPUT_POST, "addSquadron");
    $c = filter_input(INPUT_POST, "c");
    $f = filter_input(INPUT_POST, "f");
    $b = filter_input(INPUT_POST, "b");
    if($b == 0){
        $b = 'NULL';
    }
    $sql = "SELECT id FROM squadrons WHERE name='$n' LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $sname_check = mysqli_num_rows($query);
    if($sname_check < 1) {
        $sql = "INSERT INTO squadrons (name, code, faction, acg_unit) VALUES ('$n', '$c', '$f', '$b')";
        if(mysqli_query($dbx, $sql)) {  
            if($f === "RAF"){
                echo "Squadron created.";
            } else if ($f === "LW") {
                echo "Staffel created."; 
            } else {
                echo "Eskadrilya created";
            }
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {
        if($f === "RAF"){
            echo "Squadron name is already existing.";
        } else if($f === "LW") {
            echo "Staffel name is already existing."; 
        } else {
            echo "Eskadrilya name is already existing.";
        }
        exit();
    }
}

if(filter_has_var(INPUT_POST, "changeSquadronName")) {
    $edit_id = $_SESSION["edit_id"];
    $n = filter_input(INPUT_POST, "changeSquadronName");
    $f = filter_input(INPUT_POST, "f");
    $sql = "SELECT id FROM squadrons WHERE name='$n' LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $sname_check = mysqli_num_rows($query);
    if($sname_check < 1) {
        if(changeSquadron("name", $n, $edit_id)) {
            if($f === "RAF"){
                echo "Squadron name changed.";
            } else if($f === "LW") {
                echo "Staffel name changed."; 
            } else {
                echo "Eskadrilya name changed."; 
            }
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {

        $result = mysqli_fetch_array($query);
        $check_ID = $result[0];
        if($check_ID !== $edit_id){
            if($f === "RAF"){
                echo "Squadron name is already existing.";
            } else if($f === "LW") {
                echo "Staffel name is already existing."; 
            } else {
                echo "Eskadrilya name is already existing.";
            }
                exit();
            } else {
           echo "";
           exit();
        }    
    }
}

if(filter_has_var(INPUT_POST, "changeSquadronCode")) {
    $edit_id = $_SESSION["edit_id"];
    $c = filter_input(INPUT_POST, "changeSquadronCode");
    $f = filter_input(INPUT_POST, "f");
    $sql = "SELECT id FROM squadrons WHERE code='$c' LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $sname_check = mysqli_num_rows($query);
    if($sname_check < 1) {
        if(changeSquadron("code", $c, $edit_id)) {
            if($f === "RAF"){
                echo "Squadron code changed.";
            } else if($f === "LW") {
                echo "Staffel code changed."; 
            } else {
                echo "Eskadrilya code changed.";
            }
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {

        $result = mysqli_fetch_array($query);
        $check_ID = $result[0];
        if($check_ID !== $edit_id){
            if($f === "RAF"){
                echo "Squadron code is already existing.";
            } else if($f === "LW"){
                echo "Staffel code is already existing."; 
            } else {
                echo "Eskadrilya code is already existing.";
            }
            exit();
        } else {
           echo "";
           exit();
        }    
    }
}

if(filter_has_var(INPUT_POST, "changeSquadronFaction")) {
    $edit_id = $_SESSION["edit_id"];
    $f = filter_input(INPUT_POST, "changeSquadronFaction");
    if(changeSquadron("faction", $f, $edit_id)) {
        if($f === "RAF"){
            echo "Squadron faction changed.";
        } else if($f === "LW") {
            echo "Staffel faction changed."; 
        } else {
            echo "Eskadrilya faction changed.";
        }
        exit();
    } else
    {
        echo "Error during database manipulation.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "changeSquadronACGUnit")) {
    $edit_id = $_SESSION["edit_id"];
    $b = filter_input(INPUT_POST, "changeSquadronACGUnit");
    // if($b == 0){
    //     $b = NULL;
    // }
    if(changeSquadron("acg_unit", $b, $edit_id)) {
        if($f === "RAF"){
            echo "Squadron acg_unit changed.";
        } else if($f === "LW") {
            echo "Staffel acg_unit changed."; 
        } else {
            echo "Eskadrilya acg_unit changed.";
        }
        exit();
    } else
    {
        echo "Error during database manipulation.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "deleteSquadron")){
    $edit_id = $_SESSION["edit_id"];
    $sql = "DELETE FROM squadrons WHERE id='$edit_id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}

function changeSquadron($field, $value, $edit_id){

    global $dbx;
    $sql = "UPDATE squadrons SET $field='$value' WHERE id='$edit_id'";
    return mysqli_query($dbx, $sql);
}

