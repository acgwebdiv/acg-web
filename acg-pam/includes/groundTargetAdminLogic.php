<?php
include_once(dirname(__FILE__).'/db_connect.php');
session_start();
$dbx = getDBx();
if(filter_has_var(INPUT_POST, "addGroundTarget")) {
    $n = filter_input(INPUT_POST, "addGroundTarget");
    $sql = "SELECT id FROM groundtargets WHERE name='$n'";
    $query = mysqli_query($dbx, $sql);
    $gtname_check = mysqli_num_rows($query);
    if($gtname_check < 1) {
        $sql = "INSERT INTO groundtargets (name) VALUES ('$n')";
        if(mysqli_query($dbx, $sql)) {  
            echo "Ground target added.";
            exit();
        } else
        {
            echo "Error during database manipulation. ".$sql;
            exit();
        }
    } else {
        echo "Ground target name is already existing.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "editGroundTarget")) {
    $edit_id = $_SESSION["edit_id"];
    $n = filter_input(INPUT_POST, "editGroundTarget");
    $sql = "SELECT id FROM groundtargets WHERE name='$n'";
    $query = mysqli_query($dbx, $sql);
    $aname_check = mysqli_num_rows($query);
    if($aname_check < 1) {
        $sql = "UPDATE groundtargets SET name='$n' WHERE id=$edit_id";
        if(mysqli_query($dbx, $sql)) {  
            echo "Changes saved.";
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {
        echo "Ground target name is already existing.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "deleteGroundTarget")){
    $edit_id = $_SESSION["edit_id"];
    $sql = "DELETE FROM groundtargets WHERE id='$edit_id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}