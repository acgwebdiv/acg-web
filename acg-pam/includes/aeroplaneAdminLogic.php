<?php
include_once(dirname(__FILE__).'/db_connect.php');
session_start();
$dbx = getDBx();
if(filter_has_var(INPUT_POST, "addAeroplane")) {
    $n = filter_input(INPUT_POST, "addAeroplane");
    $f = filter_input(INPUT_POST, "f");
    $t = filter_input(INPUT_POST, "t");
    $sql = "SELECT id FROM aeroplanes WHERE name='$n' AND type=$t";
    $query = mysqli_query($dbx, $sql);
    $aname_check = mysqli_num_rows($query);
    if($aname_check < 1) {
        $sql = "INSERT INTO aeroplanes (name, faction, type) VALUES ('$n', '$f', $t)";
        if(mysqli_query($dbx, $sql)) {  
            echo "Aeroplane added.";
            exit();
        } else
        {
            echo "Error during database manipulation. ".$sql;
            exit();
        }
    } else {
        echo "Aeroplane name is already existing.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "editAeroplane")) {
    $edit_id = $_SESSION["edit_id"];
    $n = filter_input(INPUT_POST, "editAeroplane");
    $f = filter_input(INPUT_POST, "f");
    $t = filter_input(INPUT_POST, "t");
    $sql = "SELECT id FROM aeroplanes WHERE name LIKE BINARY '$n' AND type=$t";
    $query = mysqli_query($dbx, $sql);
    $aname_check = mysqli_num_rows($query);
    if($aname_check < 1) {
        $sql = "UPDATE aeroplanes SET name='$n', faction='$f', type=$t  WHERE id=$edit_id";
        if(mysqli_query($dbx, $sql)) {  
            echo "Changes saved.";
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {
        echo "Aeroplane name is already existing.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "deleteSquadron")){
    $edit_id = $_SESSION["edit_id"];
    $sql = "DELETE FROM aeroplanes WHERE id='$edit_id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}