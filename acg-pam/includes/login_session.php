<?php
include_once(dirname(__FILE__).'/db_connect.php');
// function checkLogin() {
// /* ***************************************************************************** 
//     This is the standard developing login routine. Type in the name and press submit.
//     If the user exists you will change to his account.
//     ONLY FOR DEVELOPING PURPOSE!!!
//   *****************************************************************************
// */    
// //    if(!isset($_SESSION['username'])) {
// //        $_SESSION['username'] = 'Visitor';
// //        $_SESSION['userID'] = 0;
// //        $_SESSION['callsign'] = 'Visitor';
// //        $_SESSION['admin'] = FALSE;
// //        $_SESSION['superUser'] = FALSE;
// //    }
    
//     if(isset($_SESSION['superUser'])) {
//         $superUser = $_SESSION['superUser'];    
//     } else {
//         $superUser = FALSE;
//     }
// //    $newUserInfo = FALSE;
    
//     if(filter_has_var(INPUT_POST, "username")) {
        
//         $newUser = filter_input(INPUT_POST, "username");
//         if($newUser === "SidneyCamm" || $newUser === "WillyMesserschmitt"){
//             $superUser = !$superUser;
//             $_SESSION['superUser'] = $superUser;
//         } elseif($superUser) {
            
//             $newUserInfo = getUserInfo($newUser);
//             if($newUserInfo) {
//                 // If user exists, change session values;
//                 $_SESSION['username'] = $newUserInfo['username'];
//                 $_SESSION['userID'] = $newUserInfo['userid'];
//                 $_SESSION['callsign'] = $newUserInfo['callsign'];
//                 $_SESSION['admin'] = $newUserInfo['admin'];
//                 $_SESSION['mapViewer'] = $newUserInfo['mapViewer'];
//                 $_SESSION['missionBuilder'] = $newUserInfo['missionBuilder'];
//             }
//         }
//     }
    
//     if(!isset($_SESSION['username'])||!$superUser) {
//         $_SESSION['username'] = 'Visitor';
//         $_SESSION['userID'] = 0;
//         $_SESSION['callsign'] = 'Visitor';
//         $_SESSION['admin'] = FALSE;
//         $_SESSION['mapViewer'] = FALSE;
//         $_SESSION['missionBuilder'] = FALSE;
//         $_SESSION['superUser'] = FALSE;
//     }
    
// //    echo("username: ".$_SESSION['username']."/");
// //    echo("userID: ".$_SESSION['userID']."/");
// //    echo("callsign: ".$_SESSION['callsign']."/");
// //    echo("admin: ".$_SESSION['admin']."/");
// //    echo("superUser: ".$_SESSION['superUser']);
    
    
// }

function checkLogin($user) {
/* ***************************************************************************** 
    Login via phpBB forum. Code from:
    http://www.3cc.org/blog/2010/03/integrating-your-existing-site-into-phpbb3/
    I hope this will work but have no chance to test it on my system.  
  *****************************************************************************
 */
    
    if($user->data['username_clean'] == anonymous){
        $_SESSION['username'] = 'Visitor';
        $_SESSION['userID'] = 0;
        $_SESSION['callsign'] = 'Visitor';
        $_SESSION['admin'] = FALSE;
    } else {
        
        $newUserInfo = getUserInfo($user->data['username_clean']);
        if($newUserInfo){
            $_SESSION['username'] = $newUserInfo['username'];
            $_SESSION['userID'] = $newUserInfo['userid'];
            $_SESSION['callsign'] = $newUserInfo['callsign'];
            $_SESSION['admin'] = $newUserInfo['admin'];
            $_SESSION['mapViewer'] = $newUserInfo['mapViewer'];
            $_SESSION['missionBuilder'] = $newUserInfo['missionBuilder'];
        } else {
          $_SESSION['username'] = 'Visitor';
            $_SESSION['userID'] = 0;
            $_SESSION['callsign'] = 'Visitor';
            $_SESSION['admin'] = FALSE;
            $_SESSION['missionBuilder'] = FALSE;
            $_SESSION['superUser'] = FALSE;
        }
    }
}

function getUserInfo($username) {
    
    include_once 'db_connect.php';
    $mysqli = getDBx();
    $querry = "SELECT id, username, callsign, admin, mapViewer, missionBuilder FROM acgmembers WHERE username = ?";
    /* Prepared statement, stage 1: prepare */
    if (!($stmt = $mysqli->prepare($querry))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
        return FALSE;
    }
    /* Prepared statement, stage 2: bind and execute */
    if (!$stmt->bind_param("s", $username)) {
//        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        return FALSE;
    }
    if (!$stmt->execute()) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        return FALSE;
    }
      
    $username = NULL;
    $userId = NULL;
    $callsign = NULL;
    $admin = NULL;
    $mapViewer = NULL;
    $missionBuilder = NULL;
    if (!$stmt->bind_result($userId, $username, $callsign, $admin, $mapViewer, $missionBuilder)) {
//        echo "Binding output parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        return FALSE;
    }
        
    if (!$stmt->fetch()) {
//        echo "Fetching result failed: (" . $stmt->errno . ") " . $stmt->error;
        return FALSE;
    }
    return array('userid'=>$userId, 'username'=>$username, 'callsign'=> $callsign,
                 'admin'=>$admin, 'mapViewer'=>$mapViewer, 'missionBuilder'=>$missionBuilder);
}