<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/functions.php');
//include_once(dirname(__FILE__).'/logger.php');
//DEACTIVATE FOR LOCAL DEVELOPMENT
// include_once(dirname(__FILE__).'/forumInteract.php');
$dbx = getDBx();

if(filter_has_var(INPUT_POST, "getFaction")) {
    
    $member_id = filter_input(INPUT_POST, "getFaction");
    $mi_id = filter_input(INPUT_POST, "mi_id");
    $sql = "SELECT reports.type FROM reports LEFT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "WHERE personifiedby = $member_id AND reports.missionid = $mi_id";
    $m_query = mysqli_query($dbx, $sql);
    $report_check = mysqli_num_rows($m_query);
    if($report_check > 0){
        $m_result = mysqli_fetch_assoc($m_query);
        $m_type = $m_result["type"];
        if($m_type === 1){
            echo "RAF";
            exit();
        } else if($m_type === 2){
            echo "LW";
            exit();
        }
    }
    
    $sql = "SELECT squadrons.faction, UNIX_TIMESTAMP(transfers.transferDate) AS tstdate ".
           "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "WHERE memberid = $member_id ORDER BY tstdate DESC LIMIT 1";
    $s_query = mysqli_query($dbx, $sql);
    $s_result = mysqli_fetch_assoc($s_query);
    echo $s_result["faction"];
    exit();
}


if(filter_has_var(INPUT_POST, "getSCode")){
    
    $sIndex = filter_input(INPUT_POST, "getSCode");
    $sql = "SELECT code FROM squadrons WHERE id = $sIndex";
    $s_query = mysqli_query($dbx, $sql);
    $s_result = mysqli_fetch_assoc($s_query);
    echo $s_result["code"];
    exit();
}

if(filter_has_var(INPUT_POST, "submitLWReport")) {
         
    $missionID = filter_input(INPUT_POST, "submitLWReport");
    $authorID = filter_input(INPUT_POST, "authorID");
    $sqnID = filter_input(INPUT_POST, "sqnID");
    $swarm = filter_input(INPUT_POST, "swarm");
    $swarmPos = filter_input(INPUT_POST, "swarmPos");
    $aerodrome = filter_input(INPUT_POST, "aerodrome", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $aeroplane = filter_input(INPUT_POST, "aeroplane");
    $markings = filter_input(INPUT_POST, "markings", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $synopsis = filter_input(INPUT_POST, "synopsis", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $pilotStatus = filter_input(INPUT_POST, "pilotStatus");
    $acftStatus = filter_input(INPUT_POST, "acftStatus");
    
    editCharacterStatus($authorID, $missionID, $pilotStatus, $dbx);
    
    // check if new or edited report
    $status = "true";
    $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
    $querry = mysqli_query($dbx, $sql);
    $report_check = mysqli_num_rows($querry);
    if($report_check > 0) {
        
        $report_result = mysqli_fetch_assoc($querry);
        $report_id = $report_result["id"];
        $sql = "UPDATE reports SET squadronid=$sqnID, aerodrome='$aerodrome', ".
               "aeroplane=$aeroplane, markings='$markings', synopsis='$synopsis', ".
               "aeroplanestatus='$acftStatus', pilotstatus='$pilotStatus', ".
               "accepted=false, acceptedby='' ".
               "WHERE id=$report_id";
        if(!mysqli_query($dbx, $sql)) {
            echo "Error during database manipulation (Report edit). ".$sql;;
            exit();
        }
        
        $sql = "SELECT id FROM reportdetailslw WHERE reportid=$report_id";
        $querry = mysqli_query($dbx, $sql);
        $report_details_check = mysqli_num_rows($querry);
        if($report_details_check > 0){
            
            $sql = "UPDATE reportdetailslw SET swarm=$swarm, swarmpos=$swarmPos ".
                   "WHERE reportid=$report_id";
            if(mysqli_query($dbx, $sql)) {  
                echo $status;
                exit();
            } else
            {
                echo "Error during database manipulation (ReportDetails edit). ".$sql;
                exit();
            }
        } else {
            
            $sql = "INSERT INTO reportdetailslw (reportid, swarm, swarmpos) ".
                   "VALUES ($report_id, $swarm, $swarmPos)";
            if(mysqli_query($dbx, $sql)) {  
                echo $status;
                exit();
            } else
            {
                echo "Error during database manipulation (ReportDetails submission). ".$sql;
                exit();
            } 
        } 
        
    }
    else {
        
//        $logStr .= "REPORT with missionid = '$missionID' and authorid = '$authorID' NOT found. SUBMITING.<br>";
        
        $sql = "INSERT INTO reports (type, missionid, authorid, squadronid, aerodrome, ".
               "aeroplane, markings, synopsis, aeroplanestatus, pilotstatus, ".
               "datesubmitted, accepted) VALUES ".
               "(2, $missionID, $authorID, $sqnID, '$aerodrome', '$aeroplane', ".
               "'$markings', '$synopsis', $acftStatus, $pilotStatus, '".date("Y-m-d")."', false)";
        if(mysqli_query($dbx, $sql)) {  
            $report_id = mysqli_insert_id($dbx);
        } else
        {
            echo "Error during database manipulation (Report submission). ".$sql;
            exit();
        }
        $sql = "INSERT INTO reportdetailslw (reportid, swarm, swarmpos) ".
               "VALUES ($report_id, $swarm, $swarmPos)";
        if(mysqli_query($dbx, $sql)) {  
            echo $status;
            exit();
        } else
        {
            echo "Error during database manipulation (ReportDetails submission). ".$sql;
            exit();
        }
    }
} 

if(filter_has_var(INPUT_POST, "submitRAFReport")) {
         
    $missionID = filter_input(INPUT_POST, "submitRAFReport");
    $authorID = filter_input(INPUT_POST, "authorID");
    $sqnID = filter_input(INPUT_POST, "sqnID");
    $flight = filter_input(INPUT_POST, "flight");
    $section = filter_input(INPUT_POST, "section");
    $sectionPos = filter_input(INPUT_POST, "sectionPos");
    $aerodrome = filter_input(INPUT_POST, "aerodrome", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $aeroplane = filter_input(INPUT_POST, "aeroplane");
    $markings = filter_input(INPUT_POST, "markings", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $serialNo = filter_input(INPUT_POST, "serialNo", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $synopsis = filter_input(INPUT_POST, "synopsis", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $pilotStatus = filter_input(INPUT_POST, "pilotStatus");
    $acftStatus = filter_input(INPUT_POST, "acftStatus");
         
    editCharacterStatus($authorID, $missionID, $pilotStatus, $dbx);

    // check if new or edited report
    $status = "true";
    $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
    $querry = mysqli_query($dbx, $sql);
    $report_check = mysqli_num_rows($querry);
    if($report_check > 0) {
        
        $report_result = mysqli_fetch_assoc($querry);
        $report_id = $report_result["id"];
        $sql = "UPDATE reports SET squadronid=$sqnID, aerodrome='$aerodrome', ".
               "aeroplane=$aeroplane, markings='$markings', synopsis='$synopsis', ".
               "aeroplanestatus='$acftStatus', pilotstatus='$pilotStatus', ".
               "accepted=false, acceptedby='' ".
               "WHERE id=$report_id";
        if(!mysqli_query($dbx, $sql)) {  
            echo "Error during database manipulation (Report edit): ".$sql;
            exit();
        }
        
        $sql = "SELECT id FROM reportdetailsraf WHERE reportid=$report_id";
        $querry = mysqli_query($dbx, $sql);
        $report_details_check = mysqli_num_rows($querry);
        if($report_details_check > 0){
            
            $sql = "UPDATE reportdetailsraf SET flight=$flight, section=$section, ".
                   "sectionpos=$sectionPos, serialno='$serialNo' ".
                   "WHERE reportid=$report_id";
            if(mysqli_query($dbx, $sql)) {  
                echo $status;
                exit();
            } else
            {
                echo "Error during database manipulation (ReportDetails edit). ".$sql;
                exit();
            }
        } else {
            
            $sql = "INSERT INTO reportdetailsraf (reportid, flight, section, sectionpos, serialno) ".
                   "VALUES ($report_id, $flight, $section, $sectionPos, '$serialNo')";
            if(mysqli_query($dbx, $sql)) {  
                echo $status;
                exit();
            } else {
                echo "Error during database manipulation (ReportDetails submission). ".$sql;
                exit();
            }
        }
        
    }
    else {
        
        $sql = "INSERT INTO reports (type, missionid, authorid, squadronid, aerodrome, ".
               "aeroplane, markings, synopsis, aeroplaneStatus, pilotstatus, ".
               "datesubmitted, accepted) VALUES ".
               "(1, $missionID, $authorID, $sqnID, '$aerodrome', '$aeroplane', ".
               "'$markings', '$synopsis', $acftStatus, $pilotStatus, '".date("Y-m-d")."', false)";
        if(mysqli_query($dbx, $sql)) {  
            $report_id = mysqli_insert_id($dbx);
        } else
        {
            echo "Error during database manipulation (Report submission). ".$sql;
            exit();
        }
        $sql = "INSERT INTO reportdetailsraf (reportid, flight, section, sectionpos, serialno) ".
               "VALUES ($report_id, $flight, $section, $sectionPos, '$serialNo')";
        if(mysqli_query($dbx, $sql)) {  
            echo $status;
            exit();
        } else
        {
            echo "Error during database manipulation (ReportDetails submission). ".$sql;
            exit();
        }
    }
} 

if(filter_has_var(INPUT_POST, "submitVVSReport")) {
         
    $missionID = filter_input(INPUT_POST, "submitVVSReport");
    $authorID = filter_input(INPUT_POST, "authorID");
    $sqnID = filter_input(INPUT_POST, "sqnID");
    $aerodrome = filter_input(INPUT_POST, "aerodrome", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $aeroplane = filter_input(INPUT_POST, "aeroplane");
    $markings = filter_input(INPUT_POST, "markings", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $synopsis = filter_input(INPUT_POST, "synopsis", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $pilotStatus = filter_input(INPUT_POST, "pilotStatus");
    $acftStatus = filter_input(INPUT_POST, "acftStatus");
    
    editCharacterStatus($authorID, $missionID, $pilotStatus, $dbx);
    
    // check if new or edited report
    $status = "true";
    $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
    $querry = mysqli_query($dbx, $sql);
    $report_check = mysqli_num_rows($querry);
    if($report_check > 0) {
        
        $report_result = mysqli_fetch_assoc($querry);
        $report_id = $report_result["id"];
        $sql = "UPDATE reports SET squadronid=$sqnID, aerodrome='$aerodrome', ".
               "aeroplane=$aeroplane, markings='$markings', synopsis='$synopsis', ".
               "aeroplanestatus='$acftStatus', pilotstatus='$pilotStatus', ".
               "accepted=false, acceptedby='' ".
               "WHERE id=$report_id";
        if(!mysqli_query($dbx, $sql)) {
            echo "Error during database manipulation (Report edit). ".$sql;;
            exit();
        }
        
        // $sql = "SELECT id FROM reportdetailslw WHERE reportid=$report_id";
        // $querry = mysqli_query($dbx, $sql);
        // $report_details_check = mysqli_num_rows($querry);
        // if($report_details_check > 0){
            
        //     $sql = "UPDATE reportdetailslw SET swarm=$swarm, swarmpos=$swarmPos ".
        //           "WHERE reportid=$report_id";
        //     if(mysqli_query($dbx, $sql)) {  
        //         echo $status;
        //         exit();
        //     } else
        //     {
        //         echo "Error during database manipulation (ReportDetails edit). ".$sql;
        //         exit();
        //     }
        // } else {
            
        //     $sql = "INSERT INTO reportdetailslw (reportid, swarm, swarmpos) ".
        //           "VALUES ($report_id, $swarm, $swarmPos)";
        //     if(mysqli_query($dbx, $sql)) {  
                echo $status;
                exit();
        //     } else
        //     {
        //         echo "Error during database manipulation (ReportDetails submission). ".$sql;
        //         exit();
        //     } 
        // } 
        
    }
    else {
        
//        $logStr .= "REPORT with missionid = '$missionID' and authorid = '$authorID' NOT found. SUBMITING.<br>";
        
        $sql = "INSERT INTO reports (type, missionid, authorid, squadronid, aerodrome, ".
               "aeroplane, markings, synopsis, aeroplanestatus, pilotstatus, ".
               "datesubmitted, accepted) VALUES ".
               "(3, $missionID, $authorID, $sqnID, '$aerodrome', '$aeroplane', ".
               "'$markings', '$synopsis', $acftStatus, $pilotStatus, '".date("Y-m-d")."', false)";
        if(mysqli_query($dbx, $sql)) {  
            $report_id = mysqli_insert_id($dbx);
        } else
        {
            echo "Error during database manipulation (Report submission). ".$sql;
            exit();
        }
        // $sql = "INSERT INTO reportdetailslw (reportid, swarm, swarmpos) ".
        //       "VALUES ($report_id, $swarm, $swarmPos)";
        // if(mysqli_query($dbx, $sql)) {  
            echo $status;
            exit();
        // } else
        // {
        //     echo "Error during database manipulation (ReportDetails submission). ".$sql;
        //     exit();
        // }
    }
} 

function editCharacterStatus($id, $missionID, $pilotStatus, $dbx){
    
//    $sql = "SELECT histDate FROM missions WHERE id = $missionID";
//    $querry = mysqli_query($dbx, $sql);
//    $result = mysqli_fetch_assoc($querry);
//    $histDate = $result['histDate'];
    
//    $sql = "SELECT missions.id FROM reports ".
//           "LEFT JOIN missions ON missions.id = reports.missionID ".
//           "WHERE reports.authorID = $id AND missions.id >= $missionID";
    
    //Check if this is the most recent mission or an older mission for the character
    //Update only if it is the most recent mission for the character.
    $sql = "SELECT COUNT(reports.missionID) FROM `reports` ".  
           "WHERE authorID = $id AND missionID > $missionID";
    $querry = mysqli_query($dbx, $sql);
    $report_check = mysqli_num_rows($querry);
    if($report_check > 0) {
        
        $missionsAfterEditMission = mysqli_fetch_row($querry);
        if($missionsAfterEditMission[0] == 0){
            
           switch($pilotStatus) {
                case 1:
                    $characterStatus = 1;
                    break;
                case 2:
                    $characterStatus = 1;
                    break;
                case 3:
                    $characterStatus = 2;
                    break;
                case 4:
                    $characterStatus = 3;
                    break;
                default:
                    $characterStatus = 1;
                    break;
            }

            $sql = "UPDATE careercharacters SET characterstatus=$characterStatus ".
                   "WHERE id = $id";
            mysqli_query($dbx, $sql); 
        }  
    }
}



if(filter_has_var(INPUT_POST, "submitLWClaim")) {

    $missionID = filter_input(INPUT_POST, "submitLWClaim");
    $authorID = filter_input(INPUT_POST, "authorID");
    $claimID = filter_input(INPUT_POST, "claimID");
    $aeroplane = filter_input(INPUT_POST, "aeroplane");
    $cTime = filter_input(INPUT_POST, "cTime", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $place = filter_input(INPUT_POST, "place", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $opponent = filter_input(INPUT_POST, "opponent", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $typofDestr = filter_input(INPUT_POST, "typofDestr");
    $typofImpact = filter_input(INPUT_POST, "typofImpact");
    $fateofCrew = filter_input(INPUT_POST, "fateofCrew");
    $groundConf = filter_input(INPUT_POST, "groundConf");
    $witness = filter_input(INPUT_POST, "witness");
    
    $submitStatus = "true"; 
    
    if($groundConf == 1){
        
        $witness_id = -1;
    } else {
        
        if($witness !== ""){

            $sql = "SELECT id FROM acgmembers WHERE callsign = '$witness'";
            $query = mysqli_query($dbx, $sql);
            $result = mysqli_fetch_assoc($query);
            $witness_id = $result["id"];

        } else {
            $witness_id = null;
        } 
    }

    if($claimID !== ""){
        
        //check if new or edited claim
        $sql = "SELECT count(id) FROM claimslw WHERE id=$claimID";
        $querry = mysqli_query($dbx, $sql);
        $report_check = mysqli_num_rows($querry);
        if($report_check > 0) {

            if(is_null($witness_id)){
                //edited claim, no witness
                $sql = "UPDATE claimslw SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                   "opponent='$opponent', typeofdestr=$typofDestr, typeofimpact=$typofImpact, ".
                   "fateofcrew=$fateofCrew, witness=NULL, confirmed=FALSE, accepted=0 WHERE id=$claimID";
            } else {
                //edited claim, with witness     
                //check if witness has been edited, if yes, set confirmed to false.
                $sql = "SELECT acgmembers.callsign AS witness_callsign FROM acgmembers ".
                    "LEFT JOIN claimslw ON acgmembers.id = claimslw.witness ".
                    "WHERE claimslw.id = $claimID";
                // echo $sql;
                // exit;
                $query = mysqli_query($dbx, $sql);
                $c_num = mysqli_num_rows($query);

                if($c_num < 1){
                    //witness has been edited
                    $sql = "UPDATE claimslw SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                    "opponent='$opponent', typeofdestr=$typofDestr, typeofimpact=$typofImpact, ".
                    "fateofcrew=$fateofCrew, witness=$witness_id, confirmed=FALSE, accepted=0 WHERE id=$claimID";
                    if($witness_id > 0){
                        notifyWitness($missionID, $authorID, $witness_id, $aeroplane, $dbx);
                    }
                    
                } else {
                    $witness_result = mysqli_fetch_assoc($query);
                    $witness_callsign = $witness_result["witness_callsign"];
                    if($witness_callsign === $witness){
                        $sql = "UPDATE claimslw SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                        "opponent='$opponent', typeofdestr=$typofDestr, typeofimpact=$typofImpact, ".
                        "fateofcrew=$fateofCrew, witness=$witness_id, accepted=0 WHERE id=$claimID";
                    } else {
                        $sql = "UPDATE claimslw SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                        "opponent='$opponent', typeofdestr=$typofDestr, typeofimpact=$typofImpact, ".
                        "fateofcrew=$fateofCrew, witness=$witness_id, confirmed=FALSE, accepted=0 WHERE id=$claimID";
                        if($witness_id > 0){
                            notifyWitness($missionID, $authorID, $witness_id, $aeroplane, $dbx);
                        }
                    }
                }               
            }
            if(mysqli_query($dbx, $sql)) {  
                echo $submitStatus;
                exit();
            } else  {
                echo "Error during database manipulation (Claim edit). ".$sql;
                exit();
            }
        }
    }
        
    $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
    $querry = mysqli_query($dbx, $sql);
    $report_result = mysqli_fetch_assoc($querry);
    $report_id = $report_result["id"];

    if(!is_null($witness_id)){

       $sql = "INSERT INTO claimslw (reportid, aeroplane, claimtime, place, ".
              "opponent, typeofdestr, typeofimpact, fateofcrew, witness) ".
              "VALUES ($report_id, $aeroplane, '$cTime', '$place', '$opponent', ".
              "$typofDestr, $typofImpact, $fateofCrew, $witness_id)";
       if($witness_id > 0){
            notifyWitness($missionID, $authorID, $witness_id, $aeroplane, $dbx);
        }
    } else {
    
       $sql = "INSERT INTO claimslw (reportid, aeroplane, claimtime, place, ".
              "opponent, typeofdestr, typeofimpact, fateofcrew) ".
              "VALUES ($report_id, $aeroplane, '$cTime', '$place', '$opponent', ".
              "$typofDestr, $typofImpact, $fateofCrew)";
    }

    if(mysqli_query($dbx, $sql)) {  
        echo $submitStatus;
        exit();
    } else {
        echo "Error during database manipulation (Claim insert). ".$sql;
        exit();
    }
}

function notifyWitness($missionID, $authorID, $witness_id, $aeroplane, $dbx){
        $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
        $querry = mysqli_query($dbx, $sql);
        $report_result = mysqli_fetch_assoc($querry);
        $report_id = $report_result["id"];
        
        $sql = "SELECT careercharacters.personifiedBy FROM careercharacters  WHERE id = '$authorID'";
        $querry = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($querry);
        $sender_id = $result["personifiedBy"];
        
        $sql = "SELECT missions.id AS m_id, realdate, reports.type, acgmembers.callsign AS sender ".
        "FROM missions RIGHT JOIN reports ON reports.missionid = missions.id ".
        "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
        "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ". 
        "WHERE reports.id = $report_id";
        $result = mysqli_query($dbx, $sql);
        $row = mysqli_fetch_assoc($result);

        $sql = "SELECT name FROM aeroplanes WHERE id=$aeroplane";
        $a_result = mysqli_query($dbx, $sql);
        $a_row = mysqli_fetch_assoc($a_result);

        $message = "This is an automatic created message. ".
                   $row["sender"]." appealed to you as a witness for the claim of a ".$a_row["name"].
                   " in mission ".$row["m_id"]." from ".$row["realdate"].".";
        $type = $row["type"];
        if($type == 1){
            $notification = htmlspecialchars("[url=http://aircombatgroup.co.uk/acg-pam/reportRAF.php?r_id=".$report_id."]".$message."[/url]");
        } else if($type == 2){
            $notification = htmlspecialchars("[url=http://aircombatgroup.co.uk/acg-pam/reportLW.php?r_id=".$report_id."]".$message."[/url]");
        }
        //DEACTIVATE FOR LOCAL DEVELOPMENT
        // sendphpbbpm_fromID($notification, $sender_id, $witness_id, 'Witness appeal');
    
}

if(filter_has_var(INPUT_POST, "submitRAFClaim")) {

    $missionID = filter_input(INPUT_POST, "submitRAFClaim");
    $authorID = filter_input(INPUT_POST, "authorID");
    $claimID = filter_input(INPUT_POST, "claimID");
    $claimStatus = filter_input(INPUT_POST, "claimStatus");
    $claimAeroplane = filter_input(INPUT_POST, "claimAeroplane");
    $shared = filter_input(INPUT_POST, "shared");
    $claimDescription = filter_input(INPUT_POST, "claimDescription", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    
    $submitStatus = "true";
    
    if($claimID !== ""){
        //check if new or edited claim
        $sql = "SELECT count(id) FROM claimsraf WHERE id=$claimID";
        $querry = mysqli_query($dbx, $sql);
        $report_check = mysqli_num_rows($querry);
        if($report_check > 0) {

            $sql = "UPDATE claimsraf SET aeroplane=$claimAeroplane, enemystatus=$claimStatus, ".
                   "shared=$shared, description='$claimDescription', accepted=0 WHERE id=$claimID";
            if(mysqli_query($dbx, $sql)) {  
                echo $submitStatus;
                exit();
            } else  {
                echo "Error during database manipulation (Claim edit). ".$sql;
                exit();
            }
        }
    }
    
    $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
    $querry = mysqli_query($dbx, $sql);
    $report_result = mysqli_fetch_assoc($querry);
    $report_id = $report_result["id"];

    $sql = "INSERT INTO claimsraf (reportid, aeroplane, enemystatus, shared, description)
    VALUES ($report_id, $claimAeroplane, $claimStatus, $shared, '$claimDescription')";
    if(mysqli_query($dbx, $sql)) {  
        echo $submitStatus;
        exit();
    } else
    {
        echo "Error during database manipulation (Claim insert). ".$sql;
        exit();
    }
}

if(filter_has_var(INPUT_POST, "submitVVSClaim")) {

    $missionID = filter_input(INPUT_POST, "submitVVSClaim");
    $authorID = filter_input(INPUT_POST, "authorID");
    $claimID = filter_input(INPUT_POST, "claimID");
    $aeroplane = filter_input(INPUT_POST, "aeroplane");
    $cTime = filter_input(INPUT_POST, "cTime", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $place = filter_input(INPUT_POST, "place", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $groundConf = filter_input(INPUT_POST, "groundConf");
    $witness = filter_input(INPUT_POST, "witness");
    $groupClaim = filter_input(INPUT_POST, "groupClaim");
    $claimDescription = filter_input(INPUT_POST, "claimDescription", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    
    $submitStatus = "true"; 
    
    if($groundConf == 1){
        
        $witness_id = -1;
    } else {
        
        if($witness !== ""){

            $sql = "SELECT id FROM acgmembers WHERE callsign = '$witness'";
            $query = mysqli_query($dbx, $sql);
            $result = mysqli_fetch_assoc($query);
            $witness_id = $result["id"];

        } else {
            $witness_id = null;
        } 
    }

    if($claimID !== ""){
        
        //check if new or edited claim
        $sql = "SELECT count(id) FROM claimsvvs WHERE id=$claimID";
        $querry = mysqli_query($dbx, $sql);
        $report_check = mysqli_num_rows($querry);
        if($report_check > 0) {

            if(is_null($witness_id)){
                //edited claim, no witness
                $sql = "UPDATE claimsvvs SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                   "witness=NULL, confirmed=FALSE, groupClaim=$groupClaim, description='$claimDescription', ".
                   "accepted=0 WHERE id=$claimID";
            } else {
                //edited claim, with witness     
                //check if witness has been edited, if yes, set confirmed to false.
                $sql = "SELECT acgmembers.callsign AS witness_callsign FROM acgmembers ".
                    "LEFT JOIN claimsvvs ON acgmembers.id = claimsvvs.witness ".
                    "WHERE claimsvvs.id = $claimID";
                $query = mysqli_query($dbx, $sql);
                $c_num = mysqli_num_rows($query);

                if($c_num < 1){
                    //witness has been edited
                    $sql = "UPDATE claimsvvs SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                    "witness=$witness_id, confirmed=FALSE, groupClaim=$groupClaim, description='$claimDescription', ".
                    "accepted=0 WHERE id=$claimID";
                    if($witness_id > 0){
                        notifyWitness($missionID, $authorID, $witness_id, $aeroplane, $dbx);
                    }
                    
                } else {
                    $witness_result = mysqli_fetch_assoc($query);
                    $witness_callsign = $witness_result["witness_callsign"];
                    if($witness_callsign === $witness){
                        $sql = "UPDATE claimsvvs SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                    "witness=$witness_id, confirmed=FALSE, groupClaim=$groupClaim, description='$claimDescription', ".
                    "accepted=0 WHERE id=$claimID";
                    } else {
                        $sql = "UPDATE claimsvvs SET aeroplane=$aeroplane, claimtime='$cTime', place='$place', ".
                    "witness=$witness_id, confirmed=FALSE, groupClaim=$groupClaim, description='$claimDescription', ".
                    "accepted=0 WHERE id=$claimID";
                        if($witness_id > 0){
                            notifyWitness($missionID, $authorID, $witness_id, $aeroplane, $dbx);
                        }
                    }
                }               
            }
            if(mysqli_query($dbx, $sql)) {  
                echo $submitStatus;
                exit();
            } else  {
                echo "Error during database manipulation (Claim edit). ".$sql;
                exit();
            }
        }
    }
        
    $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
    $querry = mysqli_query($dbx, $sql);
    $report_result = mysqli_fetch_assoc($querry);
    $report_id = $report_result["id"];

    if(!is_null($witness_id)){

       $sql = "INSERT INTO claimsvvs(reportid, aeroplane, claimtime, place, ".
              "witness, groupClaim, description) ".
              "VALUES ($report_id, $aeroplane, '$cTime', '$place', ".
              "$witness_id, $groupClaim, '$claimDescription')";
       if($witness_id > 0){
            notifyWitness($missionID, $authorID, $witness_id, $aeroplane, $dbx);
        }
    } else {
    
       $sql = "INSERT INTO claimsvvs(reportid, aeroplane, claimtime, place, ".
              "groupClaim, description) ".
              "VALUES ($report_id, $aeroplane, '$cTime', '$place', ".
              "$groupClaim, '$claimDescription')";
    }

    if(mysqli_query($dbx, $sql)) {  
        echo $submitStatus;
        exit();
    } else {
        echo "Error during database manipulation (Claim insert). ".$sql;
        exit();
    }
}

if(filter_has_var(INPUT_POST, "submitGroundClaim")) {

    $missionID = filter_input(INPUT_POST, "submitGroundClaim");
    $authorID = filter_input(INPUT_POST, "authorID");
    $claimID = filter_input(INPUT_POST, "claimID");
    $claimObject = filter_input(INPUT_POST, "claimObject");
    $amount = filter_input(INPUT_POST, "amount");
    $claimDescription = filter_input(INPUT_POST, "claimDescription");
    
    if($claimID !== ""){
        //check if new or edited claim
        $sql = "SELECT count(id) FROM claimsground WHERE id=$claimID";
        $querry = mysqli_query($dbx, $sql);
        $report_check = mysqli_num_rows($querry);
        if($report_check > 0) {

            $sql = "UPDATE claimsground SET object=$claimObject, amount=$amount, ".
                   "description='$claimDescription', accepted=0 WHERE id=$claimID";
            if(mysqli_query($dbx, $sql)) {  
                echo "Ground claim edited.";
                exit();
            } else  {
                echo "Error during database manipulation (Ground claim edit). ".$sql;
                exit();
            }
        }
    }
    
    $sql = "SELECT id FROM reports WHERE missionid = '$missionID' AND authorid = '$authorID'";
    $querry = mysqli_query($dbx, $sql);
    $report_result = mysqli_fetch_assoc($querry);
    $report_id = $report_result["id"];

    $sql = "INSERT INTO claimsground (reportid, object, amount, description)
    VALUES ($report_id, $claimObject, $amount, '$claimDescription')";
    if(mysqli_query($dbx, $sql)) {  
        echo "Ground claim submitted.";
        exit();
    } else
    {
        echo "Error during database manipulation (Ground claim insert). ".$sql;
        exit();
    }
}

if(filter_has_var(INPUT_POST, "clearLWClaim")) {

    $claimID = filter_input(INPUT_POST, "clearLWClaim");

    $sql = "DELETE FROM claimslw WHERE id = $claimID";
    if(mysqli_query($dbx, $sql)) {  
        echo "Claim deleted.";
        exit();
    } else
    {
        echo "Error during database manipulation (Claim delete).";
        exit();
    }
        
}

if(filter_has_var(INPUT_POST, "clearRAFClaim")) {

    $claimID = filter_input(INPUT_POST, "clearRAFClaim");

    $sql = "DELETE FROM claimsraf WHERE id = $claimID";
    if(mysqli_query($dbx, $sql)) {  
        echo "Claim deleted.";
        exit();
    } else
    {
        echo "Error during database manipulation (Claim delete).";
        exit();
    }
        
}

if(filter_has_var(INPUT_POST, "clearVVSClaim")) {

    $claimID = filter_input(INPUT_POST, "clearVVSClaim");

    $sql = "DELETE FROM claimsvvs WHERE id = $claimID";
    if(mysqli_query($dbx, $sql)) {  
        echo "Claim deleted.";
        exit();
    } else
    {
        echo "Error during database manipulation (Claim delete).";
        exit();
    }
        
}

if(filter_has_var(INPUT_POST, "clearGroundClaim")) {

    $claimID = filter_input(INPUT_POST, "clearGroundClaim");

    $sql = "DELETE FROM claimsground WHERE id = $claimID";
    if(mysqli_query($dbx, $sql)) {  
        echo "Ground claim deleted.";
        exit();
    } else
    {
        echo "Error during database manipulation (Ground claim delete).";
        exit();
    }
        
}

if(filter_has_var(INPUT_POST, "witnessCheck")) {
    
    $witness = filter_input(INPUT_POST, "witnessCheck");
    $sql = "SELECT id FROM acgmembers WHERE callsign = '$witness'";
    $query = mysqli_query($dbx, $sql);
    $w_num = mysqli_num_rows($query);
    if($w_num < 1){
        echo "Witness not found";
        exit();
    } else {
        echo "Witness existing";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "editReportAuthorMission")) {
    
    $reportID = filter_input(INPUT_POST, "editReportAuthorMission");
    $missionID = filter_input(INPUT_POST, "mi");
    $authorID = filter_input(INPUT_POST, "ch");
    $sql = "UPDATE reports SET missionID=$missionID, authorID=$authorID ".
           "WHERE id = $reportID";
    if(mysqli_query($dbx, $sql)){
        echo "Author/Mission changed.";
        exit();
    } else {
        echo "Error during database manipulation (author/mission edit).";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "deleteReport")){
    $id = filter_input(INPUT_POST, "deleteReport");
    $sql = "DELETE FROM reports WHERE id='$id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}