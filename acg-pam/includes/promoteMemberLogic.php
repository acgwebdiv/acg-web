<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/characterDBFunctions.php');
include_once(dirname(__FILE__).'/functions.php');
session_start();
$dbx = getDBx();

if(filter_has_var(INPUT_POST, "promote")) {
    $edit_id = $_SESSION["edit_id"];
    $r = filter_input(INPUT_POST, "promote");
    $y = filter_input(INPUT_POST, "y");
    $m = filter_input(INPUT_POST, "m");
    $d = filter_input(INPUT_POST, "d");
    $c = filter_input(INPUT_POST, "c", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    
    if($y==="" | $m==="" | $d===""){
        $pdate = date("Y-m-d");
    } else {
        $check_tDate = mktime(0, 0, 0, $m, $d, $y);
        $pdate = date("Y-m-d", $check_tDate);
    }
    
    $sql = "INSERT INTO promotions (memberID, value, date, comment) ".
            " VALUES ($edit_id, $r, '$pdate', '$c')";
    if(mysqli_query($dbx, $sql)) {
        updateForumRank($edit_id);
        echo "Promotion/demotion successful.";
        exit();
    } else {
        echo "Error during database manipulation.";
        exit();
    }   
}

if(filter_has_var(INPUT_POST, "deletePromotion")){
    $edit_id = $_SESSION["edit_id"];
    $id = filter_input(INPUT_POST, "deletePromotion");
    $sql = "DELETE FROM promotions WHERE id='$id'";
    if(mysqli_query($dbx, $sql)) {
        updateForumRank($edit_id);
        echo "Promotion/Demotion deleted";
        exit();
    }
}

function updateForumRank($memberId){
    
    $dbx = getDBx();
    $sql = "SELECT acgmembers.username FROM acgmembers ".
           "WHERE acgmembers.id = $memberId";
    $query = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($query);
    $username = strtolower($result["username"]);
    
    //Connect to forum database and get forum user_id of username.
    $dbxForum = getForumDBx();
    $sql = "SELECT phpbb_users.user_id FROM phpbb_users ".
           "WHERE phpbb_users.username_clean = '$username'";
    $query = mysqli_query($dbxForum, $sql);
    $result = mysqli_fetch_assoc($query);
    $forum_user_id = $result["user_id"];
    
    //Get most recent rank from pam database
    $pam_rankID = getMostRecentRankID($memberId, $dbx);
    
    //Translate rank id from PAM to Forum
    switch($pam_rankID){
        
        case 1: 
            $forum_rankID = 25;
            break;
        case 2: 
            $forum_rankID = 26;
            break;
        case 3: 
            $forum_rankID = 27;
            break;
        case 4: 
            $forum_rankID = 2;
            break;
        case 5: 
            $forum_rankID = 3;
            break;
        case 6: 
            $forum_rankID = 4;
            break;
        case 7: 
            $forum_rankID = 5;
            break;
        case 8: 
            $forum_rankID = 6;
            break;
        case 9: 
            $forum_rankID = 7;
            break;
        case 10: 
            $forum_rankID = 8;
            break;
        case 11: 
            $forum_rankID = 9;
            break;
        case 12: 
            $forum_rankID = 11;
            break;
        case 13: 
            $forum_rankID = 10;
            break;
        case 14: 
            $forum_rankID = 12;
            break;
        case 15: 
            $forum_rankID = 20;
            break;
        case 16: 
            $forum_rankID = 13;
            break;
        case 17: 
            $forum_rankID = 14;
            break;
        case 18: 
            $forum_rankID = 15;
            break;
        case 19: 
            $forum_rankID = 16;
            break;
        case 20: 
            $forum_rankID = 17;
            break;
        case 21: 
            $forum_rankID = 18;
            break;
        case 22: 
            $forum_rankID = 19;
            break;
        case 23: 
            $forum_rankID = 21;
            break;
        case 24: 
            $forum_rankID = 22;
            break;
        case 25:
            $forum_rankID = 24;
            break;
        case 26:
            $forum_rankID = 23;
            break;
        case 27:
            $forum_rankID = 33;
            break;
        case 28:
            $forum_rankID = 47;
            break;
        case 29:
            $forum_rankID = 48;
            break;
        case 30:
            $forum_rankID = 48;
            break;
        case 31:
            $forum_rankID = 49;
            break;
        case 32:
            $forum_rankID = 50;
            break;
        case 33:
            $forum_rankID = 51;
            break;
        case 34:
            $forum_rankID = 52;
            break;
        case 35:
            $forum_rankID = 53;
            break;
        case 36:
            $forum_rankID = 54;
            break;
        case 37:
            $forum_rankID = 55;
            break;
        case 38:
            $forum_rankID = 56;
            break;
        case 39:
            $forum_rankID = 57;
            break;
        case 40:
            $forum_rankID = 58;
            break;
            
    }
    
    //Change rank_id in forum database
    $sql = "UPDATE phpbb_users SET user_rank=$forum_rankID WHERE user_id=$forum_user_id";
    $result = mysqli_query($dbxForum, $sql);
    
}



