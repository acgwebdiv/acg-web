<?php
include_once(dirname(__FILE__).'/db_connect.php');
session_start();
$dbx = getDBx();
if(filter_has_var(INPUT_POST, "addCampaign")) {
    $n = filter_input(INPUT_POST, "addCampaign");
    $sql = "SELECT id FROM campaigns WHERE name='$n' LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $cname_check = mysqli_num_rows($query);
    if($cname_check < 1) {
        $sql = "INSERT INTO campaigns (name) VALUES ('$n')";
        if(mysqli_query($dbx, $sql)) {

            echo "Campaign created.";
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {
        echo "Campaign name is already existing.";
        exit(); 
    }
}

if(filter_has_var(INPUT_POST, "changeCampaignName")) {
    $edit_id = $_SESSION["edit_id"];
    $n = filter_input(INPUT_POST, "changeCampaignName");
    $sql = "SELECT id FROM campaigns WHERE name='$n' LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $cname_check = mysqli_num_rows($query);
    if($cname_check < 1) {

        if(changeCampaign("name", $n, $edit_id)) {
            echo "Campaign name changed.";
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {

        $result = mysqli_fetch_array($query);
        $check_ID = $result[0];
        if($check_ID !== $edit_id){
           echo "Campaign name is already existing.";
           exit(); 
        } else {
           echo "";
           exit();
        }    
    }
}

if(filter_has_var(INPUT_POST, "deleteCampaign")){
    $edit_id = $_SESSION["edit_id"];
    $sql = "DELETE FROM campaigns WHERE id='$edit_id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}

function changeCampaign($field, $value, $edit_id){

    global $dbx;
    $sql = "UPDATE campaigns SET $field='$value' WHERE id='$edit_id'";
    return mysqli_query($dbx, $sql);
}