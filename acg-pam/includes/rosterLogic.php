<?php
include_once(dirname(__FILE__).'/db_connect.php');
session_start();
$dbx = getDBx();

if(filter_has_var(INPUT_POST, "saveAircraft")) {
    $memberID = filter_input(INPUT_POST, "saveAircraft");
    $aircraftID = filter_input(INPUT_POST, "a");
    
    $sql = "UPDATE hangar SET memberID='$memberID' WHERE id='$aircraftID'";
    if(mysqli_query($dbx, $sql)) {
        echo "Aircraft assignment saved.";
        exit();
    } else {
        echo "Error during database manipulation (Aircraft assignment memberID=".$memberID.")";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "saveRoster")) {
    $positionID = filter_input(INPUT_POST, "saveRoster");
    $memberID = filter_input(INPUT_POST, "m");
    $squadronID = filter_input(INPUT_POST, "s");
    
    $sql = "SELECT id FROM roster WHERE squadronID = $squadronID AND positionID = $positionID";
    $querry = mysqli_query($dbx, $sql);
    $roster_check = mysqli_num_rows($querry);
    
    if($roster_check > 0){
        $roster_result = mysqli_fetch_assoc($querry);
        $roster_id = $roster_result["id"];
        
        if($memberID == 0){
            $sql = "DELETE FROM roster WHERE id='$roster_id'";
            if(mysqli_query($dbx, $sql)) {
                echo "Roster assignment saved.";
                exit();
            } else {
                echo "Error during database manipulation (Aircraft assignment positionID=".$positionID.")";
                exit();
            }
        } else {
            $sql = "UPDATE roster SET memberID='$memberID' WHERE id='$roster_id'";
            if(mysqli_query($dbx, $sql)) {
                echo "Roster assignment saved.";
                exit();
            } else {
                echo "Error during database manipulation (Aircraft assignment positionID=".$positionID.")";
                exit();
            } 
        }
    } else {
        
        if($memberID != 0){
            $sql = "INSERT INTO roster(memberID, squadronID, positionID) VALUES ".
                   "($memberID, $squadronID, $positionID)";
            if(mysqli_query($dbx, $sql)) {
                echo "Roster assignment saved.";
                exit();
            } else {
                echo "Error during database manipulation (Aircraft assignment positionID=".$positionID.")";
                exit();
            } 
        }
    }
    
}

