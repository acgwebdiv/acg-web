<?php
//Creates the html code for a select form.
//$formID defines the identifier for the select form
//$optionArray an Array defining the selectable options. Array[0] gives and ID while Array[1] is the displayed text
//$selectedOption gives an ID corresponding to a value in $optionArray for preseletion
function createSelectForm($formID, $optionArray, $selectedOption){
    
    echo "<select id='".$formID."'>";
    for($i = 0; $i < count($optionArray); $i++){
        
        if($selectedOption == $optionArray[$i][0]){
            echo "<option value = '".$optionArray[$i][0]."' selected >".$optionArray[$i][1]."</option>";
        } else {
            echo "<option value = '".$optionArray[$i][0]."' >".$optionArray[$i][1]."</option>";
        }
    }
    echo "</select>";
}

//Creates the html code for a select form.
//$formName defines the name of the select form for batch processing
//$formID defines the identifier for the select form
//$optionArray an Array defining the selectable options. Array[0] gives and ID while Array[1] is the displayed text
//$selectedOption gives an ID corresponding to a value in $optionArray for preseletion
function createSelectFormWithName($formName, $formID, $optionArray, $selectedOption){
    
    echo "<select name='".$formName."' id='".$formID."'>";
    for($i = 0; $i < count($optionArray); $i++){
        
        if($selectedOption == $optionArray[$i][0]){
            echo "<option value = '".$optionArray[$i][0]."' selected >".$optionArray[$i][1]."</option>";
        } else {
            echo "<option value = '".$optionArray[$i][0]."' >".$optionArray[$i][1]."</option>";
        }
    }
    echo "</select>";
}

//Creates the html code for a select form and returns it as a string.
//$formName defines the name of the select form for batch processing
//$formID defines the identifier for the select form
//$optionArray an Array defining the selectable options. Array[0] gives and ID while Array[1] is the displayed text
//$selectedOption gives an ID corresponding to a value in $optionArray for preseletion
function getSelectFormWithName($formName, $formID, $optionArray, $selectedOption){
    
    $returnStr = "<select name='".$formName."' id='".$formID."'>";
    for($i = 0; $i < count($optionArray); $i++){
        
        if($selectedOption == $optionArray[$i][0]){
            $returnStr .= "<option value = '".$optionArray[$i][0]."' selected >".$optionArray[$i][1]."</option>";
        } else {
            $returnStr .= "<option value = '".$optionArray[$i][0]."' >".$optionArray[$i][1]."</option>";
        }
    }
    $returnStr .= "</select>";
    return $returnStr;
}

//Creates the html code for the select form options only
function createSelectOptions($optArray, $selectedOption) {
    while($row = mysqli_fetch_row($optArray)) {
        $selected = ($selectedOption == $row[0] ? "selected" : "");
        echo("<option value=".$row[0]." ".$selected."> ".$row[1]."</option>");
    }
}

function createPageSelect($n_pages, $page, $link) {
    
    $X = 5;
    if($n_pages > 1){
        
        if(($n_pages > $X+3)&&($page <= $X+2)){
                
            for($i = 1; $i <= ($page + $X); $i++) {
               if($page == $i){ 
                   echo "<span class='selectedPageSpan'>".$i."</span>";
               } else {
                   echo "<span><a href='".$link."page=".$i."'>".$i."</a></span>";
               }
            }
            echo "<span>...</span>";
            echo "<span><a href='".$link."page=".$n_pages."'>".$n_pages."</a></span>";
                
        } else if(($n_pages > $X+3)&&($page >= $n_pages-$X-1)) {

            echo "<span><a href='".$link."page=1'>1</a></span>";
            echo "<span>...</span>";
            for($i = ($page - $X); $i <= $n_pages; $i++) {
               if($page == $i){ 
                   echo "<span class='selectedPageSpan'>".$i."</span>";
               } else {
                   echo "<span><a href='".$link."page=".$i."'>".$i."</a></span>";
               }
            }
               
        }else if($n_pages > $X+3) {

            echo "<span><a href='".$link."page=1'>1</a></span>";
            echo "<span>...</span>";
            for($i = ($page-$X); $i <= ($page+$X); $i++) {
               if($page == $i){ 
                   echo "<span class='selectedPageSpan'>".$i."</span>";
               } else {
                   echo "<span><a href='".$link."page=".$i."'>".$i."</a></span>";
               }
            }
            echo "<span>...</span>";
            echo "<span><a href='".$link."page=".$n_pages."'>".$n_pages."</a></span>";
            
        } else {
        
           for($i = 1; $i <= $n_pages; $i++) {
               if($page == $i){ 
                   echo "<span class='selectedPageSpan'>".$i."</span>";
               } else {
                   echo "<span><a href='".$link."page=".$i."'>".$i."</a></span>";
               }
            } 
        }
    }
}

function createPageIndex($index, $link){
    
    if($index == "%"){
            echo "<span class='selectedPageSpan'>All</span>";
        } else {
            echo "<span><a href='".$link."index=%'>All</a></span>";
        }
    foreach(range('a','z') as $alphaKey)
    {
        if($index == $alphaKey.'%'){
            echo "<span class='selectedPageSpan'>".strtoupper($alphaKey)."</span>";
        } else {
            echo "<span><a href='".$link."index=".$alphaKey."'>".strtoupper($alphaKey)."</a></span>";
        }
    } 
}

function createPageIndexExt($index, $link){
    
    if($index == "%"){
        echo "<span class='selectedPageSpan'>All</span>";
    } else {
        echo "<span><a href='".$link."index=%'>All</a></span>";
    }
    if($index == "-1"){
        echo "<span class='selectedPageSpan'>#$%</span>";
    } else {
        echo "<span><a href='".$link."index=-1'>#$%</a></span>";
    }
    foreach(range(0,9) as $alphaKey)
    {
        if($index == $alphaKey){
            echo "<span class='selectedPageSpan'>".strtoupper($alphaKey)."</span>";
        } else {
            echo "<span><a href='".$link."index=".$alphaKey."'>".strtoupper($alphaKey)."</a></span>";
        }
    }
    foreach(range('a','z') as $alphaKey)
    {
        if($index == $alphaKey){
            echo "<span class='selectedPageSpan'>".strtoupper($alphaKey)."</span>";
        } else {
            echo "<span><a href='".$link."index=".$alphaKey."'>".strtoupper($alphaKey)."</a></span>";
        }
    } 
}

function parseImageTags($string){
    
    $str = str_ireplace("[img]", "<img src='", $string);
    $str = str_ireplace("[/img]", "' alt='IMAGE' width='90%' class='synopImg'>", $str);
    return $str;
}

function realToHistDate($date, $dateArray){
    
    $nDates = count($dateArray);
    for($i = $nDates-1; $i >= 0; $i--){
        
        $dateToCheck = strtotime($date);
        $dateToCompare = strtotime($dateArray[$i][0]);
        if($dateToCheck >= $dateToCompare){
            return date_format(date_create($dateArray[$i][1]), "Y-m-d");
        }
    } 
    return date_format(date_create($dateArray[0][1]), "Y-m-d");
}

function lengthOfServiceByDates($joiningDate, $dischargeDate) {
        
    if($dischargeDate === NULL){
        $dateEnd = date_add(new DateTime("now"), date_interval_create_from_date_string('1 days'));
        $lengthofservice = date_diff($dateEnd, date_create($joiningDate));
    } else {
        $dateEnd = date_add(date_create($dischargeDate), date_interval_create_from_date_string('1 days'));
        $lengthofservice = date_diff($dateEnd, date_create($joiningDate));
    }
    $years = date_interval_format($lengthofservice, '%y' );
    $month = date_interval_format($lengthofservice, '%m' );
    $days = date_interval_format($lengthofservice, '%d' );

    $returnStr = "";
    if($years === "1"){
        $returnStr .= "$years year, ";
    } else {
        $returnStr .= "$years years, ";
    }
    if($month === "1"){
        $returnStr .= "$month month, ";
    } else {
        $returnStr .= "$month months, ";
    }
    if($days === "1"){
        $returnStr .= "$days day";
    } else {
        $returnStr .= "$days days";
    }
    return $returnStr;
}

function lengthOfService($su_array) {
    
    $nStatuses = count($su_array);
    $lengthofserviceacc = new DateTime('00:00');
    $laststatus = $su_array[0]["statusID"];
    $laststatusDate = $su_array[0]["date"];
    
    for($i = 1; $i < $nStatuses; $i++){
        
        $nextstatus = $su_array[$i]["statusID"];
        $nextstatusDate = $su_array[$i]["date"];
        
        if(($laststatus == 1 || $laststatus == 3) && ($nextstatus == 2)){
            
            $dateEnd = date_add(date_create($nextstatusDate), date_interval_create_from_date_string('1 days'));
            $lengthofserviceLast = date_diff($dateEnd, date_create($laststatusDate));
            $lengthofserviceacc = date_add($lengthofserviceacc, $lengthofserviceLast);
            
            $laststatus = $nextstatus;
            $laststatusDate = $nextstatusDate;
        }
    }
    
    $laststatus = $su_array[$nStatuses-1]["statusID"];
    $laststatusDate = $su_array[$nStatuses-1]["date"];
    
    if($laststatus == 1 || $laststatus == 3){
        $dateEnd = date_add(new DateTime("now"), date_interval_create_from_date_string('1 days'));
        $lengthofserviceLast = date_diff($dateEnd, date_create($laststatusDate));
        $lengthofserviceacc = date_add($lengthofserviceacc, $lengthofserviceLast);
    }
    $lengthofservice = date_diff($lengthofserviceacc, new DateTime('00:00'));
    
    $years = date_interval_format($lengthofservice, '%y' );
    $month = date_interval_format($lengthofservice, '%m' );
    $days = date_interval_format($lengthofservice, '%d' );

    $returnStr = "";
    if($years === "1"){
        $returnStr .= "$years year, ";
    } else {
        $returnStr .= "$years years, ";
    }
    if($month === "1"){
        $returnStr .= "$month month, ";
    } else {
        $returnStr .= "$month months, ";
    }
    if($days === "1"){
        $returnStr .= "$days day";
    } else {
        $returnStr .= "$days days";
    }
    return $returnStr;
}

function lengthOfServiceByMemberID($user_id, $dbx) {
    
    $sql = "SELECT memberstatuslog.id, memberstatuslog.statusID, memberstatus.status, memberstatuslog.date, memberstatuslog.comment, ".
           "UNIX_TIMESTAMP(memberstatuslog.date) AS mspdate ".
           "FROM memberstatuslog LEFT JOIN memberstatus ON memberstatuslog.statusID = memberstatus.id ".
           "WHERE memberID = $user_id ORDER BY mspdate DESC";
    $su_query = mysqli_query($dbx, $sql);
    $su_array = mysqli_fetch_all($su_query, MYSQLI_BOTH);
    
    return lengthOfService($su_array);
}

    
function lengthOfServiceShort($joiningDate, $dischargeDate) {
    
    if($dischargeDate === NULL){
        $dateEnd = date_add(new DateTime("now"), date_interval_create_from_date_string('1 days'));
        $lengthofservice = date_diff($dateEnd, date_create($joiningDate));
    } else {
        $dateEnd = date_add(date_create($dischargeDate), date_interval_create_from_date_string('1 days'));
        $lengthofservice = date_diff($dateEnd, date_create($joiningDate));
    }
    $years = date_interval_format($lengthofservice, '%y' );
    $month = date_interval_format($lengthofservice, '%m' );
    $days = date_interval_format($lengthofservice, '%d' );
    
    $returnStr = "y: ".$years." m: ".$month." d: ".$days;
//    if($years === "1"){
//        $returnStr .= "$years year, ";
//    } else {
//        $returnStr .= "$years years, ";
//    }
//    if($month === "1"){
//        $returnStr .= "$month month, ";
//    } else {
//        $returnStr .= "$month months, ";
//    }
//    if($days === "1"){
//        $returnStr .= "$days day";
//    } else {
//        $returnStr .= "$days days";
//    }
    return $returnStr;
}

function pam_htmlentities_filter($str) {
 
    return htmlentities($str, ENT_QUOTES);   
}