<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/functions.php');
//DEACTIVATE FOR LOCAL DEVELOPMENT
include_once(dirname(__FILE__).'/forumInteract.php');
include_once(dirname(__FILE__).'/decorationsChecks.php');
session_start();
$dbx = getDBx();

if(filter_has_var(INPUT_POST, "changeAccepted")) {
    
    $a = filter_input(INPUT_POST, "changeAccepted");
    $report_id = filter_input(INPUT_POST, "reportID");
    $member_id = filter_input(INPUT_POST, "memberID");
    if($a==="true"){
        $acceptedBy = filter_input(INPUT_POST, "acceptedBy");
    } else {
        $acceptedBy = 0;
    }
    
    $sql = "UPDATE reports SET accepted=$a, acceptedby=$acceptedBy WHERE id=$report_id";
    if(mysqli_query($dbx, $sql)) {
        
        checkDecorationsForMember($member_id, $dbx);
        echo "Report accept status changed.";
        exit();
    } else {
        echo "Error during database manipulation (report accept).";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "changeClaimAccepted")) {
    
    $accepted = filter_input(INPUT_POST, "changeClaimAccepted");
    $claim_id = filter_input(INPUT_POST, "claimID");
    $faction = filter_input(INPUT_POST, "faction");
    
    if($faction=="RAF"){
        $sql = "UPDATE claimsraf SET accepted=$accepted WHERE id=$claim_id";
    } else if($faction=="LW"){
        $sql = "UPDATE claimslw SET accepted=$accepted WHERE id=$claim_id";
    } else if($faction=="VVS"){
        $sql = "UPDATE claimsvvs SET accepted=$accepted WHERE id=$claim_id";
    } else if($faction=="ground"){
        $sql = "UPDATE claimsground SET accepted=$accepted WHERE id=$claim_id";
    }
    
    if(mysqli_query($dbx, $sql)) {
        
        echo "Claim accept status changed.";
        exit();
    } else {
        echo "Error during database manipulation (claim accept). ".$sql;
        exit();
    }
}

if(filter_has_var(INPUT_POST, "submitComment")) {
    
    $comment = filter_input(INPUT_POST, "submitComment", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    $report_id = filter_input(INPUT_POST, "reportID");
    $authorID = filter_input(INPUT_POST, "authorID");
    
    $sql = "SELECT missions.id AS m_id, missions.realDate, reports.type, acgmembers.id AS receiverid ".
           "FROM missions RIGHT JOIN reports ON reports.missionid = missions.id ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ". 
           "WHERE reports.id = $report_id";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $message = "This is an automatic created message. ".
               "Your report for mission ".$row["m_id"]." from ".$row["realDate"].
               " was commented.";
    $type = $row["type"];
    $receiverID = $row["receiverid"];
    if($type == 1){
        $notification = htmlspecialchars("[url=http://aircombatgroup.co.uk/acg-pam/reportRAF.php?r_id=".$report_id."]".$message."[/url]");
    } else if($type == 2){
        $notification = htmlspecialchars("[url=http://aircombatgroup.co.uk/acg-pam/reportLW.php?r_id=".$report_id."]".$message."[/url]");
    } else if($type == 3){
        $notification = htmlspecialchars("[url=http://aircombatgroup.co.uk/acg-pam/reportVVS.php?r_id=".$report_id."]".$message."[/url]");
    }

    sendphpbbpm_fromID($notification, $authorID, $receiverID, 'Comment received'); 

    $sql = "INSERT INTO comments (commentauthorid, commentdate, commenttext, reportid) ".
           "VALUES ($authorID, '".date("Y-m-d")."' , '$comment', $report_id)";
    if(mysqli_query($dbx, $sql)) {
        echo "Report comment submitted.";
        exit();
    } else {
        echo "Error during database manipulation (comment submission).";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "deleteComment")){
    
    $comment_id = filter_input(INPUT_POST, "deleteComment");
    $sql = "DELETE FROM comments WHERE id=$comment_id";
    if(mysqli_query($dbx, $sql)) {
        echo "Comment deleted.";
        exit();
    } else {
        echo "Error during database manipulation (comment deletion).";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "confirmClaim")) {
    
    $confirm = filter_input(INPUT_POST, "confirmClaim");
    $claim_id = filter_input(INPUT_POST, "claimID");
    $faction = filter_input(INPUT_POST, "faction");
    
    if($faction=="LW"){
        $sql = "UPDATE claimslw SET confirmed=$confirm WHERE id=$claim_id";
    } else if($faction=="VVS"){
        $sql = "UPDATE claimsvvs SET confirmed=$confirm WHERE id=$claim_id";
    }
    
    if(mysqli_query($dbx, $sql)) {
        echo "TRUE";
        exit();
    } else {
        echo "Error during database manipulation (claim confirm).";
        exit();
    }
}
