<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/functions.php');
session_start();
$dbx = getDBx();
if(filter_has_var(INPUT_POST, "n")) {
         
    // GATHER THE POSTED DATA INTO LOCAL VARIABLES
    $n = filter_input(INPUT_POST, "n");
    $c = filter_input(INPUT_POST, "c");
    $y = filter_input(INPUT_POST, "y");
    $m = filter_input(INPUT_POST, "m");
    $d = filter_input(INPUT_POST, "d");
    $a = filter_input(INPUT_POST, "a");
    $s = filter_input(INPUT_POST, "s");

    // check if membername already taken
    $sql = "SELECT id FROM acgmembers WHERE username = '" . $n . "' LIMIT 1";
    $querry = mysqli_query($dbx, $sql);
    $mname_check = mysqli_num_rows($querry);
    if($mname_check > 0) {

        echo "Membername already taken. Please choose another membername.";
        exit();
    }

    // check if callsign already taken
    $sql = "SELECT id FROM acgmembers WHERE callsign = '" . $c . "' LIMIT 1";
    $querry = mysqli_query($dbx, $sql);
    $callsign_check = mysqli_num_rows($querry);
    if($callsign_check > 0) {

        echo "Callsign already taken. Please choose another callsign.";
        exit();
    }
    
    
    // Begin Insertion of data into the database
    // Add user info into the database table
    $sql = "INSERT INTO acgmembers (username, callsign, admin) VALUES (".
            "'$n', '$c', '$a')";
    $querry = mysqli_query($dbx, $sql);
    $id = mysqli_insert_id($dbx);
    $sql = "INSERT INTO transfers (memberid, squadronid, transferdate) ".
           " VALUES ($id, $s, '$y-$m-$d')";
    $querry = mysqli_query($dbx, $sql);
    $sql = "INSERT INTO memberstatuslog (memberID, statusID, date, comment) ".
            " VALUES ($id, 1, '$y-$m-$d', 'Initial join')";
    $querry = mysqli_query($dbx, $sql);
    $sql = "SELECT name FROM squadrons WHERE id=$s";
    $querry = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($querry);
    echo  "Member added: <br>".
            "<table>".
            "<tr><td>ID:</td><td>$id</td></tr>".
            "<tr><td>Membername:</td><td>$n</td></tr>".
            "<tr><td>Callsign:</td><td>$c</td></tr>".
            "<tr><td>Joining date:</td><td>$y-$m-$d</td></tr>".
            "<tr><td>Status:</td><td>active</td></tr>".
            "<tr><td>Admin:</td><td>$a</td></tr>".
            "<tr><td>Squadron/Staffel:</td><td>".$result["name"]."</td></tr>".
            "</table>";
    exit();
} 

if(filter_has_var(INPUT_POST, "membernamecheck")) {

    $n = filter_input(INPUT_POST, "membernamecheck");
    $sql = "SELECT id FROM acgmembers WHERE username = '" . $n . "' LIMIT 1";
    $querry = mysqli_query($dbx, $sql);
    $mname_check = mysqli_num_rows($querry);
    if($mname_check < 1) {

        echo "Membername available.";
        exit();
    } else {

        echo "Membername is already taken.";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "callsigncheck")) {

    $c = filter_input(INPUT_POST, "callsigncheck");
    $sql = "SELECT id FROM acgmembers WHERE callsign = '" . $c . "' LIMIT 1";
    $querry = mysqli_query($dbx, $sql);
    $callsign_check = mysqli_num_rows($querry);
    if($callsign_check < 1) {

        echo "Callsign available.";
        exit();
    } else {

        echo "Callsign is already taken";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "changeMembername")) {
    $edit_id = $_SESSION["edit_id"];
    $n = filter_input(INPUT_POST, "changeMembername");
    $sql = "SELECT id FROM acgmembers WHERE username = '$n' LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $mname_check = mysqli_num_rows($query);
    if($mname_check < 1) {

        if(changeMember("username", $n, $edit_id)) {
            echo "Membername changed.";
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {

        $result = mysqli_fetch_array($query);
        $check_ID = $result[0];
        if($check_ID !== $edit_id){
           echo "Membername is already taken.";
           exit(); 
        } else {
           echo "";
           exit();
        }    
    }
}

if(filter_has_var(INPUT_POST, "changeCallsign")) {
    $edit_id = $_SESSION["edit_id"];
    $c = filter_input(INPUT_POST, "changeCallsign");
    $sql = "SELECT id FROM acgmembers WHERE callsign = '$c' LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $callsign_check = mysqli_num_rows($query);
    if($callsign_check < 1) {

        if(changeMember("callsign", $c, $edit_id)) {
            echo "Callsign changed.";
            exit();
        } else
        {
            echo "Error during database manipulation.";
            exit();
        }
    } else {

        $result = mysqli_fetch_array($query);
        $check_ID = $result[0];

        if($check_ID !== $edit_id){
            echo "Callsign is already taken.";
            exit(); 
        } else {
            echo "";
            exit();
        }   
    }
}

if(filter_has_var(INPUT_POST, "changeAdmin")) {
    $edit_id = $_SESSION["edit_id"];
    $c = filter_input(INPUT_POST, "changeAdmin");
    $mb = filter_input(INPUT_POST, "mb");
    $mv = filter_input(INPUT_POST, "mv");
    $sql = "SELECT admin, mapViewer, missionBuilder FROM acgmembers WHERE id=$edit_id LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_array($query);
    $check_admin = $result[0];
    $check_mv = $result[1];
    $check_mb = $result[2];
    if($check_admin === $c && $check_mv === $mv && $check_mb === $mb){
        echo "";
        exit();
    } else {
        if(!$c) {
            $sql = "SELECT COUNT(admin) FROM acgmembers WHERE admin=true";
            $query = mysqli_query($dbx, $sql);
            $result = mysqli_fetch_array($query);
            if($result[0] < 2) {
                echo "Pilot and Mission database needs at least one admin.";
                exit();
            } 
        }
        if(changeMember("admin", $c, $edit_id)
            && changeMember("mapViewer", $mv, $edit_id)
            && changeMember("missionBuilder", $mb, $edit_id)) {
            echo "Member rights changed.";
            exit();
        } else {
            echo "Error during database manipulation.";
            exit();
        }
    }
}

if(filter_has_var(INPUT_POST, "deleteMember")){
    $edit_id = $_SESSION["edit_id"];
    $sql = "DELETE FROM acgmembers WHERE id='$edit_id'";
    if(mysqli_query($dbx, $sql)) {
        echo "deleted";
        exit();
    }
}

function changeMember($field, $value, $edit_id){

    global $dbx;
    $sql = "UPDATE acgmembers SET $field='$value' WHERE id='$edit_id'";
    return mysqli_query($dbx, $sql);
}

if(filter_has_var(INPUT_POST, "statusUpdate")) {
    $edit_id = $_SESSION["edit_id"];
    $r = filter_input(INPUT_POST, "statusUpdate");
    $y = filter_input(INPUT_POST, "y");
    $m = filter_input(INPUT_POST, "m");
    $d = filter_input(INPUT_POST, "d");
    $c = filter_input(INPUT_POST, "c", FILTER_CALLBACK, array("options"=>"pam_htmlentities_filter"));
    
    if($y==="" | $m==="" | $d===""){
        $sudate = date("Y-m-d");
    } else {
        $check_tDate = mktime(0, 0, 0, $m, $d, $y);
        $sudate = date("Y-m-d", $check_tDate);
    }
    
    $sql = "INSERT INTO memberstatuslog (memberID, statusID, date, comment) ".
            " VALUES ($edit_id, $r, '$sudate', '$c')";
    if(mysqli_query($dbx, $sql)) {
        echo "Status update successful.";
        exit();
    } else {
        echo "$sql Error during database manipulation.";
        exit();
    }   
}

if(filter_has_var(INPUT_POST, "deleteStatusUpdate")){
    $edit_id = $_SESSION["edit_id"];
    $id = filter_input(INPUT_POST, "deleteStatusUpdate");
    $sql = "DELETE FROM memberstatuslog WHERE id='$id'";
    if(mysqli_query($dbx, $sql)) {
        echo "Status update deleted";
        exit();
    }
}