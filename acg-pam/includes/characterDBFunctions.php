<?php
function randName($faction, $type, $dbx) {
    
    $sql = "SELECT name FROM namepool ".
           "WHERE faction = '$faction' AND type = '$type' ".
           "ORDER BY RAND() LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    if($query) {
        $result = mysqli_fetch_assoc($query);
        return $result["name"];
    } else {
        return FALSE;
    } 
}

function addCharacter($memberID, $fname, $lname, $dbx) {
    
    // If member is Sabre (ID==9)
    if($memberID == 9) {
        $fname = "Edwin";
        $lname = "Blackadder";
    }
    $sql = "INSERT INTO careercharacters (firstname, lastname, personifiedby, characterstatus) ".
           " VALUES ('$fname', '$lname', $memberID, 1)";
    if(mysqli_query($dbx, $sql)) {
        return mysqli_insert_id($dbx);
    } else {
        return FALSE;
    }   
}

function createCharacter($memberID, $faction, $dbx){
    
    $firstName = randName($faction, "FirstName", $dbx);
    $lastName = randName($faction, "LastName", $dbx);
    if($firstName !== "" && $lastName !== ""){
        return addCharacter($memberID, $firstName, $lastName, $dbx);
    } else {
        return FALSE;
    }

}

function getFaction($memberID, $dbx) {
    
    $sql = "SELECT squadrons.faction, UNIX_TIMESTAMP(transfers.transferdate) AS tstdate ".
           "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "WHERE memberid = $memberID ORDER BY tstdate DESC LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $numrow = mysqli_num_rows($query);
    if($numrow>0){
        $result = mysqli_fetch_assoc($query);
        return $result["faction"];
    } else {
        return FALSE;
    }
}

function getFactionAtMission($memberID, $missionID, $dbx) {
    
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $querry = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($querry);
    $realDate = $result["realDate"];
    
    $sql = "SELECT squadrons.faction, UNIX_TIMESTAMP(transfers.transferDate) AS tstdate ".
           "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "WHERE memberid = $memberID AND transfers.transferDate <= '$realDate' ".
           "ORDER BY tstdate DESC LIMIT 1";
    $query = mysqli_query($dbx, $sql);
 
    $numrow = mysqli_num_rows($query);
    if($numrow>0){
        $result = mysqli_fetch_assoc($query);
        return $result["faction"];
    } else {
        return FALSE;
    }
}

function getCharacterFaction($characterID, $dbx){
    
    $sql = "SELECT careercharacters.id, MAX(missions.realDate) AS mxrdate FROM careercharacters ". 
           "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
           "LEFT JOIN missions ON reports.missionID = missions.id ". 
           "WHERE careercharacters.id = $characterID";
    $query = mysqli_query($dbx, $sql);
    $numrow = mysqli_num_rows($query);
    if($numrow>0){
        $result = mysqli_fetch_assoc($query);
        $mxrdate = $result["mxrdate"];
        
        $sql = "SELECT squadrons.faction, transfers.transferdate FROM transfers ".
           "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "RIGHT JOIN careercharacters ON transfers.memberID = careercharacters.personifiedBy ".
           "WHERE careercharacters.id = $characterID ".
           "AND transfers.transferdate <= '$mxrdate' ORDER BY transferdate DESC LIMIT 1";
        $query = mysqli_query($dbx, $sql);
        $numrow = mysqli_num_rows($query);
        if($numrow>0){
            $result = mysqli_fetch_assoc($query);
            return $result["faction"];
        } else {
            return FALSE;
        }
    } else {
        return FALSE;
    }   
}

function getCharacterIDatMission($memberID, $missionID, $dbx){
    
    $sql="SELECT careercharacters.id, maxMissionTable.maxMission FROM acgmembers ".
         "LEFT JOIN careercharacters ON acgmembers.id = careercharacters.personifiedBy ".
         "LEFT JOIN ". 
            "(SELECT MAX(reports.missionID) AS maxMission, reports.authorID FROM reports ".
            "GROUP BY authorID) AS maxMissionTable ".
         "ON careercharacters.id = maxMissionTable.authorID ".
         "WHERE acgmembers.id = $memberID AND maxMissionTable.maxMission <= $missionID ".
         "ORDER BY maxMissionTable.maxMission DESC LIMIT 1 ";
    $querry = mysqli_query($dbx, $sql);
    $numrow = mysqli_num_rows($querry);
    if($numrow>0){
        $result = mysqli_fetch_assoc($querry);
        return $result["id"];
    } else {
//        echo $sql."<br>";
        return FALSE;
    }

}

function getMostRecentRankID($memberID, $dbx){
    
    $faction = getFaction($memberID, $dbx);
    $sql = "SELECT promotions.id, ranks.id AS rankID, promotions.date ".
           "FROM promotions LEFT JOIN ranks ON (promotions.value, ranks.faction) = (ranks.value, '$faction') ".
           "WHERE memberID = $memberID ORDER BY promotions.date DESC LIMIT 1";
    $querry = mysqli_query($dbx, $sql);
    $numrow = mysqli_num_rows($querry);
    if($numrow>0){
        $result = mysqli_fetch_assoc($querry);
        return $result["rankID"];
    } else {
        return FALSE;
    }
    
}   

function getRankValueAtMission($memberID, $missionID, $dbx){
    
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $querry = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($querry);
    $realDate = $result["realDate"];
    
    $sql = "SELECT promotions.value, promotions.date ".
           "FROM promotions ".
           "WHERE promotions.memberid = $memberID AND promotions.date < '$realDate'".
           "ORDER BY promotions.date DESC LIMIT 1";
    $querry = mysqli_query($dbx, $sql);
    $numrow = mysqli_num_rows($querry);
    if($numrow>0){
        $result = mysqli_fetch_assoc($querry);
        return $result["value"];
    } else {
        return FALSE;
    }
}

function getRankAbreviationAtMission($memberID, $missionID, $dbx){
    
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $querry = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($querry);
    $realDate = $result["realDate"];
    $faction = getFactionAtMission($memberID, $missionID, $dbx);
    
    $sql = "SELECT ranks.abreviation, promotions.date ".
           "FROM promotions ".
           "LEFT JOIN ranks ON (promotions.value, ranks.faction) = (ranks.value, '$faction') ". 
           "WHERE promotions.memberid = $memberID AND promotions.date < '$realDate' ".
           "ORDER BY promotions.date DESC LIMIT 1";
    $querry = mysqli_query($dbx, $sql);

    $numrow = mysqli_num_rows($querry);
    if($numrow>0){
        $result = mysqli_fetch_assoc($querry);
        return $result["abreviation"];
    } else {
        return FALSE;
    }
}

function getSquadronAtMission($memberID, $missionID, $dbx){
    
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $querry = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($querry);
    $realDate = $result["realDate"];
    
    $sql = "SELECT squadrons.name, transfers.transferDate ".
           "FROM transfers ".
           "LEFT JOIN squadrons ON transfers.squadronID = squadrons.id ".
           "WHERE transfers.memberID = $memberID AND transfers.transferDate <= '$realDate'".
           "ORDER BY promotions.date DESC LIMIT 1";
    $querry = mysqli_query($dbx, $sql);
    $numrow = mysqli_num_rows($querry);
    if($numrow>0){
        $result = mysqli_fetch_assoc($querry);
        return $result["name"];
    } else {
        return FALSE;
    }
}