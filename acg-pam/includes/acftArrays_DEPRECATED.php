<?php
$rafFlyable = array(
    "Hawker Hurricane I DH5-20",
    "Hawker Hurricane I DH5-20 100 OCT",
    "Hawker Hurricane IA Rotol",
    "Hawker Hurricane IA Rotol 100 OCT",
    "Supermarine Spitfire MK I",
    "Supermarine Spitfire MK I 100 OCT",
    "Supermarine Spitfire IA",
    "Supermarine Spitfire IA 100 OCT",
    "Supermarine Spitfire IIA",
    "Tiger Moth"
);

$lwFlyable = array(
    "BF 109 E-1",
    "BF 109 E-3",
    "BF 109 E-3/B",
    "BF 109 E-4",
    "BF 109 E-4/B",
    "BF 109 E-4/N",
    "BF 110 C-4",
    "BF 110 C-7",
    "JU 87 B-2",
    "JU 88 A-1",
    "HE 111 H-2",
    "HE 111 P-2"
);