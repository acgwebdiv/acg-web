<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/forumInteract.php');
session_start();
$dbx = getDBx();


if(filter_has_var(INPUT_POST, "sendAARReminder")){
    
    $id = filter_input(INPUT_POST, "sendAARReminder");
   
    $sql = "CALL `BoX_PAM_BlacklistDetail`($id)";
    $result = mysqli_query($dbx, $sql);
    
    if(mysqli_num_rows($result)>0){ 
        while($row = mysqli_fetch_assoc($result)) {
            
            $member_id = $row["id"];
            $m_name = $row["missionName"];
            $m_id = $row["missionID"];
            $sender_id = $_SESSION['userID'];
            
            $message = "This is an automatic created message. ".
                      "You are still missing an After Action Report for mission ".
                      "$m_id - $m_name. Please submit a report ".
                      "as soon as possible. Otherwise you might be banned from next mission.";
                       
            $notification = htmlspecialchars("[url=http://aircombatgroup.co.uk/acg-pam/missionReports.php?m_id=$m_id&sqn=$sqn]".$message."[/url]");

            //DEACTIVATE FOR LOCAL DEVELOPMENT
            sendphpbbpm_fromID($notification, $sender_id, $member_id, 'After action report reminder');
        }
    }

    echo "Reminder sent.";
    exit();
}