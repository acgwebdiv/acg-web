<?php
include_once(dirname(__FILE__).'/db_connect.php');
include_once(dirname(__FILE__).'/functions.php');
session_start();
$dbx = getDBx();

if(filter_has_var(INPUT_POST, "reportForDuty")) {
    $missionID = filter_input(INPUT_POST, "reportForDuty");
    $squadronID = filter_input(INPUT_POST, "s");
    $userID = filter_input(INPUT_POST, "u");
    $faction = filter_input(INPUT_POST, "f");
    
    // Get mission data.
    $sql = "SELECT campaignID FROM missions  WHERE id = $missionID";
    $query = mysqli_query($dbx, $sql);
    $mi_result = mysqli_fetch_assoc($query);
    $mi_campaignID = $mi_result["campaignID"];
    
    
    // Check if character is designated as author of the report
    if(isset($_SESSION["designatedCharacter"])){
        $c_id = $_SESSION["designatedCharacter"];
        unset($_SESSION["designatedCharacter"]);
    } else {
        $c_id = FALSE;
    }

    
    // If a designated Character is defined: us it; else investigate if new character is needed.
    if(!$c_id){

        // Check if member has an active character, if yes: get character info, else: generate new character
        $sql = "SELECT id FROM careercharacters ".
               "WHERE personifiedBy = $userID AND characterstatus = 1 ORDER BY id DESC LIMIT 1";
        $query = mysqli_query($dbx, $sql);
        $character_check = mysqli_num_rows($query);
        
        if($character_check < 1) {

            include_once(dirname(__FILE__).'/characterDBFunctions.php');
            $c_id = createCharacter($userID, $faction, $dbx);

        } else {

            $c_result = mysqli_fetch_assoc($query);
            $c_id = $c_result["id"];

            //Check if new campaign and if member wants new character or keep old
            $sql = "SELECT missions.campaignID FROM missions ".
                   "LEFT JOIN reports ON reports.missionID = missions.id ".
                   "WHERE authorID = $c_id ORDER BY missions.campaignID DESC LIMIT 1";
            $query = mysqli_query($dbx, $sql);
            $has_written_report_check = mysqli_num_rows($query);
            
            if($has_written_report_check > 0){

                $c_id_result = mysqli_fetch_assoc($query);
                $last_c_id = $c_id_result["campaignID"];
                if($last_c_id !== $mi_campaignID){
                   
                    $_SESSION['designatedCharacter'] = $c_id;
                    echo("newCharacterQuery");
                    die();
                }   
            }
        }  
    } 
    // Get character info
    $sql = "SELECT firstname, lastname FROM careercharacters ".
           "WHERE id = $c_id";
    $query = mysqli_query($dbx, $sql);
    $c_result = mysqli_fetch_assoc($query);
    $firstName = $c_result["firstname"];
    $lastName = $c_result["lastname"];

    unset($_SESSION["designatedCharacter"]);
    
    //Check if user is already in database for this mission and delete if neccessary.
    $sql = "SELECT attendance.id, careercharacters.personifiedBy ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "WHERE attendance.missionID = $missionID AND personifiedBy = $userID";
    $result = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_assoc($result);
        $id = $row["id"];
        $sql = "DELETE FROM attendance WHERE id = $id";
        mysqli_query($dbx, $sql);
    }
    
    $sql = "INSERT INTO attendance(missionID, characterID, squadronID, dispersalStatus) ".
           "VALUES ($missionID, $c_id, $squadronID, 0)";
    if(mysqli_query($dbx, $sql)) {
        echo "true";
        exit();
    } else {
        echo "Error during database manipulation. ".$sql;
        exit();
    }
}

if(filter_has_var(INPUT_POST, "cancelReportForDuty")) {
    $missionID = filter_input(INPUT_POST, "cancelReportForDuty");
    $userID = filter_input(INPUT_POST, "u");
    
    //Check if user is already in database for this mission and delete if neccessary.
    $sql = "SELECT attendance.id, careercharacters.personifiedBy ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "WHERE attendance.missionID = $missionID AND personifiedBy = $userID";
    $result = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_assoc($result);
        $id = $row["id"];
        $sql = "DELETE FROM attendance WHERE id = $id";
        if(mysqli_query($dbx, $sql)) {
            
            echo "true";
            exit();
        } else {
            echo "Error during database manipulation. ".$sql;
            exit();
        }
    }
}

if(filter_has_var(INPUT_POST, "updateButton")) {
    
    $userID = filter_input(INPUT_POST, "updateButton");
    $mi_id = filter_input(INPUT_POST, "mi");
    $squadronID = filter_input(INPUT_POST, "s");
    
    //Check if user is listed in attendance database for dispersal and check of
    //dispersal status
    $sql = "SELECT attendance.id, attendance.dispersalStatus, careercharacters.personifiedBy ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "WHERE attendance.missionID = $mi_id AND personifiedBy = $userID AND attendance.squadronID = $squadronID";
    $aresult = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($aresult)>0){
        echo "true";
        exit();
    } else {
        echo "false";
        exit();
    }
}

if(filter_has_var(INPUT_POST, "saveDispersalLW")) {
    $positionID = filter_input(INPUT_POST, "saveDispersalLW");
    $memberID = filter_input(INPUT_POST, "m");
    $squadronID = filter_input(INPUT_POST, "s");
    $missionID = filter_input(INPUT_POST, "mi");
    
    //Set current member on position to dispersalStatus 0
    $sql = "SELECT id FROM attendance WHERE missionID = $missionID AND squadronID = $squadronID ".
           "AND positionID = $positionID";
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query) > 0){
        $result = mysqli_fetch_assoc($query);
        $id = $result["id"];
        $sql = "UPDATE attendance SET dispersalStatus=0, positionID=NULL WHERE id='$id'";
        if(!mysqli_query($dbx, $sql)) {

            echo "Error during database manipulation (Dispersal status => 0) ".sql;
            exit();
        }
    }
    
    //Set member to position and dispersalStatus 1
    $sql = "SELECT attendance.id, careercharacters.personifiedBy ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "WHERE attendance.missionID = $missionID AND personifiedBy = $memberID AND squadronID = $squadronID";
    $query = mysqli_query($dbx, $sql);
    $member_check = mysqli_num_rows($query);
    if(mysqli_num_rows($query) > 0){
        $result = mysqli_fetch_assoc($query);
        $id = $result["id"];
        $sql = "UPDATE attendance SET dispersalStatus=1, positionID=$positionID ".
               "WHERE id = $id"; 
        if(mysqli_query($dbx, $sql)) {
            echo "Dispersal assignment saved.";
            exit();
        } else {
            echo "Error during database manipulation (Dispersal assignment positionID=".$positionID.") ".$sql;
            exit();
        }
    } else {
        
        if($memberID == 0){
            exit();
        } else {
            echo "Error during database manipulation (Dispersal member not found) ".$sql;
            exit();
        }
    }   
}

if(filter_has_var(INPUT_POST, "saveDispersalRAF")) {
    $positionID = filter_input(INPUT_POST, "saveDispersalRAF");
    $memberID = filter_input(INPUT_POST, "m");
    $squadronID = filter_input(INPUT_POST, "s");
    $missionID = filter_input(INPUT_POST, "mi");
    $flightID = filter_input(INPUT_POST, "flt");
    $sectionID = filter_input(INPUT_POST, "sect");
    
    //Set current member on position to dispersalStatus 0
    $sql = "SELECT id FROM attendance WHERE missionID = $missionID AND squadronID = $squadronID ".
           "AND positionID = $positionID";
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query) > 0){
        $result = mysqli_fetch_assoc($query);
        $id = $result["id"];
        $sql = "UPDATE attendance SET dispersalStatus=0, positionID=NULL WHERE id='$id'";
        if(!mysqli_query($dbx, $sql)) {

            echo "Error during database manipulation (Dispersal status => 0) ".sql;
            exit();
        }
    }
    
    //Set member to position and dispersalStatus 1
    $sql = "SELECT attendance.id, careercharacters.personifiedBy ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "WHERE attendance.missionID = $missionID AND personifiedBy = $memberID AND squadronID = $squadronID";
    $query = mysqli_query($dbx, $sql);
    $member_check = mysqli_num_rows($query);
    if(mysqli_num_rows($query) > 0){
        $result = mysqli_fetch_assoc($query);
        $id = $result["id"];
        $sql = "UPDATE attendance SET dispersalStatus=1, positionID=$positionID, ".
               "flightsraf=$flightID, sectionraf=$sectionID ".
               "WHERE id = $id"; 
        if(mysqli_query($dbx, $sql)) {
            echo "Dispersal assignment saved.";
            exit();
        } else {
            echo "Error during database manipulation (Dispersal assignment positionID=".$positionID.") ".$sql;
            exit();
        }
    } else {
        
        if($memberID == 0){
            exit();
        } else {
            echo "Error during database manipulation (Dispersal member not found) ".$sql;
            exit();
        }
    }   
}

if(filter_has_var(INPUT_POST, "addPilot")) {
    $pilot = filter_input(INPUT_POST, "addPilot");
    $missionID = filter_input(INPUT_POST, "m");
    $squadronID = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT name FROM squadrons WHERE id = $squadronID";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        $sqnName = $row["name"];
    }
    
    $sql = "SELECT id FROM acgmembers WHERE callsign = '$pilot'";
    $query = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($query);
    $userID = $result["id"];
    
    //Get id of second most recent mission 
    $sql = "SELECT missions.id FROM missions ORDER BY id desc LIMIT 2";
    $query = mysqli_query($dbx, $sql);
    $m_result = mysqli_fetch_all($query);
    $secondMostRecentMissionID = $m_result[1];      
    
    //Get latest character at time of mission
    $sql = "SELECT careercharacters.id, reports.missionID, reports.pilotStatus FROM careercharacters ".
    "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
    "WHERE personifiedBy = $userID AND missionID <= $missionID ORDER BY missionID desc LIMIT 1";
    $query = mysqli_query($dbx, $sql);
    $character_check = mysqli_num_rows($query);
    if($character_check < 1) {

        include_once(dirname(__FILE__).'/characterDBFunctions.php');
        $c_id = createCharacter($userID, $faction, $dbx);

    } else {

        $c_result = mysqli_fetch_assoc($query);
        $c_id = $c_result["id"];
        $checkMissionID = $c_result["missionID"];
        $checkPilotStatus = $c_result["pilotStatus"];
        if(($checkMissionID == $secondMostRecentMissionID)&& ($checkPilotStatus > 2 )){
            include_once(dirname(__FILE__).'/characterDBFunctions.php');
            $c_id = createCharacter($userID, $faction, $dbx);
        }
    }  

    // Get character info
    $sql = "SELECT firstname, lastname FROM careercharacters ".
           "WHERE id = $c_id";
    $query = mysqli_query($dbx, $sql);
    $c_result = mysqli_fetch_assoc($query);
    $firstName = $c_result["firstname"];
    $lastName = $c_result["lastname"];
    
    //Check if user is already in database for this mission and delete if neccessary.
    $sql = "SELECT attendance.id, careercharacters.personifiedBy ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "WHERE attendance.missionID = $missionID AND personifiedBy = $userID";
    $result = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_assoc($result);
        $id = $row["id"];
        $sql = "DELETE FROM attendance WHERE id = $id";
        mysqli_query($dbx, $sql);
    }
    
    $sql = "INSERT INTO attendance(missionID, characterID, squadronID, dispersalStatus) ".
           "VALUES ($missionID, $c_id, $squadronID, 0)";
    if(mysqli_query($dbx, $sql)) {
        echo "true";
        exit();
    } else {
        echo "Error during database manipulation. ".$sql;
        exit();
    }
}

//Script under testing

if(filter_has_var(INPUT_POST, "getUPilots")) {
    $mi_id = filter_input(INPUT_POST, "getUPilots");
    $sqn = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT name FROM squadrons WHERE id = $sqn";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        $sqnName = $row["name"];
    }
    
    $sql = "SELECT missions.realdate ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_realDate = $m_row["realdate"];
    
    //Get unassgned pilots (dispersalStatus = 0)
    $sql = "SELECT attendance.id, acgmembers.callsign, ".
           "careercharacters.personifiedBy, ".
           "careercharacters.lastName, careercharacters.firstName, ranks.abreviation, ranks.value ". 
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN ".
                    "(SELECT promotions.memberid, promotions.value FROM promotions ".
                    "JOIN ".
                        "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                        "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                    "ON (currentrank.memberid, currentrank.pdate) = ".
                    "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
               "ON currentrank2.memberid = careercharacters.personifiedBy ".
           "LEFT JOIN ranks ON (currentrank2.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ".
           "AND attendance.dispersalStatus = 0 ".
           "ORDER BY ranks.value DESC";
    $upresult = mysqli_query($dbx, $sql);
    $uparray = mysqli_fetch_all($upresult);
    echo json_encode($uparray);
    exit();
    
}

if(filter_has_var(INPUT_POST, "getAPilotsDisplayRAF")) {
    
    $mi_id = filter_input(INPUT_POST, "getAPilotsDisplayRAF");
    $sqn = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT name FROM squadrons WHERE id = $sqn";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        $sqnName = $row["name"];
    }
    
    $sql = "SELECT missions.realdate ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_realDate = $m_row["realdate"];
    
    //Get all pilots
    $sql = "SELECT attendance.id, attendance.characterID, acgmembers.callsign, attendance.positionID, ".
           "attendance.flightsraf, attendance.sectionraf, ranks.value, hangar.designation, ".
           "attendance.dispersalStatus, ".
           "careercharacters.personifiedBy, careercharacters.lastName, careercharacters.firstName, ranks.abreviation ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN hangar ON careercharacters.personifiedBy = hangar.memberID ".
           "LEFT JOIN ".
                    "(SELECT promotions.memberid, promotions.value FROM promotions ".
                    "JOIN ".
                        "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                        "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                    "ON (currentrank.memberid, currentrank.pdate) = ".
                    "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
               "ON currentrank2.memberid = attendance.characterID ".
           "LEFT JOIN ranks ON (currentrank2.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ".
           "ORDER BY attendance.positionID";
    
    $apresult = mysqli_query($dbx, $sql);
    $roster_array = array(); //Array linking position in dispersal to memberID
    $memberArray = array(); //Array linking memberID to callsign
    while($row = mysqli_fetch_assoc($apresult)) {
        if(!is_null($row["positionID"])){
            $roster_array[$row["positionID"]] = $row["personifiedBy"];
        }
        $memberArray[$row["personifiedBy"]]["name"] = $row["firstName"]." '".$row["callsign"]."' ".$row["lastName"];
        $memberArray[$row["personifiedBy"]]["rank"] = $row["abreviation"];
        $memberArray[$row["personifiedBy"]]["rankValue"] = $row["value"];
        $memberArray[$row["personifiedBy"]]["dispersalStatus"] = $row["dispersalStatus"];
        $memberArray[$row["personifiedBy"]]["flightsraf"] = $row["flightsraf"];
        $memberArray[$row["personifiedBy"]]["sectionraf"] = $row["sectionraf"];
        $memberArray[$row["personifiedBy"]]["designation"] = $row["designation"];
    }
    
    // Access al flight info from database for form input.
    $sql = "SELECT id, name FROM flightsraf";
    $flight_result = mysqli_query($dbx, $sql);
    $flightIDArray = array();
    $flightIDArray[] = array("0", "");
    while($row = mysqli_fetch_assoc($flight_result)){
        $flightIDArray[] = array($row["id"], $row["name"]);
    }
    unset($flightIDArray[3]);

    // Access al section info from database for form input.
    $sql = "SELECT id, name FROM sectionraf";
    $section_result = mysqli_query($dbx, $sql);
    $sectionIDArray = array();
    $sectionIDArray[] = array("0", "");
    $sectionASSOCArray = array();
    while($row = mysqli_fetch_assoc($section_result)){
        $sectionIDArray[] = array($row["id"], $row["name"]);
        $sectionASSOCArray[$row["id"]] = $row["name"];
    }
    unset($sectionIDArray[9]);
    
    $displayArray = array();
    $aFlt = array();
    $bFlt = array();
    foreach ($roster_array as $positionID => $memberID) {

        if($memberArray[$memberID]["dispersalStatus"]==1){

            //Getting members of A FLT
            if($memberArray[$memberID]["flightsraf"]==1){

                $name = $memberArray[$memberID]["name"];
                $rank = $memberArray[$memberID]["rank"];
                $rankValue = $memberArray[$memberID]["rankValue"];
                $section = $sectionASSOCArray[$memberArray[$memberID]["sectionraf"]];
                $designation = $memberArray[$memberID]["designation"];
                $aFlt[] = array("position"=>$positionID, "name"=>$name, "rank"=>$rank,
                    "rankValue"=>$rankValue, "section"=>$section, "designation"=>$designation);
            } else if($memberArray[$memberID]["flightsraf"]==2){

                $name = $memberArray[$memberID]["name"];
                $rank = $memberArray[$memberID]["rank"];
                $rankValue = $memberArray[$memberID]["rankValue"];
                $section = $sectionASSOCArray[$memberArray[$memberID]["sectionraf"]];
                $designation = $memberArray[$memberID]["designation"];
                $bFlt[] = array("position"=>$positionID, "name"=>$name, "rank"=>$rank,
                    "rankValue"=>$rankValue, "section"=>$section, "designation"=>$designation);
            }
        }
    }
    if(count($aFlt)>0){
        $displayArray[] = array("<h3>A Flight</h3>");
    }
    $currentSection = "";
    for($n = 0; $n < count($aFlt); $n++) {
        
        $pilotArray = $aFlt[$n];
        $rowArray = array();
        if($pilotArray["section"] != $currentSection){
            $currentSection = $pilotArray["section"];
            $currentPosition = 1;
            $displayArray[] =array("<b>".$currentSection."<b>");
            
            if($pilotArray["position"] == 1){
               $rowArray[] = ("OC ".$sqnName." Sqn"); 
            } else if($n == 0) {
               $rowArray[] = ("OC A Flt"); 
            } else {
               if($pilotArray["rankValue"]<8){
                   $rowArray[] = ("NCO IC Sect");
               } else {
                   $rowArray[] = ("OIC Sect");
               }
            }
        } else {
            $rowArray[] = ($currentPosition);
        }
        $rowArray[] = ($pilotArray["rank"]);
        $rowArray[] = ($pilotArray["name"]." <br>(".$pilotArray["designation"].")");
        $displayArray[] = $rowArray;
        $currentPosition++;
    }
    if(count($bFlt)>0){
        $displayArray[] = array("<h3>B Flight</h3>");
    }
    $currentSection = "";
    for($n = 0; $n < count($bFlt); $n++) {

        $pilotArray = $bFlt[$n];
        $rowArray = array();
        if($pilotArray["section"] != $currentSection){
            $currentSection = $pilotArray["section"];
            $currentPosition = 1;
            $displayArray[] = array("<b>".$currentSection."<b>");
            
            if($pilotArray["position"] == 1){
               $rowArray[] = ("OC ".$sqnName." Sqn"); 
            } else if($n == 0) {
               $rowArray[] = ("OC B Flt"); 
            } else {
               if($pilotArray["rankValue"]<8){
                   $rowArray[] = ("NCO IC Sect");
               } else {
                   $rowArray[] = ("OIC Sect");
               }
            }
        } else {
            $rowArray[] = ($currentPosition);
        }
        $rowArray[] = ($pilotArray["rank"]);
        $rowArray[] = ($pilotArray["name"]." <br>(".$pilotArray["designation"].")");
        $displayArray[] = $rowArray;
        $currentPosition++;
    }   

    echo json_encode($displayArray);
    exit();
    
}

if(filter_has_var(INPUT_POST, "getAPilotsSetupRAF")) {
    
    $mi_id = filter_input(INPUT_POST, "getAPilotsSetupRAF");
    $sqn = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT missions.realdate ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_realDate = $m_row["realdate"];
    
    //Get all pilots
    $sql = "SELECT attendance.id, attendance.characterID, acgmembers.callsign, attendance.positionID, ".
           "attendance.flightsraf, attendance.sectionraf, ranks.value, ".
           "attendance.dispersalStatus, ".
           "careercharacters.personifiedBy, careercharacters.lastName, careercharacters.firstName, ranks.abreviation ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN ".
                    "(SELECT promotions.memberid, promotions.value FROM promotions ".
                    "JOIN ".
                        "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                        "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                    "ON (currentrank.memberid, currentrank.pdate) = ".
                    "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
               "ON currentrank2.memberid = attendance.characterID ".
           "LEFT JOIN ranks ON (currentrank2.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ".
           "ORDER BY attendance.positionID";
    
    $apresult = mysqli_query($dbx, $sql);
    $roster_array = array(); //Array linking position in dispersal to memberID
    $memberIDArray = array(); //Array with memberID and callsign
    $memberIDArray[] = array(0, "");
    $memberArray = array(); //Array linking memberID to callsign
    while($row = mysqli_fetch_assoc($apresult)) {
        if(!is_null($row["positionID"])){
            $roster_array[$row["positionID"]] = $row["personifiedBy"];
        }
        $memberIDArray[] = array($row["personifiedBy"], $row["callsign"]);
        $memberArray[$row["personifiedBy"]]["name"] = $row["firstName"]." '".$row["callsign"]."' ".$row["lastName"];
        $memberArray[$row["personifiedBy"]]["rank"] = $row["abreviation"];
        $memberArray[$row["personifiedBy"]]["rankValue"] = $row["value"];
        $memberArray[$row["personifiedBy"]]["dispersalStatus"] = $row["dispersalStatus"];
        $memberArray[$row["personifiedBy"]]["flightsraf"] = $row["flightsraf"];
        $memberArray[$row["personifiedBy"]]["sectionraf"] = $row["sectionraf"];
    }
    
    // Access al flight info from database for form input.
    $sql = "SELECT id, name FROM flightsraf";
    $flight_result = mysqli_query($dbx, $sql);
    $flightIDArray = array();
    $flightIDArray[] = array("0", "");
    while($row = mysqli_fetch_assoc($flight_result)){
        $flightIDArray[] = array($row["id"], $row["name"]);
    }
    unset($flightIDArray[3]);

    // Access al section info from database for form input.
    $sql = "SELECT id, name FROM sectionraf";
    $section_result = mysqli_query($dbx, $sql);
    $sectionIDArray = array();
    $sectionIDArray[] = array("0", "");
    $sectionASSOCArray = array();
    while($row = mysqli_fetch_assoc($section_result)){
        $sectionIDArray[] = array($row["id"], $row["name"]);
        $sectionASSOCArray[$row["id"]] = $row["name"];
    }
    unset($sectionIDArray[9]);
    
    $setupArray = array();
    $position = array(4, 3, 2, 1);
    $setupArray[] = array("<b>Flight/Section</b>",
                           flightSelectForm("flightDispersal", "flight1", $position, 
                           $roster_array, $memberArray, "flightsraf", $flightIDArray),
                           flightSelectForm("sectionDispersal", "section1", $position,
                           $roster_array, $memberArray, "sectionraf", $sectionIDArray)); 
    
    $setupArray[] = array("OC Sqn",
                           memberSelectForm(1, $roster_array, $memberIDArray));
    $setupArray[] = array("2",
                           memberSelectForm(2, $roster_array, $memberIDArray));
    $setupArray[] = array("3",
                           memberSelectForm(3, $roster_array, $memberIDArray));
    $setupArray[] = array("4",
                           memberSelectForm(4, $roster_array, $memberIDArray));
    
    $position = array(8, 7, 6, 5);
    $setupArray[] = array("<b>Flight/Section</b>",
                           flightSelectForm("flightDispersal", "flight2", $position, 
                           $roster_array, $memberArray, "flightsraf", $flightIDArray),
                           flightSelectForm("sectionDispersal", "section2", $position,
                           $roster_array, $memberArray, "sectionraf", $sectionIDArray)); 
    
    $setupArray[] = array("OC Flt/Sect",
                           memberSelectForm(5, $roster_array, $memberIDArray));
    $setupArray[] = array("2",
                           memberSelectForm(6, $roster_array, $memberIDArray));
    $setupArray[] = array("3",
                           memberSelectForm(7, $roster_array, $memberIDArray));
    $setupArray[] = array("4",
                           memberSelectForm(8, $roster_array, $memberIDArray));
    
    $position = array(12, 11, 10, 9);
    $setupArray[] = array("<b>Flight/Section</b>",
                           flightSelectForm("flightDispersal", "flight3", $position, 
                           $roster_array, $memberArray, "flightsraf", $flightIDArray),
                           flightSelectForm("sectionDispersal", "section3", $position,
                           $roster_array, $memberArray, "sectionraf", $sectionIDArray)); 
    
    $setupArray[] = array("OC Flt/Sect",
                           memberSelectForm(9, $roster_array, $memberIDArray));
    $setupArray[] = array("2",
                           memberSelectForm(10, $roster_array, $memberIDArray));
    $setupArray[] = array("3",
                           memberSelectForm(11, $roster_array, $memberIDArray));
    $setupArray[] = array("4",
                           memberSelectForm(12, $roster_array, $memberIDArray));
                
    $position = array(16, 15, 14, 13);
    $setupArray[] = array("<b>Flight/Section</b>",
                           flightSelectForm("flightDispersal", "flight4", $position, 
                           $roster_array, $memberArray, "flightsraf", $flightIDArray),
                           flightSelectForm("sectionDispersal", "section4", $position,
                           $roster_array, $memberArray, "sectionraf", $sectionIDArray)); 
    
    $setupArray[] = array("OC Flt/Sect",
                           memberSelectForm(13, $roster_array, $memberIDArray));
    $setupArray[] = array("2",
                           memberSelectForm(14, $roster_array, $memberIDArray));
    $setupArray[] = array("3",
                           memberSelectForm(15, $roster_array, $memberIDArray));
    $setupArray[] = array("4",
                           memberSelectForm(16, $roster_array, $memberIDArray));
    
    $position = array(20, 19, 18, 17);
    $setupArray[] = array("<b>Flight/Section</b>",
                           flightSelectForm("flightDispersal", "flight5", $position, 
                           $roster_array, $memberArray, "flightsraf", $flightIDArray),
                           flightSelectForm("sectionDispersal", "section5", $position,
                           $roster_array, $memberArray, "sectionraf", $sectionIDArray)); 
    
    $setupArray[] = array("OC Flt/Sect",
                           memberSelectForm(17, $roster_array, $memberIDArray));
    $setupArray[] = array("2",
                           memberSelectForm(18, $roster_array, $memberIDArray));
    $setupArray[] = array("3",
                           memberSelectForm(19, $roster_array, $memberIDArray));
    $setupArray[] = array("4",
                           memberSelectForm(20, $roster_array, $memberIDArray));

    echo json_encode($setupArray);
    exit();
    
}

if(filter_has_var(INPUT_POST, "getAPilotsDisplayLW")) {
    
    $mi_id = filter_input(INPUT_POST, "getAPilotsDisplayLW");
    $sqn = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT name FROM squadrons WHERE id = $sqn";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        $sqnName = $row["name"];
    }
    
    $sql = "SELECT missions.realdate ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_realDate = $m_row["realdate"];
    
    //Get all pilots
    $sql = "SELECT attendance.id, attendance.characterID, acgmembers.callsign, attendance.positionID, ".
           "attendance.flightsraf, attendance.sectionraf, ranks.value, hangar.designation, ".
           "attendance.dispersalStatus, ".
           "careercharacters.personifiedBy, careercharacters.lastName, careercharacters.firstName, ranks.abreviation ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN hangar ON careercharacters.personifiedBy = hangar.memberID ".
           "LEFT JOIN ".
                    "(SELECT promotions.memberid, promotions.value FROM promotions ".
                    "JOIN ".
                        "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                        "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                    "ON (currentrank.memberid, currentrank.pdate) = ".
                    "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
               "ON currentrank2.memberid = attendance.characterID ".
           "LEFT JOIN ranks ON (currentrank2.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ".
           "ORDER BY attendance.positionID";
    
    $apresult = mysqli_query($dbx, $sql);
    $roster_array = array(); //Array linking position in dispersal to memberID
    $memberArray = array(); //Array linking memberID to callsign
    while($row = mysqli_fetch_assoc($apresult)) {
        if(!is_null($row["positionID"])){
            $roster_array[$row["positionID"]] = $row["personifiedBy"];
        }
        $memberArray[$row["personifiedBy"]]["name"] = $row["firstName"]." '".$row["callsign"]."' ".$row["lastName"];
        $memberArray[$row["personifiedBy"]]["rank"] = $row["abreviation"];
        $memberArray[$row["personifiedBy"]]["rankValue"] = $row["value"];
        $memberArray[$row["personifiedBy"]]["dispersalStatus"] = $row["dispersalStatus"];
        $memberArray[$row["personifiedBy"]]["flightsraf"] = $row["flightsraf"];
        $memberArray[$row["personifiedBy"]]["sectionraf"] = $row["sectionraf"];
        $memberArray[$row["personifiedBy"]]["designation"] = $row["designation"];
    }
    
    $displayArray = array();
    $currentSchwarm = 0;
    foreach ($roster_array as $positionID => $memberID){
        
        if($memberArray[$memberID]["dispersalStatus"]==1){
            
            $schwarmInfo = getSchwarmInfo($positionID);
            if($currentSchwarm != $schwarmInfo[0]){
                $currentSchwarm = $schwarmInfo[0];
                $displayArray[] = array("<b>".$currentSchwarm.".Schwarm<b>");
            }
            $displayArray[] = array(htmlentities($schwarmInfo[2]),
                                    $memberArray[$memberID]["rank"],
                                    $memberArray[$memberID]["name"]." <br>(".$memberArray[$memberID]["designation"].")");
        }
    }
    echo json_encode($displayArray);
    exit();
    
}

if(filter_has_var(INPUT_POST, "getAPilotsSetupLW")) {
    
    $mi_id = filter_input(INPUT_POST, "getAPilotsSetupLW");
    $sqn = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT missions.realdate ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_realDate = $m_row["realdate"];
    
    //Get all pilots
    $sql = "SELECT attendance.id, attendance.characterID, acgmembers.callsign, attendance.positionID, ".
           "attendance.flightsraf, attendance.sectionraf, ranks.value, ".
           "attendance.dispersalStatus, ".
           "careercharacters.personifiedBy, careercharacters.lastName, careercharacters.firstName, ranks.abreviation ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN ".
                    "(SELECT promotions.memberid, promotions.value FROM promotions ".
                    "JOIN ".
                        "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                        "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                    "ON (currentrank.memberid, currentrank.pdate) = ".
                    "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
               "ON currentrank2.memberid = attendance.characterID ".
           "LEFT JOIN ranks ON (currentrank2.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ".
           "ORDER BY attendance.positionID";
    
    $apresult = mysqli_query($dbx, $sql);
    $roster_array = array(); //Array linking position in dispersal to memberID
    $memberIDArray = array(); //Array with memberID and callsign
    $memberIDArray[] = array(0, "");
    $memberArray = array(); //Array linking memberID to callsign
    while($row = mysqli_fetch_assoc($apresult)) {
        if(!is_null($row["positionID"])){
            $roster_array[$row["positionID"]] = $row["personifiedBy"];
        }
        $memberIDArray[] = array($row["personifiedBy"], $row["callsign"]);
    }
    
    $setupArray = array();
    $setupArray[] = array("<b>1.Schwarm</b>");
    $setupArray[] = array(htmlentities("Staffelkapitän"), memberSelectForm(1, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(2, $roster_array, $memberIDArray));
    $setupArray[] = array(htmlentities("Rottenführer"), memberSelectForm(3, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(4, $roster_array, $memberIDArray));
    
    $setupArray[] = array("<b>2.Schwarm</b>");
    $setupArray[] = array(htmlentities("Schwarmführer"), memberSelectForm(5, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(6, $roster_array, $memberIDArray));
    $setupArray[] = array(htmlentities("Rottenführer"), memberSelectForm(7, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(8, $roster_array, $memberIDArray));
   
    $setupArray[] = array("<b>3.Schwarm</b>");
    $setupArray[] = array(htmlentities("Schwarmführer"), memberSelectForm(9, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(10, $roster_array, $memberIDArray));
    $setupArray[] = array(htmlentities("Rottenführer"), memberSelectForm(11, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(12, $roster_array, $memberIDArray));
    
    $setupArray[] = array("<b>4.Schwarm</b>");
    $setupArray[] = array(htmlentities("Schwarmführer"), memberSelectForm(13, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(14, $roster_array, $memberIDArray));
    $setupArray[] = array(htmlentities("Rottenführer"), memberSelectForm(15, $roster_array, $memberIDArray));
    $setupArray[] = array("Katschmarek", memberSelectForm(16, $roster_array, $memberIDArray));
    
    echo json_encode($setupArray);
    exit();
}

if(filter_has_var(INPUT_POST, "getAPilotsDisplayLWStab")) {
    
    $mi_id = filter_input(INPUT_POST, "getAPilotsDisplayLWStab");
    $sqn = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT name FROM squadrons WHERE id = $sqn";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        $sqnName = $row["name"];
    }
    
    $sql = "SELECT missions.realdate ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_realDate = $m_row["realdate"];
    
    //Get all pilots
    $sql = "SELECT attendance.id, attendance.characterID, acgmembers.callsign, attendance.positionID, ".
           "attendance.flightsraf, attendance.sectionraf, ranks.value, hangar.designation, ".
           "attendance.dispersalStatus, ".
           "careercharacters.personifiedBy, careercharacters.lastName, careercharacters.firstName, ranks.abreviation ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN hangar ON careercharacters.personifiedBy = hangar.memberID ".
           "LEFT JOIN ".
                    "(SELECT promotions.memberid, promotions.value FROM promotions ".
                    "JOIN ".
                        "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                        "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                    "ON (currentrank.memberid, currentrank.pdate) = ".
                    "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
               "ON currentrank2.memberid = attendance.characterID ".
           "LEFT JOIN ranks ON (currentrank2.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ".
           "ORDER BY attendance.positionID";
    
    $apresult = mysqli_query($dbx, $sql);
    $roster_array = array(); //Array linking position in dispersal to memberID
    $memberArray = array(); //Array linking memberID to callsign
    while($row = mysqli_fetch_assoc($apresult)) {
        if(!is_null($row["positionID"])){
            $roster_array[$row["positionID"]] = $row["personifiedBy"];
        }
        $memberArray[$row["personifiedBy"]]["name"] = $row["firstName"]." '".$row["callsign"]."' ".$row["lastName"];
        $memberArray[$row["personifiedBy"]]["rank"] = $row["abreviation"];
        $memberArray[$row["personifiedBy"]]["rankValue"] = $row["value"];
        $memberArray[$row["personifiedBy"]]["dispersalStatus"] = $row["dispersalStatus"];
        $memberArray[$row["personifiedBy"]]["flightsraf"] = $row["flightsraf"];
        $memberArray[$row["personifiedBy"]]["sectionraf"] = $row["sectionraf"];
        $memberArray[$row["personifiedBy"]]["designation"] = $row["designation"];
    }
    
    $displayArray = array();
    $currentSchwarm = 0;
    foreach ($roster_array as $positionID => $memberID){
        
        if($memberArray[$memberID]["dispersalStatus"]==1){
            
            $schwarmInfo = getStabSchwarmInfo($positionID);
            if($currentSchwarm != $schwarmInfo[0]){
                $currentSchwarm = $schwarmInfo[0];
                $displayArray[] = array("<b>".$currentSchwarm.".Schwarm<b>");
            }
            $displayArray[] = array(htmlentities($schwarmInfo[2]),
                                    $memberArray[$memberID]["rank"],
                                    $memberArray[$memberID]["name"]." <br>(".$memberArray[$memberID]["designation"].")");
        }
    }
    
    echo json_encode($displayArray);
    exit();
    
}

if(filter_has_var(INPUT_POST, "getAPilotsSetupLWStab")) {
    
    $mi_id = filter_input(INPUT_POST, "getAPilotsSetupLWStab");
    $sqn = filter_input(INPUT_POST, "s");
    $faction = filter_input(INPUT_POST, "f");
    
    $sql = "SELECT missions.realdate ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_realDate = $m_row["realdate"];
    
    //Get all pilots
    $sql = "SELECT attendance.id, attendance.characterID, acgmembers.callsign, attendance.positionID, ".
           "attendance.flightsraf, attendance.sectionraf, ranks.value, ".
           "attendance.dispersalStatus, ".
           "careercharacters.personifiedBy, careercharacters.lastName, careercharacters.firstName, ranks.abreviation ".
           "FROM attendance ".
           "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".  
           "LEFT JOIN ".
                    "(SELECT promotions.memberid, promotions.value FROM promotions ".
                    "JOIN ".
                        "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                        "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                    "ON (currentrank.memberid, currentrank.pdate) = ".
                    "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
               "ON currentrank2.memberid = attendance.characterID ".
           "LEFT JOIN ranks ON (currentrank2.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ".
           "ORDER BY attendance.positionID";
    
    $apresult = mysqli_query($dbx, $sql);
    $roster_array = array(); //Array linking position in dispersal to memberID
    $memberIDArray = array(); //Array with memberID and callsign
    $memberIDArray[] = array(0, "");
    $memberArray = array(); //Array linking memberID to callsign
    while($row = mysqli_fetch_assoc($apresult)) {
        if(!is_null($row["positionID"])){
            $roster_array[$row["positionID"]] = $row["personifiedBy"];
        }
        $memberIDArray[] = array($row["personifiedBy"], $row["callsign"]);
    }
    
    $setupArray = array();
    $setupArray[] = array("Kommodore", memberSelectForm(1, $roster_array, $memberIDArray));
    $setupArray[] = array("Adjutant", memberSelectForm(2, $roster_array, $memberIDArray));
    $setupArray[] = array("Operations Offizier", memberSelectForm(3, $roster_array, $memberIDArray));
    $setupArray[] = array("Technischer Offizier", memberSelectForm(4, $roster_array, $memberIDArray));
       
    echo json_encode($setupArray);
    exit();
}

function getSchwarmInfo($position){
    
    
    switch ($position) {
        case 1:
            return array(1, 1, 'Staffelführer');
        case 2:
            return array(1, 2, 'Katschmarek');
        case 3:
            return array(1, 3, 'Rottenführer');
        case 4:
            return array(1, 4, 'Katschmarek');
        case 5:
            return array(2, 1, 'Schwarmführer');
        case 6:
            return array(2, 2, 'Katschmarek');
        case 7:
            return array(2, 3, 'Rottenführer');
        case 8:
            return array(2, 4, 'Katschmarek');
        case 9:
            return array(3, 1, 'Schwarmführer');
        case 10:
            return array(3, 2, 'Katschmarek');
        case 11:
            return array(3, 3, 'Rottenführer');
        case 12:
            return array(3, 4, 'Katschmarek');
        case 13:
            return array(4, 1, 'Schwarmführer');
        case 14:
            return array(4, 2, 'Katschmarek');
        case 15:
            return array(4, 3, 'Rottenführer');
        case 16:
            return array(4, 4, 'Katschmarek');
        }
}

function getStabSchwarmInfo($position){
    
    
    switch ($position) {
        case 1:
            return array(1, 1, 'Kommodore');
        case 2:
            return array(1, 2, 'Adjutant');
        case 3:
            return array(1, 3, 'Operations Offizier');
        case 4:
            return array(1, 4, 'Technischer Offizier');
        }
}

function flightSelectForm($name, $id, $position, $roster_array, $memberArray, $searchElement, $elementIDArray){
    
    $elementID = 0;
    foreach ($position as $positionID) {
        if(array_key_exists($positionID, $roster_array)){
            $memberID = $roster_array[$positionID];
            $elementID = $memberArray[$memberID][$searchElement];
        }
    }
    return getSelectFormWithName($name, $id, $elementIDArray, $elementID);
}

function memberSelectForm($position, $roster_array, $memberIDArray){
    if(array_key_exists($position, $roster_array)){
        $memberID = $roster_array[$position];
    } else {
        $memberID = 0;
    }
    return getSelectFormWithName("dispersal", $position, $memberIDArray, $memberID);
}