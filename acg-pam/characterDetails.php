<?php
    include(dirname(__FILE__).'/header0.php');
    
    $user_id = filter_input(INPUT_GET, "c_id");
    $dbx = getDBx();
    $sql = "SELECT careercharacters.firstName, acgmembers.callsign, careercharacters.lastName, ".
           "characterstatus.status AS cstatus, ".
           "MAX(missions.realDate) AS mxrdate, MIN(missions.realDate) AS mnrdate ".
           "FROM careercharacters ". 
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "LEFT JOIN characterstatus ON careercharacters.characterstatus = characterstatus.id ".
           "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
           "LEFT JOIN missions ON reports.missionID = missions.id ". 
           "WHERE careercharacters.id = $user_id";
//    echo $sql;
    $mresult = mysqli_query($dbx, $sql);
    $mrow = mysqli_fetch_assoc($mresult);
    $mxrdate = $mrow["mxrdate"];
    $mnrdate = $mrow["mnrdate"];
    
    $sql = "SELECT campaigns.id, campaigns.name, ".
           "MAX(missions.histDate) AS mxhdate, MIN(missions.histDate) AS mnhdate ".
           "FROM careercharacters ". 
           "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
           "LEFT JOIN missions ON reports.missionID = missions.id ".
           "LEFT JOIN campaigns ON campaigns.id = missions.campaignID ".
           "WHERE careercharacters.id = $user_id GROUP BY campaigns.id";
//    echo $sql;
    $dresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT squadrons.faction, transfers.transferdate FROM transfers ".
           "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "RIGHT JOIN careercharacters ON transfers.memberID = careercharacters.personifiedBy ".
           "WHERE careercharacters.id = $user_id ".
           "AND transfers.transferdate <= '$mxrdate' ORDER BY transferdate DESC LIMIT 1";
    $fresult = mysqli_query($dbx, $sql);
    $frow = mysqli_fetch_assoc($fresult);
    $faction = $frow["faction"];
//    echo $sql;
    
    // Number of sorties
    $sql = "SELECT COUNT(*) FROM reports ".  
           "WHERE authorID=$user_id AND reports.accepted=1";
    $result = mysqli_query($dbx, $sql);
    $nreportsrow = mysqli_fetch_array($result);
    $nreports = $nreportsrow[0];
    
    $sql = "SELECT pilotstatus.status, COUNT(reports.id) FROM reports ".
           "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
           "WHERE authorID=$user_id AND reports.accepted=1 ". 
           "GROUP BY pilotstatus.status ORDER BY pilotstatus.id ASC";
    $npilotstatusresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT aeroplanestatus.status, COUNT(reports.id) FROM reports ".
           "LEFT JOIN aeroplanestatus ON aeroplanestatus.id = reports.aeroplanestatus ".
           "WHERE authorID=$user_id AND reports.accepted=1 ".
           "GROUP BY aeroplanestatus.status ORDER BY aeroplanestatus.id ASC";
    $naeroplanestatusresult = mysqli_query($dbx, $sql);
    
    if($faction == "RAF"){
        $sql = "SELECT claimstatusraf.status, SUM(1-claimsraf.shared*0.5) AS points ".
           "FROM claimsraf LEFT JOIN reports ON claimsraf.reportid = reports.id ".
           "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
           "WHERE authorID = $user_id AND reports.accepted=1 AND claimsraf.accepted = 1 ".
           "GROUP BY claimsraf.enemystatus";
        $rafvictoriesresult = mysqli_query($dbx, $sql);
    } else if($faction == "LW"){
        $sql = "SELECT COUNT(claimslw.id), claimslw.confirmed ".
           "FROM claimslw LEFT JOIN reports ON claimslw.reportid = reports.id ".
           "WHERE authorID = $user_id AND reports.accepted=1 AND claimslw.accepted = 1 ".
           "GROUP BY claimslw.confirmed";
        $lwvictoriesresult = mysqli_query($dbx, $sql);
    }  else if($faction == "VVS"){
        $sql = "SELECT COUNT(claimsvvs.id), claimsvvs.confirmed ".
           "FROM claimsvvs LEFT JOIN reports ON claimsvvs.reportid = reports.id ".
           "WHERE authorID = $user_id AND reports.accepted=1 AND claimsvvs.accepted = 1 AND claimsvvs.groupClaim = 0 ".
           "GROUP BY claimsvvs.confirmed";
        $vvsvictoriesresult = mysqli_query($dbx, $sql);
    
        $sql = "SELECT COUNT(vgc.id), vgc.confirmed ".
           "FROM reports ".
           "LEFT JOIN vvs_group_claims vgc ON (reports.missionID, reports.squadronID) = (vgc.missionID, vgc.squadronID) ".
           "WHERE authorID = $user_id AND reports.accepted = 1 AND vgc.accepted = 1 AND vgc.groupClaim = 1 ".
           "GROUP BY vgc.confirmed";
        $vvsgvictoriesresult = mysqli_query($dbx, $sql);
    }
    
    $sql = "SELECT groundtargets.name, SUM(claimsground.amount) ".
           "FROM claimsground LEFT JOIN reports ON claimsground.reportid = reports.id ".
           "LEFT JOIN groundtargets ON claimsground.object = groundtargets.id ".
           "WHERE authorID = $user_id AND reports.accepted=1 AND claimsground.accepted = 1";
           "GROUP BY claimsground.object";
    // echo $sql;
    $groundvictoriesresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT missions.realDate, missions.histDate FROM missions";
    $miresult = mysqli_query($dbx, $sql);
    $missiondates = mysqli_fetch_all($miresult);
//    echo var_dump($missiondates);
    
    $sql = "(SELECT squadrons.name, transfers.transferdate FROM transfers ".
           "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "RIGHT JOIN careercharacters ON transfers.memberID = careercharacters.personifiedBy ".
           "WHERE careercharacters.id = $user_id ".
           "AND transfers.transferdate <= '$mxrdate' AND transfers.transferdate > '$mnrdate')".
           "UNION ".
            "(SELECT squadrons.name, transfers.transferdate FROM transfers ".
           "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "RIGHT JOIN careercharacters ON transfers.memberID = careercharacters.personifiedBy ".
           "WHERE careercharacters.id = $user_id ".
           "AND transfers.transferdate <= '$mnrdate' ORDER BY transferdate DESC LIMIT 1)".
           "ORDER BY transferdate DESC";
//        echo $sql;
    $tresult = mysqli_query($dbx, $sql);
    $transferdates = mysqli_fetch_all($tresult);
    if(count($transferdates)>0){
        $transferdates[count($transferdates)-1][1] = $mrow["mnrdate"];
    }
    
//    echo var_dump($transferdates);
    
    $sql = "(SELECT ranks.name, promotions.date, promotions.comment, ranks.image, promotions.value ".
           "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
           "RIGHT JOIN careercharacters ON promotions.memberID = careercharacters.personifiedBy ".
           "WHERE careercharacters.id = $user_id ".
           "AND promotions.date <= '$mxrdate' AND promotions.date > '$mnrdate' )".
           "UNION ".
           "(SELECT ranks.name, promotions.date, promotions.comment, ranks.image, promotions.value ".
           "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
           "RIGHT JOIN careercharacters ON promotions.memberID = careercharacters.personifiedBy ".
           "WHERE careercharacters.id = $user_id ".
           "AND promotions.date <= '$mnrdate' ORDER BY date DESC LIMIT 1)". 
           "ORDER BY date DESC";
//            echo $sql;
    $presult = mysqli_query($dbx, $sql);
    $promotiondates = mysqli_fetch_all($presult);
    if(count($promotiondates)>0){
        $promotiondates[count($promotiondates)-1][1] = $mrow["mnrdate"];
    }
//    echo var_dump($promotiondates);
    
    //Check if member has received his wings
    if($faction == "RAF"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "WHERE decorations.characterID = $user_id AND awards.abreviation = 'AB'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            if($promotiondates[0][4]<8){
                $wingsImage = $wingsrow["image"]."Fabric.png";
            } else {
                $wingsImage = $wingsrow["image"]."Brass.png";
            }
        } else {
            $wingsImage = "";
        }
    } else if($faction == "LW"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "WHERE decorations.characterID = $user_id AND awards.abreviation = 'FBAgd'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            $wingsImage = $wingsrow["image"];
        } else {
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "WHERE decorations.characterID = $user_id AND awards.abreviation = 'FBA'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                $wingsImage = $wingsrow["image"];
            } else {
                $wingsImage = "";
            }
        }
    }   else if($faction == "VVS"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "WHERE decorations.characterID = $user_id AND awards.abreviation = 'AB_VVS'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            $wingsImage = $wingsrow["image"];
        } 
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-201.1</p>
<h3>Character profile:</h3>
<div>
    <p>This page shows the profile for <?php echo($mrow["firstName"]." ".$mrow["lastName"]);?>. The displayed
    information is based on submitted and approved After Action Reports.</p>
    
    <h3>General information:</h3>
    
    <div class="uniform">
        <?php if($faction == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/RAFUniform.png">
        <?php } else if($faction == "LW"){ ?>
        <img src="imgsource/LW-ranks/LWUniform.png">
        <?php } else if($faction == "VVS"){ ?>
        <img src="imgsource/VVS-ranks/VVSUniform.png">
        <?php } ?>
    </div>

    <div class="uniformRank">
        <?php if($faction == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/<?php echo $promotiondates[0][3]; ?>">
        <?php } else if($faction == "LW"){ ?>
        <img src="imgsource/LW-ranks/<?php echo $promotiondates[0][3]; ?>">
        <?php } else if($faction == "VVS"){ ?>
        <img src="imgsource/VVS-ranks/<?php echo $promotiondates[0][3]; ?>">
        <?php } ?>
    </div>
    
    <div class="uniformWings">
        <?php if($wingsImage != ""){ ?>
        <img src="imgsource/medals-small/<?php echo $wingsImage; ?>">
        <?php } ?>
    </div >
    
    <div class="memberGeneralInformation">
        <table>
            <tr><td>Name:</td><td><?php echo($mrow["lastName"].", ".$mrow["firstName"]);?></td></tr>
            <tr><td>Callsign:</td><td><?php echo $mrow["callsign"];?></td></tr>
            <tr><td>Status:</td><td><?php echo $mrow["cstatus"];?></td></tr>
<?php
    while($drow = mysqli_fetch_assoc($dresult)){
?>
            <tr><td></td></tr>
            <tr><td>Campaign:</td><td><?php echo $drow["name"];?></td></tr>
            <tr><td>Service period:</td>
                <td><?php echo(date_format(date_create($drow["mnhdate"]), "Y-m-d").
                        " - ". date_format(date_create($drow["mxhdate"]), "Y-m-d"));?> </td></tr>
            <tr><td>Length of service:</td><td><?php echo lengthOfServiceByDates($drow["mnhdate"], $drow["mxhdate"]);?></td></tr> 
<?php            
    }
?>
        </table>
    </div>
</div>
<div>
    <hr>
    <img src="includes/awardStripesFactory.php?c_id_big=<?php echo $user_id; ?>" alt="Decorations"><br>

    
    <h3>Sorties overview:</h3>
    <table>
        <tr><td>Sorties:</td><td><?php echo $nreports;?></td></tr>
    </table>
    <?php if($nreports > 0) { ?>
    <div style="display: table-cell;">
        <table>
            <tr><td>Pilot status</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($npstatusrow = mysqli_fetch_row($npilotstatusresult)){?>
            <tr><td><?php echo $npstatusrow[0]; ?>:</td><td><?php echo $npstatusrow[1]; ?></td></tr>
            <?php } ?> 
        </table>    
    </div>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Aircraft status</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($nastatusrow = mysqli_fetch_row($naeroplanestatusresult)){?>
            <tr><td><?php echo $nastatusrow[0]; ?>:</td><td><?php echo $nastatusrow[1]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
    <?php if($faction == "RAF") { ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Victories RAF</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($rafvictoriesrow = mysqli_fetch_row($rafvictoriesresult)){?>
            <tr><td><?php echo $rafvictoriesrow[0]; ?>:</td><td><?php echo $rafvictoriesrow[1]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
    <?php } else if($faction == "LW") { ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Victories LW</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($lwvictoriesrow = mysqli_fetch_row($lwvictoriesresult)){
                $lwclaimstatus = ($lwvictoriesrow[1]==1)?"Confirmed":"Unconfirmed"; ?>
            <tr><td><?php echo $lwclaimstatus; ?>:</td><td><?php echo $lwvictoriesrow[0]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
    <?php } else if($faction == "VVS") { ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Victories VVS</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php 
            if(mysqli_num_rows($vvsvictoriesresult)>0){
            ?>
            <tr><td>Personal:</td></tr>
            <?php
                while($vvsvictoriesrow = mysqli_fetch_row($vvsvictoriesresult)){
                    $vvsclaimstatus = ($vvsvictoriesrow[1]==1)?"confirmed":"unconfirmed"; 
            ?>
                    <tr><td><?php echo("&nbsp".$vvsclaimstatus); ?>:</td><td><?php echo $vvsvictoriesrow[0]; ?></td></tr>
            <?php 
                } 
            }
            if(mysqli_num_rows($vvsgvictoriesresult)>0){
            ?>
            <tr><td>Group:</td></tr>
            <?php
                while($vvsvictoriesrow = mysqli_fetch_row($vvsgvictoriesresult)){
                    $vvsclaimstatus = ($vvsvictoriesrow[1]==1)?"confirmed":"unconfirmed";
            ?>
                    <tr><td><?php echo("&nbsp".$vvsclaimstatus); ?>:</td><td><?php echo $vvsvictoriesrow[0]; ?></td></tr>
            <?php 
                } 
            }
            ?>
        </table>      
    </div>
    <?php } ?>
    
    <?php 
   if(mysqli_num_rows($groundvictoriesresult) > 0){ ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Ground victories</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($groundvictoriesrow = mysqli_fetch_row($groundvictoriesresult)){ ?>
            <tr><td><?php echo $groundvictoriesrow[0]; ?>:</td><td><?php echo $groundvictoriesrow[1]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
    <?php 
            }
     } ?>
</div>
<div>
    <hr>
    <h3>Transfer overview:</h3>
    <table>
        <tr>
            <th>Date:</th>
            <th>To squadron/Staffel:</th>                      
        </tr>
        <?php
            for($n = 0; $n < count($transferdates); $n++){ 
        ?>
        <tr>
            <td><?php echo realToHistDate($transferdates[$n][1], $missiondates);?></td>
            <td><?php echo $transferdates[$n][0];?></td>
        </tr>
        <?php } ?>
    </table>
</div>
<div>
    <hr>
    <h3>Promotion/Demotion overview:</h3>
    <table>
        <tr>
            <th>Date:</th>
            <th>To rank:</th>
            <th>Comment:</th> 
        </tr>
        <?php
            for($n = 0; $n < count($promotiondates); $n++){ 
        ?>
        <tr>
            <td><?php echo realToHistDate($promotiondates[$n][1], $missiondates);?></td>
            <td><?php echo $promotiondates[$n][0];?></td>
            <td><?php echo $promotiondates[$n][2];?></td>
        </tr>
        <?php } ?>
    </table>
</div>
<?php include(dirname(__FILE__).'/footer.php');
