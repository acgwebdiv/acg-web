<?php
    include(dirname(__FILE__).'/header0.php');
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")){
        $mi_id = filter_input(INPUT_GET, "m_id");  
    }
    $sql = "SELECT missions.name AS m_name, missions.histdate, missions.realdate, ".
           "missions.missionstatus, campaigns.name AS c_name, missionstatus.status AS statusstr ".
           "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ".
           "LEFT JOIN missionstatus ON missionstatus.id = missions.missionstatus ". 
           "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_status = $m_row["missionstatus"];
    $m_faction = getFaction($_SESSION["userID"], $dbx);
    $mi_realDate = $m_row["realdate"];
    
    // Load claims from database and add them to the form
    $sql = "SELECT aeroplanes.name, claimsraf.id, claimstatusraf.status, claimsraf.shared, ".
           "claimsraf.description, claimsraf.accepted, claimsraf.reportID, reports.missionID, ".
           "squadrons.name as sName ". 
           "FROM claimsraf ".
           "LEFT JOIN reports ON reports.id = claimsraf.reportid ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = claimsraf.aeroplane ".
           "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.missionID = $mi_id ORDER BY reports.squadronID, enemystatus, claimsraf.id";
    $raf_claim_result = mysqli_query($dbx, $sql);
    
    // Load claims from database and add them to the form
    $sql = "SELECT claimslw.id, aeroplanes.name, claimslw.claimtime, claimslw.accepted, ".
           "claimslw.confirmed, claimslw.witness AS witnessid, acgmembers.callsign AS witnesscallsign, ".
           "fateofcrew.type AS fateofcrew, claimslw.opponent, claimslw.place, ".
           "typeofdestr.type AS typeofdestr, typeofimpact.type AS typeofimpact, ".
           "squadrons.name as sName, claimslw.reportID ".
           "FROM claimslw ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = claimslw.aeroplane ".
           "LEFT JOIN fateofcrew ON fateofcrew.id = claimslw.fateofcrew ".
           "LEFT JOIN typeofdestr ON typeofdestr.id = claimslw.typeofdestr ".
           "LEFT JOIN typeofimpact ON typeofimpact.id = claimslw.typeofimpact ".
           "LEFT JOIN acgmembers ON acgmembers.id = claimslw.witness ".
           "LEFT JOIN reports ON reports.id = claimslw.reportid ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.missionID = $mi_id ORDER BY reports.squadronID, confirmed, claimslw.id";
//    echo $sql;
    $lw_claim_result = mysqli_query($dbx, $sql);
        
    // Load claims from database and add them to the form
    $sql = "SELECT claimsvvs.id, aeroplanes.name, claimsvvs.claimTime, claimsvvs.accepted, ".
           "claimsvvs.confirmed, claimsvvs.witness AS witnessid, acgmembers.callsign AS witnesscallsign, ".
           "claimsvvs.place, claimsvvs.groupClaim, claimsvvs.description, ".
           "squadrons.name as sName, claimsvvs.reportID ".
           "FROM claimsvvs ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = claimsvvs.aeroplane ".
           "LEFT JOIN acgmembers ON acgmembers.id = claimsvvs.witness ".
           "LEFT JOIN reports ON reports.id = claimsvvs.reportid ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.missionID = $mi_id ORDER BY reports.squadronID, confirmed, claimsvvs.id";
    // echo $sql;
    $vvs_claim_result = mysqli_query($dbx, $sql);
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>
<p class="form_id">ACG-PAM/300-200.1</p>
<div>
    <h3><?php echo($m_row["m_name"]." (".$m_row["histdate"].")"); ?></h3>
    <p>
        <?php echo($m_row["m_name"]." (".$m_row["histdate"].")"); ?>,
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date:
         <?php echo($m_row["realdate"]); ?>.
    </p>
    <p>
        Current mission status: <?php echo($m_row["statusstr"]); ?>
    </p>
</div>

<div>
    <h3>RAF claims:</h3>
    <table>
    <?php
        $currentSquadron = "";
        
        while($r_row = mysqli_fetch_assoc($raf_claim_result)) {

            $claim_id = $r_row["id"];
            if($r_row["accepted"] == -1){
                $claim_accepted = "Rejected";
            } else if($r_row["accepted"] == 1){
                $claim_accepted = "Accepted";
            } else if($r_row["accepted"] == 0){
                $claim_accepted = "";
            }
            $shared = $r_row["shared"]? "Shared" : "";
            $link = "reportRAF.php?r_id=".$r_row["reportID"];
            if(!is_null($claim_id)){
                
                if($r_row["sName"]!==$currentSquadron){
                    $currentSquadron = $r_row["sName"];
    ?>
        <tr>
            <td colspan="5" style="border-bottom: 1px solid black;"><?php echo $currentSquadron; ?></td>
        </tr>
    <?php
                }
    ?>
        <tr>
            <td><?php echo($r_row["name"]);?></a></td>
            <td><?php echo($r_row["status"]);?></td>
            <td><?php echo($shared);?></td>
            <td><?php echo($claim_accepted);?></td>
            <td><button onclick="location.href = '<?php echo $link ?>'">REPORT</button></td>
        </tr>
        <tr>
            <td colspan="4"><?php echo($r_row["description"]);?></td>
        </tr>
    <?php
            }
        }
    ?>
    </table>
</div>
<div id="claims">
    <h3>Luftwaffe claims:</h3>
    <?php
    $currentSquadron = "";
    
    while($r_row = mysqli_fetch_assoc($lw_claim_result)) {
        if($r_row["id"]!=null) {

            $claim_id = $r_row["id"];
            $claim_confirmed = $r_row["confirmed"]? "Confirmed" : "";
            if($r_row["accepted"] == -1){
                $claim_accepted = "Rejected";
            } else if($r_row["accepted"] == 1){
                $claim_accepted = "Accepted";
            } else if($r_row["accepted"] == 0){
                $claim_accepted = "";
            }
            $witness_callsign = $r_row["witnesscallsign"];
            $link = "reportLW.php?r_id=".$r_row["reportID"];
            $groundConf = ($r_row["witnessid"] == -1);
            if($groundConf){
                $witness_callsign = "Ground Unit";
            }
            if(!is_null($claim_id)){
                
                if($r_row["sName"]!==$currentSquadron){
                    $currentSquadron = $r_row["sName"];
    ?>
        
    <br>
    <?php echo $currentSquadron; ?>
    <br>
    <?php
                }
    ?>
        <hr>
        <table>
            <tr><td><i>Time:</i></td><td><i>Place:</i></td>
            <td><i>Aircraft:</i></td><td><i>Opponent:</i></td>
        <td><?php echo $claim_accepted ?></td>
        </tr>
        <tr><td class="claimLW"><?php echo($r_row["claimtime"]); ?></td><td class="claimLW"><?php echo($r_row["place"]); ?></td>
            <td class="claimLW"><?php echo($r_row["name"]); ?></td><td class="claimLW"><?php echo($r_row["opponent"]); ?></td></tr>
        <tr><td><i>Type of destruction:</i></td><td><i>Type of impact on the ground:</i></td>
            <td><i>Fate of crew:</i></td><td><i>Witness:</i></td><td><i>Confirmed:</i></td></tr>
        <tr><td class="claimLW"><?php echo($r_row["typeofdestr"]); ?></td><td class="claimLW"><?php echo($r_row["typeofimpact"]); ?></td>
            <td class="claimLW"><?php echo($r_row["fateofcrew"]); ?></td>
            <td class="claimLW"><?php echo($witness_callsign); ?></td>
            <td class="claimLW"><?php echo $claim_confirmed ?></td>
            <?php } ?>
        </tr>
        <tr><td><button onclick="location.href = '<?php echo $link ?>'">REPORT</button></td></tr>
        </table>
        <br>
    <?php } } ?>
</div>
<div id="claims">
    <h3>Voyenno-Vozdushnye Sily claims:</h3>
    <?php
    $currentSquadron = "";
    
    while($r_row = mysqli_fetch_assoc($vvs_claim_result)) {
        if($r_row["id"]!=null) {

            $claim_id = $r_row["id"];
            $claim_confirmed = $r_row["confirmed"]? "Confirmed" : "";
            if($r_row["accepted"] == -1){
                $claim_accepted = "Rejected";
            } else if($r_row["accepted"] == 1){
                $claim_accepted = "Accepted";
            } else if($r_row["accepted"] == 0){
                $claim_accepted = "";
            }
            $witness_callsign = $r_row["witnesscallsign"];
            $groupClaim = $r_row["groupClaim"]? "X" : "";
            $link = "reportVVS.php?r_id=".$r_row["reportID"];
            $groundConf = ($r_row["witnessid"] == -1);
            if($groundConf){
                $witness_callsign = "Ground Unit";
            }
            if(!is_null($claim_id)){
                
                if($r_row["sName"]!==$currentSquadron){
                    $currentSquadron = $r_row["sName"];
                    
    ?>
        
    <br>
    <?php echo $currentSquadron; ?>
    <br>
    <?php
                }
    ?>
        <hr>
        <table>
            <tr>
                <td>Time:</td><td>Place:</td><td>Aircraft:</td><td><?php echo $claim_accepted ?></td>
            </tr>
            <tr>
                <td class="claimLW"><?php echo($r_row["claimTime"]); ?></td><td class="claimLW"><?php echo($r_row["place"]); ?></td>
                <td class="claimLW"><?php echo($r_row["name"]); ?></td>
            </tr>
            <tr>
                <td>Group claim</td><td>Witness:</td><td>Confirmed:</td>
            </tr>
            <tr>
                <td class="claimLW"><?php echo $groupClaim; ?></td>
                <td class="claimLW"><?php echo($witness_callsign); ?></td>
                <td class="claimLW"><?php echo $claim_confirmed ?></td>
            <?php } ?>
        </tr>
        <tr>
            <td colspan="4"><?php echo($r_row["description"]);?></td>
        </tr>
        <tr><td><button onclick="location.href = '<?php echo $link ?>'">REPORT</button></td></tr>
        </table>
        <br>
    <?php } } ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');