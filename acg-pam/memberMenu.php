<script type='text/javascript' src="jscript/flexcroll.js"></script>
</head>
<body id="memberMenuBody">
    <div class="mainContainer">
        <div class="pageTop">
            <?php
            include(dirname(__FILE__).'/primMenu.php'); 
            include(dirname(__FILE__).'/userMenu.php');
            ?>
            <div class="secMenu">
                <ul>
                    <li><a href="memberList.php">All Members</a></li>
                    <li><a href="characterList.php">All Characters</a></li>
                </ul>
                <?php
                    if(filter_has_var(INPUT_GET, "m_id")){
                        $user_id = filter_input(INPUT_GET, "m_id");           
                ?>
                <ul class='memberMenu'>
                <li><a href="memberDetails.php?m_id=<?php echo($user_id); ?>">General information</a></li>
                <li><a href="memberCharacters.php?m_id=<?php echo($user_id); ?>">Characters</a></li>
                <li><a href="memberReports.php?m_id=<?php echo($user_id); ?>">Attended missions</a></li>
                <li><a href="memberClaims.php?m_id=<?php echo($user_id); ?>">Claims</a></li>
                </ul>
                <?php
                    } else if(filter_has_var(INPUT_GET, "c_id")){
                        $user_id = filter_input(INPUT_GET, "c_id");           
                ?>
                <ul class='memberMenu'>
                <li><a href="characterDetails.php?c_id=<?php echo($user_id); ?>">General information</a></li>
                <li><a href="characterDecorations.php?c_id=<?php echo($user_id); ?>">Decorations</a></li>
                <li><a href="characterReports.php?c_id=<?php echo($user_id); ?>">Attended missions</a></li>
                <li><a href="characterClaims.php?c_id=<?php echo($user_id); ?>">Claims</a></li>
                </ul>
                <?php  
                
                    } else { 

                        $sql = "SELECT squadrons.id, squadrons.name, squadrons.faction FROM squadrons ".
                               "ORDER BY squadrons.faction DESC, squadrons.rank ASC";
                        $smenu_result = mysqli_query($dbx, $sql);
                        $link = filter_input(INPUT_SERVER, 'PHP_SELF')."?";
                        include './squadronSubMenu.php';
                    } ?>
            </div>
        </div>
        <div class="pageMiddle">