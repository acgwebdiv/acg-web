<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=encoding">
    <title>ACG Pilot- and Mission Database</title>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="styles/dataTables.css">
    <link rel="shortcut icon" href="../includes/images/favicon.png" type="image/png">
    <link rel="icon" href="../includes/images/favicon.png" type="image/png">
    
    