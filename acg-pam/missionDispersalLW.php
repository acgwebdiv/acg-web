<?php

////Get all pilots
//$sql = "SELECT attendance.id, attendance.memberID, acgmembers.callsign, attendance.positionID, ".
//       "attendance.flightsraf, attendance.sectionraf, attendance.sectionpos, ".
//       "attendance.swarmslw, attendance.swarmpos ".
//       "FROM attendance ".
//       "LEFT JOIN acgmembers ON attendance.memberID = acgmembers.id ". 
//       "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ";
//$apresult = mysqli_query($dbx, $sql);
//$roster_array = array(); //Array linking position in dispersal to memberID
//$memberIDArray = array(); //Array with memberID and callsign
//$memberIDArray[] = array(0, "");
//$memberArray = array(); //Array linking memberID to callsign
//while($row = mysqli_fetch_assoc($apresult)) {
//    $roster_array[$row["positionID"]] = $row["memberID"];
//    $memberIDArray[] = array($row["memberID"], $row["callsign"]);
//    $memberArray[$row["memberID"]] = $row["callsign"];
//}


function memberSelectForm($position, $roster_array, $memberIDArray){
    if(array_key_exists($position, $roster_array)){
        $memberID = $roster_array[$position];
    } else {
        $memberID = 0;
    }
    createSelectFormWithName("dispersal", $position, $memberIDArray, $memberID);
}

function getSchwarmInfo($position){
    
    
    switch ($position) {
        case 1:
            return array(1, 1, 'Staffelführer');
        case 2:
            return array(1, 2, 'Katschmarek');
        case 3:
            return array(1, 3, 'Rottenführer');
        case 4:
            return array(1, 4, 'Katschmarek');
        case 5:
            return array(2, 1, 'Schwarmführer');
        case 6:
            return array(2, 2, 'Katschmarek');
        case 7:
            return array(2, 3, 'Rottenführer');
        case 8:
            return array(2, 4, 'Katschmarek');
        case 9:
            return array(3, 1, 'Schwarmführer');
        case 10:
            return array(3, 2, 'Katschmarek');
        case 11:
            return array(3, 3, 'Rottenführer');
        case 12:
            return array(3, 4, 'Katschmarek');
        case 13:
            return array(4, 1, 'Schwarmführer');
        case 14:
            return array(4, 2, 'Katschmarek');
        case 15:
            return array(4, 3, 'Rottenführer');
        case 16:
            return array(4, 4, 'Katschmarek');
        }
}
?>
<?php 
    if($isAdmin){
?>
<div id="dispersalSetupDiv" style="direction: block">
    <form id="rosterAircraft" onsubmit="return false;" >
        <table>
            <thead>
                <tr>
                    <th>Role:</th>
                    <th>Pilot:</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2"><b>1.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Staffelkapitän"); ?></td>
                    <td><?php 
                            $position = 1;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 2;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 3;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 4;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2"><b>2.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Schwarmführer"); ?></td>
                    <td><?php 
                            $position = 5;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 6;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 7;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 8;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2"><b>3.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Schwarmführer"); ?></td>
                    <td><?php 
                            $position = 9;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 10;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 11;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 12;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2"><b>4.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Schwarmführer"); ?></td>
                    <td><?php 
                            $position = 13;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 14;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 15;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 16;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
            </tbody>
        </table>
        <button id="saveDispersalBtnLW" >Save changes</button>
        <span id="saveDispersalStatus" ></span>        
    </form>
    <hr>
</div>
<?php 
    }
?>
<div>
    <table>
        <thead>
            <tr>
                <th>Role:</th>
                <th>Pilot:</th>
            </tr>
        </thead>
        <tbody>
<?php $currentSchwarm = 0; 
foreach ($roster_array as $positionID => $memberID) {
    
    if($memberArray[$memberID]["dispersalStatus"]==1){
        
        $schwarmInfo = getSchwarmInfo($positionID);
        if($currentSchwarm != $schwarmInfo[0]){
            $currentSchwarm = $schwarmInfo[0];
            echo("<tr><td><b>".$currentSchwarm.".Schwarm<b></td><tr>");
        }
?>
            <tr>
                <td><?php echo(htmlentities($schwarmInfo[2])); ?></td>
                <td witdth="20"><?php echo($memberArray[$memberID]["rank"]); ?></td>
                <td><?php echo($memberArray[$memberID]["name"]); ?></td>
            </tr>
    
<?php
        }
    }
?>
        </tbody>
    </table>
</div>

