<?php
    include(dirname(__FILE__).'/header0.php');
    
    $user_id = filter_input(INPUT_GET, "m_id");
    $dbx = getDBx();
    
    $sql = "SELECT callsign FROM acgmembers WHERE id = $user_id";
    $query = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($query);
    $callsign = $result["callsign"];
    
    //Get all character id's of member
    $sql = "SELECT careercharacters.id FROM careercharacters ".
           "WHERE careercharacters.personifiedBy = $user_id ORDER BY id";
    $cresult = mysqli_query($dbx, $sql);
    $c_id_array = mysqli_fetch_all($cresult);
    $character_array = array();
    for($n = 0; $n < count($c_id_array); $n++){
        
        $c_id = $c_id_array[$n][0];
        
        $sql = "SELECT careercharacters.firstname, careercharacters.lastname, ".
           "characterstatus.status AS cstatus, ".
           "MAX(missions.realDate) AS mxrdate, MIN(missions.realDate) AS mnrdate ".
           "FROM careercharacters ". 
           "LEFT JOIN characterstatus ON careercharacters.characterstatus = characterstatus.id ".
           "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
           "LEFT JOIN missions ON reports.missionID = missions.id ". 
           "WHERE careercharacters.id = $c_id";
//        echo $sql;
        $mresult = mysqli_query($dbx, $sql);
        $mrow = mysqli_fetch_assoc($mresult);
        $character_array[$c_id]["firstName"] = $mrow["firstname"];
        $character_array[$c_id]["lastName"] = $mrow["lastname"];
        $character_array[$c_id]["status"] = $mrow["cstatus"];
        $mxrdate = $mrow["mxrdate"];
        $mnrdate = $mrow["mnrdate"];
        
        $sql = "SELECT campaigns.id, campaigns.name, ".
           "MAX(missions.histDate) AS mxhdate, MIN(missions.histDate) AS mnhdate ".
           "FROM careercharacters ". 
           "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
           "LEFT JOIN missions ON reports.missionID = missions.id ".
           "LEFT JOIN campaigns ON campaigns.id = missions.campaignID ".
           "WHERE careercharacters.id = $c_id GROUP BY campaigns.id";
        //    echo $sql;
        $dresult = mysqli_query($dbx, $sql);
        $campaignArray = array();
        $campaign_result_array = mysqli_fetch_all($dresult);
        for($m = 0; $m < count($campaign_result_array); $m++){
            
            $camp_id = $campaign_result_array[$m][0];
            $campaignArray[$m]["cName"] = $campaign_result_array[$m][1];
            $campaignArray[$m]["maxHistDate"] = $campaign_result_array[$m][2];
            $campaignArray[$m]["minHistDate"] = $campaign_result_array[$m][3];
        }
        $character_array[$c_id]["campaignArray"] = $campaignArray; 
        
        $sql = "SELECT squadrons.faction, transfers.transferdate FROM transfers ".
           "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
           "RIGHT JOIN careercharacters ON transfers.memberID = careercharacters.personifiedBy ".
           "WHERE careercharacters.id = $c_id ".
           "AND transfers.transferdate <= '$mxrdate' ORDER BY transferdate DESC LIMIT 1";
        $fresult = mysqli_query($dbx, $sql);
        $frow = mysqli_fetch_assoc($fresult);
        $faction = $frow["faction"];
        $character_array[$c_id]["faction"] = $frow["faction"];
        
        // Number of sorties
        $sql = "SELECT COUNT(*) FROM reports ".  
               "WHERE authorID=$c_id AND reports.accepted=1";
        $result = mysqli_query($dbx, $sql);
        $nreportsrow = mysqli_fetch_array($result);
        $character_array[$c_id]["sorties"] = $nreportsrow[0];
                
//            $sql = "SELECT pilotstatus.status, COUNT(reports.id) FROM reports ".
//                   "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
//                   "WHERE authorID=$user_id AND reports.accepted=1 ". 
//                   "GROUP BY pilotstatus.status ORDER BY pilotstatus.id ASC";
//            $npilotstatusresult = mysqli_query($dbx, $sql);
//            
//            $sql = "SELECT aeroplanestatus.status, COUNT(reports.id) FROM reports ".
//                   "LEFT JOIN aeroplanestatus ON aeroplanestatus.id = reports.aeroplanestatus ".
//                   "WHERE authorID=$user_id AND reports.accepted=1 ".
//                   "GROUP BY aeroplanestatus.status ORDER BY aeroplanestatus.id ASC";
//            $naeroplanestatusresult = mysqli_query($dbx, $sql);
        
        if($faction == "RAF"){
            $sql = "SELECT claimstatusraf.status, SUM(1-claimsraf.shared*0.5) AS points ".
               "FROM claimsraf LEFT JOIN reports ON claimsraf.reportid = reports.id ".
               "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
               "WHERE authorID = $c_id AND reports.accepted=1 AND claimsraf.accepted = 1 ".
               "GROUP BY claimsraf.enemystatus";
            $rafvictoriesresult = mysqli_query($dbx, $sql);
            $character_array[$c_id]["victories"] = $rafvictoriesresult;
        } else if($faction == "LW"){
            $sql = "SELECT COUNT(claimslw.id), claimslw.confirmed ".
               "FROM claimslw LEFT JOIN reports ON claimslw.reportid = reports.id ".
               "WHERE authorID = $c_id AND reports.accepted=1 AND claimslw.accepted = 1 ".
               "GROUP BY claimslw.confirmed";
            $lwvictoriesresult = mysqli_query($dbx, $sql);
            $character_array[$c_id]["victories"] = $lwvictoriesresult;
        } else if($faction == "VVS"){
            $sql = "SELECT COUNT(claimsvvs.id), claimsvvs.confirmed ".
                "FROM claimsvvs LEFT JOIN reports ON claimsvvs.reportid = reports.id ".
                "WHERE authorID = $c_id AND reports.accepted=1 AND claimsvvs.accepted = 1 AND claimsvvs.groupClaim = 0 ".
                "GROUP BY claimsvvs.confirmed";
            $vvsvictoriesresult = mysqli_query($dbx, $sql);
            $character_array[$c_id]["victories"] = $vvsvictoriesresult;
            $sql = "SELECT COUNT(vgc.id), vgc.confirmed ".
                "FROM reports ".
                "LEFT JOIN vvs_group_claims vgc ON (reports.missionID, reports.squadronID) = (vgc.missionID, vgc.squadronID) ".
                "WHERE authorID = $c_id AND reports.accepted = 1 AND vgc.accepted = 1 AND vgc.groupClaim = 1 ".
                "GROUP BY vgc.confirmed";
            // echo $sql;
            $vvsgvictoriesresult = mysqli_query($dbx, $sql);
            $character_array[$c_id]["groupvictories"] = $vvsgvictoriesresult;
        }
        
        $sql = "SELECT SUM(claimsground.amount) ".
           "FROM claimsground LEFT JOIN reports ON claimsground.reportid = reports.id ".
           "WHERE authorID = $c_id AND reports.accepted=1 AND claimsground.accepted = 1";
        $groundvictoriesresult = mysqli_query($dbx, $sql);
        $character_array[$c_id]["gvictories"] = $groundvictoriesresult;
                
        // Get last rank
        $sql = "(SELECT ranks.abreviation, promotions.date, ranks.image, promotions.value ".
               "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
               "RIGHT JOIN careercharacters ON promotions.memberID = careercharacters.personifiedBy ".
               "WHERE careercharacters.id = $c_id ".
               "AND promotions.date <= '$mxrdate' AND promotions.date > '$mnrdate' )".
               "UNION ".
               "(SELECT ranks.abreviation, promotions.date, ranks.image, promotions.value ".
               "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
               "RIGHT JOIN careercharacters ON promotions.memberID = careercharacters.personifiedBy ".
               "WHERE careercharacters.id = $c_id ".
               "AND promotions.date <= '$mnrdate' ORDER BY date DESC LIMIT 1)". 
               "ORDER BY date DESC LIMIT 1";
    //            echo $sql;
        $presult = mysqli_query($dbx, $sql);
        $promotiondates = mysqli_fetch_all($presult);
        if(count($promotiondates)>0){
            $character_array[$c_id]["rank"] = $promotiondates[0][0];
            $character_array[$c_id]["rankImage"] = $promotiondates[0][2];
            $character_array[$c_id]["rankValue"] = $promotiondates[0][3];
            $character_array[$c_id]["faction"] = $faction;
        }
        
        // Get last squadron/Staffel
        $sql = "(SELECT squadrons.name, transfers.transferdate FROM transfers ".
               "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
               "RIGHT JOIN careercharacters ON transfers.memberID = careercharacters.personifiedBy ".
               "WHERE careercharacters.id = $c_id ".
               "AND transfers.transferdate <= '$mxrdate' AND transfers.transferdate > '$mnrdate')".
               "UNION ".
                "(SELECT squadrons.name, transfers.transferdate FROM transfers ".
               "LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
               "RIGHT JOIN careercharacters ON transfers.memberID = careercharacters.personifiedBy ".
               "WHERE careercharacters.id = $c_id ".
               "AND transfers.transferdate <= '$mnrdate' ORDER BY transferdate DESC LIMIT 1)".
               "ORDER BY transferdate DESC LIMIT 1";
    //        echo $sql;
        $tresult = mysqli_query($dbx, $sql);
        $transferdates = mysqli_fetch_all($tresult);
        if(count($transferdates)>0){
            $character_array[$c_id]["squadron"] = $transferdates[0][0];
        }
        
            //Check if member has received his wings
        if($faction == "RAF"){
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
                   "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
                   "WHERE decorations.characterID = $c_id AND awards.abreviation = 'AB'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                if($character_array[$c_id]["rankValue"]<8){
                    $character_array[$c_id]["wingsImage"] = $wingsrow["image"]."Fabric.png";
                } else {
                    $character_array[$c_id]["wingsImage"] = $wingsrow["image"]."Brass.png";
                }
            } else {
                $character_array[$c_id]["wingsImage"] = "";
            }
        } else if($faction == "LW"){
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
                   "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
                   "WHERE decorations.characterID = $c_id AND awards.abreviation = 'FBAgd'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                $character_array[$c_id]["wingsImage"] = $wingsrow["image"];
            } else {
                $sql = "SELECT awards.abreviation, awards.image FROM awards ".
                   "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
                   "WHERE decorations.characterID = $c_id AND awards.abreviation = 'FBA'";
                $wingsQuery = mysqli_query($dbx, $sql);
                if(mysqli_num_rows($wingsQuery)>0){
                    $wingsrow = mysqli_fetch_assoc($wingsQuery);
                    $character_array[$c_id]["wingsImage"] = $wingsrow["image"];
                } else {
                    $character_array[$c_id]["wingsImage"] = "";
                }
            }
        }   else if($faction == "VVS"){
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
                   "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
                   "WHERE decorations.characterID = $c_id AND awards.abreviation = 'AB_VVS'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                $character_array[$c_id]["wingsImage"] = $wingsrow["image"];
            } 
        }
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-201.1</p>
<h3>Member profile:</h3>
<div>
    <p>This page shows the career characters for <?php echo $callsign; ?>. The displayed
    information is based on submitted and approved After Action Reports.</p>
    <hr>
    <?php 
    for($n = 0; $n < count($c_id_array); $n++){
        
        $c_id = $c_id_array[$n][0];
        $link = "characterDetails.php?c_id=".$c_id;
    ?>
    
    <a href="<?php echo($link);?>">
        <h3><?php echo($character_array[$c_id]["rank"]." ".
            $character_array[$c_id]["firstName"]." ".$character_array[$c_id]["lastName"]);?>:</h3>
    </a>
    <div class="uniform">
        <?php if($character_array[$c_id]["faction"] == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/RAFUniform.png">
        <?php } else if($character_array[$c_id]["faction"] == "LW"){ ?>
        <img src="imgsource/LW-ranks/LWUniform.png">
        <?php } else if($character_array[$c_id]["faction"] == "VVS"){ ?>
        <img src="imgsource/VVS-ranks/VVSUniform.png">
        <?php } ?>
    </div>

    <div class="uniformRank">
        <?php if($character_array[$c_id]["faction"] == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/<?php echo $character_array[$c_id]["rankImage"]; ?>">
        <?php } else if($character_array[$c_id]["faction"] == "LW"){ ?>
        <img src="imgsource/LW-ranks/<?php echo $character_array[$c_id]["rankImage"]; ?>">
        <?php } else if($character_array[$c_id]["faction"] == "VVS"){ ?>
        <img src="imgsource/VVS-ranks/<?php echo $character_array[$c_id]["rankImage"]; ?>">
        <?php } ?>
    </div>
    
    <div class="uniformWings">
        <?php if($character_array[$c_id]["wingsImage"] != ""){ ?>
        <img src="imgsource/medals-small/<?php echo $character_array[$c_id]["wingsImage"]; ?>">
        <?php } ?>
    </div >
    
    <div class="memberGeneralInformation">
        <table>
            <tr><td>Status:</td><td><?php echo $character_array[$c_id]["status"];?></td></tr>
<!--            <tr><td>Service entry date:</td><td><?php echo date_format(date_create($character_array[$c_id]["mnhdate"]), "Y-m-d");?></td></tr>
            <tr><td>Last mission date:</td><td><?php echo date_format(date_create($character_array[$c_id]["mxhdate"]), "Y-m-d");?></td></tr>
            <tr><td>Length of service:</td><td><?php echo lengthOfServiceByDates($character_array[$c_id]["mnhdate"], $character_array[$c_id]["mxhdate"]);?></td></tr> -->
            <tr><td>Squadron/Staffel:</td><td><?php echo $character_array[$c_id]["squadron"];?></td></tr>
        <?php        
            $campaignArray = $character_array[$c_id]["campaignArray"]; 
            for($m = 0; $m < count($campaignArray); $m++){
        ?>
            <tr><td></td></tr>
            <tr><td>Campaign:</td><td><?php echo $campaignArray[$m]["cName"];?></td></tr>
            <tr><td>Service period:</td>
                <td><?php echo(date_format(date_create($campaignArray[$m]["minHistDate"]), "Y-m-d").
                        " - ". date_format(date_create($campaignArray[$m]["maxHistDate"]), "Y-m-d"));?> </td></tr>
            <tr><td>Length of service:</td>
                <td><?php echo lengthOfServiceByDates($campaignArray[$m]["minHistDate"], $campaignArray[$m]["maxHistDate"]);?></td></tr> 
        <?php            
            }
        ?> 
        </table>
    </div>
    <div>
        <p>
            Sorties: <?php echo $character_array[$c_id]["sorties"];?>
            <?php
                if($character_array[$c_id]["faction"] == "RAF"){
                    while($row = mysqli_fetch_array($character_array[$c_id]["victories"])){
                        echo(" - ".$row[0].": ".$row[1]);
                    }
                } else if($character_array[$c_id]["faction"] == "LW"){
                    while($row = mysqli_fetch_array($character_array[$c_id]["victories"])){
                        $claimStatus = ($row[1])?"Confirmed: ":"Unconfirmed: ";
                        echo(" - ".$claimStatus.$row[0]);  
                    }  
                } else if($character_array[$c_id]["faction"] == "VVS"){
                    if(mysqli_num_rows($character_array[$c_id]["victories"]) > 0){
                        echo(" - Personal victories ");
                    }
                    while($row = mysqli_fetch_array($character_array[$c_id]["victories"])){
                        $claimStatus = ($row[1])?"Confirmed: ":"Unconfirmed: ";
                        echo(" ".$claimStatus.$row[0]);  
                    }
                    if(mysqli_num_rows($character_array[$c_id]["groupvictories"]) > 0){
                        echo(" - Group victories ");
                    }
                    while($row = mysqli_fetch_array($character_array[$c_id]["groupvictories"])){
                        $claimStatus = ($row[1])?"Confirmed: ":"Unconfirmed: ";
                        echo(" ".$claimStatus.$row[0]);  
                    }
                } 
                $row = mysqli_fetch_array($character_array[$c_id]["gvictories"]);
                if($row[0] > 0){
                    echo(" - Ground victories: ".$row[0]);
                }
            ?>
        </p>
        <img src="includes/awardStripesFactory.php?c_id_small=<?php echo $c_id; ?>">
    </div>
    <hr>
    <?php } ?>
    
</div>

<?php include(dirname(__FILE__).'/footer.php');