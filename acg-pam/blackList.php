<?php
    include(dirname(__FILE__).'/header0.php');
    
    // Setting up indices to spread content over several pages.
    $sqn = "NULL";
    if(filter_has_var(INPUT_GET, "sqn")) {
        $sqn = filter_input(INPUT_GET, "sqn");
    } 
    $dbx = getDBx();
    $sql = "CALL `BoX_PAM_BlacklistDetail`($sqn)";
    $result = mysqli_query($dbx, $sql);
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script type="text/javascript">

</script>

<?php include(dirname(__FILE__).'/blackListMenu.php'); ?> 
<p class="form_id">ACG-PAM/300-100.1</p>
<h3>After Action Report Status:</h3>
<div>
    <p>This ACG Flight Report Status page shows all members whose After Action Report
    has not yet been written and/or accepted, and therefore impacts on their flight 
    status for the next mission.</p>
    <p>The columns show the name of the member, the mission name, the steam-id that
    the member was flying the mission under and the status of the Flight Report. 
    You can click on the status of the Flight Report to go directly to the mission 
    screen or the report.</p>
    <p>A button for writing or editing the reports will be displayed on the right 
    side should your name be listed below. Please note that it takes some time for 
    the adjutants to check and accept all reports.</p>
    <p>Note further that the status of your Flight Report will be transfered to 
    the mission control once a new mission starts. All members still listed below 
    at this stage will automatially be despawned from the server. The status will 
    not be be updated once the mission has started, so any attempt to file a successful 
    Flight Report after mission start will not allow you to fly in that mission.</p>
<div class="indexSelect">
    <?php createPageIndex($indexLetter, "blackList.php?".$sqnPost."&"); ?>
</div>
    <table>
        <tr>
            <th>Name:</th>
            <th>Mission:</th>
            <th>Flew under:</th>
            <th>Status:</th>
            <th style="width: 100px;"></th>
        </tr>
        <?php
            $member_id = "";
            while($row = mysqli_fetch_assoc($result)) {
                
                $showButton = ($_SESSION["userID"]==$row["id"])?TRUE:FALSE;
                $mi_id = $row["missionID"];
                
                if(is_null($row["accepted"])){
                    
                    $link = "missionDetails.php?m_id=".$row["MissionID"];
                    $cause = "Awaiting Report Submission";
                    
                } else if($row["accepted"] == 0) {
                
                    if($row['type']==1){
                        $link = "reportRAF.php?r_id=".$row["report_id"];  
                    } else if($row['type']==2){
                        $link = "reportLW.php?r_id=".$row["report_id"];
                    } else if($row['type']==3){
                        $link = "reportVVS.php?r_id=".$row["report_id"];
                    }
                    $cause = "Report: AAR-".$row['type']."-".$row['report_id']." is under review.";
                }
                if($member_id != $row["id"]){
                    
                    $member_id = $row["id"];
        ?>
        <tr>
            <td><?php echo $row["callsign"];?></td>
            <td><?php echo $row["missionName"];?></td>
            <td><?php echo $row["boxname"];?></td>
            <td><a href="<?php echo($link);?>"><?php echo $cause;?></a></td>

        <?php
                    if($showButton){
        ?>
                        <td><button onclick="editReport(<?php echo($mi_id.", ".$_SESSION["userID"]); ?>)">Write/Edit report</button></td>
        <?php
                    }
        ?>
        </tr>
        <?php 
                } else {
        ?>
        <tr>
            <td></td>
            <td><?php echo $row["missionName"];?></td>
            <td><?php echo $row["boxname"];?></td>
            <td><a href="<?php echo($link);?>"><?php echo $cause;?></a></td>
        <?php
                if($showButton){
        ?>
            <td><button onclick="editReport(<?php echo($mi_id.", ".$_SESSION["userID"]); ?>)">Write/Edit report</button></td>
        <?php
                }
        ?>
        </tr>
        <?php  
                }          
            } 
        ?>
    </table>
</div>
<div class="pageSelect">
    <?php  createPageSelect($n_pages, $page, "blackList.php?".$sqnPost."&".$indexPost."&"); ?>
</div>

<?php include(dirname(__FILE__).'/footer.php');