<?php
    include(dirname(__FILE__).'/header0.php');
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")){
        $mi_id = filter_input(INPUT_GET, "m_id");  
    }
    $sql = "SELECT missions.name AS m_name, missions.histdate, missions.realdate, ".
           "missions.missionstatus, campaigns.name AS c_name ".
           "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
           "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_status = $m_row["missionstatus"];
    $m_faction = getFaction($_SESSION["userID"], $dbx);
    
    //Number of pilots for each faction and squadron/Staffel
    $sql = "SELECT DISTINCT squadrons.id, squadrons.name FROM reports ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 AND squadrons.faction = 'RAF' ".
           "ORDER BY squadrons.id ASC";
    $query = mysqli_query($dbx, $sql);
    $totalRAF = 0;
    $rafSqnArray = array();
    $rafAttnArray = array();
    while($row = mysqli_fetch_assoc($query)){
        $squadronID = $row["id"];
        $rafSqnArray[] = array("name"=>$row["name"], "id"=>$squadronID);
        $sql = "SELECT COUNT(reports.id) AS npilots, aeroplanes.name FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.squadronID = $squadronID GROUP BY aeroplanes.id ";
        $squery = mysqli_query($dbx, $sql);
        
        $pilotsFromSquadron = 0;
        $bufferArray = array();
        $bufferAcftArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $bufferAcftArray[$srow["name"]] = $srow["npilots"];
            $totalRAF += $srow["npilots"];
            $pilotsFromSquadron += $srow["npilots"];
        }
        $bufferArray["npilots"] = $pilotsFromSquadron;
        $bufferArray["acft"] =  $bufferAcftArray;
        $rafAttnArray[$row["name"]] = $bufferArray;
    }
    $sql = "SELECT DISTINCT squadrons.id, squadrons.name FROM reports ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 AND squadrons.faction = 'VVS' ".
           "ORDER BY squadrons.id ASC";
    $query = mysqli_query($dbx, $sql);
    $totalVVS = 0;
    $vvsSqnArray = array();
    $vvsAttnArray = array();
    while($row = mysqli_fetch_assoc($query)){
        $squadronID = $row["id"];
        $vvsSqnArray[] = array("name"=>$row["name"], "id"=>$squadronID);
        $sql = "SELECT COUNT(reports.id) AS npilots, aeroplanes.name FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.squadronID = $squadronID GROUP BY aeroplanes.id ";
        $squery = mysqli_query($dbx, $sql);
        
        $pilotsFromSquadron = 0;
        $bufferArray = array();
        $bufferAcftArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $bufferAcftArray[$srow["name"]] = $srow["npilots"];
            $totalVVS += $srow["npilots"];
            $pilotsFromSquadron += $srow["npilots"];
        }
        $bufferArray["npilots"] = $pilotsFromSquadron;
        $bufferArray["acft"] =  $bufferAcftArray;
        $vvsAttnArray[$row["name"]] = $bufferArray;
    }
    $sql = "SELECT DISTINCT squadrons.id, squadrons.name FROM reports ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 AND squadrons.faction = 'LW' ".
           "ORDER BY squadrons.id ASC";
    $query = mysqli_query($dbx, $sql);
    $totalLW = 0;
    $lwSqnArray = array();
    $lwAttnArray = array();
    while($row = mysqli_fetch_assoc($query)){
        $squadronID = $row["id"];
        $lwSqnArray[] = array("name"=>$row["name"], "id"=>$squadronID);
        $sql = "SELECT COUNT(reports.id) AS npilots, aeroplanes.name FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.squadronID = $squadronID GROUP BY aeroplanes.id ";
        $squery = mysqli_query($dbx, $sql);
        
        $pilotsFromSquadron = 0;
        $bufferArray = array();
        $bufferAcftArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $bufferAcftArray[$srow["name"]] = $srow["npilots"];
            $totalLW += $srow["npilots"];
            $pilotsFromSquadron += $srow["npilots"];
        }
        $bufferArray["npilots"] = $pilotsFromSquadron;
        $bufferArray["acft"] =  $bufferAcftArray;
        $lwAttnArray[$row["name"]] = $bufferArray;
    }
    
    //Number of victories for each faction and squadron/Staffel
    $lwVictArray = array();
    for($n = 0; $n < count($lwSqnArray); $n++){
        $squadronID = $lwSqnArray[$n]['id'];
        $sqnVictArray = array();
        $sqnConfArray = array();
        $sqnUcnfArray = array();
        $sql = "SELECT aeroplanes.name, COUNT(claimslw.confirmed) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimslw ON reports.id = claimslw.reportID ".
               "LEFT JOIN aeroplanes ON claimslw.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimslw.confirmed = 1 AND claimslw.accepted = 1 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnConfArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, COUNT(claimslw.confirmed) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimslw ON reports.id = claimslw.reportID ".
               "LEFT JOIN aeroplanes ON claimslw.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimslw.confirmed = 0 AND claimslw.accepted = 1 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnUcnfArray[$srow["name"]] = $srow["amount"];
        }
        $sqnVictArray["confirmed"] = $sqnConfArray;
        $sqnVictArray["unconfirmed"] = $sqnUcnfArray;
        
        $lwVictArray[$lwSqnArray[$n]["name"]] = $sqnVictArray;
    }
    $vvsVictArray = array();
    for($n = 0; $n < count($vvsSqnArray); $n++){
        $squadronID = $vvsSqnArray[$n]['id'];
        $sqnVictArray = array();
        $sqnConfArray = array();
        $sqnUcnfArray = array();
        $sqnConfGArray = array();
        $sqnUcnfGArray = array();
        $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 1 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 0 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnConfArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 0 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 0 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnUcnfArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 1 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 1 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnConfGArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 0 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 1 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnUcnfGArray[$srow["name"]] = $srow["amount"];
        }
        $sqnVictArray["confirmed"] = $sqnConfArray;
        $sqnVictArray["unconfirmed"] = $sqnUcnfArray;
        $sqnVictArray["confirmedGroup"] = $sqnConfGArray;
        $sqnVictArray["unconfirmedGroup"] = $sqnUcnfGArray;
        
        $vvsVictArray[$vvsSqnArray[$n]["name"]] = $sqnVictArray;
    }
    $rafVictArray = array();
    for($n = 0; $n < count($rafSqnArray); $n++){
        $squadronID = $rafSqnArray[$n]['id'];
        $sqnVictArray = array();
        $sqnDestArray = array();
        $sqnProbArray = array();
        $sqnDamgArray = array();
        $sql = "SELECT aeroplanes.name, claimsraf.enemyStatus, SUM(1-claimsraf.shared*0.5) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
               "LEFT JOIN aeroplanes ON claimsraf.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsraf.enemystatus = 1 AND claimsraf.accepted = 1 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnDestArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, claimsraf.enemyStatus, SUM(1-claimsraf.shared*0.5) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
               "LEFT JOIN aeroplanes ON claimsraf.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsraf.enemystatus = 2 AND claimsraf.accepted = 1 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnProbArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, claimsraf.enemyStatus, COUNT(claimsraf.id) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
               "LEFT JOIN aeroplanes ON claimsraf.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsraf.enemystatus = 3 AND claimsraf.accepted = 1 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnDamgArray[$srow["name"]] = $srow["amount"];
        }
        $sqnVictArray["destroyed"] = $sqnDestArray;
        $sqnVictArray["probable"] = $sqnProbArray;
        $sqnVictArray["damaged"] = $sqnDamgArray;
        
        $rafVictArray[$rafSqnArray[$n]["name"]] = $sqnVictArray;
    }

    //Losses
    $rafLossesArray = array();
    for($n = 0; $n < count($rafSqnArray); $n++){
        $squadronID = $rafSqnArray[$n]['id'];
        $sqnLossesArray = array();
        $sqnDamgArray = array();
        $sqnLostArray = array();
        $sql = "SELECT aeroplanes.name, COUNT(reports.aeroplanestatus) AS amount ".
               "FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 2 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnDamgArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, COUNT(reports.aeroplanestatus) AS amount ".
               "FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 3 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnLostArray[$srow["name"]] = $srow["amount"];
        }
        $sqnLossesArray["damaged"] = $sqnDamgArray;
        $sqnLossesArray["lost"] = $sqnLostArray;
        
        $rafLossesArray[$rafSqnArray[$n]["name"]] = $sqnLossesArray;
    }
    $vvsLossesArray = array();
    for($n = 0; $n < count($vvsSqnArray); $n++){
        $squadronID = $vvsSqnArray[$n]['id'];
        $sqnLossesArray = array();
        $sqnDamgArray = array();
        $sqnLostArray = array();
        $sql = "SELECT aeroplanes.name, COUNT(reports.aeroplanestatus) AS amount ".
               "FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 2 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnDamgArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, COUNT(reports.aeroplanestatus) AS amount ".
               "FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 3 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnLostArray[$srow["name"]] = $srow["amount"];
        }
        $sqnLossesArray["damaged"] = $sqnDamgArray;
        $sqnLossesArray["lost"] = $sqnLostArray;
        
        $vvsLossesArray[$vvsSqnArray[$n]["name"]] = $sqnLossesArray;
    }
    $lwLossesArray = array();
    for($n = 0; $n < count($lwSqnArray); $n++){
        $squadronID = $lwSqnArray[$n]['id'];
        $sqnLossesArray = array();
        $sqnDamgArray = array();
        $sqnLostArray = array();
        $sql = "SELECT aeroplanes.name, COUNT(reports.aeroplanestatus) AS amount ".
               "FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 2 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnDamgArray[$srow["name"]] = $srow["amount"];
        }
        $sql = "SELECT aeroplanes.name, COUNT(reports.aeroplanestatus) AS amount ".
               "FROM reports ".
               "LEFT JOIN aeroplanes ON reports.aeroplane = aeroplanes.id ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 3 AND reports.squadronID = $squadronID ".
               "GROUP BY aeroplanes.name";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnLostArray[$srow["name"]] = $srow["amount"];
        }
        $sqnLossesArray["damaged"] = $sqnDamgArray;
        $sqnLossesArray["lost"] = $sqnLostArray;
        
        $lwLossesArray[$lwSqnArray[$n]["name"]] = $sqnLossesArray;
    }

?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>
<p class="form_id">ACG-PAM/300-210.1</p>
<div>
    <h3><?php echo($m_row["m_name"]." (".$m_row["histdate"].")"); ?></h3>
    <p>
        <?php echo($m_row["m_name"]." (".$m_row["histdate"].")"); ?>,
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date:
         <?php echo($m_row["realdate"]); ?>.
    </p>
    <p>The mission's current status is: Closed</p>
<h3>Mission Synopsis:</h3>

<p>The following squadrons/Staffeln participated in this mission:</p>
<div>
    <div class="missionSynopsisParticipation">
        
        <?php if($totalRAF > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>RAF:</b></td>
                <td></td>
                <td><?php echo $totalRAF; ?></td>
            </tr>
            <?php 
                foreach($rafAttnArray as $sqn => $sqnRow) { 
                    $acftArray = $sqnRow["acft"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td><b>
                    <?php // echo $sqnRow["npilots"];?>
                </b></td>
            </tr>
            <?php foreach($acftArray as $acft => $npilots){ ?>
            <tr>
                <td></td>
                <td><?php echo $acft; ?></td>
                <td><?php echo $npilots; ?></td>
            </tr>
                <?php } } ?>
        </table>
        
        <?php } ?>
        
        <?php if($totalVVS > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>VVS:</b></td>
                <td></td>
                <td><?php echo $totalVVS; ?></td>
            </tr>
            <?php 
                foreach($vvsAttnArray as $sqn => $sqnRow) { 
                    $acftArray = $sqnRow["acft"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
            </tr>
            <?php foreach($acftArray as $acft => $npilots){ ?>
            <tr>
                <td></td>
                <td><?php echo $acft; ?></td>
                <td><?php echo $npilots; ?></td>
            </tr>
            <?php } } ?>
        </table>
        
        <?php } ?>
        
    </div>
    <div class="missionSynopsisParticipation">
        
        <?php if($totalLW > 0){ ?>
            
        }
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>Luftwaffe:</b></td>
                <td></td>
                <td><?php echo $totalLW; ?></td>
            </tr>
            <?php 
                foreach($lwAttnArray as $sqn => $sqnRow) { 
                    $acftArray = $sqnRow["acft"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
            </tr>
            <?php foreach($acftArray as $acft => $npilots){ ?>
            <tr>
                <td></td>
                <td><?php echo $acft; ?></td>
                <td><?php echo $npilots; ?></td>
            </tr>
            <?php } } ?>
        </table>
        
        <?php } ?>
    
    </div>
    <p>The following aircraft are claimed to be destroyed or damaged:</p>
    <div class="missionSynopsisParticipation">
        
        <?php if($totalRAF > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>RAF:</b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                foreach($rafVictArray as $sqn => $sqnVictArray) { 
                    $sqnDestArray = $sqnVictArray["destroyed"];
                    $sqnProbArray = $sqnVictArray["probable"];
                    $sqnDamgArray = $sqnVictArray["damaged"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                if(count($sqnDestArray)>0){ 
            ?>
            <tr>
                <td></td>
                <td>Destroyed</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnDestArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnProbArray)>0){
            ?>
            <tr>
                <td></td>
                <td>Probable</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnProbArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnDamgArray)>0){
            ?>
            <tr>
                <td></td>
                <td>Damaged</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnDamgArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php   
                    }
                }
                }
            ?>  
        </table>
        
        <?php } ?>
        
        <?php if($totalVVS > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>VVS:</b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                foreach($vvsVictArray as $sqn => $sqnVictArray) { 
                    $sqnConfArray = $sqnVictArray["confirmed"];
                    $sqnUcnfArray = $sqnVictArray["unconfirmed"];
                    $sqnConfGArray = $sqnVictArray["confirmedGroup"];
                    $sqnUcnfGArray = $sqnVictArray["unconfirmedGroup"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                if(count($sqnConfGArray)>0){ 
            ?>
            <tr>
                <td>Group claim</td>
                <td>Confirmed</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnConfGArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnUcnfGArray)>0){
            ?>
            <tr>
                <td>Group claim</td>
                <td>Unconfirmed</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnUcnfGArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php 
                    } 
                }
                if(count($sqnConfArray)>0){ 
            ?>
            <tr>
                <td>Individual claim</td>
                <td>Confirmed</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnConfArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnUcnfArray)>0){
            ?>
            <tr>
                <td>Individual claim</td>
                <td>Unconfirmed</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnUcnfArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php 
                    } 
                }
                }
            ?>  
        </table>
        
        <?php } ?>
        
    </div>
    <div class="missionSynopsisParticipation">
        
        <?php if($totalLW > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>Luftwaffe:</b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                foreach($lwVictArray as $sqn => $sqnVictArray) { 
                    $sqnConfArray = $sqnVictArray["confirmed"];
                    $sqnUcnfArray = $sqnVictArray["unconfirmed"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                if(count($sqnConfArray)>0){ 
            ?>
            <tr>
                <td></td>
                <td>Confirmed</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnConfArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnUcnfArray)>0){
            ?>
            <tr>
                <td></td>
                <td>Unconfirmed</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnUcnfArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php 
                    } 
                }
                }
            ?>  
        </table>
        
        <?php } ?>
        
    </div>
    <p>The following aircraft are reported damaged or lost:</p>
    <div class="missionSynopsisParticipation">
        
        <?php if($totalRAF > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>RAF:</b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                foreach($rafLossesArray as $sqn => $sqnLossesArray) { 
                    $sqnDamgArray = $sqnLossesArray["damaged"];
                    $sqnLostArray = $sqnLossesArray["lost"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                if(count($sqnDamgArray)>0){ 
            ?>
            <tr>
                <td></td>
                <td>Damaged</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnDamgArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnLostArray)>0){
            ?>
            <tr>
                <td></td>
                <td>Lost</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnLostArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php   
                    }
                }
                }
            ?>  
        </table>
        
        <?php } ?>
        
         <?php if($totalVVS > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>VVS:</b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                foreach($vvsLossesArray as $sqn => $sqnLossesArray) { 
                    $sqnDamgArray = $sqnLossesArray["damaged"];
                    $sqnLostArray = $sqnLossesArray["lost"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                if(count($sqnDamgArray)>0){ 
            ?>
            <tr>
                <td></td>
                <td>Damaged</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnDamgArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnLostArray)>0){
            ?>
            <tr>
                <td></td>
                <td>Lost</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnLostArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php   
                    }
                }
                }
            ?>  
        </table>
        
        <?php } ?>
    
    </div>
    <div class="missionSynopsisParticipation">
        
        <?php if($totalLW > 0){ ?>
        
        <table style="border-collapse:collapse;">
            <tr>
                <td><b>Luftwaffe:</b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                foreach($lwLossesArray as $sqn => $sqnLossesArray) { 
                    $sqnDamgArray = $sqnLossesArray["damaged"];
                    $sqnLostArray = $sqnLossesArray["lost"];
            ?>
            <tr style="border-top: black 1px solid">
                <td><b><?php echo $sqn; ?></b></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php 
                if(count($sqnDamgArray)>0){ 
            ?>
            <tr>
                <td></td>
                <td>Damaged</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnDamgArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php  
                    }  
                }
                if(count($sqnLostArray)>0){
            ?>
            <tr>
                <td></td>
                <td>Lost</td>
                <td></td>
                <td></td>
            </tr>
            <?php
                    foreach($sqnLostArray as $acft => $amount){
            ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><?php echo $acft; ?></td>
                        <td><?php echo $amount; ?></td>
                    </tr>
            <?php   
                    }
                }
                }
            ?>  
        </table>
        
        <?php } ?>
        
    </div>
</div>

</div>
<?php include(dirname(__FILE__).'/footer.php');