<?php 
$sql = "SELECT positionID, memberID FROM roster WHERE squadronID = $sqn";
$result = mysqli_query($dbx, $sql);
//$roster_array = mysqli_fetch_all($result);
$roster_array = array();
while($row = mysqli_fetch_assoc($result)) {
    $roster_array[$row["positionID"]] = $row["memberID"];
}

function memberSelectForm($position, $roster_array, $memberIDArray){
    if(array_key_exists($position, $roster_array)){
        $memberID = $roster_array[$position];
    } else {
        $memberID = 0;
    }
    createSelectFormWithName("roster", $position, $memberIDArray, $memberID);
}

?>
<div class="rosterSetup">
    <form id="rosterAircraft" onsubmit="return false;" >
        <table>
            <thead>
                <tr>
                    <th>Role:</th>
                    <th>Pilot:</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2"><b>1.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Staffelkapitän"); ?></td>
                    <td><?php 
                            $position = 1;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 2;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 3;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 4;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2"><b>2.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Schwarmführer"); ?></td>
                    <td><?php 
                            $position = 5;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 6;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 7;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 8;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2"><b>3.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Schwarmführer"); ?></td>
                    <td><?php 
                            $position = 9;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 10;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 11;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 12;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td colspan="2"><b>4.Schwarm</b></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Schwarmführer"); ?></td>
                    <td><?php 
                            $position = 13;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 14;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><?php echo htmlentities("Rottenführer"); ?></td>
                    <td><?php 
                            $position = 15;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Katschmarek</td>
                    <td><?php 
                            $position = 16;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
            </tbody>
        </table>
        <button id="saveRosterBtn" >Save changes</button>
        <span id="saveRosterStatus" ></span>        
    </form>
</div>

