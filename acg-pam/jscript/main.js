

function gebid(x) {
    
    return document.getElementById(x);
}


function readCookie(name){
    
   return(document.cookie.match('(^|; )'+name+'=([^;]*)')||0)[2];
}

function addSelectForm(formID, optionArray, selectedOption) {
    
    str = "<select id='" + formID + "'>";
    for(n = 0; n < optionArray.length; n++){
        str += "<option value = '" + optionArray[n][0] + "' ";
        str += (selectedOption == optionArray[n][0] ? "selected" : "" ) + " >";
        str += optionArray[n][1] + "</option>";
    }
    str += "</select>";
    return str;    
}

function addSelectFormWithName(formName, formID, optionArray, selectedOption) {
    
    str = "<select " + formName + "id='" + formID + "'>";
    for(n = 0; n < optionArray.length; n++){
        str += "<option value = '" + optionArray[n][0] + "' ";
        str += (selectedOption == optionArray[n][0] ? "selected" : "" ) + " >";
        str += optionArray[n][1] + "</option>";
    }
    str += "</select>";
    return str;    
}