function addCampaign(){
    var n = gebid("campaignName").value;
    var rx = /[^a-z0-9 ]/gi;
    if(rx.test(n)){
        gebid("addCampaignStatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("addCampaignStatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("addCampaignStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/campaignAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("addCampaignStatus").innerHTML = ajax.responseText;      
            }
        };
        ajax.send("addCampaign="+n);
    }
}

function changeCampaignName(){
    var n = gebid("campaignName").value;
    var rx = /[^a-z0-9 ]/gi;
    if(rx.test(n)){
        gebid("cnamestatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("cnamestatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("cnamestatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/campaignAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("cnamestatus").innerHTML = ajax.responseText;      
            }
        };
        ajax.send("changeCampaignName="+n);
    }
}

function deleteCampaign(){
    if(confirm("Do you realy want to delete this campaign?")) {
        var ajax = ajaxObj("POST", "./includes/campaignAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.href="./campaignAdministration.php";
            }
        };
        ajax.send("deleteCampaign=true");  
    }
}
    


