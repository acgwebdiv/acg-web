function editReport(mission_id, member_id){
    
    var ajax = ajaxObj("POST", "./includes/reportLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var faction = ajax.responseText;
            if(faction === "RAF"){
                window.location = "createReportRAF.php?mi_id="+mission_id;
            } else if(faction === "LW"){
                window.location = "createReportLW.php?mi_id="+mission_id;
            } else if(faction === "VVS"){
                window.location = "createReportVVS.php?mi_id="+mission_id;
            } 
        }
    };
    ajax.send("getFaction="+member_id+"&mi_id="+mission_id);
}

//function showReport(report_id, type){
//    
//    if(type === 1){
//        window.location = "reportRAF.php?r_id="+report_id;
//    } else if(faction === 2){
//        window.location = "reportLW.php?r_id="+report_id;
//    }
//}

function getSquCode(){
   
    var s = gebid("squadron").value;
    var ajax = ajaxObj("POST", "./includes/reportLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("squadronCode").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("getSCode="+s);
}

function addRAFClaim(divName, claimID, counter, claimStatusArray, claimStatus, claimAcftArray, 
                     claimAcft, shared, description){
    
    var idStr = "c" + (counter + 1);
    var newClaimDiv = document.createElement('div');
    newClaimDiv.className = "claim";
    newClaimDiv.id = idStr;
    var divStr = "";
    divStr += "<span id='claimID" + idStr + "' style='display: none' >" + claimID + "</span>";
    divStr += addSelectForm("claimStatus" + idStr, claimStatusArray, claimStatus);
    divStr += addSelectForm("claimAeroplane" + idStr, claimAcftArray, claimAcft);
    divStr += "<span id='sharedSpan'>Shared</span>";
    divStr += "<input type='checkbox' id='shared"  + idStr + "' " + (shared ? "checked" : "" ) + " >";
    divStr += "<input type='button' value='X' onclick='removeRAFClaim(\"" + divName +"\", \"" + idStr + "\")' ><br>";
    divStr += "<textarea id='claimDescription" + idStr + "' rows='1' cols='50' maxlength='200'>"+description+"</textarea>";
    newClaimDiv.innerHTML = divStr;
    gebid(divName).appendChild(newClaimDiv);
    counter++;
    return counter;
}

function addLWClaim(divName, claimID, counter, claimAcftArray, claimAcft, claimTime, opponent, 
                    place, typeOfDestArray, typofDestr, typeOfImpactArray, typofImpact,
                    fateOfCrewArray, fateofCrew, witnessID, witness){
    
    var idStr = "c" + (counter + 1);
    var groundConf = "";
    if(witnessID === -1){
        groundConf = " checked ";
        witness = "";
    }
    var newClaimDiv = document.createElement('div');
    newClaimDiv.className = "claim";
    newClaimDiv.id = idStr;
    var divStr = "";
    divStr += "<span id='claimID" + idStr + "' style='display: none' >" + claimID + "</span>";
    divStr += "<table><tr>";
    divStr += "<td><input type='button' value='X' onclick='removeLWClaim(\"" + divName +"\", \"" + idStr + "\")' ></td></tr>";
    divStr += "<tr><td>Time</td><td>Place</td><td>Aircraft</td><td>Markings</td><td>Ground Confirmation</td></tr>";
    divStr += "<tr><td><input type='text' id='cTime"+ idStr +"' size='5' maxlength='5' value = '"+ claimTime +"' ></td>";
    divStr += "<td><input type='text' id='place"+ idStr +"' value = '"+ place +"' ></td>";
    divStr += "<td>" + addSelectForm("claimAcft"+ idStr, claimAcftArray, claimAcft) + "</td>";
    divStr += "<td><input type='text' id='opponent"+ idStr +"' value = '"+ opponent +"' ></td>";
    divStr += "<td><input type='checkbox' id='groundConf"+ idStr +"'"+ groundConf +" ></td></tr></table>";
    divStr += "<table><tr><td>Type of destruction:</td>";
    divStr += "<td>Type of impact on the ground:</td>";
    divStr += "<td>Fate of crew:</td>";
    divStr += "<td>Witness</td></tr>";
    divStr += "<tr><td>" + addSelectForm("typofDestr" + idStr, typeOfDestArray, typofDestr) + "</td>";
    divStr += "<td>" + addSelectForm("typofImpact" + idStr, typeOfImpactArray, typofImpact) + "</td>";
    divStr += "<td>" + addSelectForm("fateofCrew" + idStr, fateOfCrewArray, fateofCrew) + "</td>";
    divStr += "<td><input type='text' id='witness" + idStr +"' value = '"+ witness +"' onchange='checkWitness(\"" + idStr + "\")'>";
    divStr += "<span id='witnessStatus" + idStr + "'></span></td></tr></table>";
    newClaimDiv.innerHTML = divStr;
    gebid(divName).appendChild(newClaimDiv);
    counter++;
    return counter;
}

function addVVSClaim(divName, claimID, counter, claimAcftArray, claimAcft, claimTime, 
                    place, witnessID, witness, groupClaim, description){
    
    var idStr = "c" + (counter + 1);
    var groundConf = "";
    if(witnessID === -1){
        groundConf = " checked ";
        witness = "";
    }
    var newClaimDiv = document.createElement('div');
    newClaimDiv.className = "claim";
    newClaimDiv.id = idStr;
    var divStr = "";
    divStr += "<span id='claimID" + idStr + "' style='display: none' >" + claimID + "</span>";
    divStr += "<table><tr>";
    divStr += "<td><input type='button' value='X' onclick='removeVVSClaim(\"" + divName +"\", \"" + idStr + "\")' ></td></tr>";
    divStr += "<tr><td>Time</td><td>Place</td><td>Aircraft</td><td>Ground Confirmation</td></tr>";
    divStr += "<tr><td><input type='text' id='cTime"+ idStr +"' size='5' maxlength='5' value = '"+ claimTime +"' ></td>";
    divStr += "<td><input type='text' id='place"+ idStr +"' value = '"+ place +"' ></td>";
    divStr += "<td>" + addSelectForm("claimAcft"+ idStr, claimAcftArray, claimAcft) + "</td>";
    divStr += "<td><input type='checkbox' id='groundConf"+ idStr +"'"+ groundConf +" ></td></tr></table>";
    divStr += "<table><tr>";
    divStr += "<td>Group claim</td>";
    divStr += "<td>Witness</td></tr>";
    divStr += "<tr><td><input type='checkbox' id='groupClaim"  + idStr + "' " + (groupClaim ? "checked" : "" ) + " ></td>";
    divStr += "<td><input type='text' id='witness" + idStr +"' value = '"+ witness +"' onchange='checkWitness(\"" + idStr + "\")'>";
    divStr += "<span id='witnessStatus" + idStr + "'></span></td></tr></table>";
    divStr += "<textarea id='claimDescription" + idStr + "' rows='5' cols='50' maxlength='500'>"+description+"</textarea>";
    newClaimDiv.innerHTML = divStr;
    gebid(divName).appendChild(newClaimDiv);
    counter++;
    return counter;
}

function addGroundClaim(divName, claimID, counter, claimObjArray, claimObj, description, amount){
    
    var idStr = "gc" + (counter + 1);
    var newClaimDiv = document.createElement('div');
    newClaimDiv.className = "groundclaims";
    newClaimDiv.id = idStr;
    spinnerName = "groundSpinner" + idStr;
    var divStr = "";
    divStr += "<span id='groundClaimID" + idStr + "' style='display: none' >" + claimID + "</span>";
    divStr += "<input type='button' value='X' onclick='removeGroundClaim(\"" + divName +"\", \"" + idStr + "\")' ><br>";
    divStr += addSelectForm("claimObject" + idStr, claimObjArray, claimObj);
    divStr += "<span style='margin-left: 10px'>Amount:</span>";
    divStr += "<span class='spinnerButton'><input type='button' value='<' onclick='changeSpinner(\"" + spinnerName +"\", " + (- 1) + ", 1)' ></span>";
    divStr += "<span id='" + spinnerName + "'>" + amount + "</span>";
    divStr += "<span class='spinnerButton'><input type='button' value='>' onclick='changeSpinner(\"" + spinnerName +"\", " + (+ 1) + ", 1)' ></span><br>";
    divStr += "<textarea id='groundClaimDescription" + idStr + "' rows='5' cols='50' maxlength='500'>"+description+"</textarea>";
    newClaimDiv.innerHTML = divStr;
    gebid(divName).appendChild(newClaimDiv);
    counter++;
    return counter;
}

function checkWitness(counter){
    var witness = gebid("witness"+counter).value;
    gebid("witnessStatus"+counter).innerHTML = 'checking...';
    if(witness !== ""){
        var ajax = ajaxObj("POST", "./includes/reportLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("witnessStatus"+counter).innerHTML = ajax.responseText;      
            }
        };
        ajax.send("witnessCheck="+witness);
    } else {
        gebid("witnessStatus"+counter).innerHTML = "";
    }
    
}

function changeSpinner(spinnerName, amount, lowerLimit){
    
    var isValue = parseInt(gebid(spinnerName).innerHTML);
    var setValue = Math.max(isValue+amount, lowerLimit);
    gebid(spinnerName).innerHTML = setValue;
}

function removeLWClaim(divName, id){
    var claimID = gebid("claimID"+id).innerHTML;
    if(claimID !== ""){
        var ajax = ajaxObj("POST", "./includes/reportLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("submitStatus").innerHTML = ajax.responseText;
                     
            }
        };
        ajax.send("clearLWClaim="+claimID);
    }
    var parent = gebid(divName);
    var child = gebid(id);
    parent.removeChild(child); 
}


function removeRAFClaim(divName, id){
    var claimID = gebid("claimID"+id).innerHTML;
    if(claimID !== ""){
        var ajax = ajaxObj("POST", "./includes/reportLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("submitStatus").innerHTML = ajax.responseText;
                     
            }
        };
        ajax.send("clearRAFClaim="+claimID);
    }
    var parent = gebid(divName);
    var child = gebid(id);
    parent.removeChild(child); 
}

function removeVVSClaim(divName, id){
    var claimID = gebid("claimID"+id).innerHTML;
    if(claimID !== ""){
        var ajax = ajaxObj("POST", "./includes/reportLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("submitStatus").innerHTML = ajax.responseText;
                     
            }
        };
        ajax.send("clearVVSClaim="+claimID);
    }
    var parent = gebid(divName);
    var child = gebid(id);
    parent.removeChild(child); 
}

function removeGroundClaim(divName, id){
    var claimID = gebid("groundClaimID"+id).innerHTML;
    if(claimID !== ""){
        var ajax = ajaxObj("POST", "./includes/reportLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("submitStatus").innerHTML = ajax.responseText;
                     
            }
        };
        ajax.send("clearGroundClaim="+claimID);
    }
    var parent = gebid(divName);
    var child = gebid(id);
    parent.removeChild(child); 
}

function submitLWReport(missionID, authorID){
    
    var informationComplete = true;
    var feedbackTxt = "";
    var sqn = gebid("squadron").value;
    var swarm = gebid("swarm").value;
    var swarmPos = gebid("swarmPos").value;
    var aerodrome = encodeURIComponent(gebid("base").value);
    var aeroplane = gebid("actType").value;
    var markings = encodeURIComponent(gebid("markings").value);
    var synopsis = encodeURIComponent(gebid("synopsis").value);
    var pilotStatus = gebid("pilotStatus").value;
    var acftStatus = gebid("acftStatus").value;
    
    if(aerodrome === ""){
        informationComplete = false;
        feedbackTxt = feedbackTxt + "Aerodrome missing. ";
    }
    if(markings === ""){
        informationComplete = false;
        feedbackTxt = feedbackTxt + "Markings missing. ";
    }
    
    if(!informationComplete){
        alert(feedbackTxt);
        return false;
    } else {
        var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
        ajax.send("submitLWReport="+missionID+"&authorID="+authorID+"&sqnID="+sqn+
                "&swarm="+swarm+"&swarmPos="+swarmPos+"&aerodrome="+aerodrome+
                "&aeroplane="+aeroplane+"&markings="+markings+"&synopsis="+synopsis+
                "&pilotStatus="+pilotStatus+"&acftStatus="+acftStatus);
        if(ajax.responseText != "true"){
            alert("An error occured during report submission. Notify the database administration. Error: "+ajax.responseText);
            return false;
        } else {
            return true;
        }
//        gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;
    }      
}

function submitRAFReport(missionID, authorID){
    
    var informationComplete = true;
    var feedbackTxt = "";
    var sqn = gebid("squadron").value;
    var flight = gebid("flight").value;
    var section = gebid("section").value;
    var sectionPos = gebid("sectionPos").value;
    var aerodrome = encodeURIComponent(gebid("base").value);
    var aeroplane = gebid("actType").value;
    var markings = encodeURIComponent(gebid("markings").value);
    var serialNo = encodeURIComponent(gebid("serialNo").value);
    var synopsis = encodeURIComponent((gebid("synopsis").value));
    var pilotStatus = gebid("pilotStatus").value;
    var acftStatus = gebid("acftStatus").value;
    
    if(aerodrome === ""){
        informationComplete = false;
        feedbackTxt = feedbackTxt + "Aerodrome missing. ";
    }
    if(markings === ""){
        informationComplete = false;
        feedbackTxt = feedbackTxt + "Markings missing. ";
    }
    
    if(!informationComplete){
        alert(feedbackTxt);
        return false;
    } else {
        var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
        ajax.send("submitRAFReport="+missionID+"&authorID="+authorID+"&sqnID="+sqn+
                "&flight="+flight+"&section="+section+"&sectionPos="+sectionPos+"&aerodrome="+aerodrome+
                "&aeroplane="+aeroplane+"&markings="+markings+"&serialNo="+serialNo+
                "&synopsis="+synopsis+"&pilotStatus="+pilotStatus+"&acftStatus="+acftStatus);
        if(ajax.responseText != "true"){
            alert("An error occured during report submission. Notify the database administration. Error: "+ajax.responseText);
            return false;
        } else {
            return true;
        }
//        gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;
    }      
}

function submitVVSReport(missionID, authorID){
    
    var informationComplete = true;
    var feedbackTxt = "";
    var sqn = gebid("squadron").value;
    var aerodrome = encodeURIComponent(gebid("base").value);
    var aeroplane = gebid("actType").value;
    var markings = encodeURIComponent(gebid("markings").value);
    var synopsis = encodeURIComponent(gebid("synopsis").value);
    var pilotStatus = gebid("pilotStatus").value;
    var acftStatus = gebid("acftStatus").value;
    
    if(aerodrome === ""){
        informationComplete = false;
        feedbackTxt = feedbackTxt + "Aerodrome missing. ";
    }
    if(markings === ""){
        informationComplete = false;
        feedbackTxt = feedbackTxt + "Markings missing. ";
    }
    
    if(!informationComplete){
        alert(feedbackTxt);
        return false;
    } else {
        var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
        ajax.send("submitVVSReport="+missionID+"&authorID="+authorID+"&sqnID="+sqn+
                "&aerodrome="+aerodrome+
                "&aeroplane="+aeroplane+"&markings="+markings+"&synopsis="+synopsis+
                "&pilotStatus="+pilotStatus+"&acftStatus="+acftStatus);
        if(ajax.responseText != "true"){
            alert("An error occured during report submission. Notify the database administration. Error: "+ajax.responseText);
            return false;
        } else {
            return true;
        }
//        gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;
    }      
}

function submitLWClaim(missionID, authorID, counter){
    
    var claimID = gebid("claimID"+counter).innerHTML;
    var aeroplane = gebid("claimAcft"+counter).value;
    var cTime = encodeURIComponent(gebid("cTime"+counter).value);
    var place = encodeURIComponent(gebid("place"+counter).value);
    var opponent = encodeURIComponent(gebid("opponent"+counter).value);
    var groundConf = (gebid("groundConf"+counter).checked === true) ? 1 : 0;
    var typofDestr = gebid("typofDestr"+counter).value;
    var typofImpact = gebid("typofImpact"+counter).value;
    var fateofCrew = gebid("fateofCrew"+counter).value;
    var witnessStatus = gebid("witnessStatus"+counter).innerHTML;
    var witness = (witnessStatus === "Witness existing") ? gebid("witness"+counter).value : "";
    
    if(cTime === ""){
        gebid("submitStatus").innerHTML = "Please fill out all neccesarry fields (Claim Time).";
    } else {
        //Sync
        var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
        ajax.send("submitLWClaim="+missionID+"&authorID="+authorID+"&claimID="+claimID+"&aeroplane="+aeroplane+
                "&cTime="+cTime+"&place="+place+"&opponent="+opponent+"&typofDestr="+typofDestr+
                "&typofImpact="+typofImpact+"&fateofCrew="+fateofCrew+"&groundConf="+groundConf+"&witness="+witness);
        if(ajax.responseText != "true"){
            alert("An error occured during submission: "+
                  ajax.responseText+
                  " Please inform the administrator.");
        }
        gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;
    }
     
}

function submitRAFClaim(missionID, authorID, counter){

    var claimID = gebid("claimID"+counter).innerHTML;
    var claimStatus = gebid("claimStatus"+counter).value;
    var claimAeroplane = gebid("claimAeroplane"+counter).value;
    var shared = gebid("shared"+counter).checked;
    var description = encodeURIComponent(gebid("claimDescription"+counter).value);    
    
    var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
    ajax.send("submitRAFClaim="+missionID+"&authorID="+authorID+"&claimID="+claimID+
            "&claimStatus="+claimStatus+"&claimAeroplane="+claimAeroplane+"&shared="+shared+
            "&claimDescription="+description);
    if(ajax.responseText != "true"){
            alert("An error occured during submission: "+
            ajax.responseText+
            " Please inform the administrator.");
    }
    gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;   
}

function submitVVSClaim(missionID, authorID, counter){
    
    var claimID = gebid("claimID"+counter).innerHTML;
    var aeroplane = gebid("claimAcft"+counter).value;
    var cTime = encodeURIComponent(gebid("cTime"+counter).value);
    var place = encodeURIComponent(gebid("place"+counter).value);
    var groundConf = (gebid("groundConf"+counter).checked === true) ? 1 : 0;
    var witnessStatus = gebid("witnessStatus"+counter).innerHTML;
    var witness = (witnessStatus === "Witness existing") ? gebid("witness"+counter).value : "";
    var groupClaim = gebid("groupClaim"+counter).checked;
    var description = encodeURIComponent(gebid("claimDescription"+counter).value);
    
    if(cTime === ""){
        gebid("submitStatus").innerHTML = "Please fill out all neccesarry fields (Claim Time).";
    } else {
        //Sync
        var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
        ajax.send("submitVVSClaim="+missionID+"&authorID="+authorID+"&claimID="+claimID+"&aeroplane="+aeroplane+
                "&cTime="+cTime+"&place="+place+"&groundConf="+groundConf+"&witness="+witness+"&groupClaim="+groupClaim+
            "&claimDescription="+description);
        if(ajax.responseText != "true"){
            alert("An error occured during submission: "+
                  ajax.responseText+
                  " Please inform the administrator.");
        }
        gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;
    }
     
}

function submitGroundClaim(missionID, authorID, counter){

    var claimID = gebid("groundClaimID"+counter).innerHTML;
    var claimObject = gebid("claimObject"+counter).value;
    var amount = gebid("groundSpinner"+counter).innerHTML;
    var description = gebid("groundClaimDescription"+counter).value;
    var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
    ajax.send("submitGroundClaim="+missionID+"&authorID="+authorID+"&claimID="+claimID+
            "&claimObject="+claimObject+"&amount="+amount+
            "&claimDescription="+description);
    gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;   
}


function clearClaims(missionID, authorID){
   
    var ajax = syncAjaxObj("POST", "./includes/reportLogic.php");
    ajax.send("clearClaims="+missionID+"&authorID="+authorID);
    gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML = gebid("submitStatus").innerHTML + ajax.responseText;   
}

function saveAuthorMissionChange(reportID){
    
    var missionId = gebid("mi"+reportID).value;
    var characterID = gebid("ch"+reportID).value;
    gebid("rStatus"+reportID).innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/reportLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            
            gebid("rStatus"+reportID).innerHTML = ajax.responseText;
            if(ajax.responseText === "Author/Mission changed.") {
                window.location.reload();
            }
        }
    };
    ajax.send("editReportAuthorMission="+reportID+"&mi="+missionId+"&ch="+characterID);
}

function deleteReport(reportID){
    if(confirm("Do you realy want to delete this report?")) {
        var ajax = ajaxObj("POST", "./includes/reportLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.reload();
            }
        };
        ajax.send("deleteReport="+reportID);  
    }
}