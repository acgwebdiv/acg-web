function addGroundTarget(){
    var n = gebid("groundTargetName").value;
    var rx = /[^a-z0-9/.\- ]/gi;
    if(rx.test(n)){
        gebid("addGroundTargetStatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("addGroundTargetStatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("addGroundTargetStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/groundTargetAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("addGroundTargetStatus").innerHTML = ajax.responseText;
                if(ajax.responseText === "Ground target added."){
                    window.location.reload();
                }
            }
        };
        ajax.send("addGroundTarget="+n);
    }
}

function changeGroundTarget(){
    var n = gebid("groundTargetName").value;
    var rx = /[^a-z0-9/.\- ]/gi;
    if(rx.test(n)){
        gebid("gtChangeStatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("gtChangeStatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("gtChangeStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/groundTargetAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("gtChangeStatus").innerHTML = ajax.responseText;
                if(ajax.responseText === "Changes saved."){
                    window.location.reload();
                }
            }
        };
        ajax.send("editGroundTarget="+n);
    }
}

function deleteGroundTarget(){
    if(confirm("Do you realy want to delete this ground target?")) {
        var ajax = ajaxObj("POST", "./includes/groundTargetAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.href="./groundTargetAdministration.php";
            }
        };
        ajax.send("deleteGroundTarget=true");  
    }
}
    





