function transferMember(){
    var s = gebid("squadron").value;
    var y = gebid("tdateY").value;
    var m = gebid("tdateM").value;
    var d = gebid("tdateD").value;
    var rx = /[^0-9]/gi;
    if(rx.test(y) || rx.test(m) || rx.test(d)){
        gebid("transferStatus").innerHTML = 'Transfer date has wrong format.';
    } else {
        gebid("transferStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/transferMemberLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("transferStatus").innerHTML = ajax.responseText;
                window.location.reload();
            }
        };
        ajax.send("transfer="+s+"&y="+y+"&m="+m+"&d="+d); 
    }
}

function deleteTransfer(id){
    if(confirm("Do you realy want to delete this transfer?")) {
        var ajax = ajaxObj("POST", "./includes/transferMemberLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.reload();
            }
        };
        ajax.send("deleteTransfer="+id);  
    }
}