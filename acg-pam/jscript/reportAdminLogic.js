function changeAccepted(a, reportID, acceptedBy, memberID) {

    gebid("submitStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/reportAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            gebid("submitStatus").innerHTML = response;
            if(response == "Report accept status changed."){
                location.reload();
            }
        }
    };
    ajax.send("changeAccepted="+!a+"&reportID="+reportID+"&acceptedBy="+acceptedBy+"&memberID="+memberID);
}

function submitComment(reportID, authorID){
    
    var comment = encodeURIComponent(gebid("comment").value);
    if(comment !== ""){
        gebid("submitStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/reportAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                var response = ajax.responseText;
                gebid("submitStatus").innerHTML = response;
                if(response == "Report comment submitted."){
                    location.reload();
                }
            }
        };
        ajax.send("submitComment="+comment+"&reportID="+reportID+"&authorID="+authorID);
    }
}

function deleteComment(commentID){
    if(confirm("Do you realy want to delete this comment?")) {
        var ajax = ajaxObj("POST", "./includes/reportAdminLogic.php");
        gebid("submitStatus").innerHTML = 'checking ...';
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                var response = ajax.responseText;
                gebid("submitStatus").innerHTML = response;
                if(response == "Comment deleted."){
                    location.reload();
                }
            }
        };
        ajax.send("deleteComment="+commentID);  
    }
}

function confirmClaim(confirmed, claimID, faction) {

    var ajax = ajaxObj("POST", "./includes/reportAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            if(response == "TRUE"){
                location.reload();
            }
        }
    };
    ajax.send("confirmClaim="+!confirmed+"&claimID="+claimID+"&faction="+faction);
}

function changeClaimAccepted(accepted, claimID, faction){

    gebid("submitStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/reportAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            gebid("submitStatus").innerHTML = response;
            if(response == "Claim accept status changed."){
                location.reload();
            }
        }
    };
    ajax.send("changeClaimAccepted="+accepted+"&claimID="+claimID+"&faction="+faction);
}