function checkName(x){
        
    if(x.length === 0){
        return false;
    }
    var rx = /[^a-z0-9 \-]/gi;
    return !rx.test(x);
}

function checkCallsign(x){

    if(x.length === 0){
        return false;
    }
    var rx = /[^a-z0-9]/gi;
    return !rx.test(x);
}

function checkNumber(x, digits){

    var tf = gebid(x).value;
    var rx = new RegExp("^[0-9]{"+digits+"}$");
    return rx.test(tf);
}

function jChecklist(){
    
    var activate = true;
    activate = activate && gebid("aboutUS").checked;
    activate = activate && gebid("commitment").checked;
    activate = activate && gebid("informHC").checked;
    activate = activate && gebid("changeSteamName").checked;
    activate = activate && gebid("fieldBook").checked;
    activate = activate && gebid("informCO").checked;
    
    if(activate){      
        gebid("addMemberButton").disabled = false;
    } else {
        gebid("addMemberButton").disabled = true;
    }
    
    
}

function addMember(){

    var n = gebid("membername").value;
    var c = gebid("callsign").value;
    var y = gebid("jdateY").value;
    var m = gebid("jdateM").value;
    var d = gebid("jdateD").value;
    var a = gebid("admin").checked;
    var s = gebid("squadron").value;
    var rx = /[^0-9]/gi;
    if(n === "" || c === "" || y === "" || m === "" || d === "") {

        gebid("submitStatus").innerHTML = "Please fill out all fields.";
    } else if(!checkName(n)) {

        gebid("submitStatus").innerHTML = "Membername format not allowed.";
    } else if(!checkCallsign(c)) {

        gebid("submitStatus").innerHTML = "Callsign format not allowed.";
    } else if(rx.test(y) || rx.test(m) || rx.test(d)){

        gebid("submitStatus").innerHTML = "Joining date format not allowed.";
    } else {

        gebid("submitStatus").innerHTML = 'please wait ...';
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("submitStatus").innerHTML = ajax.responseText;
            }
        };
        ajax.send("n="+n+"&c="+c+"&y="+y+"&m="+m+"&d="+d+"&a="+a+"&s="+s);
    }
}  

function checkDBforMembername(){
    var n = gebid("membername").value;
    if(n !== ""){
        gebid("mnamestatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("mnamestatus").innerHTML = ajax.responseText;;      
            }
        };
        ajax.send("membernamecheck="+n);
    }
}

function checkDBforCallsign(){
    var c = gebid("callsign").value;
    if(c !== ""){
        gebid("callsignstatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("callsignstatus").innerHTML = ajax.responseText;
            }
        };
        ajax.send("callsigncheck="+c);
    }
}

function changeMembername(){
    var n = gebid("membername").value;
    if(!checkName(n)){
        gebid("mnamestatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("mnamestatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("mnamestatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("mnamestatus").innerHTML = ajax.responseText;      
            }
        };
        ajax.send("changeMembername="+n);
    }
}
    
function changeCallsign(){
    var c = gebid("callsign").value;
    if(!checkCallsign(c)){
        gebid("callsignstatus").innerHTML = 'Callsign has wrong format.';
    } else if (c === "") {
        gebid("callsignstatus").innerHTML = 'Callsign must not be empty.';
    } else {
        gebid("callsignstatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("callsignstatus").innerHTML = ajax.responseText;
            }
        };
        ajax.send("changeCallsign="+c);
    }
}
    

function changeAdmin(){
    var a = (gebid("admin").checked === true) ? 1 : 0;
    var mb = (gebid("missionBuilder").checked === true) ? 1 : 0;
    var mv = (gebid("mapViewer").checked === true) ? 1 : 0;
    gebid("adminStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("adminStatus").innerHTML = ajax.responseText;
        }
    };
    ajax.send("changeAdmin="+a+"&mb="+mb+"&mv="+mv);
}

function deleteMember(){
    if(confirm("Do you realy want to delete this member?")) {
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.href="./memberAdministration.php";
            }
        };
        ajax.send("deleteMember=true");  
    }
}

function updateStatus(){
    var s = gebid("status").value;
    var y = gebid("sudateY").value;
    var m = gebid("sudateM").value;
    var d = gebid("sudateD").value;
    var c = encodeURIComponent(gebid("sucomment").value);
    var rx = /[^0-9]/gi;
    if(rx.test(y) || rx.test(m) || rx.test(d)){
        gebid("statusUpdateStatus").innerHTML = 'Status date has wrong format.';
    } else {
        gebid("statusUpdateStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("statusUpdateStatus").innerHTML = ajax.responseText;
                window.location.reload();
            }
        };
        ajax.send("statusUpdate="+s+"&y="+y+"&m="+m+"&d="+d+"&c="+c); 
    }
}

function deleteStatusUpdate(id){
    if(confirm("Do you realy want to delete this status update?")) {
        var ajax = ajaxObj("POST", "./includes/memberAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("statusUpdateStatus").innerHTML = ajax.responseText;
                window.location.reload();
            }
        };
        ajax.send("deleteStatusUpdate="+id);  
    }
}