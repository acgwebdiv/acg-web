function saveAircraftMember(aircraftID){
    
    var memberID = gebid(aircraftID).value;

    var ajax = syncAjaxObj("POST", "./includes/rosterLogic.php");
    ajax.send("saveAircraft="+memberID+"&a="+aircraftID);
    if(ajaxReturn(ajax) === true) {
        gebid("saveAircraftStatus").innerHTML = ajax.responseText;
    }       
}

function saveRosterMember(positionID, squadronID){
    
    var memberID = gebid(positionID).value;
   
    var ajax = syncAjaxObj("POST", "./includes/rosterLogic.php");
    ajax.send("saveRoster="+positionID+"&m="+memberID+"&s="+squadronID); 
    if(ajaxReturn(ajax) === true) {
        gebid("saveRosterStatus").innerHTML = ajax.responseText;
    }
}
