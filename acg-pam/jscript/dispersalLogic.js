function enterDispersal(m_id, squ){
    window.location = "missionDispersal.php?mi_id="+m_id+"&squ="+squ;
} 

function reportForDuty(mission, squadron, user, faction) {
    
    var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            gebid("submitStatus").innerHTML = ajax.responseText;
            if(response === "true"){
                window.location = "missionDispersal.php?mi_id="+mission+"&squ="+squadron;
            } else if(response === "newCharacterQuery"){
                window.location = "newCharacterQuery.php?mi_id="+mission+"&isReport=0&s="+squadron+"&f="+faction;
            }
        }
    };
    ajax.send("reportForDuty="+mission+"&s="+squadron+"&u="+user+"&f="+faction);
}

function cancelReportForDuty(mission, user) {

    gebid("submitStatus").innerHTML = 'please wait ...';
    var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            gebid("submitStatus").innerHTML = ajax.responseText;
            if(response === "true"){
                location.reload();
            }
        }
    };
    ajax.send("cancelReportForDuty="+mission+"&u="+user);
}

function exitDispersal(id) {

    window.location = "missionDetails.php?m_id="+id;
}

function updateButton(userID, squadronID, missionID){
    
    var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            gebid("submitStatus").innerHTML = ajax.responseText;
            if(response === "true"){
                gebid("dutyBtnSpan").style.display = "none";
                gebid("cancelDutyBtnSpan").style.display = "inline-block";
            } else {
                gebid("dutyBtnSpan").style.display = "inline-block";
                gebid("cancelDutyBtnSpan").style.display = "none";
            }
            gebid("submitStatus").innerHTML = "&nbsp;";
        }
    };
    ajax.send("updateButton="+userID+"&s="+squadronID+"&mi="+missionID);
}


function saveDispersalMemberLW(positionID, squadronID, missionID){
    
    var memberID = gebid(positionID).value;
   
    var ajax = syncAjaxObj("POST", "./includes/dispersalLogic.php");
    ajax.send("saveDispersalLW="+positionID+"&m="+memberID+"&s="+squadronID+"&mi="+missionID); 
    if(ajaxReturn(ajax) === true) {
//        alert(ajax.responseText);
        gebid("saveDispersalStatus").innerHTML = ajax.responseText;
    }
}

function saveDispersalMemberRAF(positionID, positionIDFlight, positionIDSection, squadronID, missionID){
    
    var memberID = gebid(positionID).value;
    var flightID = gebid(positionIDFlight).value;
    var sectionID = gebid(positionIDSection).value;
   
    var ajax = syncAjaxObj("POST", "./includes/dispersalLogic.php");
    ajax.send("saveDispersalRAF="+positionID+"&m="+memberID+"&s="+squadronID+"&mi="+missionID+
              "&flt="+flightID+"&sect="+sectionID); 
    if(ajaxReturn(ajax) === true) {
        gebid("saveDispersalStatus").innerHTML = ajax.responseText;
    }
}

function checkPilot(){
    var witness = gebid("addPilotInput").value;
    gebid("addPilotStatus").innerHTML = 'checking...';
    if(witness !== ""){
        var ajax = ajaxObj("POST", "./includes/reportLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                if(ajax.responseText === "Witness existing" ){
                   gebid("addPilotStatus").innerHTML = "Pilot found.";  
                } else {
                   gebid("addPilotStatus").innerHTML = "Pilot not found.";
                }    
            }
        };
        ajax.send("witnessCheck="+witness);
    } else {
        gebid("addPilotStatus").innerHTML = "";
    }
}

function addPilot(mission, squadron, faction){
    
    var pilotStatus = gebid("addPilotStatus").innerHTML;
    var pilot = gebid("addPilotInput").value;

    if(pilotStatus === "Pilot found."){
        var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                
                var response = ajax.responseText;
                gebid("submitStatus").innerHTML = ajax.responseText;
                if(response === "true"){

                    autoRefresh(10000);
                }
            }
        };
        ajax.send("addPilot="+pilot+"&m="+mission+"&s="+squadron+"&f="+faction);
    } else {
        gebid("addPilotStatus").innerHTML = "";
    }
}

function kickPilot(mission, user){
    
    var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
    var showSetup;
    if(gebid("dispersalSetupDiv").style.display == "none"){
        showSetup = 0;
    } else {
        showSetup = 1;
    }
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            gebid("submitStatus").innerHTML = ajax.responseText;
            if(response === "true"){
                
                autoRefresh(10000);
            }
        }
    };
    ajax.send("cancelReportForDuty="+mission+"&u="+user);
}

function getUnassignedPilots(mission, faction, squadron, isAdmin){
    
    gebid("submitStatus").innerHTML = 'updating unassigned pilots ...';
    var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            
            var upArray = JSON.parse(ajax.responseText);
              
            var table = gebid("unassignedPilotsTable");
            for(var i = table.rows.length - 1; i >= 0; i--)
            {
                table.deleteRow(i);
            }
              
            for(n = 0; n < upArray.length; n++){
                
                row = table.insertRow(n);
                cell1 = row.insertCell(0);
                cell1.innerHTML = upArray[n][5] + " " + upArray[n][4] + " '" + upArray[n][1] + "' " + upArray[n][3];
                if(isAdmin){
                    cell2 = row.insertCell(1);
                    cell2.innerHTML = "<button onclick='kickPilot(" + mission + ", " + upArray[n][2] + ")'>X</button>";
                } 
            }
        }
        gebid("submitStatus").innerHTML = '&nbsp;';
    };
    ajax.send("getUPilots="+mission+"&s="+squadron+"&f="+faction);

}

function getAssignedPilotsDisplay(mission, faction, squadron){
    
    gebid("submitStatus").innerHTML = 'updating assigned display ...';
    var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {

            var displayArray = JSON.parse(ajax.responseText);
            var table = gebid("dispersalDisplayTable");
            for(var i = table.rows.length - 1; i >= 0; i--)
            {
                table.deleteRow(i);
            }
            addRowsToTable(table, displayArray);
        }
        gebid("submitStatus").innerHTML = '&nbsp;';
    };
    
    if(faction == "RAF"){
        //RAF
        ajax.send("getAPilotsDisplayRAF="+mission+"&s="+squadron+"&f="+faction);

    } else if (faction == "LW") {
        if(squadron == 11){
            //STAB
            ajax.send("getAPilotsDisplayLWStab="+mission+"&s="+squadron+"&f="+faction);
        } else {
            //LW
            ajax.send("getAPilotsDisplayLW="+mission+"&s="+squadron+"&f="+faction);
        }
    }
    

}

function getAssignedPilotsSetup(mission, faction, squadron){
    

    gebid("submitStatus").innerHTML = 'updating assigned pilots setup ...';
    var ajax = ajaxObj("POST", "./includes/dispersalLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {

            var setupArray = JSON.parse(ajax.responseText);
            var table = gebid("dispersalSetupTable");
            for(var i = table.rows.length - 1; i >= 0; i--)
            {
                table.deleteRow(i);
            }
            addRowsToTable(table, setupArray);
        }
        gebid("submitStatus").innerHTML = '&nbsp;';
    };
    
    if(faction == "RAF"){
        //RAF
        ajax.send("getAPilotsSetupRAF="+mission+"&s="+squadron+"&f="+faction);

    } else if (faction == "LW") {
        if(squadron == 11){
            //STAB
            ajax.send("getAPilotsSetupLWStab="+mission+"&s="+squadron+"&f="+faction);
        } else {
            //LW
            ajax.send("getAPilotsSetupLW="+mission+"&s="+squadron+"&f="+faction);
        }
    }

}

function addRowsToTable(table, contentArray){
    
    for(var n = 0; n < contentArray.length; n++){
                
        var row = table.insertRow(n);
        
        for(var m = 0; m < contentArray[n].length; m++){
            var cell = row.insertCell(-1);
            cell.innerHTML = contentArray[n][m];
        }
    }
}
    
    