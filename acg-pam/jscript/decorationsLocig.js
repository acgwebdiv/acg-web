function awardDecoration(id, awardedBy){
    var year = gebid("ddateY"+id).value;
    var month = gebid("ddateM"+id).value;
    var day = gebid("ddateD"+id).value;
    var comment = encodeURIComponent(gebid("dcomment"+id).value);
    var ajax = ajaxObj("POST", "./includes/decorationsLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            if(ajax.responseText === "decoration awarded.") {
                window.location.reload();
            } else {
                alert(ajax.responseText);
            }
        }
    };
    ajax.send("awardDecoration="+id+"&y="+year+"&m="+month+"&d="+day+"&ab="+awardedBy+"&c="+comment);
}

function revokeDecoration(id){
    
    var ajax = ajaxObj("POST", "./includes/decorationsLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            
            if(ajax.responseText === "decoration revoked.") {
                window.location.reload();
            }
        }
    };
    ajax.send("revokeDecoration="+id);
}

