function addMission(){

    var nm = gebid("missionName").value;
    var c = gebid("campaign").value;
    var ry = gebid("rdateY").value;
    var rm = gebid("rdateM").value;
    var rd = gebid("rdateD").value;
    var hy = gebid("hdateY").value;
    var hm = gebid("hdateM").value;
    var hd = gebid("hdateD").value;
    var hh = gebid("hdateH").value;
    var hmm = gebid("hdateMM").value;
    var rx = /[^0-9]/gi;
    if(rx.test(ry) || rx.test(rm) || rx.test(rd)){
        gebid("submitStatus").innerHTML = 'Real date has wrong format.';
    } else if(ry === "" || rm === "" || rd === ""){
        gebid("submitStatus").innerHTML = 'Real date must not be empty.';
    } else if(rx.test(hy) || rx.test(hm) || rx.test(hd) || rx.test(hh) || rx.test(hmm)){
        gebid("submitStatus").innerHTML = 'Historic date has wrong format.';
    } else if(ry === "" || rm === "" || rd === "" || hh === "" || hmm === ""){
        gebid("submitStatus").innerHTML = 'Historic date must not be empty.';
    } else  {           
        gebid("submitStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("submitStatus").innerHTML = ajax.responseText;
                if(ajax.responseText === "Mission created") {
                    window.location.href="./missionAdministration.php";
                }
            }
        };
        ajax.send("n="+nm+"&c="+c+"&ry="+ry+"&rm="+rm+"&rd="+rd+"&hy="+hy+"&hm="+hm+"&hd="+hd+"&hh="+hh+"&hmm="+hmm);
    }
}

function changeMissionName(){
    var n = gebid("missionName").value;
    gebid("mNameStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("mNameStatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("changeMissionName="+n);
}

function changeMissionCampaign(){
    var c = gebid("missionCampaign").value;
    gebid("mCampaignstatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("mCampaignstatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("changeMissionCampaign="+c);
}

function changeRDate(){
    var y = gebid("rdateY").value;
    var m = gebid("rdateM").value;
    var d = gebid("rdateD").value;
    var rx = /[^0-9]/gi;
    if(rx.test(y) || rx.test(m) || rx.test(d)){
        gebid("rDateStatus").innerHTML = 'Real date has wrong format.';
    } else if(y === "" || m === "" || d === "") {
        gebid("rDateStatus").innerHTML = 'Real date must not be empty.';
    } else {           
        gebid("rDateStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("rDateStatus").innerHTML = ajax.responseText;
            }
        };
        ajax.send("ry="+y+"&m="+m+"&d="+d);
    }
}

function changeHDate(){
    var y = gebid("hdateY").value;
    var m = gebid("hdateM").value;
    var d = gebid("hdateD").value;
    var h = gebid("hdateH").value;
    var mm = gebid("hdateMM").value;
    var rx = /[^0-9]/gi;
    if(rx.test(y) || rx.test(m) || rx.test(d) || rx.test(h) || rx.test(mm)){
        gebid("hDateStatus").innerHTML = 'Historic date has wrong format.';
    } else if(y === "" || m === "" || d === "" || y === "" || mm === "") {
        gebid("hDateStatus").innerHTML = 'Historic date must not be empty.';
    } else {           
        gebid("hDateStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("hDateStatus").innerHTML = ajax.responseText;
            }
        };
        ajax.send("hy="+y+"&m="+m+"&d="+d+"&h="+h+"&mm="+mm);
    }
}

function changeMissionStatus(){
    var s = gebid("missionStatus").value;
    gebid("mStatusStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("mStatusStatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("changeMissionStatus="+s);
}

function resetMission(){
    if(confirm("Do you realy want to reset this mission?")) {
        var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("rDateStatus").innerHTML = ajax.responseText;
                changeRDate();
            }
        };
        ajax.send("resetMission=true");  
    }
}

function deleteMission(){
    if(confirm("Do you realy want to delete this mission?")) {
        var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.href="./missionAdministration.php";
            }
        };
        ajax.send("deleteMission=true");
    }
}

function saveMissionBriefing(mi_id, faction, type){
    
    switch(type){
        case 1:
            saveMissionBriefingType1(mi_id, faction);
            break;
        case 2:
            saveMissionBriefingType2(mi_id, faction);
            break;
    }
}

function saveMissionBriefingType1(mi_id, faction){
    
    var b = encodeURIComponent(gebid("missionBriefing").value);
    gebid("briefingStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("briefingStatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("saveMissionBriefing="+mi_id+"&faction="+faction+"&type="+1+"&text1="+b);
}

function saveMissionBriefingType2(mi_id, faction){
    
    var t1 = encodeURIComponent(gebid("text1").value);
    var t2 = encodeURIComponent(gebid("text2").value);
    var t3 = encodeURIComponent(gebid("text3").value);
    var t4 = encodeURIComponent(gebid("text4").value);
    var t5 = encodeURIComponent(gebid("text5").value);
    var t6 = encodeURIComponent(gebid("text6").value);
    var t7 = encodeURIComponent(gebid("text7").value);
    var t8 = encodeURIComponent(gebid("text8").value);
    var t9 = encodeURIComponent(gebid("text9").value);
    var t10 = encodeURIComponent(gebid("text10").value);
    var t11 = encodeURIComponent(gebid("text11").value);
    var t12 = encodeURIComponent(gebid("text12").value);
    
    gebid("briefingStatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/missionAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("briefingStatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("saveMissionBriefing="+mi_id+"&faction="+faction+"&type="+2+
            "&text1="+t1+"&text2="+t2+"&text3="+t3+"&text4="+t4+"&text5="+t5+
            "&text6="+t6+"&text7="+t7+"&text8="+t8+"&text9="+t9+"&text10="+t10+
            "&text11="+t11+"&text12="+t12);
}