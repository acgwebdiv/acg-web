function addSquadron(){
    var n = gebid("squadronName").value;
    var c = gebid("squadronCode").value;
    var f = gebid("squadronFaction").value;
    var rx = /[^a-z0-9/. /(/)]/gi;
    if(rx.test(n)){
        gebid("addSquadronStatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("addSquadronStatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("addSquadronStatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/squadronAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("addSquadronStatus").innerHTML = ajax.responseText;      
            }
        };
        ajax.send("addSquadron="+n+"&c="+c+"&f="+f+"&b="+b);
    }
}

function changeSquadronName(){
    var n = gebid("squadronName").value;
    var f = gebid("squadronFaction").value;
    var rx = /[^a-z0-9/. /(/)]/gi;
    if(rx.test(n)){
        gebid("snamestatus").innerHTML = 'Name has wrong format.';
    } else if (n === "") {
        gebid("snamestatus").innerHTML = 'Name must not be empty.';
    } else {
        gebid("snamestatus").innerHTML = 'checking ...';
        var ajax = ajaxObj("POST", "./includes/squadronAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                gebid("snamestatus").innerHTML = ajax.responseText;      
            }
        };
        ajax.send("changeSquadronName="+n+"&f="+f);
    }
}

function changeSquadronCode(){
    var c = gebid("squadronCode").value;
    var f = gebid("squadronFaction").value;
    var rx = /[^a-z0-9/. /(/)]/gi;
    gebid("scodestatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/squadronAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("scodestatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("changeSquadronCode="+c+"&f="+f);
}

function changeSquadronFaction(){
    var f = gebid("squadronFaction").value;
    gebid("sFactionstatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/squadronAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("sFactionstatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("changeSquadronFaction="+f);
}

function changeSquadronACGUnit(){
    var b = gebid("squadronACGUnit").value;
    gebid("sACGUnitstatus").innerHTML = 'checking ...';
    var ajax = ajaxObj("POST", "./includes/squadronAdminLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            gebid("sACGUnitstatus").innerHTML = ajax.responseText;      
        }
    };
    ajax.send("changeSquadronACGUnit="+b);
}

function deleteSquadron(){
    var f = gebid("squadronFaction").value;
    if(confirm("Do you realy want to delete this unit?")) {
        var ajax = ajaxObj("POST", "./includes/squadronAdminLogic.php");
        ajax.onreadystatechange = function() {
            if(ajaxReturn(ajax) === true) {
                window.location.href="./squadronAdministration.php";
            }
        };
        ajax.send("deleteSquadron=true");  
    }
}
    





