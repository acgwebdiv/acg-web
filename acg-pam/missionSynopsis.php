<?php
    
    $characterIDArray = array();

    //Number of pilots for each and squadron/Staffel with strength.
    $sql = "SELECT COUNT(reports.id) AS npilots, squadrons.name, reports.squadronID FROM reports ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.type = 1 AND reports.accepted = 1 ".
           "AND reports.missionID = $mi_id GROUP BY name"; 
    $query = mysqli_query($dbx, $sql);
    $allRAF = array();
    $allRAFid = array();
    $fullStrengthRAF = array();
    $mormalStrengthRAF = array();
    $limitedStrengthRAF = array();
    $verylimitedStrengthRAF = array();
    $npilotsRAF = 0;
    while($row = mysqli_fetch_assoc($query)){
        $sqn = $row["name"];
        $npilots = $row["npilots"];
        $allRAF[] = $row["name"];
        $allRAFid[] = $row["squadronID"];
        $npilotsRAF += $npilots;
        if($npilots > 12){
//            echo($sqn.": ".$npilots." added to fullstrenght.");
            $fullStrengthRAF[] = $sqn;
        } else if($npilots > 9){
//            echo($sqn.": ".$npilots." added to normalstrength.");
            $mormalStrengthRAF[] = $sqn;
        } else if($npilots > 6){
//            echo($sqn.": ".$npilots." added to limitedstrength.");
            $limitedStrengthRAF[] = $sqn;
        } else {
//            echo($sqn.": ".$npilots." added to verylimitedstrength.");
            $verylimitedStrengthRAF[] = $sqn;
        }
    }
    
    $sql = "SELECT COUNT(reports.id) AS npilots, squadrons.name, reports.squadronID FROM reports ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.type = 2 AND reports.accepted = 1 ".
           "AND reports.missionID = $mi_id GROUP BY name"; 
    $query = mysqli_query($dbx, $sql);
    $allLW = array();
    $allLWid = array();
    $fullStrengthLW = array();
    $mormalStrengthLW = array();
    $limitedStrengthLW = array();
    $verylimitedStrengthLW = array();
    $npilotsLW = 0;
    while($row = mysqli_fetch_assoc($query)){
        $sqn = $row["name"];
        $npilots = $row["npilots"];
        $allLW[] = $row["name"];
        $allLWid[] = $row["squadronID"];
        $npilotsLW += $npilots;
        if($npilots > 12){
//            echo($sqn.": ".$npilots." added to fullstrenght.");
            $fullStrengthLW[] = $sqn;
        } else if($npilots > 9){
//            echo($sqn.": ".$npilots." added to normalstrength.");
            $mormalStrengthLW[] = $sqn;
        } else if($npilots > 6){
//            echo($sqn.": ".$npilots." added to limitedstrength.");
            $limitedStrengthLW[] = $sqn;
        } else {
//            echo($sqn.": ".$npilots." added to verylimitedstrength.");
            $verylimitedStrengthLW[] = $sqn;
        }
    }
    
    //Number of pilots for each and squadron/Staffel with strength.
    $sql = "SELECT COUNT(reports.id) AS npilots, squadrons.name, reports.squadronID FROM reports ".
           "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
           "WHERE reports.type = 3 AND reports.accepted = 1 ".
           "AND reports.missionID = $mi_id GROUP BY name"; 
    $query = mysqli_query($dbx, $sql);
    $allVVS = array();
    $allVVSid = array();
    $fullStrengthVVS = array();
    $mormalStrengthVVS = array();
    $limitedStrengthVVS = array();
    $verylimitedStrengthVVS = array();
    $npilotsVVS = 0;
    while($row = mysqli_fetch_assoc($query)){
        $sqn = $row["name"];
        $npilots = $row["npilots"];
        $allVVS[] = $row["name"];
        $allVVSid[] = $row["squadronID"];
        $npilotsVVS += $npilots;
        if($npilots > 12){
//            echo($sqn.": ".$npilots." added to fullstrenght.");
            $fullStrengthVVS[] = $sqn;
        } else if($npilots > 9){
//            echo($sqn.": ".$npilots." added to normalstrength.");
            $mormalStrengthVVS[] = $sqn;
        } else if($npilots > 6){
//            echo($sqn.": ".$npilots." added to limitedstrength.");
            $limitedStrengthVVS[] = $sqn;
        } else {
//            echo($sqn.": ".$npilots." added to verylimitedstrength.");
            $verylimitedStrengthVVS[] = $sqn;
        }
    }
    
    //Number of claims
    $rafStatsArray = array();
    for($n = 0; $n < count($allRAFid); $n++){
        $squadronID = $allRAFid[$n];
        $sqnStatsArray = array();
        $sqnDest = 0;
        $sqnProb = 0;
        $sqnDamg = 0;
        $sqnGrnd = 0;
        $sql = "SELECT SUM(1-claimsraf.shared*0.5) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsraf.enemystatus = 1 AND claimsraf.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnDest = $srow["amount"];
        }
        $sql = "SELECT SUM(1-claimsraf.shared*0.5) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsraf.enemystatus = 2 AND claimsraf.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnProb = $srow["amount"];
        }
        $sql = "SELECT COUNT(claimsraf.id) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsraf.enemystatus = 3 AND claimsraf.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnDamg = $srow["amount"];
        }
        $sql = "SELECT SUM(claimsground.amount) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsground ON reports.id = claimsground.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsground.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnGrnd = $srow["amount"];
        }
        $sqnStatsArray["destroyed"] = $sqnDest;
        $sqnStatsArray["probable"] = $sqnProb;
        $sqnStatsArray["damaged"] = $sqnDamg;
        $sqnStatsArray["ground"] = $sqnGrnd;
        
        $sql = "SELECT COUNT(reports.id) FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 2 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($squery);
        $sqnTotalDamaged = $srow[0];
        
        $sql = "SELECT COUNT(reports.id) FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 3 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($squery);
        $sqnTotalLost = $srow[0];
        
        $sql = "SELECT authorID FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.pilotStatus = 2 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $woundedArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $characterID =  $srow["authorID"];
            $woundedArray[] = $characterID;
            if(!in_array($characterID, $characterIDArray)){
                $characterIDArray[] = $characterID;
            }
        }
        
        $sql = "SELECT authorID FROM reports ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND (reports.pilotStatus = 3 OR reports.pilotStatus = 4)  AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $kiapowArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $characterID =  $srow["authorID"];
            $kiapowArray[] = $characterID;
            if(!in_array($characterID, $characterIDArray)){
                $characterIDArray[] = $characterID;
            }   
        }
        
        $sqnStatsArray["aircraftLost"] = $sqnTotalLost;
        $sqnStatsArray["aircraftDamaged"] = $sqnTotalDamaged;
        $sqnStatsArray["wounded"] = $woundedArray;
        $sqnStatsArray["kiapow"] = $kiapowArray;
        
        $rafStatsArray[$allRAF[$n]] = $sqnStatsArray;
    }
    
    $lwStatsArray = array();
    for($n = 0; $n < count($allLWid); $n++){
        $squadronID = $allLWid[$n];
        $sqnStatsArray = array();
        $sqnConf = 0;
        $sqnUConf = 0;
        $sqnGrnd = 0;
        $sql = "SELECT COUNT(1) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimslw ON reports.id = claimslw.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimslw.confirmed = 1 AND claimslw.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnConf = $srow["amount"];
        }
        $sql = "SELECT COUNT(1) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimslw ON reports.id = claimslw.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimslw.confirmed = 0 AND claimslw.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnUConf = $srow["amount"];
        }
        $sql = "SELECT SUM(claimsground.amount) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsground ON reports.id = claimsground.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsground.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnGrnd = $srow["amount"];
        }
        $sqnStatsArray["confirmed"] = $sqnConf;
        $sqnStatsArray["unconfirmed"] = $sqnUConf;
        $sqnStatsArray["ground"] = $sqnGrnd;
        
        $sql = "SELECT COUNT(reports.id) FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 2 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($squery);
        $sqnTotalDamaged = $srow[0];
        
        $sql = "SELECT COUNT(reports.id) FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 3 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($squery);
        $sqnTotalLost = $srow[0];
        
        $sql = "SELECT authorID FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.pilotStatus = 2 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $woundedArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $characterID =  $srow["authorID"];
            $woundedArray[] = $characterID;
            if(!in_array($characterID, $characterIDArray)){
                $characterIDArray[] = $characterID;
            }
        }
        
        $sql = "SELECT authorID FROM reports ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND (reports.pilotStatus = 3 OR reports.pilotStatus = 4)  AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $kiapowArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $characterID =  $srow["authorID"];
            $kiapowArray[] = $characterID;
            if(!in_array($characterID, $characterIDArray)){
                $characterIDArray[] = $characterID;
            }   
        }
        
        $sqnStatsArray["aircraftLost"] = $sqnTotalLost;
        $sqnStatsArray["aircraftDamaged"] = $sqnTotalDamaged;
        $sqnStatsArray["wounded"] = $woundedArray;
        $sqnStatsArray["kiapow"] = $kiapowArray;
        
        $lwStatsArray[$allLW[$n]] = $sqnStatsArray;
    }
    
    $vvsStatsArray = array();
    for($n = 0; $n < count($allVVSid); $n++){
        $squadronID = $allVVSid[$n];
        $sqnStatsArray = array();
        $sqnConf = 0;
        $sqnUConf = 0;
        $sqnConfG = 0;
        $sqnUConfG = 0;
        $sqnGrnd = 0;
        $sql = "SELECT COUNT(1) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 1 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 0 AND reports.squadronID = $squadronID";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnConf = $srow["amount"];
        }
        $sql = "SELECT COUNT(1) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 0 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 0 AND reports.squadronID = $squadronID";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnUConf = $srow["amount"];
        }
        $sql = "SELECT COUNT(1) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 1 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 1 AND reports.squadronID = $squadronID";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnConfG = $srow["amount"];
        }
        $sql = "SELECT COUNT(1) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsvvs.confirmed = 0 AND claimsvvs.accepted = 1 ".
               "AND claimsvvs.groupClaim = 1 AND reports.squadronID = $squadronID";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnUConfG = $srow["amount"];
        }
        $sql = "SELECT SUM(claimsground.amount) AS amount ".
               "FROM reports ".
               "RIGHT JOIN claimsground ON reports.id = claimsground.reportID ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND claimsground.accepted = 1 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        while($srow = mysqli_fetch_assoc($squery)){
            $sqnGrnd = $srow["amount"];
        }
        $sqnStatsArray["confirmed"] = $sqnConf;
        $sqnStatsArray["unconfirmed"] = $sqnUConf;
        $sqnStatsArray["confirmedGroup"] = $sqnConfG;
        $sqnStatsArray["unconfirmedGroup"] = $sqnUConfG;
        $sqnStatsArray["ground"] = $sqnGrnd;
        
        $sql = "SELECT COUNT(reports.id) FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 2 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($squery);
        $sqnTotalDamaged = $srow[0];
        
        $sql = "SELECT COUNT(reports.id) FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.aeroplanestatus = 3 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($squery);
        $sqnTotalLost = $srow[0];
        
        $sql = "SELECT authorID FROM reports ". 
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND reports.pilotStatus = 2 AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $woundedArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $characterID =  $srow["authorID"];
            $woundedArray[] = $characterID;
            if(!in_array($characterID, $characterIDArray)){
                $characterIDArray[] = $characterID;
            }
        }
        
        $sql = "SELECT authorID FROM reports ".
               "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
               "AND (reports.pilotStatus = 3 OR reports.pilotStatus = 4)  AND reports.squadronID = $squadronID ";
        $squery = mysqli_query($dbx, $sql);
        $kiapowArray = array();
        while($srow = mysqli_fetch_assoc($squery)){
            $characterID =  $srow["authorID"];
            $kiapowArray[] = $characterID;
            if(!in_array($characterID, $characterIDArray)){
                $characterIDArray[] = $characterID;
            }   
        }
        
        $sqnStatsArray["aircraftLost"] = $sqnTotalLost;
        $sqnStatsArray["aircraftDamaged"] = $sqnTotalDamaged;
        $sqnStatsArray["wounded"] = $woundedArray;
        $sqnStatsArray["kiapow"] = $kiapowArray;
        
        $vvsStatsArray[$allVVS[$n]] = $sqnStatsArray;
    }
    
    $rafDestArray = array();
    $rafProbArray = array();
    $rafDamgArray = array();
    $rafGrndArray = array();
    $sql = "SELECT aeroplanes.name, claimsraf.enemyStatus, SUM(1-claimsraf.shared*0.5) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
           "LEFT JOIN aeroplanes ON claimsraf.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsraf.enemystatus = 1 AND claimsraf.accepted = 1 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $rafDestArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT aeroplanes.name, claimsraf.enemyStatus, SUM(1-claimsraf.shared*0.5) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
           "LEFT JOIN aeroplanes ON claimsraf.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsraf.enemystatus = 2 AND claimsraf.accepted = 1 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $rafProbArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT aeroplanes.name, claimsraf.enemyStatus, COUNT(claimsraf.id) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsraf ON reports.id = claimsraf.reportID ".
           "LEFT JOIN aeroplanes ON claimsraf.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsraf.enemystatus = 3 AND claimsraf.accepted = 1 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $rafDamgArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT groundtargets.id, groundtargets.name, SUM(claimsground.amount) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsground ON reports.id = claimsground.reportID ".
           "LEFT JOIN groundtargets ON claimsground.object = groundtargets.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsground.accepted = 1 AND reports.type = 1 GROUP BY groundtargets.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $rafGrndArray[$srow["name"]] = $srow["amount"];
    }
    
    $lwConfArray = array();
    $lwUConfArray = array();
    $lwGrndArray = array();
    $sql = "SELECT aeroplanes.name, COUNT(claimslw.confirmed) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimslw ON reports.id = claimslw.reportID ".
           "LEFT JOIN aeroplanes ON claimslw.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimslw.confirmed = 1 AND claimslw.accepted = 1 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $lwConfArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT aeroplanes.name, COUNT(claimslw.confirmed) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimslw ON reports.id = claimslw.reportID ".
           "LEFT JOIN aeroplanes ON claimslw.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimslw.confirmed = 0 AND claimslw.accepted = 1 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $lwUConfArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT groundtargets.id, groundtargets.name, SUM(claimsground.amount) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsground ON reports.id = claimsground.reportID ".
           "LEFT JOIN groundtargets ON claimsground.object = groundtargets.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsground.accepted = 1 AND reports.type = 2 GROUP BY groundtargets.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $lwGrndArray[$srow["name"]] = $srow["amount"];
    }
    
    $vvsConfArray = array();
    $vvsUConfArray = array();
    $vvsConfGArray = array();
    $vvsUConfGArray = array();
    $vvsGrndArray = array();
    $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
           "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsvvs.confirmed = 1 AND claimsvvs.accepted = 1 ".
           "AND claimsvvs.groupClaim = 0 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $vvsConfArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
           "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsvvs.confirmed = 0 AND claimsvvs.accepted = 1 ".
           "AND claimsvvs.groupClaim = 0 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $vvsUConfArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
           "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsvvs.confirmed = 1 AND claimsvvs.accepted = 1 ".
           "AND claimsvvs.groupClaim = 1 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $vvsConfGArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT aeroplanes.name, COUNT(claimsvvs.confirmed) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsvvs ON reports.id = claimsvvs.reportID ".
           "LEFT JOIN aeroplanes ON claimsvvs.aeroplane = aeroplanes.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsvvs.confirmed = 0 AND claimsvvs.accepted = 1 ".
           "AND claimsvvs.groupClaim = 1 GROUP BY aeroplanes.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $vvsUConfGArray[$srow["name"]] = $srow["amount"];
    }
    $sql = "SELECT groundtargets.id, groundtargets.name, SUM(claimsground.amount) AS amount ".
           "FROM reports ".
           "RIGHT JOIN claimsground ON reports.id = claimsground.reportID ".
           "LEFT JOIN groundtargets ON claimsground.object = groundtargets.id ".
           "WHERE reports.missionID = $mi_id AND reports.accepted = 1 ".
           "AND claimsground.accepted = 1 AND reports.type = 2 GROUP BY groundtargets.name";
    $squery = mysqli_query($dbx, $sql);
    while($srow = mysqli_fetch_assoc($squery)){
        $vvsGrndArray[$srow["name"]] = $srow["amount"];
    }
    
    //Select promotins and decorations gained with this mission.
    $sql = "SELECT realDate FROM missions WHERE id >= $mi_id ORDER BY realDate ASC LIMIT 2";
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>1){
        $dateArray = mysqli_fetch_all($query);
        $whereClause = "WHERE date >= '".$dateArray[0][0]."' AND date < '".$dateArray[1][0]."'";
        $whereClauseC = " AND realDate < '".$dateArray[1][0]."'";
    } else {
        $dateArray = mysqli_fetch_all($query);
        $whereClause = "WHERE date >= '".$dateArray[0][0]."'";
        $whereClauseC = "";
    }

    $sql = "SELECT promotions.memberID, promotions.value FROM promotions ".$whereClause;
    $query = mysqli_query($dbx, $sql);
    $rafPromotionArray = array();
    $lwPromotionArray = array();
    $vvsPromotionArray = array();
    while($row = mysqli_fetch_assoc($query)){
        $member_id = $row["memberID"];
        $sql = "SELECT careercharacters.id, missions.realDate FROM acgmembers ". 
               "LEFT JOIN careercharacters ON acgmembers.id = careercharacters.personifiedBy ".
               "LEFT JOIN reports ON careercharacters.id = reports.authorID ".
               "LEFT JOIN missions ON reports.missionID = missions.id ".
               "WHERE acgmembers.id = $member_id ".$whereClauseC.
               " ORDER BY realDate DESC LIMIT 1";
        $cquery = mysqli_query($dbx, $sql);
        $crow = mysqli_fetch_assoc($cquery);
        $characterID = $crow["id"];
        
        $faction = getFactionAtMission($row["memberID"], $mi_id, $dbx);
        if($characterID != null & !in_array($characterID, $characterIDArray)){
            $characterIDArray[] = $characterID;
            $value = $row["value"];
            $sql = "SELECT name FROM ranks WHERE faction = '$faction' AND value = $value";
            $fquery = mysqli_query($dbx, $sql);
            $prow = mysqli_fetch_assoc($fquery);
            if($faction == "RAF"){
                $rafPromotionArray[$characterID] = $prow["name"];
            } else if($faction == "LW"){
                $lwPromotionArray[$characterID] = $prow["name"];
            } else if($faction == "VVS"){
                $vvsPromotionArray[$characterID] = $prow["name"];
            }
        }   
    }

    $sql = "SELECT awards.name, decorations.characterID FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.ID ".$whereClause;
    $query = mysqli_query($dbx, $sql);
    $rafDecorationsArray = array();
    $lwDecorationsArray = array();
    $vvsDecorationsArray = array();
    while($row = mysqli_fetch_assoc($query)){
        $faction = getCharacterFaction($row["characterID"], $dbx);
        if(!in_array($row["characterID"], $characterIDArray)){
            $characterIDArray[] = $row["characterID"];
        }
        if($faction == "RAF"){
            $rafDecorationsArray[$row["name"]][] = $row["characterID"];
        } else if($faction === "LW") {
            $lwDecorationsArray[$row["name"]][] = $row["characterID"];
        } else if($faction === "VVS") {
            $vvsDecorationsArray[$row["name"]][] = $row["characterID"];
        }
    }
    
    //All ranks, names and callsign of members
    $memberArray = array();
    foreach ($characterIDArray as $characterID) {
        $sql = "SELECT acgmembers.id AS m_id, ".
               "careercharacters.firstName, acgmembers.callsign, careercharacters.lastName ".
               "FROM careercharacters ".
               "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
               "WHERE careercharacters.id = $characterID";
               $query = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($query)>0){
            while($row = mysqli_fetch_assoc($query)){
                $rank = getRankAbreviationAtMission($row["m_id"], $mi_id, $dbx);
                $member = $rank." ".$row["firstName"]." '".$row["callsign"]."' ".$row["lastName"];
                $memberArray[$characterID] = $member;
            }
        }       
    }
    
    function inTextListing($inArray){
        
        switch(count($inArray)){
            
            case 0:
                return "";
            case 1: 
                return $inArray[0];
            case 2:
                return $inArray[0]." and ".$inArray[1];
            default:
                $ret = $inArray[0];
                for($n = 1; $n < count($inArray)-1; $n++){
                    $ret.=", ".$inArray[$n];
                }
                $ret.=" and ".$inArray[count($inArray)-1];
                return $ret;
        }
        
    }
    
    function numberToWord($number){
        switch($number){
            
            case 1:
                return "one";
            case 2:
                return "two";
            case 3:
                return "three";
            case 4:
                return "four";
            case 5:
                return "five";
            case 6:
                return "six";
            case 7:
                return "seven";
            case 8:
                return "eight";
            case 9:
                return "nine";
            case 10:
                return "ten";
            case 11:
                return "eleven";
            case 12:
                return "twelve";
            case 13:
                return "thirteen";
            case 14:
                return "fourteen";
            case 15:
                return "fifteen";
            case 16:
                return "sixteen";
            case 17:
                return "seventeen";
            case 18:
                return "eighteen";
            case 19:
                return "nineteen";
            case 20:
                return "twenty";
            
        }
    }

?>
<p>The mission's current status is: Closed</p>
<h2>Mission Synopsis:</h2>
<div class="missionSynopsisParticipation">
    <h3>Allied units:</h3>
    <!------------ RAF MISSION SYNOPSIS ------------>
    <!---------------------------------------------->
    <?php if($npilotsRAF > 0) { ?>
    <p>
        Royal Air Force squadrons <?php echo(inTextListing($allRAF)); ?> departed for combat
        operations on <?php echo($m_row["histdate"]); ?>.
        <?php
        $fullstrength = FALSE;
        $limitedstrength = FALSE;
        if(count($fullStrengthRAF)>0){ 
            echo(inTextListing($fullStrengthRAF)." operated under full strength. ");
            $fullstrength = TRUE;
        }
        if(count($limitedStrengthRAF)>0){
            if($fullstrength){
                echo("While ".inTextListing($limitedStrengthRAF)." operated under limited strength. ");
            } else {
                echo(inTextListing($limitedStrengthRAF)." operated under limited strength. ");
            }
            $limitedstrength = TRUE;    
        }
        if(count($verylimitedStrengthRAF)>0){ 
            if(($fullstrength & !$limitedstrength) | !$fullstrength & $limitedstrength){
                echo("While ".inTextListing($verylimitedStrengthRAF)." had to operate under very limited strength. ");
            } else if($fullstrength & $limitedstrength){
                echo(inTextListing($verylimitedStrengthRAF)." had to operate under very limited strength. ");
            }
        }
        ?>
        In total the RAF fielded <?php echo($npilotsRAF); ?> aircraft.
    </p>

    <?php
    foreach($rafStatsArray as $sqn => $sqnStatsArray){
        $dest = $sqnStatsArray["destroyed"];
        $prob = $sqnStatsArray["probable"];
        $damg = $sqnStatsArray["damaged"];
        $grnd = $sqnStatsArray["ground"];
        
        echo("<u>".$sqn.":</u>");
        echo("<p>");
        
        if($dest > 0 & $prob == 0){
            echo($sqn." claims ".floatval($dest)." enemy aircraft destroyed. ");
        } else if($dest > 0 & $prob > 0){
            echo($sqn." claims ".floatval($dest)." enemy aircraft destroyed and ".floatval($prob)." enemy aircraft probable destroyed. ");
        } else if($dest == 0 & $prob > 0){
            echo($sqn." claims ".floatval($prob)." enemy aircraft probable destroyed. ");
        }
        if($damg > 0){
            echo(floatval($damg)." enemy aircraft were damaged by ".$sqn.". ");
        }
        if($grnd > 0){
            echo($sqn." destroyed ".floatval($grnd)." enemy ground targets.");
        }
        echo("<br>");
        
        $aircraftLost = $sqnStatsArray["aircraftLost"];
        $aircraftDamaged = $sqnStatsArray["aircraftDamaged"];
        $woundedArray = $sqnStatsArray["wounded"];
        $kiapowArray = $sqnStatsArray["kiapow"];
        $savedPilots = $aircraftLost - count($kiapowArray);
        
        if($aircraftDamaged > 0 & $aircraftLost == 0){
            $acfString = ($aircraftDamaged > 1)?" aircrafts":" aircraft";
            echo(ucfirst(numberToWord($aircraftDamaged)).$acfString." from ".$sqn." returned damaged. "); 
        } else if($aircraftDamaged > 0 & $aircraftLost > 0){
            $acfString = ($aircraftDamaged > 1)?" aircrafts":" aircraft";
            $wereString = ($aircraftLost > 1)?" were":" was";
            
            echo(ucfirst(numberToWord($aircraftDamaged)).$acfString." from ".$sqn." returned damaged, ".
                 "while ".numberToWord($aircraftLost).$wereString." lost. ");
            if($savedPilots > 0){
                $pilotString = ($savedPilots>1)?" pilots":" pilot";
                $himselfString = ($savedPilots>1)?" themselves":" himself";
                $wereString = ($savedPilots>1)?" were":" was";
                echo(ucfirst(numberToWord($savedPilots)).$pilotString.$wereString." able to save".$himselfString.".");
            }
        } else if($aircraftDamaged == 0 & $aircraftLost > 0){
            $acfString = ($aircraftLost > 1)?" aircrafts":" aircraft";
            $wereString = ($aircraftLost > 1)?" were":" was";

            echo(ucfirst(numberToWord($aircraftLost)).$acfString.$wereString." lost. ");
            if($savedPilots > 0){
                $pilotString = ($savedPilots>1)?" pilots":" pilot";
                $himselfString = ($savedPilots>1)?" themselves":" himself";
                $wereString = ($savedPilots>1)?" were":" was";
                echo(ucfirst(numberToWord($savedPilots)).$pilotString.$wereString." able to save".$himselfString.".");
            }
        }else {
            echo("The whole squadron returned without losses or sustained damage. ");
        }
        echo("<br>");
        
        if(count($woundedArray)>0){
            $pilotString = (count($woundedArray)>1)?" pilots":" pilot";
            $wereString = (count($woundedArray)>1)?" were":" was";
            echo("The following ".$pilotString.$wereString." wounded during this mission:" );
            echo("<ul>");
            foreach ($woundedArray as $pilot) {
                echo("<li>".$memberArray[$pilot]."</li>");
            }
            echo("</ul>");
        }
        
        if(count($kiapowArray)>0){
            $pilotString = (count($kiapowArray)>1)?" pilots":" pilot";
            $areString = (count($kiapowArray)>1)?" are":" is";
            echo("The following ".$pilotString.$areString." missing and assumed dead or captured by the enemy." );
            echo("<ul>");
            foreach ($kiapowArray as $pilot) {
                echo("<li>".$memberArray[$pilot]."</li>");
            }
            echo("</ul>");
        }
    }
    ?>
    
    ------------------------------------------
    <p>The following enemy casualties are claimed:</p>
    Destroyed:<br>
    <ul>
        <?php foreach($rafDestArray as $acft => $amount) {
            echo("<li>".floatval($amount)." ".$acft);
        } ?>
    </ul>
    Probable:<br>
    <ul>
        <?php foreach($rafProbArray as $acft => $amount) {
            echo("<li>".floatval($amount)." ".$acft);
        } ?>
    </ul>
    Damaged:<br>
    <ul>
        <?php foreach($rafDamgArray as $acft => $amount) {
            echo("<li>".floatval($amount)." ".$acft);
        } ?>
    </ul><br>
    <p>The following ground targets were destroyed:</p>
    <ul>
        <?php foreach($rafGrndArray as $objt => $amount) {
            echo("<li>".floatval($amount)." ".$objt);
        } ?>
    </ul>
    
    <?php if(count($rafPromotionArray)>0){
            
            echo("------------------------------------------");
            echo("<p>The following promotions were issued:</p>");
            
            echo("<ul>");
            foreach($rafPromotionArray as $id => $rank) {
                echo("<li>".$memberArray[$id]." to ".$rank."</li>");
            }
            echo("</ul>");
    } ?>

    <?php if(count($rafDecorationsArray)>0){
            
            echo("------------------------------------------");
            echo("<p>We are happy to announce the following decorations:</p>");
            
            echo("<ul>");
            foreach($rafDecorationsArray as $medal => $idArray) {
                $nameArray = array();
                foreach ($idArray as $id) {
                    $nameArray[] = $memberArray[$id];
                }
                $wereString = (count($nameArray)>1)?" were":" was";
                if($medal == "Mentioned in Dispatches"){
                    echo("<li>".inTextListing($nameArray).$wereString." mentioned in dispatches.</li>");
                } else {
                    echo("<li>".inTextListing($nameArray).$wereString." awarded the ".$medal."</li>");
                }   
            }
            echo("</ul>");
            echo("Congratulations.");
        } 
    } ?>
    
    <!------------ VVS MISSION SYNOPSIS ------------>
    <!---------------------------------------------->
    
    <?php if($npilotsVVS > 0) { ?>
    <p>
        Voyenno-Vozdushnye Sily Eskadrilya <?php echo(inTextListing($allVVS)); ?> departed for combat
        operations on <?php echo($m_row["histdate"]); ?>.
        <?php
        $fullstrength = FALSE;
        $limitedstrength = FALSE;
        if(count($fullStrengthVVS)>0){ 
            echo(inTextListing($fullStrengthVVS)." operated under full strength. ");
            $fullstrength = TRUE;
        }
        if(count($limitedStrengthVVS)>0){
            if($fullstrength){
                echo("While ".inTextListing($limitedStrengthVVS)." operated under limited strength. ");
            } else {
                echo(inTextListing($limitedStrengthVVS)." operated under limited strength. ");
            }
            $limitedstrength = TRUE;    
        }
        if(count($verylimitedStrengthVVS)>0){ 
            if(($fullstrength & !$limitedstrength) | !$fullstrength & $limitedstrength){
                echo("While ".inTextListing($verylimitedStrengthVVS)." had to operate under very limited strength. ");
            } else if($fullstrength & $limitedstrength){
                echo(inTextListing($verylimitedStrengthVVS)." had to operate under very limited strength. ");
            }
        }
        ?>
        In total the Voyenno-Vozdushnye Sily fielded <?php echo($npilotsVVS); ?> aircraft.
    </p>

    <?php
    foreach($vvsStatsArray as $sqn => $sqnStatsArray){
        $conf = $sqnStatsArray["confirmed"];
        $uconf = $sqnStatsArray["unconfirmed"];
        $confg = $sqnStatsArray["confirmedGroup"];
        $uconfg = $sqnStatsArray["unconfirmedGroup"];
        $grnd = $sqnStatsArray["ground"];
        
        echo("<u>".$sqn.":</u>");
        echo("<p>");
        
        if($conf > 0 & $uconf == 0){
            echo($sqn." claims ".floatval($conf + $confg)." enemy aircraft confirmed destroyed");
            if($conf > 1){
                echo(", of which ".floatval($conf)." are individual claims.");
            } else if($conf > 0){
                echo(", of which ".floatval($conf)." is an individual claim.");
            } else {
                echo(".");
            }
        } else if($conf > 0 & $uconf > 0){
            echo($sqn." claims ".floatval($conf + $confg)." enemy aircraft confirmed destroyed");
            if($conf > 1){
                echo(", of which ".floatval($conf)." are individual claims,");
            } else if($conf > 0){
                echo(", of which ".floatval($conf)." is an individual claim,");
            }
            echo(" and ".floatval($uconf + $uconfg)." enemy aircraft unconfirmed destroyed");
            if($uconf > 1){
                echo(", of which ".floatval($uconf)." are individual claims,");
            } else if($uconf > 0){
                echo(", of which ".floatval($uconf)." is an individual claim,");
            } else {
                echo(".");
            }
        } else if($conf == 0 & $uconf > 0){
            echo($sqn." claims ".floatval($uconf + $uconfg)." enemy aircraft unconfirmed destroyed");
             if($uconf > 1){
                echo(", of which ".floatval($uconf)." are individual claims.");
            } else if($uconf > 0){
                echo(", of which ".floatval($uconf)." is an individual claim.");
            } else {
                echo(".");
            }
        }
        
        
        
        if($grnd > 0){
            echo(floatval($grnd)." enemy ground targets were destroyed by ".$sqn.".");
        }
        echo("<br>");
        
        $aircraftLost = $sqnStatsArray["aircraftLost"];
        $aircraftDamaged = $sqnStatsArray["aircraftDamaged"];
        $woundedArray = $sqnStatsArray["wounded"];
        $kiapowArray = $sqnStatsArray["kiapow"];
        $savedPilots = $aircraftLost - count($kiapowArray);
        
        if($aircraftDamaged > 0 & $aircraftLost == 0){
            $acfString = ($aircraftDamaged > 1)?" aircrafts":" aircraft";
            echo(ucfirst(numberToWord($aircraftDamaged)).$acfString." from ".$sqn." returned damaged. "); 
        } else if($aircraftDamaged > 0 & $aircraftLost > 0){
            $acfString = ($aircraftDamaged > 1)?" aircrafts":" aircraft";
            $wereString = ($aircraftLost > 1)?" were":" was";
            
            echo(ucfirst(numberToWord($aircraftDamaged)).$acfString." from ".$sqn." returned damaged, ".
                 "while ".numberToWord($aircraftLost).$wereString." lost. ");
            if($savedPilots > 0){
                $pilotString = ($savedPilots>1)?" pilots":" pilot";
                $himselfString = ($savedPilots>1)?" themselves":" himself";
                $wereString = ($savedPilots>1)?" were":" was";
                echo(ucfirst(numberToWord($savedPilots)).$pilotString.$wereString." able to save".$himselfString.".");
            }
        } else if($aircraftDamaged == 0 & $aircraftLost > 0){
            $acfString = ($aircraftLost > 1)?" aircrafts":" aircraft";
            $wereString = ($aircraftLost > 1)?" were":" was";

            echo(ucfirst(numberToWord($aircraftLost)).$acfString.$wereString." lost. ");
            if($savedPilots > 0){
                $pilotString = ($savedPilots>1)?" pilots":" pilot";
                $himselfString = ($savedPilots>1)?" themselves":" himself";
                $wereString = ($savedPilots>1)?" were":" was";
                echo(ucfirst(numberToWord($savedPilots)).$pilotString.$wereString." able to save".$himselfString.".");
            }
        }else {
            echo("The whole squadron returned without losses or sustained damage. ");
        }
        echo("<br>");
        
        if(count($woundedArray)>0){
            $pilotString = (count($woundedArray)>1)?" pilots":" pilot";
            $wereString = (count($woundedArray)>1)?" were":" was";
            echo("The following ".$pilotString.$wereString." wounded during this mission:" );
            echo("<ul>");
            foreach ($woundedArray as $pilot) {
                echo("<li>".$memberArray[$pilot]."</li>");
            }
            echo("</ul>");
        }
        
        if(count($kiapowArray)>0){
            $pilotString = (count($kiapowArray)>1)?" pilots":" pilot";
            $areString = (count($kiapowArray)>1)?" are":" is";
            echo("The following ".$pilotString.$areString." missing and assumed dead or captured by the enemy." );
            echo("<ul>");
            foreach ($kiapowArray as $pilot) {
                echo("<li>".$memberArray[$pilot]."</li>");
            }
            echo("</ul>");
        }
    }
    ?>
    
    ------------------------------------------
    <p>The following enemy casualties are claimed:</p>
    Confirmed:<br>
    <ul>
        <?php foreach($vvsConfArray as $acft => $amount) {
            echo("<li>".floatval($amount)." ".$acft);
        } ?>
    </ul>
    Unconfirmed:<br>
    <ul>
        <?php foreach($vvsUConfArray as $acft => $amount) {
            echo("<li>".floatval($amount)." ".$acft);
        } ?>
    </ul>
    <p>The following ground targets were destroyed:</p>
    <ul><br>
        <?php foreach($vvsGrndArray as $objt => $amount) {
            echo("<li>".floatval($amount)." ".$objt);
        } ?>
    </ul>
    
    <?php if(count($vvsPromotionArray)>0){
            
            echo("------------------------------------------");
            echo("<p>The following promotions were issued:</p>");
            
            echo("<ul>");
            foreach($vvsPromotionArray as $id => $rank) {
                echo("<li>".$memberArray[$id]." to ".$rank."</li>");
            }
            echo("</ul>");
    } ?>

    <?php if(count($vvsDecorationsArray)>0){
            
            echo("------------------------------------------");
            echo("<p>We are happy to announce the following decorations:</p>");
            
            echo("<ul>");
            foreach($vvsDecorationsArray as $medal => $idArray) {
                $nameArray = array();
                foreach ($idArray as $id) {
                    $nameArray[] = $memberArray[$id];
                }
                $wereString = (count($nameArray)>1)?" were":" was";
                if($medal == "Mentioned in Wehrmachtsbericht"){
                    echo("<li>".inTextListing($nameArray).$wereString." mentioned in dispatches.</li>");
                } else {
                    echo("<li>".inTextListing($nameArray).$wereString." awarded the ".$medal."</li>");
                }   
            }
            echo("</ul>");
            echo("Congratulations.");
        } 
    }?>
    
</div>
<div class="missionSynopsisParticipation">
    <h3>Axis units:</h3>
    
    <!------------ LW MISSION SYNOPSIS ------------>
    <!---------------------------------------------->
    
    <?php if($npilotsLW > 0) { ?>
    <p>
        Luftwaffe Staffeln <?php echo(inTextListing($allLW)); ?> departed for combat
        operations on <?php echo($m_row["histdate"]); ?>.
        <?php
        $fullstrength = FALSE;
        $limitedstrength = FALSE;
        if(count($fullStrengthLW)>0){ 
            echo(inTextListing($fullStrengthLW)." operated under full strength. ");
            $fullstrength = TRUE;
        }
        if(count($limitedStrengthLW)>0){
            if($fullstrength){
                echo("While ".inTextListing($limitedStrengthLW)." operated under limited strength. ");
            } else {
                echo(inTextListing($limitedStrengthLW)." operated under limited strength. ");
            }
            $limitedstrength = TRUE;    
        }
        if(count($verylimitedStrengthLW)>0){ 
            if(($fullstrength & !$limitedstrength) | !$fullstrength & $limitedstrength){
                echo("While ".inTextListing($verylimitedStrengthLW)." had to operate under very limited strength. ");
            } else if($fullstrength & $limitedstrength){
                echo(inTextListing($verylimitedStrengthLW)." had to operate under very limited strength. ");
            }
        }
        ?>
        In total the Luftwaffe fielded <?php echo($npilotsLW); ?> aircraft.
    </p>

    <?php
    foreach($lwStatsArray as $sqn => $sqnStatsArray){
        $conf = $sqnStatsArray["confirmed"];
        $uconf = $sqnStatsArray["unconfirmed"];
        $grnd = $sqnStatsArray["ground"];
        
        echo("<u>".$sqn.":</u>");
        echo("<p>");
        
        if($conf > 0 & $uconf == 0){
            echo($sqn." claims ".floatval($conf)." enemy aircraft confirmed destroyed. ");
        } else if($conf > 0 & $uconf > 0){
            echo($sqn." claims ".floatval($conf)." enemy aircraft confirmed destroyed and ".floatval($uconf)." enemy aircraft unconfirmed destroyed. ");
        } else if($conf == 0 & $uconf > 0){
            echo($sqn." claims ".floatval($uconf)." enemy aircraft unconfirmed destroyed. ");
        }
        if($grnd > 0){
            echo(floatval($grnd)." enemy ground targets were destroyed by ".$sqn.".");
        }
        echo("<br>");
        
        $aircraftLost = $sqnStatsArray["aircraftLost"];
        $aircraftDamaged = $sqnStatsArray["aircraftDamaged"];
        $woundedArray = $sqnStatsArray["wounded"];
        $kiapowArray = $sqnStatsArray["kiapow"];
        $savedPilots = $aircraftLost - count($kiapowArray);
        
        if($aircraftDamaged > 0 & $aircraftLost == 0){
            $acfString = ($aircraftDamaged > 1)?" aircrafts":" aircraft";
            echo(ucfirst(numberToWord($aircraftDamaged)).$acfString." from ".$sqn." returned damaged. "); 
        } else if($aircraftDamaged > 0 & $aircraftLost > 0){
            $acfString = ($aircraftDamaged > 1)?" aircrafts":" aircraft";
            $wereString = ($aircraftLost > 1)?" were":" was";
            
            echo(ucfirst(numberToWord($aircraftDamaged)).$acfString." from ".$sqn." returned damaged, ".
                 "while ".numberToWord($aircraftLost).$wereString." lost. ");
            if($savedPilots > 0){
                $pilotString = ($savedPilots>1)?" pilots":" pilot";
                $himselfString = ($savedPilots>1)?" themselves":" himself";
                $wereString = ($savedPilots>1)?" were":" was";
                echo(ucfirst(numberToWord($savedPilots)).$pilotString.$wereString." able to save".$himselfString.".");
            }
        } else if($aircraftDamaged == 0 & $aircraftLost > 0){
            $acfString = ($aircraftLost > 1)?" aircrafts":" aircraft";
            $wereString = ($aircraftLost > 1)?" were":" was";

            echo(ucfirst(numberToWord($aircraftLost)).$acfString.$wereString." lost. ");
            if($savedPilots > 0){
                $pilotString = ($savedPilots>1)?" pilots":" pilot";
                $himselfString = ($savedPilots>1)?" themselves":" himself";
                $wereString = ($savedPilots>1)?" were":" was";
                echo(ucfirst(numberToWord($savedPilots)).$pilotString.$wereString." able to save".$himselfString.".");
            }
        }else {
            echo("The whole squadron returned without losses or sustained damage. ");
        }
        echo("<br>");
        
        if(count($woundedArray)>0){
            $pilotString = (count($woundedArray)>1)?" pilots":" pilot";
            $wereString = (count($woundedArray)>1)?" were":" was";
            echo("The following ".$pilotString.$wereString." wounded during this mission:" );
            echo("<ul>");
            foreach ($woundedArray as $pilot) {
                echo("<li>".$memberArray[$pilot]."</li>");
            }
            echo("</ul>");
        }
        
        if(count($kiapowArray)>0){
            $pilotString = (count($kiapowArray)>1)?" pilots":" pilot";
            $areString = (count($kiapowArray)>1)?" are":" is";
            echo("The following ".$pilotString.$areString." missing and assumed dead or captured by the enemy." );
            echo("<ul>");
            foreach ($kiapowArray as $pilot) {
                echo("<li>".$memberArray[$pilot]."</li>");
            }
            echo("</ul>");
        }
    }
    ?>
    
    ------------------------------------------
    <p>The following enemy casualties are claimed:</p>
    Confirmed:<br>
    <ul>
        <?php foreach($lwConfArray as $acft => $amount) {
            echo("<li>".floatval($amount)." ".$acft);
        } ?>
    </ul>
    Unconfirmed:<br>
    <ul>
        <?php foreach($lwUConfArray as $acft => $amount) {
            echo("<li>".floatval($amount)." ".$acft);
        } ?>
    </ul>
    <p>The following ground targets were destroyed:</p>
    <ul><br>
        <?php foreach($lwGrndArray as $objt => $amount) {
            echo("<li>".floatval($amount)." ".$objt);
        } ?>
    </ul>
    
    <?php if(count($lwPromotionArray)>0){
            
            echo("------------------------------------------");
            echo("<p>The following promotions were issued:</p>");
            
            echo("<ul>");
            foreach($lwPromotionArray as $id => $rank) {
                echo("<li>".$memberArray[$id]." to ".$rank."</li>");
            }
            echo("</ul>");
    } ?>

    <?php if(count($lwDecorationsArray)>0){
            
            echo("------------------------------------------");
            echo("<p>We are happy to announce the following decorations:</p>");
            
            echo("<ul>");
            foreach($lwDecorationsArray as $medal => $idArray) {
                $nameArray = array();
                foreach ($idArray as $id) {
                    $nameArray[] = $memberArray[$id];
                }
                $wereString = (count($nameArray)>1)?" were":" was";
                if($medal == "Mentioned in Wehrmachtsbericht"){
                    echo("<li>".inTextListing($nameArray).$wereString." mentioned in dispatches.</li>");
                } else {
                    echo("<li>".inTextListing($nameArray).$wereString." awarded the ".$medal."</li>");
                }   
            }
            echo("</ul>");
            echo("Congratulations.");
        } 
    }?>
</div>

