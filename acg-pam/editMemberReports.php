<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    if(filter_has_var(INPUT_GET, "m_id")) {
        
        $edit_id = filter_input(INPUT_GET, "m_id");
        $_SESSION["edit_id"] = $edit_id;
    }
    $dbx = getDBx();
    $_SESSION["edit_id"] = $edit_id;
    $sql = "SELECT callsign FROM acgmembers WHERE id = $edit_id";
    $query = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($query);
    $callsign = $result["callsign"];
    
    $sql = "SELECT reports.id AS r_id, reports.type AS r_type, reports.missionid, ".
           "missions.realdate, reports.authorID, pilotstatus.status ".
           "FROM reports LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "LEFT JOIN missions ON missions.id = reports.missionid ".
           "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
           "WHERE acgmembers.id = $edit_id ORDER BY r_id ASC";
    $rep_result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT missions.id, CONCAT(missions.id, ': ', missions.realdate)  ".
           "FROM missions ORDER BY id ASC";
    $mission_result = mysqli_query($dbx, $sql);
    $mission_array = mysqli_fetch_all($mission_result, MYSQLI_NUM);
    
    $sql = "SELECT careercharacters.id, CONCAT(careercharacters.firstName, ' ', careercharacters.lastName)  ".
           "FROM careercharacters ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id = $edit_id ORDER BY id ASC";
    $character_result = mysqli_query($dbx, $sql);
    $character_array = mysqli_fetch_all($character_result, MYSQLI_NUM);
        
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-130.1</p>
<h3>Member reports:</h3>
<div>
    <p>These pages show a list submitted After Action Reports by <?php echo $callsign;?>.</p> 
    <h3>Submitted After Action Reports:</h3>
    <table>
        <thead>
            <tr>
                <th>ID:</th>
                <th>Faction:</th>
                <th>Mission/Date:</th>
                <th>Pilot Status:</th>
                <th>Author:</th>
            </tr>
        </thead>
        <?php 
            while($row = mysqli_fetch_assoc($rep_result)){
            $type = $row["r_type"];
            $r_id = $row["r_id"];
            if($type == 1){
                $faction = "RAF";
                $link = "reportRAF.php?r_id=".$r_id;
            } else if($type == 2){
                $faction = "Luftwaffe";
                $link = "reportLW.php?r_id=".$r_id;
            } else if($type == 3){
                $faction = "Voyenno-Vozdushnye Sily";
                $link = "reportVVS.php?r_id=".$r_id;
            }
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($link);?>"><?php echo $r_id;?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $faction;?></a></td>
                <td><?php createSelectForm("mi".$r_id, $mission_array, $row["missionid"]);?></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["status"];?></a></td>
                <td><?php createSelectForm("ch".$r_id, $character_array, $row["authorID"]);?></td>
                <td><button onclick="saveAuthorMissionChange(<?php echo $r_id; ?>)">Save Change</button></td>
                <td><button onclick="deleteReport(<?php echo $r_id; ?>)">Delete</button></td>
                <td><span id="<?php echo("rStatus".$r_id); ?>" ></span></td>
            </tr>
        </tbody>
        
        <?php } ?>
    </table>
</div>
<?php include(dirname(__FILE__).'/footer.php');