<?php include(dirname(__FILE__).'/header0.php'); 

    $member_id = filter_input(INPUT_GET, "m_id");
    $dbx = getDBx();
    
    // Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 20;
    $start_from = ($page - 1)*$n_entries;
          
    $sql = "SELECT reports.id AS r_id, reports.type AS r_type, reports.missionid, ".
           "missions.name AS miName, missions.realdate, missions.histdate, careercharacters.firstname, ".
           "careercharacters.lastname, squadrons.name, pilotstatus.status AS pstatus, ".
           "aeroplanestatus.status AS astatus ".
           "FROM reports LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
           "LEFT JOIN aeroplanestatus ON aeroplanestatus.id = reports.aeroplanestatus ".
           "LEFT JOIN squadrons ON squadrons.id = reports.squadronid ".
           "LEFT JOIN missions ON missions.id = reports.missionid ".
           "WHERE acgmembers.id = $member_id AND reports.accepted=1 ".
           "ORDER BY missions.realdate ASC LIMIT $start_from, $n_entries";
    $rep_result = mysqli_query($dbx, $sql);
    
    $counter = 0;
    $rep_array = array();
    while($rep_row = mysqli_fetch_assoc($rep_result)){
        $r_id = $rep_row["r_id"];
        if($rep_row['r_type']==1){
            $link = "reportRAF.php?r_id=".$rep_row["r_id"];           
            //Retrieve RAF victories        
            $victoryStr = "";
            //Destroyed enemystatus = 1
            $sql = "SELECT SUM(1-claimsraf.shared*0.5) ".
                   "FROM claimsraf LEFT JOIN reports ON reports.id = claimsraf.reportid ".
                   "WHERE reportid=$r_id AND reports.accepted=1 AND claimsraf.accepted = 1 AND claimsraf.enemystatus=1";
            $victoriesresult = mysqli_query($dbx, $sql);
            $victoryrow = mysqli_fetch_array($victoriesresult);
            $numvictories = $victoryrow[0];
            if($numvictories > 0){
                $victoryStr = $victoryStr."De: ".number_format($numvictories, 1);
            }              
            //Probable enemystatus = 2
            $sql = "SELECT SUM(1-claimsraf.shared*0.5) ".
                   "FROM claimsraf LEFT JOIN reports ON reports.id = claimsraf.reportid ".
                   "WHERE reportid=$r_id AND reports.accepted=1 AND claimsraf.accepted = 1 AND claimsraf.enemystatus=2";
            $victoriesresult = mysqli_query($dbx, $sql);
            $victoryrow = mysqli_fetch_array($victoriesresult);
            $numvictories = $victoryrow[0];
            if($numvictories > 0){
                if($victoryStr === ""){
                    $victoryStr = $victoryStr."Pr: ".number_format($numvictories, 1);
                }else{
                    $victoryStr = $victoryStr.", Pr: ".number_format($numvictories, 1);
                }
            }
            //Damaged enemystatus = 3
            $sql = "SELECT COUNT(claimsraf.id) ".
                   "FROM claimsraf LEFT JOIN reports ON reports.id = claimsraf.reportid ".
                   "WHERE reportid=$r_id AND reports.accepted=1 AND claimsraf.accepted = 1 AND claimsraf.enemystatus=3";
            $victoriesresult = mysqli_query($dbx, $sql);
            $victoryrow = mysqli_fetch_array($victoriesresult);
            $numvictories = $victoryrow[0];
            if($numvictories > 0){
                if($victoryStr === ""){
                    $victoryStr = $victoryStr."Da: ".number_format($numvictories, 0);
                }else{
                    $victoryStr = $victoryStr.", Da: ".number_format($numvictories, 0);
                }
            }
            $rep_row["victories"] = $victoryStr;
        }
        if($rep_row['r_type']==2){
            $link = "reportLW.php?r_id=".$rep_row["r_id"];           
            //Retrieve LW victories        
            $victoryStr = "";
            //Confirmed = 1
            $sql = "SELECT COUNT(claimslw.id) ".
                   "FROM claimslw LEFT JOIN reports ON reports.id = claimslw.reportid ".
                   "WHERE reportid=$r_id AND reports.accepted=1 AND claimslw.confirmed=1 AND claimslw.accepted = 1";
            $victoriesresult = mysqli_query($dbx, $sql);
            $victoryrow = mysqli_fetch_array($victoriesresult);
            $numvictories = $victoryrow[0];
            if($numvictories > 0){
                $victoryStr = $victoryStr."Conf: ".$numvictories[0];
            }              
            //Confirmed = 0
            $sql = "SELECT COUNT(claimslw.id) ".
                   "FROM claimslw LEFT JOIN reports ON reports.id = claimslw.reportid ".
                   "WHERE reportid=$r_id AND reports.accepted=1 AND claimslw.confirmed=0 AND claimslw.accepted = 1";
            $victoriesresult = mysqli_query($dbx, $sql);
            $victoryrow = mysqli_fetch_array($victoriesresult);
            $numvictories = $victoryrow[0];
            if($numvictories > 0){
                if($victoryStr === ""){
                    $victoryStr = $victoryStr."Unconf: ".$numvictories[0];
                }else{
                    $victoryStr = $victoryStr.", Unconf: ".$numvictories[0];
                }
            }
            $rep_row["victories"] = $victoryStr;
        }
        $rep_row["link"] = $link;
        $rep_array[$counter] = $rep_row;
        $counter++;
    }
    
    $sql = "SELECT callsign FROM acgmembers WHERE id = $member_id";
    $cresult = mysqli_query($dbx, $sql);
    $crow = mysqli_fetch_row($cresult);
    $callsign = $crow[0];
    
    $sql = "SELECT COUNT(reports.id) FROM reports ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id = $member_id AND reports.accepted=1";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
    
?>    
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-211.1</p>
<h3>Member profile:</h3>
<div>
    <p>These pages show a list of attended missions by <?php echo $callsign;?>
    with basic information on each mission. Click on any mission to access the full detailed
    After Action Report. The displayed information is based on submitted and approved 
    After Action Reports.</p>
    
    <h3>Attended missions:</h3>
    <table>
        <thead>
            <tr>
                <th>Name:</th>
                <th>Date:</th>
                <th>Squadron/<br>Staffel:</th>
                <th>Pilot:</th>
                <th>Victories:</th>
                <th>Pilot:</th>
                <th>Aircraft:</th>
            </tr>
        </thead>
        <?php
            $arr_length = count($rep_array);
            for($i=0; $i<$arr_length; $i++){               
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($rep_array[$i]["link"]);?>"><?php echo $rep_array[$i]["miName"];?></a></td>
                <td><a href="<?php echo($rep_array[$i]["link"]);?>"><?php echo $rep_array[$i]["realdate"];?></a></td>
                <td><a href="<?php echo($rep_array[$i]["link"]);?>"><?php echo $rep_array[$i]["name"];?></a></td>
                <td><a href="<?php echo($rep_array[$i]["link"]);?>"><?php echo($rep_array[$i]["firstname"]." ".$rep_array[$i]["lastname"]);?></a></td>
                <td><a href="<?php echo($rep_array[$i]["link"]);?>"><?php echo $rep_array[$i]["victories"];?></a></td>
                <td><a href="<?php echo($rep_array[$i]["link"]);?>"><?php echo($rep_array[$i]["pstatus"]);?></a></td>
                <td><a href="<?php echo($rep_array[$i]["link"]);?>"><?php echo($rep_array[$i]["astatus"]);?></a></td>
            </tr>
        </tbody>
        
        <?php } ?>
    </table>
</div>
<div class='pageSelect'>
    <?php createPageSelect($n_pages, $page, "memberReports.php?m_id=".$member_id."&"); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');
