<?php
    include(dirname(__FILE__).'/header0.php');
    include(dirname(__FILE__).'/includes/decorationsChecks.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    //Accessing data for member from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")) {
        
        $edit_id = filter_input(INPUT_GET, "m_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT callsign FROM acgmembers WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $callsign = $result["callsign"];
        
        // Access character data from database for editing.
        $sql = "SELECT careercharacters.id, careercharacters.firstname, careercharacters.lastname, ".
               "characterstatus.status, careercharacters.characterstatus FROM careercharacters ".
               "LEFT JOIN characterstatus ON characterstatus.id = careercharacters.characterstatus ". 
               " WHERE personifiedby = $edit_id";
        $c_query = mysqli_query($dbx, $sql);
        $c_array = mysqli_fetch_all($c_query, MYSQLI_ASSOC);
        
        $sql = "SELECT id, status FROM characterstatus";
        $cs_query = mysqli_query($dbx, $sql);
        $cs_array = mysqli_fetch_all($cs_query, MYSQLI_NUM);
                
        $sql = "SELECT squadrons.faction, UNIX_TIMESTAMP(transfers.transferdate) AS tstdate ".
               "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
               "WHERE memberid = $edit_id ORDER BY tstdate DESC";
        $cf_query = mysqli_query($dbx, $sql);
        $squadron_check = mysqli_num_rows($cf_query);
        if($squadron_check > 0){

            $cf_result = mysqli_fetch_assoc($cf_query);
            $faction = $cf_result["faction"];
        }
        
        //Awards
        $c_decorations_array = array();
        for($n = 0; $n < count($c_array); $n++){
            
            $characterID = $c_array[$n]["id"];
            checkDecorations($characterID, $dbx);
            
            $sql = "SELECT decorations.id, awards.name, decorations.date, decorations.comment, ".
                   "decorations.awarded FROM decorations ".
                   "LEFT JOIN awards ON decorations.awardID = awards.id ".
                   "WHERE decorations.characterID = $characterID ".
                   "ORDER BY decorations.date ASC";
            $c_decorations_array["$characterID"] = mysqli_query($dbx, $sql);
        }
        
        
    } 
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/decorationsLocig.js"></script>
<script src="jscript/characterLogic.js"></script>
<script type="text/javascript">

//function transfer(){
//    transferMember();
//    
//}

window.onload = function(){

    gebid("characterBtn").addEventListener("click", createCharacter, false);
};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-120.1</p>

<h3>Administrate characters for <?php echo $callsign;?> ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<hr>

<form id="newCharacterForm" onsubmit="return false;" >
    <div class="middlePageStandard">
    <p>Create, delete or dismiss career characters. Career characters represent 
    pilots that are bound to their fate. While members keep on flying missions 
    and collecting victories, no matter the outcome of each mission, career characters
    can only fly missions as long as they are not killed or captured. Once a 
    career character is killed or captured, a new character is created. It is 
    possible to dismiss characters in case members leave ACG or change faction.</p>
    <p>The first and/or last name of a career characters can be edited by typing
    the new name into the filed for first and/or last name, followed by pressing
    the <i>Change Name</i> button next to the character concerning the name change.</p>
    </div>
    <div class="middlePageStandard">
        <b>First name:</b>
        <input type="text" id="firstName" name="firstName">
        <span id="fnamestatus" ></span>
    </div>

    <div class="middlePageStandard">
        <b>Last name:</b>
        <input type="text" id="lastName" name="lastName">
        <span id="lnamestatus" ></span>
    </div>
    <button class="editButton" onclick="randomCharacterName(<?php echo $edit_id ?>)">Random name</button>

    <div id="characterStatus">
        <button id="characterBtn">Create character</button>
        <span id="characterStatus">&nbsp;</span> 
    </div>     
</form>

<div class="middlePageStandard">
    <form id="editCharacterForm" onsubmit="return false;">
    <h3>Characters:</h3>
    <hr>
    <table>
        <tr>
            <th>ID:</th>
            <th>First name:</th>
            <th>Last name:</th>
            <th>Status:</th>  
        </tr> 
    <?php 
        for($n = 0; $n < count($c_array); $n++){
                $row = $c_array[$n];
                $id = $row["id"];
    ?>   
        <tr>
            <td><?php echo $id;?></td>
            <td><?php echo $row["firstname"];?></td>
            <td><?php echo $row["lastname"];?></td>
            <td><?php echo $row["status"];?></td>
            <td><?php createSelectForm("cs".$id, $cs_array, $row["characterstatus"]);?></td>
            <td><button onclick="editCharacterStatus(<?php echo $id; ?>, '<?php echo "cs".$id; ?>')">Change status</button></td>
            <td><button onclick="editCharacterName(<?php echo $row['id']; ?>)">Change name</button></td>
            <td><button onclick="deleteCharacter(<?php echo $row['id']; ?>)">Delete</button></td>
        </tr>
    <?php } ?>
    </table>
    <hr>
    
    <div class="middlePageStandard">
    <p>Career characters can be recommended for awards depending on their performance.
    The awards will be listed for the career character once the criteria are met.
    Awards are given by pressing the <i>Award</i> button next to the award. 
    The given date is the date the award criteria was met. However this date can 
    be changed to a later point in time. It is as well possible to write a short 
    comment on the circumstances of the award. Awards can be revoked by pressing 
    the <i>Revoke</i> button.</p>
    </div>
    
    <?php
        for($n = 0; $n < count($c_array); $n++){
                $row = $c_array[$n];
                $id = $row["id"];
    ?>
    <h4>Decorations for <?php echo $row["firstname"]." ".$row["lastname"]; ?>:</h4>
    <?php while($d_row = mysqli_fetch_assoc($c_decorations_array["$id"])){
                if($d_row["date"] == ""){
                   $date = getdate();
                } else {
                   $date = getdate(strtotime($d_row["date"]));  
                }
    ?>     
    <table >
        <tr>
            <th align="left">ID:</th>
            <th align="left">Award:</th>
            <th align="left">Date:</th>
 
        </tr>
        <tr>
            <td width="5%"><?php echo $d_row["id"];?></td>
            <td width="50%"><?php echo $d_row["name"];?></td>
            <td width="20%">
                <input type="text" id="ddateY<?php echo $d_row["id"]; ?>" name="ddateY<?php echo $d_row["id"]; ?>" size="4" maxlength="4" value="<?php echo $date['year']; ?>">-
                <input type="text" id="ddateM<?php echo $d_row["id"]; ?>" name="ddateM<?php echo $d_row["id"]; ?>" size="2" maxlength="2" value="<?php echo $date['mon']; ?>">-
                <input type="text" id="ddateD<?php echo $d_row["id"]; ?>" name="ddateD<?php echo $d_row["id"]; ?>" size="2" maxlength="2" value="<?php echo $date['mday']; ?>">
            </td>
            <td width="15%">
                <?php
                if($d_row["awarded"]){ ?>
                    <button onclick="revokeDecoration(<?php echo $d_row["id"] ?>)">Revoke</button>
                <?php } else { ?>
                    <button onclick="awardDecoration(<?php echo $d_row["id"] ?>, <?php echo $_SESSION['userID']?>)">Award</button>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="4"><textarea id="dcomment<?php echo $d_row["id"]; ?>"
                rows="1" cols="50" maxlength="200"><?php echo $d_row["comment"]; ?></textarea></td>
        </tr>
    </table>
    <?php } ?>
    <hr>
    <?php } ?>
    </form>
</div>
            
<?php include(dirname(__FILE__).'/footer.php');