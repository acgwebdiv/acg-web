<?php
    
    if(filter_has_var(INPUT_GET, "sqn")) {
        $sqn = filter_input(INPUT_GET, "sqn");
    } else {
        header("location: squadronIndex.php");
    }

    $dbx = getDBx();
    $sql = "SELECT acgmembers.id, acgmembers.callsign, acgmembers.aircraftID, ranks.abreviation, ranks.value, ".
           "firststs2.date AS joiningdate, currentsts2.date AS lastStatus, currentsts2.statusID, ".
           "currentsqu2.squadronid, currentrank2.date AS pdate ".
           "FROM acgmembers ".
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MIN(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS firststs ".
                "ON (firststs.memberID, firststs.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS firststs2 ".
            "ON firststs2.memberID = acgmembers.id ".
            "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
            "ON currentsts2.memberID = acgmembers.id ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
            "LEFT JOIN ".
                "(SELECT promotions.memberid, promotions.value, promotions.date FROM promotions ".
                "JOIN ".
                    "(SELECT promotions.memberid, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                    "FROM promotions GROUP BY memberid) AS currentrank ".
                "ON (currentrank.memberid, currentrank.pdate) = ".
                "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
           "ON currentrank2.memberid = acgmembers.id ".
           "LEFT JOIN ranks ON (currentrank2.value, squadrons.faction) = (ranks.value, ranks.faction) ".
           "WHERE currentsts2.statusID = 1 AND currentsqu2.squadronid = $sqn ORDER BY value DESC, joiningdate ASC";
    $result = mysqli_query($dbx, $sql);
    $memberArray = array();
    while($row = mysqli_fetch_assoc($result)) {
        $memberId = $row["id"];
        $memberArray[$memberId]["link"] = "memberDetails.php?m_id=".$row["id"];
        $memberArray[$memberId]["rankAbreviation"] = $row["abreviation"];
        $memberArray[$memberId]["callsign"] = $row["callsign"];
        $memberArray[$memberId]["joiningdate"] = $row["joiningdate"];
        $memberArray[$memberId]["promotionDate"] = $row["pdate"];
        $memberArray[$memberId]["aircraftID"] = $row["aircraftID"];
    }
    // echo $sql;
    
    $sql = "SELECT acgmembers.id, acgmembers.callsign, acgmembers.aircraftID, ranks.abreviation, ranks.value, ".
           "firststs2.date AS joiningdate, currentsts2.date AS lastStatus, currentsts2.statusID, ".
           "currentsqu2.squadronid, currentrank2.date AS pdate ".
           "FROM acgmembers ".
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MIN(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS firststs ".
                "ON (firststs.memberID, firststs.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS firststs2 ".
            "ON firststs2.memberID = acgmembers.id ".
            "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
            "ON currentsts2.memberID = acgmembers.id ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
            "LEFT JOIN ".
                "(SELECT promotions.memberid, promotions.value, promotions.date FROM promotions ".
                "JOIN ".
                    "(SELECT promotions.memberid, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                    "FROM promotions GROUP BY memberid) AS currentrank ".
                "ON (currentrank.memberid, currentrank.pdate) = ".
                "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
           "ON currentrank2.memberid = acgmembers.id ".
           "LEFT JOIN ranks ON (currentrank2.value, squadrons.faction) = (ranks.value, ranks.faction) ".
           "WHERE currentsts2.statusID = 3 AND currentsqu2.squadronid = $sqn ORDER BY value DESC, joiningdate ASC";
    $result = mysqli_query($dbx, $sql);
    $memberArrayRNR = array();
    while($row = mysqli_fetch_assoc($result)) {
        $memberId = $row["id"];
        $memberArrayRNR[$memberId]["link"] = "memberDetails.php?m_id=".$row["id"];
        $memberArrayRNR[$memberId]["rankAbreviation"] = $row["abreviation"];
        $memberArrayRNR[$memberId]["callsign"] = $row["callsign"];
        $memberArrayRNR[$memberId]["joiningdate"] = $row["joiningdate"];
        $memberArrayRNR[$memberId]["promotionDate"] = $row["pdate"];
        $memberArrayRNR[$memberId]["aircraftID"] = $row["aircraftID"];
        $memberArrayRNR[$memberId]["lastStatus"] = $row["lastStatus"];
    }
    
    $sql = "SELECT squadrons.name FROM squadrons WHERE id=$sqn";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $sqn_name = $row["name"];