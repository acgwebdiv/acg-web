<?php
include(dirname(__FILE__).'/../header0.php');

function get_current_path($strip = true) {
	// filter function
	static $filter;
	if ($filter == null) {
		$filter = function($input) use($strip) {
			$input = str_ireplace(array("\0", '%00', "\x0a", '%0a', "\x1a", '%1a'), '', urldecode($input));
			if ($strip) {
				$input = strip_tags($input);
			}
			// or any encoding you use instead of utf-8
			$input = htmlspecialchars($input, ENT_QUOTES, 'utf-8'); 
			return trim($input);
		};
	}
	$me = $filter(basename($_SERVER['PHP_SELF']));
	$full = 'http'. (($_SERVER['SERVER_PORT'] == '443') ? 's' : '').'://'. $_SERVER['SERVER_NAME'] . $filter($_SERVER['REQUEST_URI']);
	return str_replace($me,'',$full);
}

function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function ofInterest($var) {
	$retVal = false;
	if (substr($var, -6) == "_R.php" ||substr($var, -6) == "_L.php") {
		 $retVal = true;
	}
	return $retVal;
}

if(isset($_SESSION["admin"])){
	if(!$_SESSION["admin"]){
		echo $_SESSION["admin"];
		header("location: ../message.php?m=1");
		exit();
	}
} else {
	header("location: ../message.php?m=1");
	exit();
}

//Check to generate php
if ($_POST['generate']) {
	//delete existing
	foreach(glob('*') as $file) {
		if ($_POST['generate'] == 'Gen' || $_POST['generate'] == 'Del') {
			if (substr($file, -6) == "_R.php" ||substr($file, -6) == "_L.php") {
				unlink($file);
			}
		} else {
			if ($file == $_POST['generate']) {
				unlink($file);
			}
		}
	}
	//Create
	if ($_POST['generate'] == 'Gen') {
		$strR = generateRandomString()."_R.php";
		$strL = generateRandomString()."_L.php";
		$strLF = generateRandomString(7)."_LF_L.php";
		copy("campaign.raf",$strR);
		copy("campaign.lw",$strL);
		copy("campaign.lf",$strLF);
	}
}

$srvr = "176.32.230.9";
$user = "cl45-acg-pam";
$pass = "V^hjGnK-f";
$db = "cl45-acg-pam";
$conn = mysqli_connect($srvr, $user, $pass, $db);
if ($_POST['bless']) {
	$blesslist = $_POST['bless'];
	$sql = 'delete from GC_blesslist;';
	if ($blesslist != "DELETEONLY") {
		$theblessed = explode("|", $blesslist);
		foreach($theblessed as $blessed) {
			$blessed = trim($blessed);
			if (strlen($blessed) > 0) {
				$sql .= "CALL GC_logBlessList ('" . $blessed . "');";
			}
		}
	}
	mysqli_multi_query($conn,$sql);
	while($conn->more_results()){
		$conn->next_result();
		if($res = $conn->store_result()){
			$res->free(); 
		}
	}
}
$blacklist = c_mysqli_call($conn,"GC_getMissionBlackList");
$blesslist = c_mysqli_call($conn,"GC_blessList");
mysqli_close($conn);
?>
<?php
function c_mysqli_call(mysqli $dbLink, $procName, $params="")
{
	if(!$dbLink) {
		throw new Exception("The MySQLi connection is invalid.");
	}else{
		$sql = "CALL {$procName}({$params});";
		$sqlSuccess = $dbLink->multi_query($sql);
		if($sqlSuccess){
			if($dbLink->more_results()){
				$result = $dbLink->use_result();
				$output = array();
				while($row = $result->fetch_assoc()){
					$output[] = $row['GCPlayer'];
				}
				$result->free();
				while($dbLink->more_results() && $dbLink->next_result()){
					$extraResult = $dbLink->use_result();
					if($extraResult instanceof mysqli_result){
						$extraResult->free();
					}
				}
				return $output;
			}else{
				return false;
			}
		}else{
			throw new Exception("The call failed: " . $dbLink->error);
		}
	}
}
?>
<html>
	<TITLE>ACG CAMPAIGN CONSOLE</TITLE>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script type="text/javascript">
			$().ready(function() {
				$('#add').click(function() {
					$('#blacklist option:selected').clone().appendTo('#blesslist');
					$('#blacklist option').removeAttr("selected");
					$('#blesslist option').removeAttr("selected");
					alphaSort("blesslist");
					highlight();
				});
				$('#remove').click(function() {
					!$('#blesslist option:selected').remove();
					$('#blesslist option').removeAttr("selected");
					highlight();
				});
				
			});

			function highlight(){
				var x = document.getElementById('save');
				x.style.color = '#000000';
				x.style.backgroundColor = '#FFFF00';
				x.style.display = 'block';
			}
			
			function alphaSort(element) {
				var x = document.getElementById(element);
				var i;
				var hash = {};
				var sorted = [];
				for (i = 0; i < x.length; i++) {
					if (hash[x.options[i].value] = "undefined") {
						hash[x.options[i].value] = x.options[i].value;
					}
				}
				for (var key in hash) {
					sorted.push(hash[key]);
				}
				sorted.sort();
				x.options.length = 0;
				for (i = 0; i < sorted.length; i++) {
					var el = document.createElement("option");
					el.text = sorted[i];
					el.value = sorted[i];
					x.appendChild(el);
				}
			}

			function submitme() {
				var x = document.getElementById('blesslist');
				var oForm = document.forms['postform'];
				var oe = oForm.elements['bless'];
				var str = "";
				for (i = 0; i < x.length; i++) {
					str = str + x.options[i].value + "|";
				}
				if (str.length == 0) {
					str = "DELETEONLY";
				}
				oe.value = str;
				oForm.submit();
			}

			function handle(val) {
				var oForm = document.forms['genform'];
				var oe = oForm.elements['generate'];
				oe.value = val;
				oForm.submit();
			}

			function copyToClipboard(text) {
				window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
			}
		</script>
		<style type="text/css">
			a {
				display: block;
				border: 1px solid #aaa;
				text-decoration: none;
				background-color: #fafafa;
				color: #123456;
				margin: 2px;
				clear:both;
			}
			#save {
				display: none;
				border: 1px solid #aaa;
				text-decoration: none;
				margin: 2px;
				clear:both;
			}
			#url {
				display: block;
				text-decoration: none;
				color: #0000ff;
				border: none;
				background-color: #ffffff;
				margin: none;
			}
			div {
				float:left;
				text-align: center;
				margin: 10px;
			}
			select {
				width: 250px;
				height: 500px;
			}
		</style>
	</head>
	<body>
		<div>  
			<select multiple id="blacklist">
				<?php
				foreach ($blacklist as $value) {
					echo "<option value='".$value."'>".$value."</option>";
				}
				?>
			</select>
			<a href="#" id="add">Bless... &gt;&gt;</a>
		</div>
		<form action='<?php echo $_SERVER[PHP_SELF]?>' method='post' autocomplete='off' name='postform'>
			<div>
				<select multiple id="blesslist">
					<?php
					foreach ($blesslist	as $value) {
						echo "<option value='".$value."'>".$value."</option>";
					}
					?>
				</select>
				<input name='bless' class='input' type='hidden' value=''>
				<a href="#" id="remove">&lt;&lt; Return to Hell...</a><br>
				<a href="javascript:submitme()" id="save">Save to Database</a>
			</div>
		</form>
		<form action='<?php echo $_SERVER[PHP_SELF]?>' method='post' autocomplete='off' name='genform'>
			<div>
				<input name='generate' class='input' type='hidden' value=''>
				<a href="javascript:handle('Gen')">Generate campaign web-plot URLs</a>
				<a href="javascript:handle('Del')">Delete all existing campaign web-plot URLs</a>
				<P>Existing Campaign URLs: </P>
				<?php 
				$url = get_current_path();
				echo "<table>";
				foreach(glob('*') as $file) {
					if (substr($file, -6) == "_R.php" || substr($file, -6) == "_L.php" ) {
						echo "<tr>";
						if (substr($file, -6) == "_L.php"){
							if (substr($file, -9) == "_LF_L.php"){
								echo "<td>LFlott:</td>";
							} else {
								echo "<td>LW:</td>";
							}
						} else {
							echo "<td>RAF:</td>";
						}
						echo "<td><a id='url' href='".$url.$file."' target = '_blank'>".$url.$file."</a></td>";
						echo "<td><a href='javascript:handle(&quot;".$file."&quot;)'>&nbsp;Del&nbsp;</a></td>";
						echo "<td><a href='javascript:copyToClipboard(&quot;".$url.$file."&quot;)'>&nbsp;Copy&nbsp;</a></td>";
						echo "</tr>";
					}
				}
				echo "</table>"
				?>
			</div>
		</form>
	</body>
</html>
