</head>
<body id="adminMenuBody">
    <div class="mainContainer">
        <div class="pageTop">
            <?php
            include(dirname(__FILE__).'/primMenu.php'); 
            include(dirname(__FILE__).'/userMenu.php');
            ?>
            <div class="secMenu">
                <ul>
                    <li><a href="memberAdministration.php">Members</a></li>
                    <li><a href="squadronAdministration.php">Squadrons/Staffeln</a></li>
                    <li><a href="campaignAdministration.php">Campaigns</a></li>
                    <li><a href="missionAdministration.php">Missions</a></li>
                    <li><a href="administrationIndex.php?obj_adm=1">Objects</a></li>  
                </ul>
                <?php
                    if(filter_has_var(INPUT_GET, "m_id")){
                        $user_id = filter_input(INPUT_GET, "m_id");           
                ?>
                <ul class='memberMenu'>
                <li><a href="memberAdministration.php?m_id=<?php echo($user_id); ?>">General information</a></li>
                <li><a href="editCharacter.php?m_id=<?php echo($user_id); ?>">Characters</a></li>
                <li><a href="editMemberReports.php?m_id=<?php echo($user_id); ?>">Reports</a></li>
                <li><a href="editAppointments.php?m_id=<?php echo($user_id); ?>">Appointments</a></li>
                </ul>
                <?php } ?>
                <?php
                    if(filter_has_var(INPUT_GET, "obj_adm")){
                        $user_id = filter_input(INPUT_GET, "obj_adm");           
                ?>
                <ul class='memberMenu'>
                <li><a href="aeroplaneAdministration.php?obj_adm=1">Aeroplanes</a></li>
                <li><a href="groundTargetAdministration.php?obj_adm=1">Ground targets</a></li>
                </ul>
                <?php } ?>
            </div>
        </div>
        <div class="pageMiddle">