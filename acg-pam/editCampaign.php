<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Accessing data from campaign for editing
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "c_id")) {
        
        $edit_id = filter_input(INPUT_GET, "c_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT name FROM campaigns WHERE id = $edit_id LIMIT 1";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $name = $result["name"];
    } 
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/campaignAdminLogic.js"></script>
<script type="text/javascript">

window.onload = function(){
    gebid("changeCampaignName").addEventListener("click", changeCampaignName, false);
    gebid("deleteCampaign").addEventListener("click", deleteCampaign, false);
};
</script>
<?php include './adminMenu.php'; ?> 
<p class="form_id">ACG-PAM/400-310.1</p>
<h3>Edit Campaign ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<form id="editCampaign" onsubmit="return false;" >
    <div class="middlePageStandard">
        <b>Campaign name:</b>
        <input type="text" id="campaignName" name="campaignName" value="<?php echo $name; ?>">
        <button id="changeCampaignName">Save Change</button>
        <span id="cnamestatus" ></span>
    </div>

    <div class="middlePageStandard">
        <p><button id="deleteCampaign">Delete</button> Deletes campaign from 
        database. Only campaigns that do not include a single mission should be 
        deleted from the database for consistency.</p>
    </div>
</form>
<?php include(dirname(__FILE__).'/footer.php');