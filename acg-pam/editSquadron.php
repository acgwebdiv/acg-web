<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Accessing squadron data from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "s_id")) {
        
        $edit_id = filter_input(INPUT_GET, "s_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT name, code, faction, acg_unit FROM squadrons WHERE id = $edit_id LIMIT 1";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $name = $result["name"];
        $code = $result["code"];
        $faction = $result["faction"];
        $acg_unit = $result["acg_unit"];
    }
    
    // Access al squadron info from database for form input.
    $sql = "SELECT id, name FROM squadrons ORDER BY id ASC";
    $s_result = mysqli_query($dbx, $sql);
    $s_id_array = mysqli_fetch_all($s_result);
    $base_unit_array = array();
    $base_unit_array[0][0] = 0;
    $base_unit_array[0][1] = "None";
    for($n = 1; $n < count($s_id_array); $n++){
        $base_unit_array[$n][0] = $s_id_array[$n-1][0];
        $base_unit_array[$n][1] = $s_id_array[$n-1][0]."-".$s_id_array[$n-1][1];
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/squadronAdminLogic.js"></script>
<script type="text/javascript">

window.onload = function(){
    gebid("changeSquadronName").addEventListener("click", changeSquadronName, false);
    gebid("changeSquadronCode").addEventListener("click", changeSquadronCode, false);
    gebid("changeSquadronFaction").addEventListener("click", changeSquadronFaction, false);
    gebid("changeSquadronACGUnit").addEventListener("click", changeSquadronACGUnit, false);
    gebid("deleteSquadron").addEventListener("click", deleteSquadron, false);
};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-210.1</p>
<h3>Edit Squadron/Staffel ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<form id="editSquadron" onsubmit="return false;" >
    <div class="middlePageStandard">
         <b>Squadron/Staffel name:</b>
        <input type="text" id="squadronName" name="squadronName" value="<?php echo $name; ?>">
        <button id="changeSquadronName">Save Change</button>
        <span id="snamestatus" ></span>
    </div>

    <div class="middlePageStandard">
        <b>Squadron/Staffel code:</b>
        <input type="text" id="squadronCode" name="squadronCode" value="<?php echo $code; ?>">
        <button id="changeSquadronCode">Save Change</button>
        <span id="scodestatus" ></span>
    </div>

    <div class="middlePageStandard">
        <b>Squadron/Staffel faction:</b>
        <select id="squadronFaction" name="squadronFaction">
            <option value="RAF" <?php if($faction === "RAF"){ echo "selected";}?>>Royal Air Force</option>
            <option value="LW" <?php if($faction === "LW"){ echo "selected";}?>>Luftwaffe</option>
            <option value="VVS" <?php if($faction === "VVS"){ echo "selected";}?>>Voyenno-Vozdushnye Sily</option>
        </select>
        <button id="changeSquadronFaction">Save Change</button>
        <span id="sFactionstatus" ></span>
    </div>
    
    <div class="middlePageStandard">
        <b>ACG-unit (base-unit):</b>
        <?php createSelectForm("squadronACGUnit", $base_unit_array, $acg_unit) ?>
        <button id="changeSquadronACGUnit">Save Change</button>
        <span id="sACGUnitstatus" ></span>
    </div>

    <div class="middlePageStandard">
        <p><button id="deleteSquadron">Delete</button> Deletes Squadron/Staffel 
        from database. Only Squadrons/Staffeln that do not have members or 
        performed missions should be deleted from the database for consistency.</p>
    </div>
</form>
<?php include(dirname(__FILE__).'/footer.php');
