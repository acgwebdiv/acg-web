<?php
    include(dirname(__FILE__).'/header0.php'); 
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');

    $dbx = getDBx();
    
    if(filter_has_var(INPUT_GET, "m_id")){
        $mi_id = filter_input(INPUT_GET, "m_id");
    } else {
        header("location: missionList.php");
        exit();
    }
    
    // Check if existing user otherwise redirect
    if(!isset($_SESSION["userID"])){
        header("location: message.php?m=1");
        exit();
    }
    $user_id = $_SESSION["userID"];
    $faction = getFaction($user_id, $dbx);
    
    $sql = "SELECT fate FROM fortuneTellerLog ".
           "WHERE memberID = $user_id AND missionID = $mi_id";
    $query = mysqli_query($dbx, $sql);
    $statusText = "&nbsp;";
    $disabletext = "";
    if(mysqli_num_rows($query)>0){
        $r_result = mysqli_fetch_assoc($query);
        $statusText = $r_result["fate"];
        $disabletext = "disabled";
    }

?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script type="text/javascript">
    
    function rollDice(u_id, m_id){
    var status = gebid("location").value;
    var ajax = syncAjaxObj("POST", "./includes/fortuneTellerLogic.php");
    ajax.onreadystatechange = function() {
        if(ajaxReturn(ajax) === true) {
            var response = ajax.responseText;
            gebid("submitStatus").innerHTML = response;
            location.reload();
        }
    };
    ajax.send("rollDice="+status+"&missionID="+m_id+"&memberID="+u_id);
}
    
</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>

<p class="form_id">ACG-PAM/100-001.1</p>
<div id="frontPageContainer">
   
    <?php
    if($faction==="LW"){
    ?>
    <form id="checkFateForm" onsubmit="return false;" >
        <select id="location" name="location">
            <option value=1>Crashlanded in England</option>
            <option value=2>Ditched in the channel within 10km from English coast</option>
            <option value=3>Ditched in the channel further than 10km from coast</option>
            <option value=4>Ditched in the channel within 10km from French coast</option>
        </select> 
    <?php
    }
    else if($faction==="RAF"){
    ?>
    <form id="checkFateForm" onsubmit="return false;" >
        <select id="location" name="location">
            <option value=1>Crashlanded in France</option>
            <option value=2>Ditched in the channel within 10km from French coast</option>
            <option value=3>Ditched in the channel further than 10km from coast</option>
            <option value=4>Ditched in the channel within 10km from English coast</option>
        </select> 
    <?php
    }
    ?>
    
        <div>
            <button id="submitBtn" onclick="rollDice(<?php echo($user_id.', '.$mi_id); ?>)"
            <?php echo $disabletext; ?> >Submit choice</button>
        </div>
    </form>
    <div id="submitStatus"><?php echo $statusText; ?></div>
</div>       
<?php include(dirname(__FILE__).'/footer.php');