<?php 
$sql = "SELECT positionID, memberID FROM roster WHERE squadronID = $sqn";
$result = mysqli_query($dbx, $sql);
//$roster_array = mysqli_fetch_all($result);
$roster_array = array();
while($row = mysqli_fetch_assoc($result)) {
    $roster_array[$row["positionID"]] = $row["memberID"];
}

function memberSelectForm($position, $roster_array, $memberIDArray){
    if(array_key_exists($position, $roster_array)){
        $memberID = $roster_array[$position];
    } else {
        $memberID = 0;
    }
    createSelectFormWithName("roster", $position, $memberIDArray, $memberID);
}

?>
<div class="rosterSetup">
    <form id="rosterAircraft" onsubmit="return false;" >
        <table>
            <thead>
                <tr>
                    <th>Role:</th>
                    <th>Pilot:</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Kommodore</td>
                    <td><?php 
                            $position = 1;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Adjutant</td>
                    <td><?php 
                            $position = 2;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Operations Offizier</td>
                    <td><?php 
                            $position = 3;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Technischer Offizier</td>
                    <td><?php 
                            $position = 4;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
            </tbody>
        </table>
        <button id="saveRosterBtn" >Save changes</button>
        <span id="saveRosterStatus" ></span>        
    </form>
</div>

