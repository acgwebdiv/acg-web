<?php include(dirname(__FILE__).'/header0.php'); ?>
<?php
    
    

    
?>
<link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
 

<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
<script type="text/javascript">
    
    function reloadStatistics(){
        var selectedSubject = gebid("statisticsSubject").value;
        window.location.replace("statisticsList.php?subj="+selectedSubject);
    }
    
</script>   
<?php include(dirname(__FILE__).'/mainMenu.php'); ?> 

<p class="form_id">ACG-PAM/100-002.1</p>
<h3>Statistic:</h3>
<div>
    <h3>Squadron/Staffel - Mission - statistics</h3>
    <p>These sides provide tables giving information about each squadron/Staffel for
    each mission and campaign.</p>
    <ul>
        <li><a href='statisticsDisplay.php?subj=1'>Number of sorties</a></li>
        <li><a href='statisticsDisplay.php?subj=2'>Number of aircraft that returned undamaged</a></li>
        <li><a href='statisticsDisplay.php?subj=3'>Number of aircraft that returned damaged</a></li>
        <li><a href='statisticsDisplay.php?subj=4'>Number of aircraft that were lost</a></li>
    </ul>
    
</div>
<?php include(dirname(__FILE__).'/footer.php');