<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
        exit();
    }
    
    // Accessing mission briefing data from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")) {
        
        $mi_id = filter_input(INPUT_GET, "m_id");
        $faction = filter_input(INPUT_GET, "faction");
        $type = filter_input(INPUT_GET, "type");
        $prefill = false;
        $sql = "SELECT * FROM briefings WHERE missionID = $mi_id AND faction = '$faction'";

        $query = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($query)>0){
            $result = mysqli_fetch_assoc($query);
            $briefingID = $result["id"];
            $briefingType = $result["type"];
            
            if($type == $briefingType){
                $prefill = true;
            }
        }
    
    // Accessing mission data from database.
    $sql = "SELECT missions.name AS miName, missions.histdate, missions.realdate, ".
       "campaigns.name AS c_name ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_name = $m_row["miName"];
        
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/missionAdminLogic.js"></script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?>
<p class="form_id">ACG-PAM/400-411.1</p>
<p>
<b>Select Format:</b>
<ul class='memberMenu'>
    <li><a href="editMissionBriefing.php?m_id=<?php echo $mi_id;?>&faction=<?php echo $faction;?>&type=1">Free</a></li>
    <li><a href="editMissionBriefing.php?m_id=<?php echo $mi_id;?>&faction=<?php echo $faction;?>&type=2">Form sheet</a></li>  
</ul>

</p>
<?php
    
    switch($type){
        case 1:

?>
<h3>Edit mission briefing for <?php echo $faction ?> - <?php echo($mi_name." (".$m_row["histdate"].")"); ?>, 
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date: <?php echo($m_row["realdate"]); ?>.</h3>
<form id="editMissionBriefing" onsubmit="return false;" >
    <textarea id="missionBriefing" rows="25" cols="50"><?php if($prefill){echo $result["text1"];} ?></textarea>
    <div id="briefingStatus">&nbsp;</div>
    <button id="submitBtn" onclick="saveMissionBriefing(<?php echo $mi_id; ?>, '<?php echo $faction; ?>',1)">Save Briefing</button> 
</form>
<?php   
            break;
        case 2:
            
            if($faction === "RAF") {
                $headingTXT = "Royal Air Force <br>".
                              "11.Group Mission Directive <br>";
            } else if($faction === "LW") {
                $headingTXT = "Luftwaffe <br>".
                              "JG26 and ZG76 Directive <br>";
            }

?>
<h3>Edit mission briefing for <?php echo $faction ?> - <?php echo($mi_name." (".$m_row["histdate"].")"); ?>, 
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date: <?php echo($m_row["realdate"]); ?>.</h3>
<form id="editMissionBriefing" onsubmit="return false;" >
    
    <p>
        <?php echo $headingTXT;?>
        Classified
    </p>
    <p>
        Date: <?php echo $m_row["histdate"];?><br>
        Operation: <?php echo $mi_name;?><br>
        Target: <input type="text" id="text1" name="text1" size="100" value="<?php if($prefill){echo $result["text1"];} ?>"><br>
        Secondary target/Diversions: <br>
        <input type="text" id="text2" name="text2" size="150" value="<?php if($prefill){echo $result["text2"];} ?>"><br>
        Force to target/s: <br>
        <textarea id="text3" rows="2" cols="50"><?php if($prefill){echo $result["text3"];} ?></textarea><br>
    </p>
    <p>
        Weather report:<br>
        <textarea id="text4" rows="2" cols="50"><?php if($prefill){echo $result["text4"];} ?></textarea><br>
    </p>
    <p>
        Engine start time: <input type="text" id="text5" name="text5" value="<?php if($prefill){echo $result["text5"];} ?>"><br>
        RV: <input type="text" id="text6" name="text6" value="<?php if($prefill){echo $result["text6"];} ?>"><br>
        H-Hour: <input type="text" id="text7" name="text7" value="<?php if($prefill){echo $result["text7"];} ?>"><br>
    </p>
    <p>
        Route and proceedings:<br>
        <textarea id="text8" rows="2" cols="50"><?php if($prefill){echo $result["text8"];} ?></textarea><br>
        Rules of engagement:<br>
        <textarea id="text9" rows="2" cols="50"><?php if($prefill){echo $result["text9"];} ?></textarea><br>
        Expected resistance:<br>
        <textarea id="text10" rows="2" cols="50"><?php if($prefill){echo $result["text10"];} ?></textarea><br>
        Divert: <input type="text" id="text11" name="text11" size="100" value="<?php if($prefill){echo $result["text11"];} ?>"><br>
    </p>
    <p>
        URL to map images: [img]image-url[/img]<br>
        <textarea id="text12" rows="2" cols="50"><?php if($prefill){echo $result["text12"];} ?></textarea><br>
    </p>
    <div id="briefingStatus">&nbsp;</div>
    <button id="submitBtn" onclick="saveMissionBriefing(<?php echo $mi_id; ?>, '<?php echo $faction; ?>',2)">Save Briefing</button> 
</form>
<?php   
            break;
    }
?>
<?php include(dirname(__FILE__).'/footer.php');