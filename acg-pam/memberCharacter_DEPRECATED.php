<?php
    include_once(dirname(__FILE__).'/includes/login_session.php');
    include_once(dirname(__FILE__).'/includes/db_connect.php');
    session_start();
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            header("location: message.php?msg=You have no rights to access this page.");
            exit();
        }
    } else {
        
        header("location: message.php?msg=You have no rights to access this page.");
        exit();
    }
    
    // Access character data from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")) {
        
        $edit_id = filter_input(INPUT_GET, "m_id");
        $edit_name = filter_input(INPUT_GET, "m_name");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT id, firstName, lastName, characterStatus FROM careerCharacters ".
                " WHERE personifiedBy = $edit_id";
        $c_query = mysqli_query($dbx, $sql);
        
        $sql = "SELECT squadrons.faction, UNIX_TIMESTAMP(transfers.transferDate) AS tsTDate ".
               "FROM transfers LEFT JOIN squadrons ON transfers.squadronID = squadrons.id ".
               "WHERE memberID = $edit_id ORDER BY tsTDate DESC LIMIT 1";
        $query = mysqli_query($dbx, $sql);
        $squadron_check = mysqli_num_rows($query);
        if($squadron_check < 1){

            header("location: message.php?msg=The member needs to be assigned to a squadron/Staffel first.");
            exit();
        } else {

            $s_result = mysqli_fetch_assoc($query);
            $faction = $s_result["faction"];
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=encoding">
    <title>ACG Pilot- and Mission Database</title>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
    <script src="jscript/main.js"></script>
    <script src="jscript/ajax.js"></script>
    <script src="jscript/characterLogic.js"></script>
    <script type="text/javascript">
   
    window.onload = function(){
        gebid("submitBtn").addEventListener("click", createCharacter, false);
    };
    
    </script>   
</head>
<body>
    
    <div class="mainContainer">
	<div class="pageTop">
            <?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
        </div> 
        <div class="pageMiddle">
            <h3>Administrate characters for <?php echo $edit_name; ?> ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
            <form id="newCharacterForm" onsubmit="return false;" >
                <div>
                    First name:<br>
                    <input type="text" id="firstName" name="firstName">
                    <span id="fnamestatus" ></span>
                </div>
                
                <div>
                    Last name:<br>
                    <input type="text" id="lastName" name="lastName">
                    <span id="lnamestatus" ></span>
                </div>
                <button class="editButton" onclick="randomCharacterName('<?php echo $faction; ?>')">Random name</button>

                <div id="submitStatus">&nbsp;</div>
                <button id="submitBtn">Create character</button>   
            </form>
            
            <div>
                <h3>Characters:</h3>
                <table>
                    <tr>
                        <td>ID:</td>
                        <td>First name:</td>
                        <td>Last name:</td>
                        <td>Status:</td>  
                    </tr>
                    <?php
                        while($row = mysqli_fetch_assoc($c_query)) { 
                    ?>
                    <tr>
                        <td><?php echo $row["id"];?></td>
                        <td><?php echo $row["firstName"];?></td>
                        <td><?php echo $row["lastName"];?></td>
                        <td><?php echo $row["characterStatus"];?></td>
                        <td><button onclick="deleteCharacter(<?php echo $row['id']; ?>)">Delete</button></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
	<div class="pageBottom">Air Combat Group - Mission and Pilot Database - Under Construction - Ver: 0.1 - No.64_Thaine</div>
    </div>
</body>
</html>