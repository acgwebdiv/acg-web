<?php include(dirname(__FILE__).'/header0.php'); ?>
<?php
    $dbx = getDBx();
    $sql = "SELECT COUNT(acgmembers.id) FROM acgmembers ". 
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                    "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
                "ON currentsts2.memberID = acgmembers.id ".
            "WHERE currentsts2.statusID=1";
    $result = mysqli_query($dbx, $sql);
    $nMembers = mysqli_fetch_array($result);
    
    $sql = "SELECT COUNT(id) FROM squadrons WHERE faction='RAF'";
    $result = mysqli_query($dbx, $sql);
    $nSquadrons = mysqli_fetch_array($result);
    
    $sql = "SELECT COUNT(id) FROM squadrons WHERE faction='LW'";
    $result = mysqli_query($dbx, $sql);
    $nStaffeln = mysqli_fetch_array($result);
    
    $sql = "SELECT COUNT(id) FROM reports WHERE accepted=1";
    $result = mysqli_query($dbx, $sql);
    $nReports = mysqli_fetch_array($result);

?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<?php include(dirname(__FILE__).'/mainMenu.php'); ?> 

<p class="form_id">ACG-PAM/100-001.1</p>
<div id="frontPageContainer">
    <h2 style="text-align: center;">Air Combat Group - Mission and Pilot Database</h2>
    <p style="text-align: center;"> <img src="imgsource/ww2paperworkSimulator.png" align="middle"></p>
<p>Welcome to the Air Combat Group Pilot and Mission database - The place 
       where information on ACG pilots, squadrons and missions is stored and presented.</p>
    <p>Flying the Air Combat Group campaign means flying for historical immersion. 
       We do not seek competition or try to win, we try to re-enact and get a feel
       for what it was like to be a fighter pilot during World War II.</p>
    <p>As the pilots back then, ACG-Pilots are required to write an After Action Report 
       after each campaign mission, where they claim victories and describe the 
       course of events of their mission. The reports are written, submitted and
       presented via these pages. Information from the reports is used to create 
       pilot- and squadron profiles and various other statistics.</p>
    <p>There are currently <b><?php echo $nMembers[0]; ?> active pilots</b> flying for ACG.
       The whole group consists of <b><?php echo $nSquadrons[0]; ?> Royal Air Force 
       squadrons</b> and <b><?php echo $nStaffeln[0]; ?> Luftwaffe Staffeln</b>. There
       are <b><?php echo $nReports[0]; ?> after action reports</b> stored in this 
       database, each representing a mission flown by one of our members during a 
       campaign night.</p>
    <p>Have a look around and feel welcome to register in our forums or to join our
       Teamspeak channel to say hello. We are always searching for new pilots of
       any standard or experience, even if they have never flown before.</p>
    
</div>       
<?php include(dirname(__FILE__).'/footer.php');