<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
        exit();
    }
    
    // Accessing mission data from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "mi_id")) {
        
        $edit_id = filter_input(INPUT_GET, "mi_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT name, campaignid, missionstatus FROM missions WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $name = $result["name"];
        $campaignID = $result["campaignid"];
        $missionStatus = $result["missionstatus"];

        $sql = "SELECT EXTRACT(YEAR FROM realdate), EXTRACT(MONTH FROM realdate), " .
                "EXTRACT(DAY FROM realdate) FROM missions WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_array($query);
        $rDateY = $result[0];
        $rDateM = $result[1];
        $rDateD = $result[2];

        $sql = "SELECT EXTRACT(YEAR FROM histdate), EXTRACT(MONTH FROM histdate), " .
                "EXTRACT(DAY FROM histdate), EXTRACT(HOUR FROM histdate), ".
                "EXTRACT(MINUTE FROM histdate) FROM missions WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_array($query);
        $hDateY = $result[0];
        $hDateM = $result[1];
        $hDateD = $result[2];
        $hDateH = $result[3];
        $hDateMM = $result[4];
    }
    
    $dbx = getDBx();
    $sql = "SELECT * FROM campaigns ORDER BY id ASC";
    $c_result = mysqli_query($dbx, $sql);
    
    // Accessing missionStatus data from database for editing.
    $sql = "SELECT * FROM missionstatus";
    $mStatus_query = mysqli_query($dbx, $sql);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/missionAdminLogic.js"></script>
<script type="text/javascript">

function editMissionBriefing(id, faction, type){
    window.location = "editMissionBriefing.php?m_id="+id+"&faction="+faction+"&type="+type;
}

window.onload = function(){
    gebid("changeMissionName").addEventListener("click", changeMissionName, false);
    gebid("changeMissionCampaign").addEventListener("click", changeMissionCampaign, false);
    gebid("changeRDate").addEventListener("click", changeRDate, false);
    gebid("resetMission").addEventListener("click", resetMission, false);
    gebid("changeHDate").addEventListener("click", changeHDate, false);
    gebid("changeMissionStatus").addEventListener("click", changeMissionStatus, false);
    gebid("deleteMission").addEventListener("click", deleteMission, false);
};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-410.1</p>

<h3>Edit mission ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<form id="editMission" onsubmit="return false;" >
    <div class="middlePageStandard">
        <b>Name:</b>
        <input type="text" id="missionName" name="missionName" size="50" maxlength="50" value="<?php echo $name; ?>">
        <button id="changeMissionName">Save Change</button>
        <span id="mNameStatus" ></span>
        <p>The name of the mission.</p>
    </div>
    
    <div class="middlePageStandard">
        <b>Campaign:</b>
        <select id="missionCampaign" name="missionCampaign">
            <?php createSelectOptions($c_result, $campaignID); ?> 
        </select>
        <button id="changeMissionCampaign">Save Change</button>
        <span id="mCampaignstatus" ></span>
        <p>Up to this point there's no further function of campaigns other then 
        structuring missions.</p>
    </div>

    <div class="middlePageStandard">
        <b>Real date (YYYY-MM-DD):</b>
        <input type="text" id="rdateY" name="rdateY" size="4" maxlength="4" value="<?php echo $rDateY; ?>">-
        <input type="text" id="rdateM" name="rdateM" size="2" maxlength="2" value="<?php echo $rDateM; ?>">-
        <input type="text" id="rdateD" name="rdateD" size="2" maxlength="2" value="<?php echo $rDateD; ?>">
        <button id="changeRDate">Save Change</button>
        <button id="resetMission">Reset Mission</button>
        <span id="rDateStatus" ></span>
        <p>The real date is the date at which the mission was performed as part 
        of a ACG campaign. </p>
        <p>Reset the mission by updating the real date and pressing Reset Mission.</p>
    </div>

    <div class="middlePageStandard">
        <b>Historic date (YYYY-MM-DD HH:MM):</b>
        <input type="text" id="hdateY" name="hdateY" size="4" maxlength="4" value="<?php echo $hDateY; ?>">-
        <input type="text" id="hdateM" name="hdateM" size="2" maxlength="2" value="<?php echo $hDateM; ?>">-
        <input type="text" id="hdateD" name="hdateD" size="2" maxlength="2" value="<?php echo $hDateD; ?>">
        <input type="text" id="hdateH" name="hdateH" size="2" maxlength="2" value="<?php echo $hDateH; ?>">:
        <input type="text" id="hdateMM" name="hdateMM" size="2" maxlength="2" value="<?php echo $hDateMM; ?>">
        <button id="changeHDate">Save Change</button>
        <span id="hDateStatus" ></span>
        <p>The historic date is the date at which the mission took place in a 
        historic context.</p>
    </div>

    <div class="middlePageStandard">
        <b>Mission status:</b>
        <select id="missionStatus" name="changeMissionStatus">
            <?php createSelectOptions($mStatus_query, $missionStatus); ?>
        </select>
        <button id="changeMissionStatus">Save Change</button>
        <span id="mStatusStatus" ></span>
        <p>The status of the mission has implications on the accessibility
        of mission briefings and After Action Reports (AAR's).</p>
        <p>The mission statuses are:
        <ul>
            <li><b>Briefing:</b> Before mission start. Briefings can only be seen by 
            the associated faction. AARs can not be created or edited.</li>
            <li><b>Debriefing:</b> After mission has ended. Briefings can only be seen by 
            the associated faction. AARs can be created or edited.</li>
            <li><b>Closed:</b> After debriefing period has ended (usually one week after
            the mission has ended). Breifings can be seen by all factions. AARs can 
            not be created or edited.</li>
        </ul></p>
    </div>

    <div class="middlePageStandard">
        <b>Mission briefings:</b>
        <button class="editButton" onclick="editMissionBriefing(<?php echo $edit_id; ?>, 'RAF', 1)">Edit RAF briefing</button>
        <button class="editButton" onclick="editMissionBriefing(<?php echo $edit_id; ?>, 'LW', 1)">Edit LW briefing</button>
        <button class="editButton" onclick="editMissionBriefing(<?php echo $edit_id; ?>, 'VVS', 1)">Edit VVS briefing</button>
    </div>

    <div class="middlePageStandard">
        <p><button id="deleteMission">Delete</button> Deletes mission from database. 
            Only missions that have not been debriefed and received After-Action-Reports
            (AAR's) should be deleted for consistency.</p>
    </div>
</form>
<?php include(dirname(__FILE__).'/footer.php');