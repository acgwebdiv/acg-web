<?php
    include_once(dirname(__FILE__).'/includes/login_session.php');
    include_once(dirname(__FILE__).'/includes/db_connect.php');
    include_once(dirname(__FILE__)."/includes/functions.php");
    session_start();
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?msg=You have no rights to access this page.");
            exit();
        }
    } else {
        
        header("location: message.php?msg=You have no rights to access this page.");
            exit();
    }
    
    $dbx = getDBx();
    $sql = "SELECT * FROM campaigns ORDER BY id ASC";
    $c_result = mysqli_query($dbx, $sql);    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/missionAdminLogic.js"></script>
<script type="text/javascript">

window.onload = function(){
    gebid("submitBtn").addEventListener("click", addMission, false);
};

</script>   
<?php include './adminMenu.php'; ?>
<p class="form_id">ACG-PAM/400-420.1</p>
<h3>Create new mission</h3>
<form id="newMissionForm" onsubmit="return false;" >
    <div class='middlePageStandard'>
        <b>Campaign:</b>
        <select id="campaign" name="campaign">
            <?php createSelectOptions($c_result, 1); ?> 
        </select>
    </div>

    <div class='middlePageStandard'>
        <b>Real date (YYYY-MM-DD):</b>
        <input type="text" id="rdateY" name="rdateY" size="4" maxlength="4" >-
        <input type="text" id="rdateM" name="rdateM" size="2" maxlength="2" >-
        <input type="text" id="rdateD" name="rdateD" size="2" maxlength="2" >
    </div>

    <div class='middlePageStandard'>
        <b>Historic date (YYYY-MM-DD hh:mm):</b>
        <input type="text" id="hdateY" name="hdateY" size="4" maxlength="4" >-
        <input type="text" id="hdateM" name="hdateM" size="2" maxlength="2" >-
        <input type="text" id="hdateD" name="hdateD" size="2" maxlength="2" >
        <input type="text" id="hdateH" name="hdateH" size="2" maxlength="2" >:
        <input type="text" id="hdateMM" name="hdateMM" size="2" maxlength="2" >
    </div>

    <div class='middlePageStandard'>
        <button id="submitBtn">Create mission</button>
        <span id="submitStatus">&nbsp;</span>
    </div>
    
</form>
<?php include(dirname(__FILE__).'/footer.php');