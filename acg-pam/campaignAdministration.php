<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
//            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    //Get campaigns table for splitting info on several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 10;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT * FROM campaigns ORDER BY id ASC LIMIT $start_from, $n_entries";
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM campaigns";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/campaignAdminLogic.js"></script>
<script type="text/javascript">

function editCampaign(id){
    window.location = "editCampaign.php?c_id="+id;
} 

window.onload = function(){
    gebid("addCampaignButton").addEventListener("click", addCampaign, false);

};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-300.1</p>
<h2>Campaign administration:</h2>
<div>
    <p>Campaigns are a selection of missions. Up to this point there's no further
    function of campaigns other then structuring missions. Campaigns can be changed
    and deleted using the EDIT form.</p>
    <h3>Create new campaign:</h3>
    <form id="addnewCampaignForm" onsubmit="return false;">
        <div class='middlePageStandard'>
        <b>Campaign name:</b>
        <input type="text" id="campaignName" name="campaignName">
        </div>
        
        <div class='middlePageStandard'>
        <button id="addCampaignButton">Create Campaign</button>
        <span id="addCampaignStatus" ></span>
        </div>
    </form>
</div>

<div>
    <h3>ACG-Campaigns:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>Name:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($result)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["name"];?></td>
            <td><button class="editButton" onclick="editCampaign(<?php echo $row['id']; ?>)">EDIT</button></td>
        </tr>
        <?php } ?>
    </table>
</div>

<div>
    <?php createPageSelect($n_pages, $page, 'campaignAdministration.php?'); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');