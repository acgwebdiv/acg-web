<?php
    include(dirname(__FILE__).'/header0.php');

    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');

    
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")){
        $mi_id = filter_input(INPUT_GET, "m_id");  
    }
    if(filter_has_var(INPUT_GET, "f")){
        $mi_faction = filter_input(INPUT_GET, "f");  
    }
    $m_faction = getFaction($_SESSION["userID"], $dbx);
    $sql = "SELECT missions.missionstatus FROM missions ". 
           "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_status = $m_row["missionstatus"];
    
//    echo("mi_status: ".$mi_status." mi_faction: ".$mi_faction." m_faction: ".$m_faction);
    if($mi_status == 1 && $mi_faction !== $m_faction){
        header("location: message.php?m=1");
        exit();
    }
    
    $sql = "SELECT missions.name AS miName, missions.histdate, missions.realdate, ".
       "campaigns.name AS c_name ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_name = $m_row["miName"];
    
    $type = 0;
    $sql = "SELECT * FROM briefings WHERE missionID = $mi_id AND faction = '$mi_faction'";
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>0){
        $result = mysqli_fetch_assoc($query);
        $type = $result["type"];
    }
    
//    $sql = "SELECT missions.name AS m_name, missions.histdate, missions.realdate, ".
//           "missions.missionstatus, missions.briefingraf, missions.briefinglw, ".
//           "campaigns.name AS c_name ".
//           "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
//           "WHERE missions.id = $mi_id";
//    $m_result = mysqli_query($dbx, $sql);
//    $m_row = mysqli_fetch_assoc($m_result);
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>
<?php
    
    switch($type){
        case 0:
?>
<div>
    <h3><?php echo($m_row["miName"]." (".$m_row["histdate"].")"); ?></h3>
    <p>
        <?php echo($m_row["miName"]." (".$m_row["histdate"].")"); ?>, 
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date:
         <?php echo($m_row["realdate"]); ?>.
    </p>
</div>
<?php
            break;
        case 1:
?>
<div>
    <h3>Mission briefing for <?php echo $mi_faction ?> - <?php echo($mi_name." (".$m_row["histdate"].")"); ?>, 
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date: <?php echo($m_row["realdate"]); ?>.</h3>
    <p>
        <?php echo(parseImageTags(nl2br($result["text1"])));?>
    </p>
</div>
<?php   
            break;
        case 2:
            
            if($m_faction === "RAF") {
                $headingTXT = "Royal Air Force <br>".
                              "11.Group Mission Directive <br>";
            } else if($m_faction === "LW") {
                $headingTXT = "Oberkommando der Luftwaffe <br>".
                              "JG26 and ZG76 Directive <br>";
            }

?>
<div>
    <p>
        <b><?php echo $headingTXT;?></b>
        <b>Classified</b>
    </p>
    <p>
        <b>Date:</b> <?php echo(nl2br($m_row["histdate"]));?><br>
        <b>Operation:</b> <?php echo(nl2br($m_row["miName"]));?><br>
        <b>Target:</b> <?php echo(nl2br($result["text1"])); ?><br>
        <b>Secondary target/Diversions:</b> <br>
        <?php echo(nl2br($result["text2"])); ?><br>
        <b>Force to target/s:</b> <br>
        <?php echo(nl2br($result["text3"])); ?>
    </p>
    <p>
        <b>Weather report:</b><br>
        <?php echo(nl2br($result["text4"])); ?>
    </p>
    <p>
        <b>Engine start time:</b> <?php echo(nl2br($result["text5"])); ?><br>
        <b>RV:</b> <?php echo(nl2br($result["text6"])); ?><br>
        <b>H-Hour:</b> <?php echo(nl2br($result["text7"])); ?>
    </p>
    <p>
        <b>Route and proceedings:</b><br>
        <?php echo(nl2br($result["text8"])); ?><br>
        <b>Rules of engagement:</b><br>
        <?php echo(nl2br($result["text9"])); ?><br>
        <b>Expected resistance:</b><br>
        <?php echo(nl2br($result["text10"])); ?><br>
        <b>Divert:</b> <?php echo(nl2br($result["text11"])); ?>
    </p>
    <p>
        <?php echo(nl2br(parseImageTags($result["text12"]))); ?>
    </p>
</div>
<?php   
            break;
    }
?>
<?php include(dirname(__FILE__).'/footer.php');