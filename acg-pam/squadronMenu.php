<script type='text/javascript' src="jscript/flexcroll.js"></script>
</head>
<body id="squadronMenuBody">
    <div class="mainContainer">
        <div class="pageTop">
            <?php
            include(dirname(__FILE__).'/primMenu.php'); 
            include(dirname(__FILE__).'/userMenu.php');
            ?>
            <div class="secMenu">
                <ul>
                    <li><a href="squadronIndex.php">Units Overview</a></li>
                <?php
                    if(filter_has_var(INPUT_GET, "sqn")) {
                        $sqn = filter_input(INPUT_GET, "sqn");
                ?>
                    <li><a href="squadronNotifications.php?sqn=<?php echo $sqn; ?>">Notifications</a></li>
                    <li><a href="squadronRanks.php?sqn=<?php echo $sqn; ?>">Ranks</a></li>
                    <li><a href="squadronAttendance.php?sqn=<?php echo $sqn; ?>">Attendance</a></li>
                    <li><a href="squadronRoster.php?sqn=<?php echo $sqn; ?>">Roster</a></li>
                <?php
                    }
                ?>    
                </ul>
                <?php
                    
                    $link = "squadronNotifications.php?";                  
                    if(filter_has_var(INPUT_GET, "sqn")) {
                        $link = filter_input(INPUT_SERVER, 'PHP_SELF')."?";
                        
                    }
                        
                    $sql = "SELECT squadrons.id, squadrons.name, squadrons.faction FROM squadrons ".
                           "ORDER BY squadrons.faction DESC, squadrons.rank ASC";
                    $smenu_result = mysqli_query($dbx, $sql); 
                    include './squadronSubMenu.php';
                ?>
            </div>
        </div>
        <div class="pageMiddle">