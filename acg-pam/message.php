<?php    
    include(dirname(__FILE__).'/header0.php');
?>

<?php include(dirname(__FILE__).'/header1.php'); ?>

<?php include(dirname(__FILE__).'/mainMenu.php'); ?>
        
<?php
    if(filter_has_var(INPUT_GET, "m")){
        $m = filter_input(INPUT_GET, "m");
            
        switch ($m){
            case 1:
                $username = $_SESSION['username'];
                echo("<div class='letter'><p>Dear $username,</p>"
                    . "<p>We are sorry to inform you that your clearance "
                    . "status does not qualify to receive the requested pages.</p></div>");
                break;
            case 2:
                $username = $_SESSION['username'];
                echo("<div class='letter'><p>Dear $username,</p>"
                    . "<p>You need to be assigned to a squadron/Staffel to be "
                    . "able to write an AAR.</p>"
                    . "<p>Please contact your assigned adjutant or the campaign"
                    . " administration.</p></div>");
                break;
            case 3:
                $username = $_SESSION['username'];
                echo("<div class='letter'><p>Dear $username,</p>"
                    . "<p>There was a problem during the automatic character "
                    . "generation.</p>"
                    . "<p>Please contact your assigned adjutant or the campaign"
                    . " administration.</p></div>");
                break;
            default :
            case 4:
                $username = $_SESSION['username'];
                echo("<div class='letter'><p>Dear $username,</p>"
                    . "<p>There was a problem during the load of the new character "
                    . "query.</p>"
                    . "<p>Please contact your assigned adjutant or the campaign"
                    . " administration.</p></div>");
                break;
            default :
        }
    }
?>

<?php include(dirname(__FILE__).'/footer.php');