<?php
    include(dirname(__FILE__).'/header0.php');
    
    $user_id = filter_input(INPUT_GET, "m_id");
    $dbx = getDBx();
    $sql = "SELECT acgmembers.callsign, memberstatus.status AS status ".
           "FROM acgmembers ".
           "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
            "ON currentsts2.memberID = acgmembers.id ".
            "LEFT JOIN memberstatus ON currentsts2.statusID = memberstatus.id ".
           "WHERE acgmembers.id = $user_id";
    $mresult = mysqli_query($dbx, $sql);
    $mrow = mysqli_fetch_assoc($mresult);
    // echo $sql;

    // Number of RAF sorties
    $sql = "SELECT COUNT(*) FROM reports ".
          "RIGHT JOIN careercharacters ON reports.authorid = careercharacters.id ".
          "RIGHT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
          "WHERE acgmembers.id=$user_id AND reports.accepted=1 AND reports.type = 1";
    $result = mysqli_query($dbx, $sql);
    $nrafreportsrow = mysqli_fetch_array($result);
    $sql = "SELECT COUNT(*) FROM reports ".
          "RIGHT JOIN careercharacters ON reports.authorid = careercharacters.id ".
          "RIGHT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
          "WHERE acgmembers.id=$user_id AND reports.accepted=1 AND reports.type = 2";
    $result = mysqli_query($dbx, $sql);
    $nlwreportsrow = mysqli_fetch_array($result);
    $sql = "SELECT COUNT(*) FROM reports ".
          "RIGHT JOIN careercharacters ON reports.authorid = careercharacters.id ".
          "RIGHT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
          "WHERE acgmembers.id=$user_id AND reports.accepted=1 AND reports.type = 3";
    $result = mysqli_query($dbx, $sql);
    $nvvsreportsrow = mysqli_fetch_array($result);
    
    $nreports = $nrafreportsrow[0] + $nlwreportsrow[0] + $nvvsreportsrow[0];
    
    $sql = "SELECT pilotstatus.status, COUNT(reports.id) FROM reports ".
           "RIGHT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "RIGHT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN pilotstatus ON pilotstatus.id = reports.pilotstatus ".
           "WHERE acgmembers.id=$user_id AND reports.accepted=1 ". 
           "GROUP BY pilotstatus.status ORDER BY pilotstatus.id ASC";
//    echo $sql;
    $npilotstatusresult = mysqli_query($dbx, $sql);    
    
    $sql = "SELECT aeroplanestatus.status, COUNT(reports.id) FROM reports ".
           "RIGHT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "RIGHT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN aeroplanestatus ON aeroplanestatus.id = reports.aeroplanestatus ".
           "WHERE acgmembers.id=$user_id AND reports.accepted=1 ".
           "GROUP BY aeroplanestatus.status ORDER BY aeroplanestatus.id ASC";
    $naeroplanestatusresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT claimstatusraf.status, SUM(1-claimsraf.shared*0.5) AS points ".
           "FROM claimsraf LEFT JOIN reports ON claimsraf.reportid = reports.id ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "LEFT JOIN claimstatusraf ON claimstatusraf.id = claimsraf.enemystatus ".
           "WHERE acgmembers.id = $user_id AND reports.accepted=1 AND claimsraf.accepted = 1 ".
           "GROUP BY claimsraf.enemystatus";
    $rafvictoriesresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(claimslw.id), claimslw.confirmed ".
           "FROM claimslw LEFT JOIN reports ON claimslw.reportid = reports.id ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id = $user_id AND reports.accepted=1 AND claimslw.accepted = 1 ".
           "GROUP BY claimslw.confirmed";
    $lwvictoriesresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(claimsvvs.id), claimsvvs.confirmed ".
           "FROM claimsvvs LEFT JOIN reports ON claimsvvs.reportid = reports.id ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id = $user_id AND reports.accepted=1 AND claimsvvs.accepted = 1 AND claimsvvs.groupClaim = 0 ".
           "GROUP BY claimsvvs.confirmed";
    $vvsvictoriesresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(vgc.id), vgc.confirmed ".
           "FROM reports ".
           "LEFT JOIN vvs_group_claims vgc ON (reports.missionID, reports.squadronID) = (vgc.missionID, vgc.squadronID) ".
           "LEFT JOIN careercharacters ON reports.authorID = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
           "WHERE acgmembers.id = $user_id AND reports.accepted = 1 AND vgc.accepted = 1 AND vgc.groupClaim = 1 ".
           "GROUP BY vgc.confirmed";
    $vvsgvictoriesresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT groundtargets.name, SUM(claimsground.amount) ".
           "FROM claimsground LEFT JOIN reports ON claimsground.reportid = reports.id ".
           "LEFT JOIN groundtargets ON claimsground.object = groundtargets.id ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id = $user_id AND reports.accepted=1 AND claimsground.accepted = 1 ".
           "GROUP BY claimsground.object";
    $groundvictoriesresult = mysqli_query($dbx, $sql);

    $sql = "SELECT memberstatuslog.id, memberstatuslog.statusID, memberstatus.status, memberstatuslog.date, memberstatuslog.comment, ".
           "UNIX_TIMESTAMP(memberstatuslog.date) AS mspdate ".
           "FROM memberstatuslog LEFT JOIN memberstatus ON memberstatuslog.statusID = memberstatus.id ".
           "WHERE memberID = $user_id ORDER BY mspdate DESC";
    $su_query = mysqli_query($dbx, $sql);
    $su_array = mysqli_fetch_all($su_query, MYSQLI_BOTH);
    
    $sql = "SELECT squadrons.name, transfers.transferdate, UNIX_TIMESTAMP(transferdate) AS tstdate ".
            "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
            "WHERE memberid = $user_id ORDER BY tstdate DESC";
    $tresult = mysqli_query($dbx, $sql);
    
    $sql = "SELECT squadrons.faction, UNIX_TIMESTAMP(transferdate) AS tstdate ".
            "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
            "WHERE memberid = $user_id ORDER BY tstdate DESC LIMIT 1";
    $fresult = mysqli_query($dbx, $sql);
    $frow = mysqli_fetch_assoc($fresult);
    $faction = $frow["faction"];
    
    $sql = "SELECT ranks.name, ranks.value, promotions.date, promotions.comment, UNIX_TIMESTAMP(promotions.date) AS tspdate, ".
           "ranks.image ". 
           "FROM promotions LEFT JOIN ranks ON (promotions.value, '$faction') = (ranks.value, ranks.faction) ".
           "WHERE memberid = $user_id ORDER BY tspdate DESC";
    $presult = mysqli_query($dbx, $sql);
    $parray = mysqli_fetch_all($presult, MYSQLI_BOTH);
    
    //Check if member has received his wings
    if($faction == "RAF"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "RIGHT JOIN careercharacters ON decorations.characterID = careercharacters.id ".
               "WHERE careercharacters.personifiedBy = $user_id AND awards.abreviation = 'AB'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            if($parray[0]["value"]<8){
                $wingsImage = $wingsrow["image"]."Fabric.png";
            } else {
                $wingsImage = $wingsrow["image"]."Brass.png";
            }
        } else {
            $wingsImage = "";
        }
    } else if($faction == "LW"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "RIGHT JOIN careercharacters ON decorations.characterID = careercharacters.id ".
               "WHERE careercharacters.personifiedBy = $user_id AND awards.abreviation = 'FBAgd'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            $wingsImage = $wingsrow["image"];
        } else {
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "RIGHT JOIN careercharacters ON decorations.characterID = careercharacters.id ".
               "WHERE careercharacters.personifiedBy = $user_id AND awards.abreviation = 'FBA'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                $wingsImage = $wingsrow["image"];
            } else {
                $wingsImage = "";
            }
        }
    }   else if($faction == "VVS"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN decorations ON awards.id = decorations.awardID ".
               "RIGHT JOIN careercharacters ON decorations.characterID = careercharacters.id ".
               "WHERE careercharacters.personifiedBy = $user_id AND awards.abreviation = 'AB_VVS'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            $wingsImage = $wingsrow["image"];
        } 
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-201.1</p>
<h3>Member profile:</h3>
<div>
    <p>This page shows the profile for <?php echo $mrow["callsign"];?>. The displayed
    information is based on submitted and approved After Action Reports.</p>
    
    <h3>General information:</h3>
    
    <div class="uniform">
        <?php if($faction == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/RAFUniform.png">
        <?php } else if($faction == "LW"){ ?>
        <img src="imgsource/LW-ranks/LWUniform.png">
        <?php } else if($faction == "VVS"){ ?>
        <img src="imgsource/VVS-ranks/VVSUniform.png">
        <?php } ?>
    </div>
    
    <div class="uniformRank">
        <?php if($faction == "RAF"){ ?>
        <img src="imgsource/RAF-ranks/<?php echo $parray[0]["image"]; ?>">
        <?php } else if($faction == "LW"){ ?>
        <img src="imgsource/LW-ranks/<?php echo $parray[0]["image"]; ?>">
        <?php } else if($faction == "VVS"){ ?>
        <img src="imgsource/VVS-ranks/<?php echo $parray[0]["image"]; ?>">
        <?php } ?>
    </div>
    
    <div class="uniformWings">
        <?php if($wingsImage != ""){ ?>
        <img src="imgsource/medals-small/<?php echo $wingsImage; ?>">
        <?php } ?>
    </div>
    
    <div class="memberGeneralInformation">
        <table>
            <tr><td>Name:</td><td><?php echo $mrow["callsign"];?></td></tr>
            <tr><td>Status:</td><td><?php echo $mrow["status"];?></td></tr>
            <tr><td>Length of service:</td><td><?php echo lengthOfService($su_array);?></td></tr> 
        </table>
    </div>
</div>
<div>
    <hr>
    <img src="includes/awardStripesFactory.php?m_id_big=<?php echo $user_id; ?>&f='RAF'" alt="Decorations RAF"><br>
    <img src="includes/awardStripesFactory.php?m_id_big=<?php echo $user_id; ?>&f='LW'" alt="Decorations LW"><br>
    <img src="includes/awardStripesFactory.php?m_id_big=<?php echo $user_id; ?>&f='VVS'" alt="Decorations VVS"><br>

    <h3>Sorties overview:</h3>
    <table>
        <tr><td>Sorties:</td><td><?php echo $nreports;?></td></tr>
    </table>
    <?php if($nreports >= 1) { ?>
    <div style="display: table-cell;">
        <table>
            <tr><td>Pilot status</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($npstatusrow = mysqli_fetch_row($npilotstatusresult)){ ?>
            <tr><td><?php echo $npstatusrow[0]; ?>:</td><td><?php echo $npstatusrow[1]; ?></td></tr>
            <?php } ?> 
        </table>    
    </div>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Aircraft status</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($nastatusrow = mysqli_fetch_row($naeroplanestatusresult)){?>
            <tr><td><?php echo $nastatusrow[0]; ?>:</td><td><?php echo $nastatusrow[1]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
</div>
<div>
    <?php if($nrafreportsrow[0] > 0) { ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Victories RAF</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($rafvictoriesrow = mysqli_fetch_row($rafvictoriesresult)){?>
            <tr><td><?php echo $rafvictoriesrow[0]; ?>:</td><td><?php echo $rafvictoriesrow[1]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
    <?php } ?>
    <?php if($nlwreportsrow[0] > 0) { ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Victories LW</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($lwvictoriesrow = mysqli_fetch_row($lwvictoriesresult)){
                $lwclaimstatus = ($lwvictoriesrow[1]==1)?"confirmed":"unconfirmed"; ?>
            <tr><td><?php echo $lwclaimstatus; ?>:</td><td><?php echo $lwvictoriesrow[0]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
    <?php } ?>
    <?php if($nvvsreportsrow[0] > 0) { ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Victories VVS</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php 
            if(mysqli_num_rows($vvsvictoriesresult)>0){
            ?>
            <tr><td>Personal:</td></tr>
            <?php
                while($vvsvictoriesrow = mysqli_fetch_row($vvsvictoriesresult)){
                    $vvsclaimstatus = ($vvsvictoriesrow[1]==1)?"confirmed":"unconfirmed"; 
            ?>
                    <tr><td><?php echo("&nbsp".$vvsclaimstatus); ?>:</td><td><?php echo $vvsvictoriesrow[0]; ?></td></tr>
            <?php 
                } 
            }
            if(mysqli_num_rows($vvsgvictoriesresult)>0){
            ?>
            <tr><td>Group:</td></tr>
            <?php
                while($vvsvictoriesrow = mysqli_fetch_row($vvsgvictoriesresult)){
                    $vvsclaimstatus = ($vvsvictoriesrow[1]==1)?"confirmed":"unconfirmed";
            ?>
                    <tr><td><?php echo("&nbsp".$vvsclaimstatus); ?>:</td><td><?php echo $vvsvictoriesrow[0]; ?></td></tr>
            <?php 
                } 
            }
            ?>
        </table>    
    </div>
    <?php } ?>
    <?php 
    if(mysqli_num_rows($groundvictoriesresult) > 0){ ?>
    <div style="display: table-cell; border-left: solid 1px black;">
        <table>    
            <tr><td>Ground victories</td></tr>
            <tr><td colspan="2"><hr></td></tr>
            <?php while($groundvictoriesrow = mysqli_fetch_row($groundvictoriesresult)){ ?>
            <tr><td><?php echo $groundvictoriesrow[0]; ?>:</td><td><?php echo $groundvictoriesrow[1]; ?></td></tr>
            <?php } ?>
        </table>    
    </div>
    <?php 
            }
        }
    ?>
</div>
<div class="middlePageStandard">
    <h3>Status updates:</h3>
    <table>
        <tr>
            <th>Status:</th>
            <th>Date:</th>
        </tr>
        <?php
            foreach ($su_array as $row) {
        ?>
        <tr>
            <td><?php echo $row["status"];?></td>
            <td><?php echo $row["date"];?></td>
        </tr>
        <?php } ?>
    </table>
</div>  
<div>
    <hr>
    <h3>Transfer overview:</h3>
    <table>
        <tr>
            <th>Date:</th>
            <th>To squadron/Staffel:</th>                      
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($tresult)) { 
        ?>
        <tr>
            <td><?php echo $row["transferdate"];?></td>
            <td><?php echo $row["name"];?></td>
        </tr>
        <?php } ?>
    </table>
</div>
<div>
    <hr>
    <h3>Promotion/Demotion overview:</h3>
    <table>
        <tr>
            <th>Date:</th>
            <th>To rank:</th>
            <th>Comment:</th> 
        </tr>
        <?php
//            while($row = mysqli_fetch_assoc($presult)) 
            for($n = 0; $n < count ($parray);  $n++){ 
        ?>
        <tr>
            <td><?php echo $parray[$n]["date"];?></td>
            <td><?php echo $parray[$n]["name"];?></td>
            <td><?php echo $parray[$n]["comment"];?></td>
        </tr>
        <?php } ?>
    </table>
</div>
<?php include(dirname(__FILE__).'/footer.php');

