<?php
    include(dirname(__FILE__).'/header0.php');
    
    $dbx = getDBx();
    $sql = "SELECT squadrons.id, squadrons.name, squadrons.code, squadrons.faction FROM squadrons ".
                           "ORDER BY squadrons.faction DESC, squadrons.rank ASC";
    $result = mysqli_query($dbx, $sql);
    $squadronArray = array();
    while($row = mysqli_fetch_assoc($result)) {
        $squadronId = $row["id"];
        
         $sql = "SELECT COUNT(acgmembers.id)  ".
           "FROM acgmembers ".
            "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
            "ON currentsts2.memberID = acgmembers.id ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
           "WHERE statusID = 1 AND currentsqu2.squadronid = $squadronId";
        $sresult = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($sresult);
        $apilots = $srow[0];
        
         $sql = "SELECT COUNT(acgmembers.id)  ".
           "FROM acgmembers ".
            "LEFT JOIN ".
	            "(SELECT memberstatuslog.memberID, memberstatuslog.statusID, memberstatuslog.date FROM memberstatuslog ".
    	        "JOIN ".
     		        "(SELECT memberstatuslog.memberID, MAX(UNIX_TIMESTAMP(memberstatuslog.date)) AS sdate ".
                    "FROM memberstatuslog GROUP BY memberID) AS currentsts ".
                "ON (currentsts.memberID, currentsts.sdate) = ".
                "(memberstatuslog.memberID, UNIX_TIMESTAMP(memberstatuslog.date))) AS currentsts2 ".
            "ON currentsts2.memberID = acgmembers.id ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
           "WHERE statusID = 3 AND currentsqu2.squadronid = $squadronId";
        //   echo $sql;
        $sresult = mysqli_query($dbx, $sql);
        $srow = mysqli_fetch_array($sresult);
        $rrpilots = $srow[0];
        
        $squadronId = $row["id"];
        $squadronArray[$squadronId]["activePilots"] = $apilots;
        $squadronArray[$squadronId]["rrPilots"] = $rrpilots;
        $squadronArray[$squadronId]["squadronName"] = $row["name"];
        $squadronArray[$squadronId]["squadronCode"] = $row["code"];
        $squadronArray[$squadronId]["squadronFaction"] = $row["faction"];
    }

?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/squadronMenu.php'); ?>
<p class="form_id">ACG-PAM/500-000.1</p>
<h3>ACG-Squadrons/Staffeln:</h3>
<div>
    <p>These pages list all ACG squadrons/Staffeln with their corresponding names, 
    squadron codes and number of active pilots.</p>
</div>
<div>
    <table>
        <thead>
            <tr>
                <th>Name:</th>
                <th>Code:</th>
                <th>Faction:</th>
                <th>Active Pilots:</th>
                <th>Pilots on leave:</th>
                
            </tr>
        </thead>
        <?php
            foreach ($squadronArray as $sqnID => $sqnDataArray) {
        ?>
        <tbody>
            <tr>
                <td width="50"><?php echo $sqnDataArray["squadronName"];?></td>
                <td width="50" align="center"><?php echo $sqnDataArray["squadronCode"];?></td>
                <td width="50" align="center"><?php echo $sqnDataArray["squadronFaction"];?></td>
                <td width="150" align="center"><?php echo $sqnDataArray["activePilots"];?></td>
                <td width="150" align="center"><?php echo $sqnDataArray["rrPilots"];?></td>
            </tr>    
        </tbody>
        
        <?php } ?>
    </table>
</div>
<?php include(dirname(__FILE__).'/footer.php');