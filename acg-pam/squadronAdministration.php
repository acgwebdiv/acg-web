<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Setting up indices for spreading conten over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 10;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT * FROM squadrons ORDER BY id ASC LIMIT $start_from, $n_entries";
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM squadrons";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
    
    // Access al squadron info from database for form input.
    $sql = "SELECT id, name FROM squadrons ORDER BY id ASC";
    $s_result = mysqli_query($dbx, $sql);
    $s_id_array = mysqli_fetch_all($s_result);
    $base_unit_array = array();
    $base_unit_array[0][0] = 0;
    $base_unit_array[0][1] = "None";
    for($n = 1; $n < count($s_id_array); $n++){
        $base_unit_array[$n][0] = $s_id_array[$n-1][0];
        $base_unit_array[$n][1] = $s_id_array[$n-1][0]."-".$s_id_array[$n-1][1];
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/squadronAdminLogic.js"></script>
<script type="text/javascript">

function editSquadron(id){
    window.location = "editSquadron.php?s_id="+id;
} 

window.onload = function(){
    gebid("addSquadronButton").addEventListener("click", addSquadron, false);

};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-200.1</p>
<h2>Squadron/Staffel administration:</h2>
<div>
    <p>Create/edit or delete squadrons/Staffeln. Each squadron/Staffel needs a
    name and faction. Squadron codes can be assigned to RAF squadrons. To delete
    a squadron/Staffel use the EDIT form. Make sure to delete all transfers to
    the deleted squadron/Staffel in the member section.</p>
    
    <h3>Create new squadron/Staffel:</h3>
    <form id="addnewSquadronForm" onsubmit="return false;">
        <div class="middlePageStandard">
            <b>Squadron/Staffel name:</b>
            <input type="text" id="squadronName" name="squadronName">
        </div>
        <div class="middlePageStandard">
            <b>Squadron/Staffel code:</b>
        <input type="text" id="squadronCode" name="squadronCode">
            
        </div>
        <div class="middlePageStandard">
            <b>Squadron/Staffel faction:</b>
            <select id="squadronFaction" name="squadronFaction">
                <option value="RAF">Royal Air Force</option>
                <option value="LW">Luftwaffe</option>
                <option value="VVS">Voyenno-Vozdushnye Sily</option>
             </select>    
        </div>
        
        <div class="middlePageStandard">
            <b>ACG-unit (base-unit):</b>
            <?php createSelectForm("acg_unit", $base_unit_array, 0) ?>
        </div>
        
        <div class="middlePageStandard">
            <button id="addSquadronButton">Create Squadron/Staffel</button>
            <span id="addSquadronStatus" ></span>
        </div>
    </form>
</div>

<div>
    <h3>ACG-Squadrons/Staffeln:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>Name:</th>
            <th>Code:</th>
            <th>Faction:</th>
            <th>ACG-unit:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($result)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["name"];?></td>
            <td><?php echo $row["code"];?></td>
            <td><?php echo $row["faction"];?></td>
            <td><?php echo $row["acg_unit"]==0?"":$row["acg_unit"];?></td>
            <td><button onclick="editSquadron(<?php echo $row['id']; ?>)">EDIT</button></td>
        </tr>
        <?php } ?>
    </table>
</div>

<div  class="pageSelect">
    <?php createPageSelect($n_pages, $page, 'squadronAdministration.php?'); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');