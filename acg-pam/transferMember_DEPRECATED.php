<?php
    include_once(dirname(__FILE__).'/includes/login_session.php');
    include_once(dirname(__FILE__).'/includes/db_connect.php');
    session_start();
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?msg=You have no rights to access this page.");
            exit();
        }
    } else {
        
        header("location: message.php?msg=You have no rights to access this page.");
            exit();
    }
    
    // Accessing squadron and transfer data from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")) {
        
        $edit_id = filter_input(INPUT_GET, "m_id");
        $edit_name = filter_input(INPUT_GET, "m_name");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT transfers.id, squadrons.name, transfers.transferDate, UNIX_TIMESTAMP(transferDate) AS tsTDate ".
                "FROM transfers LEFT JOIN squadrons ON transfers.squadronID = squadrons.id ".
                "WHERE memberID = $edit_id ORDER BY tsTDate DESC";
        $query = mysqli_query($dbx, $sql);
    }
    
    $sql = "SELECT * FROM squadrons";
    $s_result = mysqli_query($dbx, $sql);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=encoding">
    <title>ACG Pilot- and Mission Database</title>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
    <script src="jscript/main.js"></script>
    <script src="jscript/ajax.js"></script>
    <script src="jscript/transferMemberLogic.js"></script>
    <script type="text/javascript">
       
    window.onload = function(){
       
        gebid("transferBtn").addEventListener("click", transferMember, false);
        gebid("deleteMember").addEventListener("click", deleteTransfer(id), false);
    };   
    </script>
</head>
<body>
    <div id="mainContainer">
	<div class="pageTop">
            <?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
        </div> 
        <div class="pageMiddle">
            <h3>Transfer member <?php echo $edit_name; ?> ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
            <form id="transferMember" onsubmit="return false;" >
                <div>
                    <select id="squadron" name="squadron">
                        <?php
                            while($row = mysqli_fetch_assoc($s_result)){
                        ?>
                        <option value="<?php echo $row["id"];?>"><?php echo $row["faction"]." - ".$row["name"];?></option>
                        <?php } ?>   
                    </select>
                </div>
                
                <div>
                    Transfer date (YYYY-MM-DD). If left empty the current date will be set:<br>
                    <input type="text" id="tdateY" name="tdateY" size="4" maxlength="4">-
                    <input type="text" id="tdateM" name="tdateM" size="2" maxlength="2">-
                    <input type="text" id="tdateD" name="tdateD" size="2" maxlength="2">
                </div>
                
                <div id="transferStatus">&nbsp;</div>
                <button id="transferBtn">Transfer member</button>
            </form>
            
            <div>
                <h3>Transfers:</h3>
                <table>
                    <tr>
                        <td>ID:</td>
                        <td>To squadron/Staffel:</td>
                        <td>Transfer date:</td>                        
                    </tr>
                    <?php
                        while($row = mysqli_fetch_assoc($query)) { 
                    ?>
                    <tr>
                        <td><?php echo $row["id"];?></td>
                        <td><?php echo $row["name"];?></td>
                        <td><?php echo $row["transferDate"];?></td>
                        <td><button onclick="deleteTransfer(<?php echo $row['id']; ?>)">Delete</button></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
        
	<div class="pageBottom">Air Combat Group - Mission and Pilot Database - Under Construction - Ver: 0.1 - No.64_Thaine</div>
    </div>
</body>
</html>