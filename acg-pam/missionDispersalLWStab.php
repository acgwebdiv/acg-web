<?php

////Get all pilots
//$sql = "SELECT attendance.id, attendance.memberID, acgmembers.callsign, attendance.positionID, ".
//       "attendance.flightsraf, attendance.sectionraf, attendance.sectionpos, ".
//       "attendance.swarmslw, attendance.swarmpos ".
//       "FROM attendance ".
//       "LEFT JOIN acgmembers ON attendance.memberID = acgmembers.id ". 
//       "WHERE attendance.missionID = $mi_id AND attendance.squadronID = $sqn ";
//$apresult = mysqli_query($dbx, $sql);
//$roster_array = array(); //Array linking position in dispersal to memberID
//$memberIDArray = array(); //Array with memberID and callsign
//$memberIDArray[] = array(0, "");
//$memberArray = array(); //Array linking memberID to callsign
//while($row = mysqli_fetch_assoc($apresult)) {
//    $roster_array[$row["positionID"]] = $row["memberID"];
//    $memberIDArray[] = array($row["memberID"], $row["callsign"]);
//    $memberArray[$row["memberID"]] = $row["callsign"];
//}


function memberSelectForm($position, $roster_array, $memberIDArray){
    if(array_key_exists($position, $roster_array)){
        $memberID = $roster_array[$position];
    } else {
        $memberID = 0;
    }
    createSelectFormWithName("dispersal", $position, $memberIDArray, $memberID);
}

function getSchwarmInfo($position){
    
    
    switch ($position) {
        case 1:
            return array(1, 1, 'Kommodore');
        case 2:
            return array(1, 2, 'Adjutant');
        case 3:
            return array(1, 3, 'Operations Offizier');
        case 4:
            return array(1, 4, 'Technischer Offizier');
        }
}
?>
<?php 
    if($isAdmin){
?>
<div id="dispersalSetupDiv" style="display: block">
    <form id="rosterAircraft" onsubmit="return false;" >
        <table>
            <thead>
                <tr>
                    <th>Role:</th>
                    <th>Pilot:</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Kommodore</td>
                    <td><?php 
                            $position = 1;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Adjutant</td>
                    <td><?php 
                            $position = 2;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Operations Offizier</td>
                    <td><?php 
                            $position = 3;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td>Technischer Offizier</td>
                    <td><?php 
                            $position = 4;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
            </tbody>
        </table>
        <button id="saveDispersalBtnLW" >Save changes</button>
        <span id="saveDispersalStatus" ></span>        
    </form>
    <hr>
</div>
<?php 
    }
?>
<div>
    <table>
        <thead>
            <tr>
                <th>Role:</th>
                <th>Pilot:</th>
            </tr>
        </thead>
        <tbody>
<?php $currentSchwarm = 0; 
foreach ($roster_array as $positionID => $memberID) {
    
    if($memberArray[$memberID]["dispersalStatus"]==1){
        
        $schwarmInfo = getSchwarmInfo($positionID);
        
?>
            <tr>
                <td><?php echo(htmlentities($schwarmInfo[2])); ?></td>
                <td witdth="20"><?php echo($memberArray[$memberID]["rank"]); ?></td>
                <td><?php echo($memberArray[$memberID]["name"]); ?></td>
            </tr>
    
<?php
        }
    }
?>
        </tbody>
    </table>
</div>

