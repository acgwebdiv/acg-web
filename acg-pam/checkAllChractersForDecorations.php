<?php

include_once(dirname(__FILE__).'/includes/db_connect.php');

$dbx = getDBx();
$sql = "SELECT careercharacters.id FROM careercharacters WHERE id >= 400 AND id < 600";
$result = mysqli_query($dbx, $sql);
$cArray = mysqli_fetch_all($result);
//echo var_dump($cArray);
foreach($cArray as $row){
    
    $c_id = $row[0];
    $dbx = getDBx();
    checkDecorations($c_id, $dbx);
}


function checkDecorations($characterID, $dbx){
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    $faction = getCharacterFaction($characterID, $dbx);
    $sql = "SELECT MAX(reports.missionID) AS mxmid, MIN(reports.missionID) AS mnmid ".
           "FROM reports WHERE reports.authorid = $characterID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $mxmid = $row["mxmid"];
    $mnmid = $row["mnmid"];
    
   
    echo("<br>");
    echo("<br>");
    if($faction == "RAF"){
        
        echo("CHARACTER RAF: ".$characterID.": <br>");
        for($missionID = $mnmid; $missionID <= $mxmid; $missionID++){
            checkRAFDecorations($characterID, $missionID);
        }
    } else if($faction == "LW"){
        echo("CHARACTER LW: ".$characterID.": <br>");
        for($missionID = $mnmid; $missionID <= $mxmid; $missionID++){
            checkLWDecorations($characterID, $missionID);
        }
    }
    
    
}

function checkRAFDecorations($characterID, $missionID){
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    
    
    
    $dbx = getDBx();
    //Get date of mission
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $missionDate = $row["realDate"];


    //Get all decorations of the character
    $sql = "SELECT awards.abreviation, decorations.id, decorations.awarded FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "WHERE decorations.characterID = $characterID";
    $result = mysqli_query($dbx, $sql);
    $decorationsArray = array();
    $awardedArray = array();
    while($row = mysqli_fetch_array($result)){
        $decorationsArray[] = $row[0];
        $awardedArray[$row[0]]["id"] = $row[1];
        $awardedArray[$row[0]]["awarded"] = $row[2];
    }
    
//    echo var_dump($awardedArray);
    
    //Get all awards
    $sql = "SELECT id, abreviation FROM awards ".
           "WHERE faction = 'RAF'";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_array($result)){
        $awardArray["$row[1]"] = $row[0];
    }
    
    //Get stats for character
    $sql = "SELECT COUNT(reports.id) AS sorties, SUM(reports.aeroplaneStatus = 3) AS aeroLST ".
           "FROM reports LEFT JOIN missions ON reports.missionID = missions.id ".
           "WHERE reports.authorID = $characterID AND reports.accepted = 1 AND missions.id <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $sorties = $row["sorties"];
    $aeroLost = $row["aeroLST"];
    
    $sql = "SELECT careercharacters.personifiedBy FROM careercharacters ".
           "WHERE careercharacters.id = $characterID";
    $querry = mysqli_query($dbx, $sql);
    $result = mysqli_fetch_assoc($querry);
    $memberID = $result["personifiedBy"];
    $rankValue = getRankValueAtMission($memberID, $missionID, $dbx);
    $rank = ($rankValue < 8)?"NCO":"CO";
    
    $sql = "SELECT SUM(destrtable.pointsdestr) AS destr FROM reports ".
           "LEFT JOIN (SELECT claimsraf.reportID, (1-claimsraf.shared*0.5) AS pointsdestr ".
           "FROM claimsraf WHERE enemystatus = 1) AS destrtable ON destrtable.reportID = reports.id ".
           "WHERE accepted = 1 AND authorID = $characterID AND reports.missionID <= $missionID";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $destroyed = $row["destr"];
    
    $sql = "SELECT SUM(probtable.pointsprob) AS prob FROM reports ".
           "LEFT JOIN (SELECT claimsraf.reportID, (1-claimsraf.shared*0.5) AS pointsprob ".
           "FROM claimsraf WHERE enemystatus = 2) AS probtable ON probtable.reportID = reports.id ".
           "WHERE accepted = 1 AND authorID = $characterID AND reports.missionID <= $missionID";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $probable = $row["prob"];
    
    $sql = "SELECT COUNT(dmgtable.pointsdmg) AS dmg FROM reports ".
           "LEFT JOIN (SELECT claimsraf.reportID, 1 AS pointsdmg ".
           "FROM claimsraf WHERE enemystatus = 3) AS dmgtable ON dmgtable.reportID = reports.id ".
           "WHERE accepted = 1 AND authorID = $characterID AND reports.missionID <= $missionID";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    $damaged = $row["dmg"];
    
    $points = $destroyed*10 + $probable*5 + $damaged*2;
    if($sorties > 0){
        $rtbRatio = 1 - $aeroLost/$sorties;
    } else {
        $rtbRatio = 0;
    }
    
//    echo ("Mission: ".$missionID." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." <br>");
    
    //Check for RAF Pilot Brevet
    $medalAbr = "AB";
    $criteria = $sorties > 1;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Victory Medal with Battle of Britain Clasp
    $medalAbr = "VM";
    $criteria = $sorties > 3;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Mentioned in Dispatches
    $medalAbr = "MiD";
    $criteria = $destroyed > 4;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Distinguished Flying Medal
    $medalAbr = "DFM";
    $criteria = ($destroyed > 4 | $points > 119) & $rankValue < 8;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Distinguished Flying Medal with Bar
    $medalAbr = "DFM*";
    $criteriaA = $destroyed > 15;
    $criteriaB = $points > 199;
    $criteriaC = $sorties > 9 & $destroyed > 6 & $rtbRatio >= 0.6;
    $criteria = ($criteriaA | $criteriaB | $criteriaC) & $rankValue < 8;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Distinguished Flying Cross
    $medalAbr = "DFC";
    $criteria = ($destroyed > 7 | $points > 119) & $rankValue >= 8;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Distinguished Flying Cross with Bar
    $medalAbr = "DFC*";
    $criteria = ($destroyed > 15 | $points > 199) & $rankValue >= 8;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Distinguished Flying Cross with two Bars
    $medalAbr = "DFC**";
    $criteria = $points > 499 & $rankValue >= 8;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);
    
    //Distinguished Service Order
    $medalAbr = "DSO";
    $criteria = $sorties > 9 & $destroyed > 6 & $rtbRatio >= 0.6 & $rankValue >= 8;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);
    
    //Distinguished Service Order with Bar
    $medalAbr = "DSO*";
    $criteria = $sorties > 19 & $destroyed > 14 & $rtbRatio >= 0.85 & $rankValue >= 8;
    $infotxt = ("Mission: ".$missionID." rank: ".$rank." sorties: ".$sorties." destr: ".$destroyed." prob: ".$probable." dam: ".$damaged." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);
}

function checkLWDecorations($characterID, $missionID){
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    $dbx = getDBx();
    //Get date of mission
    $sql = "SELECT missions.realDate FROM missions WHERE missions.id = $missionID";
    $result = mysqli_query($dbx, $sql);
    $decorationsArray = array();
    $row = mysqli_fetch_assoc($result);
    $missionDate = $row["realDate"];


    //Get all decorations of the character
    $sql = "SELECT awards.abreviation, decorations.id, decorations.awarded FROM decorations ".
           "LEFT JOIN awards ON decorations.awardID = awards.id ".
           "WHERE decorations.characterID = $characterID";
    $result = mysqli_query($dbx, $sql);
    $decorationsArray = array();
    $awardedArray = array();
    while($row = mysqli_fetch_array($result)){
        $decorationsArray[] = $row[0];
        $awardedArray[$row[0]]["id"] = $row[1];
        $awardedArray[$row[0]]["awarded"] = $row[2];
    }
    
    //Get all awards
    $sql = "SELECT id, abreviation FROM awards ".
           "WHERE faction = 'LW'";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_array($result)){
        $awardArray["$row[1]"] = $row[0];
    }
    
    //Get stats for character
    $sql = "SELECT reports.authorID, COUNT(reports.id) AS sorties, SUM(reports.pilotStatus = 1) AS pilotOK, ".
           "SUM(reports.pilotStatus = 2) AS pilotWND, SUM(reports.aeroplaneStatus = 3) AS aeroLST ".
           "FROM reports LEFT JOIN missions ON reports.missionID = missions.id ".
           "WHERE reports.authorID = $characterID AND reports.accepted = 1 AND missions.id <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $sorties = $row["sorties"];
    $aeroLost = $row["aeroLST"];
    $pilotWounded = $row["pilotWND"];
    
    $sql = "SELECT SUM(claimslw.confirmed=1) AS conf, SUM(claimslw.confirmed=0) AS unconf ". 
           "FROM claimslw LEFT JOIN reports ON claimslw.reportid = reports.id ".
           "WHERE authorID = $characterID AND reports.accepted=1 AND reports.missionID <= $missionID";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $conf = $row["conf"];
    $unconf = $row["unconf"];
    
    $points = $conf + 0.5*$unconf;
    if($sorties > 0){
        $rtbRatio = 1 - $aeroLost/$sorties;
    } else {
        $rtbRatio = 0;
    }
    
    //Check for Flugzeugführer und Beobachterabzeichen
    $medalAbr = "FBA";
    $criteria = $sorties > 1;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Check for Verwundetenabzeichen in schwarz
    $medalAbr = "VA II";
    $criteria = $pilotWounded == 1;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Check for Verwundetenabzeichen in silber
    $medalAbr = "VA I";
    $criteria = $pilotWounded > 1;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Mentioned in Wehrmachtsbericht
    $medalAbr = "WB";
    $criteria = $sorties > 9 & $rtbRatio >= 0.8;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Eisernes Kreuz 2. Klasse
    $medalAbr = "EK II";
    $criteria = $conf > 3 | $pilotWounded > 1;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Eisernes Kreuz 1. Klasse
    $medalAbr = "EK I";
    $criteria = $conf > 5;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);
    
    //Ritterkreuz des Eisernen Kreuzes
    $medalAbr = "RK II";
    $criteria = $points > 14;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Ritterkreuz des Eisernen Kreuzes mit Eichenlaub
    $medalAbr = "RK I";
    $criteria = $points > 19;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Ehrenpokal der Luftwaffe
    $medalAbr = "EP";
    $criteria = $points > 24;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

    
    //Check for Flugzeugführer und Beobachterabzeichen in Gold mit Brillianten
    $medalAbr = "FBAgd";
    $criteria = $sorties > 29 & $conf > 19;
    $infotxt = ("Mission: ".$missionID." sorties: ".$sorties." wounded: ".$pilotWounded." conf: ".$conf." uconf: ".$unconf." points: ".$points." RTB-ratio: ".$rtbRatio." ");
    addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, 
                        $awardArray, $characterID, $missionDate, $dbx, $infotxt);

}

function addRemoveDecoration($medalAbr, $criteria, $decorationsArray, $awardedArray, $awardArray,
                              $characterID, $missionDate, $dbx, $infotxt){
    
    if(!in_array($medalAbr, $decorationsArray)){
        
        if($criteria){
            echo($infotxt." - ".$medalAbr." AWARDED <br>");
            $awardID = $awardArray[$medalAbr];
            $sql = "INSERT INTO decorations (characterID, awardID, date, awarded, awardedBy, availableSince) VALUES ".
                   "($characterID, $awardID, '$missionDate', true, 17, '$missionDate')";
            mysqli_query($dbx, $sql); 
        }
    } else if(!$awardedArray[$medalAbr]["awarded"]){

        if(!$criteria){
            $decorationID = $awardedArray[$medalAbr]["id"];
            $sql = "DELETE FROM decorations WHERE id=$decorationID";
            mysqli_query($dbx, $sql);
        }
    }
}
