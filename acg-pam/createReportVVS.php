<?php
    include(dirname(__FILE__).'/header0.php');
    
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    $dbx = getDBx();
    
    if(filter_has_var(INPUT_GET, "mi_id")){
        $mi_id = filter_input(INPUT_GET, "mi_id");
    } else {
        header("location: missionList.php");
        exit();
    }
    
    // Check if existing user otherwise redirect
    if(!isset($_SESSION["userID"])){
        header("location: message.php?m=1");
        exit();
    }
    
    // Check if character is designated as author of the report
    if(isset($_SESSION["designatedCharacter"])){
        $c_id = $_SESSION["designatedCharacter"];
        unset($_SESSION["designatedCharacter"]);
    } else {
        $c_id = FALSE;
    }
    
    $user_id = $_SESSION["userID"];
    $sql = "SELECT callsign, status FROM acgmembers WHERE id = $user_id";
    $query = mysqli_query($dbx, $sql);
    $u_result = mysqli_fetch_assoc($query);
       
    // Check if user is still an active member otherwise redirect
    if($u_result["status"] === "discharged"){

        header("location: message.php?m=1");
        exit();
    }
    $callsign = $u_result["callsign"];
    
    // Get ID and historic Date of mission to edit.
    $sql = "SELECT histdate, missionstatus, campaignID FROM missions WHERE id = $mi_id";
    $query = mysqli_query($dbx, $sql);
    $mi_result = mysqli_fetch_assoc($query);
    $mission_hdate = date("d M Y H:i", strtotime($mi_result["histdate"]));
    $mi_status = $mi_result["missionstatus"];
    $mi_campaignID = $mi_result["campaignID"];
    $m_faction = getFaction($_SESSION["userID"], $dbx);
       
    // Check if user has already submitted a report. If yes: load this report for editing
    $sql = "SELECT reports.id AS r_id, reports.squadronid, reports.aerodrome, reports.aeroplane, reports.markings, ".
           "reports.synopsis, reports.aeroplanestatus, reports.pilotstatus, careercharacters.id AS c_id,".
           "careercharacters.firstname, careercharacters.lastname, squadrons.faction ".
           "FROM reports LEFT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN squadrons ON reports.squadronid = squadrons.id ".
           "WHERE reports.missionid = $mi_id AND acgmembers.id = $user_id";
    // echo($sql);
    $query = mysqli_query($dbx, $sql);
    if(mysqli_num_rows($query)>0) {
        
        $r_result = mysqli_fetch_assoc($query);
        $squ_id = $r_result["squadronid"];
        $faction = $r_result["faction"];
        $firstName = $r_result["firstname"];
        $lastName = $r_result["lastname"];
        $c_id = $r_result["c_id"];
        
        $aerodrome = $r_result["aerodrome"];
        $aeroplane = $r_result["aeroplane"];
        $markings = $r_result["markings"];
        $synopsis = $r_result["synopsis"];
        $pilotStatus = $r_result["pilotstatus"];
        $aeroplaneStatus = $r_result["aeroplanestatus"];
        
    } else {
       
        $aerodrome = NULL;
        $aeroplane = NULL;
        $markings = NULL;
        $synopsis = NULL;
        $pilotStatus = NULL;
        $aeroplaneStatus = NULL;
        
        // Check if member is assigned to a squadron and get squadron info
        $sql = "SELECT squadrons.id, squadrons.faction, ".
               "UNIX_TIMESTAMP(transfers.transferdate) AS tstdate ".
               "FROM transfers LEFT JOIN squadrons ON transfers.squadronid = squadrons.id ".
               "WHERE memberid = $user_id ORDER BY tstdate DESC LIMIT 1";
        $query = mysqli_query($dbx, $sql);
        $squadron_check = mysqli_num_rows($query);
        if($squadron_check < 1){

            header("location: message.php?m=2");
            exit();
        } else {

            $s_result = mysqli_fetch_assoc($query);
            $squ_id = $s_result["id"];
            $faction = $s_result["faction"];
        }
        
        // If a designated Character is defined: us it; else investigate if new character is needed.
        if(!$c_id){
            
            // Check if member has an active character, if yes: get character info, else: generate new character
            $sql = "SELECT id FROM careercharacters ".
                   "WHERE personifiedby = $user_id AND characterstatus = 1 ORDER BY id DESC LIMIT 1";
            $query = mysqli_query($dbx, $sql);
            $character_check = mysqli_num_rows($query);
            if($character_check < 1) {

                include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
                $c_id = createCharacter($user_id, $faction, $dbx);

            } else {

                $c_result = mysqli_fetch_assoc($query);
                $c_id = $c_result["id"];

                //Check if new campaign and if member wants new character or keep old
                $sql = "SELECT missions.campaignID FROM missions ".
                       "LEFT JOIN reports ON reports.missionID = missions.id ".
                       "WHERE authorID = $c_id ORDER BY missions.campaignID DESC LIMIT 1";
                $query = mysqli_query($dbx, $sql);
                $has_written_report_check = mysqli_num_rows($query);
                if($has_written_report_check > 0){

                    $c_id_result = mysqli_fetch_assoc($query);
                    $last_c_id = $c_id_result["campaignID"];
                    if($last_c_id !== $mi_campaignID){
                        
                        $_SESSION['designatedCharacter'] = $c_id;
                        header("location: newCharacterQuery.php?mi_id=$mi_id&isReport=1");
                        die();
                    }   
                }
            }  
        } 
        // Get character info
        $sql = "SELECT firstname, lastname FROM careercharacters ".
               "WHERE id = $c_id";
        $query = mysqli_query($dbx, $sql);
        $c_result = mysqli_fetch_assoc($query);
        $firstName = $c_result["firstname"];
        $lastName = $c_result["lastname"];
        
        unset($_SESSION["designatedCharacter"]);
          
    }
    
    
//    // Get ID and historic Date of mission to edit.
//    $sql = "SELECT histdate, missionstatus FROM missions WHERE id = $mi_id";
//    $query = mysqli_query($dbx, $sql);
//    $mi_result = mysqli_fetch_assoc($query);
//    $mission_hdate = date("d M Y H:i", strtotime($mi_result["histdate"]));
//    $mi_status = $mi_result["missionstatus"];
//    $m_faction = getFaction($_SESSION["userID"], $dbx);

    // Access al squadron info from database for form input.
    $sql = "SELECT id, name FROM squadrons WHERE faction = '$faction'";
    $s_result = mysqli_query($dbx, $sql);
    
    // Access al pilotStatus info from database for form input.
    $sql = "SELECT id, status FROM pilotstatus";
    $pilotStatus_result = mysqli_query($dbx, $sql);
    
    // Access al aeroplaneStatus info from database for form input.
    $sql = "SELECT id, status FROM aeroplanestatus";
    $aeroplaneStatus_result = mysqli_query($dbx, $sql);
        
    // Load flyable (type=1) aeroplanes from the database for form input
    $sql = "SELECT id, name FROM aeroplanes ".
           "WHERE faction = '$faction' AND type = 1";
    $aeroplane_flyable_result = mysqli_query($dbx, $sql);
    
    // Load claimable (type=2) aeroplanes from the database for form input
    $sql = "SELECT id, name FROM aeroplanes ".
           "WHERE faction != '$faction' AND type = 2";
    $aeroplane_claimable_result = mysqli_query($dbx, $sql);
    $aeroplane_claimable_array = mysqli_fetch_all($aeroplane_claimable_result);
    
    // Load ground targets from the database for form input
    $sql = "SELECT id, name FROM groundtargets";
    $groundTarget_result = mysqli_query($dbx, $sql);
    $groundTarget_array = mysqli_fetch_all($groundTarget_result);
    
    // Load claims from database and add them to the form
    $sql = "SELECT claimsvvs.id, claimsvvs.aeroplane, claimsvvs.claimTime, claimsvvs.place, ".
           "claimsvvs.witness AS witnessID, claimsvvs.groupClaim, claimsvvs.description, ".
           "acgmembers.callsign AS witness FROM claimsvvs ".
           "LEFT JOIN acgmembers ON claimsvvs.witness = acgmembers.id ".
           "LEFT JOIN reports ON claimsvvs.reportid = reports.id ".
           "WHERE reports.missionid = $mi_id AND reports.authorid = $c_id";
//    echo($sql);
    $claim_result = mysqli_query($dbx, $sql);
    
    // Load ground claims from database and add them to the form
    $sql = "SELECT claimsground.id, claimsground.object, claimsground.amount, ".
           "claimsground.description, claimsground.accepted FROM claimsground ".
           "LEFT JOIN reports ON claimsground.reportid = reports.id ".
           "WHERE reports.missionid = $mi_id AND reports.authorid = $c_id";
    $groundclaim_result = mysqli_query($dbx, $sql);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script type="text/javascript">

var counter = 0;
var groundCounter = 0;
var aeroplane = <?php echo json_encode($aeroplane_claimable_array); ?>;
var groundTargets = <?php echo json_encode($groundTarget_array); ?>;

// divName, claimID, counter, claimAcftArray, claimAcft, claimTime, 
//                     place, witnessID, witness, groupClaim, description
function addClaim(divName){
    counter = addVVSClaim(divName, "", counter, aeroplane, "", "", "", 
                             -2, "", "", "");
}

function addGClaim(divName){
    groundCounter = addGroundClaim(divName, "", groundCounter, groundTargets,
                                   "", "", 1);
}

function submitReport(){

    gebid("submitStatus").innerHTML = "";
    var success = submitVVSReport(<?php echo($mi_id.', '.$c_id); ?>);
    var nodelist = document.getElementsByClassName("claim");
    for(n = 0; n < nodelist.length; n++){
        var id = nodelist[n].id;
        submitVVSClaim(<?php echo($mi_id.', '.$c_id); ?>, id);
    }
    var nodelist = document.getElementsByClassName("groundclaims");
    for(n = 0; n < nodelist.length; n++){
        var id = nodelist[n].id;
        submitGroundClaim(<?php echo($mi_id.', '.$c_id); ?>, id);
    }
    if(success){
        location.reload();
    }
}

window.onload = function(){
    gebid("submitBtn").addEventListener("click", submitReport, false);
    <?php while($row = mysqli_fetch_assoc($claim_result)) { 
        $witness_id = "";
        if(is_null($row["witnessID"])){$witness_id = -2;}else{$witness_id = $row["witnessID"];}
    ?>

            counter = addVVSClaim('claims', <?php echo($row["id"]);?>, counter,
                                aeroplane, <?php echo($row["aeroplane"]);?>, 
                                '<?php echo($row["claimTime"]);?>', '<?php echo($row["place"]);?>',
                                <?php echo $witness_id ?>,
                                '<?php echo($row["witness"]); ?>',
                                <?php echo($row["groupClaim"]);?>,
                                '<?php echo preg_replace("/\r?\n/", "\\n", addslashes($row["description"]));?>');
            checkWitness("c"+counter);
            
            // divName, claimID, counter, claimAcftArray, claimAcft, claimTime, 
//                     place, witnessID, witness, groupClaim, description)

    <?php } 
        while($row = mysqli_fetch_assoc($groundclaim_result)) { 
    ?>

            groundCounter = addGroundClaim('groundclaims', <?php echo($row["id"]);?>, 
                                           groundCounter, groundTargets,
                                           <?php echo($row["object"]);?>,
                                           '<?php echo preg_replace("/\r?\n/", "\\n", addslashes($row["description"]));?>',
                                           <?php echo($row["amount"]);?>);
    <?php } ?>
};
</script>   
<?php include(dirname(__FILE__).'/missionMenu.php'); ?> 

<p class="form_id">ACG-PAM/300-210.1</p>
<h3>After Action Report:</h3>
<form id="afterActionReportLW" onsubmit="return false;" >

    <div>
        <span class="AARSpanLeft">Mission date:</span>
        <span class="AARSpanRight"><?php echo $mission_hdate; ?></span>
    </div>

    <div>
        <span class="AARSpanLeft">Name:</span>
        <span class="AARSpanRight"><?php echo $firstName." '".$callsign."' ".$lastName; ?></span>
    </div>
    <div>
        <span class="AARSpanLeft">Rank:</span>
        <span class="AARSpanRight">TODO</span>
    </div>

    <div>    
        <span class="AARSpanLeft">Unit:</span>
        <span class="AARSpanRight">
            <select id="squadron" name="squadron">
            <?php createSelectOptions($s_result, $squ_id); ?>   
            </select>
        </span>
    </div>

    <div>
        <span class="AARSpanLeft">Type:</span>
        <span class="AARSpanRight">
            <select id="actType" name="actType">
                <?php createSelectOptions($aeroplane_flyable_result, $aeroplane); ?>
            </select>
        </span>
    </div>

    <div>
        <span class="AARSpanLeft">Markings:</span>
        <span class="AARSpanRight"><input type="text" id="markings" name="markings" value="<?php echo $markings ?>"></span>
    </div>

    <div>
        <span class="AARSpanLeft">Aerodrome:</span>
        <span class="AARSpanRight"><input type="text" id="base" name="base" value="<?php echo $aerodrome ?>"></span>
    </div>

    <div>
        <span class="AARSpanLeft">Pilot status:</span>
        <span class="AARSpanRight">
            <select id="pilotStatus" name="pilotStatus">
                <?php createSelectOptions($pilotStatus_result, $pilotStatus); ?>
            </select>
        </span>
    </div>

    <div>
        <span class="AARSpanLeft">Aircraft status:</span>
        <span class="AARSpanRight">
            <select id="acftStatus" name="acftStatus">
                <?php createSelectOptions($aeroplaneStatus_result, $aeroplaneStatus); ?>
            </select>
        </span>
    </div>

    <div id="claims">
        Claims:<br>
        (Checking "Ground Confirmation" will override any other witness information)<br>
    </div>

    <button id="addClaimBtn" onclick="addClaim('claims')">Add claim</button>
    
    <div id="groundclaims">
        Ground claims:<br>
    </div>

    <button id="addGroundClaimBtn" onclick="addGClaim('groundclaims')">Add ground claim</button>

    <div>
        Synopsis (Optional): Images can be included by using [img]image-url[/img].<br>
        <textarea id="synopsis" rows="25" cols="50" maxlength="15000"><?php echo $synopsis ?></textarea>
    </div>

    <div id="submitStatus">&nbsp;</div>
    <button id="submitBtn" >Submit report</button>
</form>
<?php include(dirname(__FILE__).'/footer.php');