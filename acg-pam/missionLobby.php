<?php

if($userID > 0){
    $userFaction = getFaction($userID, $dbx);
}

//Cycle through each squadron for displaying buttons.
$sql = "SELECT squadrons.id, squadrons.name, squadrons.faction FROM squadrons ".
       "ORDER BY squadrons.faction DESC, squadrons.id ASC";
$sresult = mysqli_query($dbx, $sql);

?>
<p>The mission's current status is: Briefing</p>
<p>Pilots are advised to read their corresponding briefing and be ready at 
dispersal <?php echo($m_row["realdate"]); ?> around 1900z. Additional details will
be given by your commanding officer in time. Guests without squadron/Staffel
are welcome to join for trying out an ACG mission.</p>
<!--<form onsubmit="return false;" >-->
<!--    <div align="center">-->
<!--        <table>-->
<!--            <tbody>-->
<?php

// while($s_row = mysqli_fetch_assoc($sresult)) {

//     $squadronID = $s_row["id"];
//     $sfaction = $s_row["faction"];
//     $buttonStr = $s_row["name"];
//     if($userID != 0 && $userFaction == $sfaction){
?>
    <!--<tr>-->
    <!--    <td><button style="width: 100%" -->
            <!--onclick="enterDispersal(<?php  //echo($mi_id.", ".$squadronID); ?>)">-->
    <!--        Enter <?php // echo $s_row["name"];?> dispersal.</button></td>-->
    <!--</tr>-->
    
<?php
//     }
// }
?>
    <!--        </tbody>-->
    <!--</table>-->
    <!--</div>-->
    <!--<div id="submitStatus">&nbsp;</div>-->
<!--</form>-->
<p>In case the mission was flown without it's status being updated. Please
contact the campaign administration.</p>
<?php
