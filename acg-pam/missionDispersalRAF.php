<?php

function flightSelectForm($name, $id, $position, $roster_array, $memberArray, $searchElement, $elementIDArray){
    
    $elementID = 0;
    foreach ($position as $positionID) {
        if(array_key_exists($positionID, $roster_array)){
            $memberID = $roster_array[$positionID];
            $elementID = $memberArray[$memberID][$searchElement];
        }
    }
    createSelectFormWithName($name, $id, $elementIDArray, $elementID);
}

function memberSelectForm($position, $roster_array, $memberIDArray){
    if(array_key_exists($position, $roster_array)){
        $memberID = $roster_array[$position];
    } else {
        $memberID = 0;
    }
    createSelectFormWithName("dispersal", $position, $memberIDArray, $memberID);
}

?>
<?php 
    if($isAdmin){
?>
<div id="dispersalSetupDiv" style="display: block">
    <form id="rosterAircraft" onsubmit="return false;" >
        <table>
            <thead>
                <tr>
                    <th>Role:</th>
                    <th>Pilot:</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b>Flight/Section</b></td>
                    <td><?php
                            $position = array(4, 3, 2, 1);
                            flightSelectForm("flightDispersal", "flight1", $position, 
                                    $roster_array, $memberArray, "flightsraf", $flightIDArray);
                    ?></td>
                    <td>/<?php
                            flightSelectForm("sectionDispersal", "section1", $position,
                                    $roster_array, $memberArray, "sectionraf", $sectionIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">OC Sqn</td>
                    <td><?php
                            $position = 1;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">2</td>
                    <td><?php 
                            $position = 2;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">3</td>
                    <td><?php 
                            $position = 3;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">4</td>
                    <td><?php 
                            $position = 4;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><b>Flight/Section</b></td>
                    <td><?php
                            $position = array(8, 7, 6, 5);
                            flightSelectForm("flightDispersal", "flight2", $position, 
                                    $roster_array, $memberArray, "flightsraf", $flightIDArray);
                    ?></td>
                    <td>/<?php
                            flightSelectForm("sectionDispersal", "section2", $position,
                                    $roster_array, $memberArray, "sectionraf", $sectionIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">OC Flt/Sect</td>
                    <td><?php
                            $position = 5;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">2</td>
                    <td><?php 
                            $position = 6;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">3</td>
                    <td><?php 
                            $position = 7;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">4</td>
                    <td><?php 
                            $position = 8;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><b>Flight/Section</b></td>
                    <td><?php
                            $position = array(12, 11, 10, 9);
                            flightSelectForm("flightDispersal", "flight3", $position, 
                                    $roster_array, $memberArray, "flightsraf", $flightIDArray);
                    ?></td>
                    <td>/<?php
                            flightSelectForm("sectionDispersal", "section3", $position,
                                    $roster_array, $memberArray, "sectionraf", $sectionIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">OC Flt/Sect</td>
                    <td><?php
                            $position = 9;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">2</td>
                    <td><?php 
                            $position = 10;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">3</td>
                    <td><?php 
                            $position = 11;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">4</td>
                    <td><?php 
                            $position = 12;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><b>Flight/Section</b></td>
                    <td><?php
                            $position = array(16, 15, 14, 13);
                            flightSelectForm("flightDispersal", "flight4", $position, 
                                    $roster_array, $memberArray, "flightsraf", $flightIDArray);
                    ?></td>
                    <td>/<?php
                            flightSelectForm("sectionDispersal", "section4", $position,
                                    $roster_array, $memberArray, "sectionraf", $sectionIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">OC Flt/Sect</td>
                    <td><?php
                            $position = 13;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">2</td>
                    <td><?php 
                            $position = 14;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">3</td>
                    <td><?php 
                            $position = 15;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">4</td>
                    <td><?php 
                            $position = 16;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td><b>Flight/Section</b></td>
                    <td><?php
                            $position = array(20, 19, 18, 17);
                            flightSelectForm("flightDispersal", "flight5", $position, 
                                    $roster_array, $memberArray, "flightsraf", $flightIDArray);
                    ?></td>
                    <td>/<?php
                            flightSelectForm("sectionDispersal", "section5", $position,
                                    $roster_array, $memberArray, "sectionraf", $sectionIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">OC Flt/Sect</td>
                    <td><?php
                            $position = 17;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">2</td>
                    <td><?php 
                            $position = 18;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">3</td>
                    <td><?php 
                            $position = 19;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
                <tr>
                    <td style="text-align: right">4</td>
                    <td><?php 
                            $position = 20;
                            memberSelectForm($position, $roster_array, $memberIDArray);
                    ?></td>
                </tr>
            </tbody>
        </table>
        <button id="saveDispersalBtnRAF" >Save changes</button>
        <span id="saveDispersalStatus" ></span>        
    </form>
    <hr>
</div>
<?php 
    }
?>
<div>
    <table>
        <tbody>
<?php 
$aFlt = array();
$bFlt = array();
foreach ($roster_array as $positionID => $memberID) {
    
    if($memberArray[$memberID]["dispersalStatus"]==1){
        
        //Getting members of A FLT
        if($memberArray[$memberID]["flightsraf"]==1){
            
            $name = $memberArray[$memberID]["name"];
            $rank = $memberArray[$memberID]["rank"];
            $rankValue = $memberArray[$memberID]["rankValue"];
            $section = $sectionASSOCArray[$memberArray[$memberID]["sectionraf"]];
            $aFlt[] = array("position"=>$positionID, "name"=>$name, "rank"=>$rank,
                "rankValue"=>$rankValue, "section"=>$section);
        } else if($memberArray[$memberID]["flightsraf"]==2){
            
            $name = $memberArray[$memberID]["name"];
            $rank = $memberArray[$memberID]["rank"];
            $rankValue = $memberArray[$memberID]["rankValue"];
            $section = $sectionASSOCArray[$memberArray[$memberID]["sectionraf"]];
            $bFlt[] = array("position"=>$positionID, "name"=>$name, "rank"=>$rank,
                "rankValue"=>$rankValue, "section"=>$section);
        }
    }
}
if(count($aFlt)>0){
    echo("<tr><td><h3>A Flight</h3></td><tr>");
}
$currentSection = "";
for($n = 0; $n < count($aFlt); $n++) {
    
    $pilotArray = $aFlt[$n];
    if($pilotArray["section"] != $currentSection){
        $currentSection = $pilotArray["section"];
        $currentPosition = 1;
        echo("<tr><td><b>".$currentSection."<b></td><tr>");
        if($pilotArray["position"] == 1){
           echo("<tr><td>OC ".$sqnName." Sqn</td>"); 
        } else if($n == 0) {
           echo("<tr><td>OC A Flt</td>"); 
        } else {
           if($pilotArray["rankValue"]<8){
               echo("<tr><td>NCO IC Sect</td>");
           } else {
               echo("<tr><td>OIC Sect</td>");
           }
        }
    } else {
        echo("<tr><td>".$currentPosition."</td>");
    }
    echo("<td>".$pilotArray["rank"]."</td><td>".$pilotArray["name"]."</td>");
    $currentPosition++;
}
if(count($bFlt)>0){
    echo("<tr><td><h3>B Flight</h3></td><tr>");
}
$currentSection = "";
for($n = 0; $n < count($bFlt); $n++) {
    
    $pilotArray = $bFlt[$n];
    if($pilotArray["section"] != $currentSection){
        $currentSection = $pilotArray["section"];
        $currentPosition = 1;
        echo("<tr><td><b>".$currentSection."<b></td><tr>");
        if($pilotArray["position"] == 1){
           echo("<tr><td>OC ".$sqnName." Sqn</td>"); 
        } else if($n == 0) {
           echo("<tr><td>OC B Flt</td>"); 
        } else {
           if($pilotArray["rankValue"]<8){
               echo("<tr><td>NCO IC Sect</td>");
           } else {
               echo("<tr><td>OIC Sect</td>");
           }
        }
    } else {
        echo("<tr><td>".$currentPosition."</td>");
    }
    echo("<td>".$pilotArray["rank"]."</td><td>".$pilotArray["name"]."</td>");
    $currentPosition++;
}   
?>
        </tbody>
    </table>
</div>

