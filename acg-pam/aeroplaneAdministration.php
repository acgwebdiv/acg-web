<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Setting up indices for spreading conten over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 10;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT * FROM aeroplanes ORDER BY type ASC, faction ASC, name ASC ".
           "LIMIT $start_from, $n_entries";
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM aeroplanes";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/aeroplaneAdminLogic.js"></script>
<script type="text/javascript">

function editSquadron(id){
    window.location = "editAeroplane.php?s_id="+id;
} 

window.onload = function(){
    gebid("addAeroplaneButton").addEventListener("click", addAeroplane, false);

};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-500.1</p>

<h2>Aeroplane administration:</h2>
<div>
    <p>These pages list all aeroplanes that can either be flown during the
    missions or claimed in the AARs. Aeroplanes belong to one faction and one
    class, which controls on which list the aeroplane will be visible.</p>
    <p>The EDIT form provides tools for editing the name, faction and classed
    of the aeroplane.</p>        
    <hr>
    <h3>Create new aeroplane:</h3>
    <form id="addnewAeroplaneForm" onsubmit="return false;">
        <div class='middlePageStandard'>
            <b>Aeroplane:</b>
            <input type="text" id="aeroplaneName" name="aeroplaneName" size="50" maxlength="50">
            <p>The name of the aeroplane.</p>
        </div>
        
        <div class='middlePageStandard'>
            <b>Aeroplane faction:</b>
            <select id="aeroplaneFaction" name="aeroplaneFaction">
                <option value="RAF">Royal Air Force</option>
                <option value="LW">Luftwaffe</option>
                <option value="VVS">Voyenno-Vozdushnye Sily</option>
            </select>
            <p>The faction of the aeroplane.</p>
        </div>
        
        <div class='middlePageStandard'>
            <b>Aeroplane type:</b>
            <select id="aeroplaneType" name="aeroplaneType">
                <option value=1>Flyable aeroplane</option>
                <option value=2>Claimable aeroplane</option>
            </select>
            <p>The type of the aeroplane decides on which list it will be available.
            Flyable aeroplanes are selectable in the mission reports as own plane. 
            Claimable aeroplanes are selectable in the claims of mission reports.</p>
        </div>
        
        <div class='middlePageStandard'>
            <button id="addAeroplaneButton">Create Aeroplane</button>
            <span id="addAeroplaneStatus" ></span>
        </div>
    </form>
    <hr>
</div>

<div>
    <h3>Aeroplanes:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>Name:</th>
            <th>Faction:</th>
            <th>Type:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($result)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["name"];?></td>
            <td><?php echo $row["faction"];?></td>
            <td><?php echo($row["type"]==1? "Flyable":"Claimable");?></td>
            <td><button onclick="editSquadron(<?php echo $row['id']; ?>)">EDIT</button></td>
        </tr>
        <?php } ?>
    </table>
</div>

<div class="pageSelect">
    <?php createPageSelect($n_pages, $page, 'aeroplaneAdministration.php?'); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');