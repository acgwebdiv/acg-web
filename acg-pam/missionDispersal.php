<?php
include(dirname(__FILE__).'/header0.php');
include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');

$dbx = getDBx();
if(filter_has_var(INPUT_GET, "mi_id")){
    $mi_id = filter_input(INPUT_GET, "mi_id");  
}
if(filter_has_var(INPUT_GET, "squ")){
    $sqn = filter_input(INPUT_GET, "squ");  
}

$sql = "SELECT missions.name AS miName, missions.histdate, missions.realdate, ".
       "missions.missionstatus, campaigns.name AS c_name ".
       "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
       "WHERE missions.id = $mi_id";
$m_result = mysqli_query($dbx, $sql);
$m_row = mysqli_fetch_assoc($m_result);
$mi_name = $m_row["miName"];
$mi_status = $m_row["missionstatus"];
$mi_realDate = $m_row["realdate"];
$m_faction = getFaction($_SESSION["userID"], $dbx);

$update = ($mi_status==1);

//Check for admin rights
if(isset($_SESSION["admin"])){
    if($_SESSION["admin"]){
        $isAdmin = TRUE;
    } else {
        $isAdmin = FALSE;
    }
} else {

    $isAdmin = FALSE;
}

if(isset($_SESSION['userID'])){
    $userID = $_SESSION['userID'];
    $callsign = $_SESSION['callsign'];
} else {
    $userID = 0;
    $callsign = '';
}
    
////Check if user is listed in attendance database for dispersal and check of
////dispersal status
//$sql = "SELECT attendance.id, attendance.dispersalStatus, careercharacters.personifiedBy ".
//       "FROM attendance ".
//       "LEFT JOIN careercharacters ON attendance.characterID = careercharacters.id ".
//       "WHERE attendance.missionID = $mi_id AND personifiedBy = $userID AND attendance.squadronID = $sqn";
//$aresult = mysqli_query($dbx, $sql);
//if(mysqli_num_rows($aresult)>0){
//    $arow = mysqli_fetch_assoc($aresult);
//    $dispersalStatus = $arow["dispersalStatus"];
//} else {
//    $dispersalStatus = -1;
//}


//Get squadronFaction and in case of LW if Stab or not for selection of right Format.
//RAF = 1, LW-Stab = 2, LW = 3
$sql = "SELECT faction, name FROM squadrons WHERE id = $sqn";
$result = mysqli_query($dbx, $sql);
while($row = mysqli_fetch_assoc($result)){
    $faction = $row["faction"];
    $sqnName = $row["name"];
}

// Access al flight info from database for form input.
$sql = "SELECT id, name FROM flightsraf";
$flight_result = mysqli_query($dbx, $sql);
$flightIDArray = array();
$flightIDArray[] = array("0", "");
while($row = mysqli_fetch_assoc($flight_result)){
    $flightIDArray[] = array($row["id"], $row["name"]);
}
unset($flightIDArray[3]);

// Access al section info from database for form input.
$sql = "SELECT id, name FROM sectionraf";
$section_result = mysqli_query($dbx, $sql);
$sectionIDArray = array();
$sectionIDArray[] = array("0", "");
$sectionASSOCArray = array();
while($row = mysqli_fetch_assoc($section_result)){
    $sectionIDArray[] = array($row["id"], $row["name"]);
    $sectionASSOCArray[$row["id"]] = $row["name"];
}
unset($sectionIDArray[9]);

//Get mission briefing
$type = 0;
$sql = "SELECT * FROM briefings WHERE missionID = $mi_id AND faction = '$m_faction'";
$query = mysqli_query($dbx, $sql);
if(mysqli_num_rows($query)>0){
    $bresult = mysqli_fetch_assoc($query);
    $type = $bresult["type"];
}

?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/dispersalLogic.js"></script>
<script type="text/javascript">

var flightIDArray = <?php echo json_encode($flightIDArray); ?>;
var sectionIDArray = <?php echo json_encode($sectionIDArray); ?>;

function saveDispersal(){

    if("<?php echo $faction;?>" == "LW"){
        saveDispersalLW();
    } else if("<?php echo $faction;?>" == "RAF"){
        saveDispersalRAF();
    }
}

function saveDispersalLW(){
        
    var nodelist = document.getElementsByName("dispersal");
    for(n = 0; n < nodelist.length; n++){
        var positionID = nodelist[n].id;
        saveDispersalMemberLW(positionID, <?php echo $sqn;?>, <?php echo $mi_id?>);
    }
    autoRefresh(10000);
//    location.reload();
}

function saveDispersalRAF(){

    var nodelistFlight = document.getElementsByName("flightDispersal");
    var nodelistSection = document.getElementsByName("sectionDispersal");
    var flightSectionPos;
    var nodelist = document.getElementsByName("dispersal");
    for(n = 0; n < nodelist.length; n++){
        flightSectionPos = Math.floor(n/4);
        var positionID = nodelist[n].id;
        var positionIDFlight = nodelistFlight[flightSectionPos].id;
        var positionIDSection = nodelistSection[flightSectionPos].id;
        saveDispersalMemberRAF(positionID, positionIDFlight, positionIDSection,
            <?php echo $sqn;?>, <?php echo $mi_id?>);
    }
    autoRefresh(10000);
//    location.reload();

}

var reloadActive = true & <?php echo(($update)?"true":"false");?>;

function setDisplay(show){
    
    if(show){
        reloadActive = false;
        getAssignedPilotsSetup(<?php echo $mi_id;?>, '<?php echo $faction;?>', <?php echo $sqn;?>);
        gebid("dispersalSetupDiv").style.display = "block";
        gebid("addPilotDiv").style.display = "block";
        gebid("showDispersalSetupBtn").textContent = "Hide dispersal setup";
    } else {
        reloadActive = true & <?php echo(($update)?"true":"false");?>;
        gebid("dispersalSetupDiv").style.display = "none";
        gebid("addPilotDiv").style.display = "none";
        gebid("showDispersalSetupBtn").textContent = "Show dispersal setup";
        autoRefresh(10000);
    }

}

function toggleDisplay(){
    
    if(gebid("dispersalSetupDiv").style.display == "none"){
        setDisplay(1);
    } else {
        setDisplay(0);
    }

}

var isAdmin = <?php echo(($isAdmin)?"true":"false");?>;
var timer = null;
function autoRefresh(t) {
    
    getUnassignedPilots(<?php echo $mi_id;?>, '<?php echo $faction;?>', <?php echo $sqn;?>, isAdmin);
    getAssignedPilotsDisplay(<?php echo $mi_id;?>, '<?php echo $faction;?>', <?php echo $sqn;?>);
    updateButton(<?php echo $userID;?>, <?php echo $sqn;?>, <?php echo $mi_id;?>);
    setTimeout("if(reloadActive){autoRefresh(10000);}", t);     
}

window.onload = function(){

    autoRefresh(10000);
    setDisplay(false);

};

</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>
<p class="form_id">ACG-PAM/300-210.1</p>
<div>
    <h3>Mission briefing for <?php echo $faction ?> - <?php echo($mi_name." (".$m_row["histdate"].")"); ?>,<br> 
    ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date: <?php echo($m_row["realdate"]); ?>.</h3>
    <hr>
    <?php if($isAdmin){ ?>
    <button id="showDispersalSetupBtn" onclick="toggleDisplay()">Show dispersal setup</button>
    <?php } ?>
</div>
<div class="dispersalSetupLeft">
    <?php 
        if($isAdmin){
    ?>
    <div id="dispersalSetupDiv" style="direction: block">
        <form id="rosterAircraft" onsubmit="return false;" >
            <table>
                <thead>
                    <tr>
                        <th>Role:</th>
                        <th>Pilot:</th>
                    </tr>
                </thead>
                <tbody id="dispersalSetupTable"></tbody>
            </table>
            <button id="saveDispersalBtn" onclick="saveDispersal()">Save Dispersal</button>
            <span id="saveDispersalStatus" ></span>        
        </form>
        <hr>
    </div>
    <?php 
        }
    ?>
    <div>
        <table>
            <thead>
                <tr>
                    <th>Role:</th>
                    <th>Pilot:</th>
                </tr>
            </thead>
            <tbody id="dispersalDisplayTable"></tbody>
        </table>
    </div>
</div>
<div class="dispersalSetupRight">
    <form onsubmit="return false;" >
    <?php if($isAdmin){ ?>
    <div id="addPilotDiv" style="direction: block">
        <table>
            <thead>
                <tr><td colspan="2">Add pilot to dispersal:</td></tr>
            </thead>
            <tr>
                <td><input type='text' id='addPilotInput' onchange='checkPilot()'></td>
                <td><span id="addPilotStatus"></span></td>
            </tr>
            <tr>
                <td><button onclick="addPilot(<?php echo($mi_id.", ".$sqn.", '".$faction."'"); ?>)">Add Pilot</button></td>
            </tr>
        </table>
    </div>
    <?php } ?>
        <div id="unassignedPilotsDiv">
            <table>
                <thead>
                    <tr><th colspan="2"><h3>Unassigned pilots:</h3></th></tr>
                </thead>
                <tbody id="unassignedPilotsTable">

                </tbody>
            </table>
        </div>
<?php
    if($mi_status == 1){
?>
        <p>Press <i>Exit dispersal</i> to leave this dispersal room. You will lose 
        your current assignment by doing this.</p>
        
        <span id="dutyBtnSpan" style="display: inline-block">
            <button onclick="reportForDuty(<?php echo($mi_id.", ".$sqn.", ".$userID.", '".$m_faction."'"); ?>)">Report for duty</button>
        </span>
        <span id="cancelDutyBtnSpan" style="display: none">
            <button onclick="cancelReportForDuty(<?php echo($mi_id.", ".$userID); ?>)">Cancel report for duty</button>
        </span>
        <span>
            <button onclick="exitDispersal(<?php echo($mi_id); ?>)">Exit dispersal</button>
        </span>    
<?php
    }
?>
    </form>
</div>
<div id="submitStatus"><br>&nbsp;</div>
<div>
    <hr>
<?php

switch($type){
    
    case 1:
?>
<div>
    <p>
        <?php echo(nl2br(parseImageTags($bresult["text1"])));?>
    </p>
</div>
<?php   
        break;
    case 2:

        if($m_faction === "RAF") {
            $headingTXT = "Royal Air Force <br>".
                          "11.Group Mission Directive <br>";
        } else if($m_faction === "LW") {
            $headingTXT = "Oberkommando der Luftwaffe <br>".
                          "JG26 and ZG76 Directive <br>";
        }

?>
<div>
    <p>
        <b><?php echo $headingTXT;?></b>
        <b>Classified</b>
    </p>
    <p>
        <b>Date:</b> <?php echo(nl2br($m_row["histdate"]));?><br>
        <b>Operation:</b> <?php echo(nl2br($m_row["miName"]));?><br>
        <b>Target:</b> <?php echo(nl2br($result["text1"])); ?><br>
        <b>Secondary target/Diversions:</b> <br>
        <?php echo(nl2br($result["text2"])); ?><br>
        <b>Force to target/s:</b> <br>
        <?php echo(nl2br($result["text3"])); ?>
    </p>
    <p>
        <b>Weather report:</b><br>
        <?php echo(nl2br($result["text4"])); ?>
    </p>
    <p>
        <b>Engine start time:</b> <?php echo(nl2br($result["text5"])); ?><br>
        <b>RV:</b> <?php echo(nl2br($result["text6"])); ?><br>
        <b>H-Hour:</b> <?php echo(nl2br($result["text7"])); ?>
    </p>
    <p>
        <b>Route and proceedings:</b><br>
        <?php echo(nl2br($result["text8"])); ?><br>
        <b>Rules of engagement:</b><br>
        <?php echo(nl2br($result["text9"])); ?><br>
        <b>Expected resistance:</b><br>
        <?php echo(nl2br($result["text10"])); ?><br>
        <b>Divert:</b> <?php echo(nl2br($result["text11"])); ?>
    </p>
    <p>
        <?php echo(nl2br(parseImageTags($result["text12"]))); ?>
    </p>
</div>
<?php   
        break;
}
?>
</div>
