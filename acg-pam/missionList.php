<?php
    include(dirname(__FILE__).'/header0.php');
    
    // Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 15;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT missions.id, missions.name AS miName, missions.realdate, ".
           "missions.histdate, missions.missionstatus, ".
           "campaigns.name, missionstatus.status AS statusstr ".
           "FROM missions LEFT JOIN campaigns ON missions.campaignid=campaigns.id ". 
           "LEFT JOIN missionstatus ON missionstatus.id = missions.missionstatus ".
           "ORDER BY id DESC LIMIT $start_from, $n_entries";
//    echo $sql;
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM missions";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>

<?php include(dirname(__FILE__).'/missionMainMenu.php'); ?> 
<p class="form_id">ACG-PAM/300-100.1</p>
<h3>ACG-Missions:</h3>
<div>
    <p>These pages list all ACG missions with their name, real- and historic date, 
    associated campaign and status. The real date is the date at which the mission
    was performed as part of a ACG campaign. The historic date is the date at
    which the mission took place in an historic context. Each mission is part of 
    a ACG campaign. The status of the mission has implications on the accessibility
    of mission briefings and After Action Reports (AAR's).</p>
    <p>The mission statuses are:
    <ul>
        <li><b>Briefing:</b> Before mission start. Briefings can only be seen by 
        the associated faction. AARs can not be created or edited.</li>
        <li><b>Debriefing:</b> After mission has ended. Briefings can only be seen by 
        the associated faction. AARs can be created or edited.</li>
        <li><b>Closed:</b> After debriefing period has ended (usually one week after
        the mission has ended). Breifings can be seen by all factions. AARs can 
        not be created or edited.</li>
    </ul>
    </p>
    <p>Click anywhere on the mission information to request mission details.</p>
    <hr>
    <table class="wideTable">
        <tr>
            <th>Name:</th>
            <th>Date:</th>
            <th>Historic Date:</th>
            <th>Campaign:</th>
            <th>Status:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($result)) { 
        ?>
        <tr>
            <td><a href="missionDetails.php?m_id=<?php echo $row["id"]; ?>"><?php echo $row["miName"];?></a></td>
            <td><a href="missionDetails.php?m_id=<?php echo $row["id"]; ?>"><?php echo $row["realdate"];?></a></td>
            <td><a href="missionDetails.php?m_id=<?php echo $row["id"]; ?>"><?php echo $row["histdate"];?></a></td>
            <td><a href="missionDetails.php?m_id=<?php echo $row["id"]; ?>"><?php echo $row["name"];?></a></td>
            <td><a href="missionDetails.php?m_id=<?php echo $row["id"]; ?>"><?php echo($row["statusstr"]);?></a></td>
        </tr>
        <?php } ?>
    </table>
</div>
<div class='pageSelect'>
    <?php createPageSelect($n_pages, $page, 'missionList.php?'); ?>
</div>

<?php include(dirname(__FILE__).'/footer.php');