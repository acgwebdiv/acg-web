<?php
    include(dirname(__FILE__).'/header0.php');
    include(dirname(__FILE__).'/squadronHeader.php');
    
    // Access al aircraft in Hangar for squadron
    $sql = "SELECT id, memberID, designation, image FROM hangar WHERE squadronID = $sqn";
    $result = mysqli_query($dbx, $sql);
    $hangar_array = array();
    while($row = mysqli_fetch_assoc($result)) {
        $aircraftId = $row["id"];
        $hangar_array[$aircraftId]["memberID"] = $row["memberID"];
        $hangar_array[$aircraftId]["designation"] = $row["designation"]." (".$row["image"].")";
    }
    
    // Get squadronFaction
    $sql = "SELECT faction FROM squadrons WHERE id = $sqn";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        $faction = $row["faction"];
    }
    
    //Creating new member array for use in createSelectFormWithName()
    $memberIDArray = array();
    $memberIDArray[] = array(0, "");
    $memberIDArray[] = array(-1, "Reserved");
    foreach ($memberArray as $memberID => $memberInfo) {
        $memberIDArray[] = array($memberID, $memberInfo["callsign"]);
    }
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/rosterLogic.js"></script>
<script type="text/javascript">

function saveAircrafts(){

    var nodelist = document.getElementsByName("aircraft");
    for(n = 0; n < nodelist.length; n++){
        var aircraftID = nodelist[n].id;
        saveAircraftMember(aircraftID);
    }
    location.reload();
}

function saveRoster(){

    var nodelist = document.getElementsByName("roster");
    for(n = 0; n < nodelist.length; n++){
        var positionID = nodelist[n].id;
        saveRosterMember(positionID, <?php echo $sqn;?>);
    }
    location.reload();
}

window.onload = function(){
    gebid("saveAircraftBtn").addEventListener("click", saveAircrafts, false);
    gebid("saveRosterBtn").addEventListener("click", saveRoster, false);
};
</script>
<?php include(dirname(__FILE__).'/squadronMenu.php'); ?> 
<p class="form_id">ACG-PAM/500-200.1</p>
<h3><?php echo $sqn_name; ?>-Members:</h3>
<div>
    <p>This page lists all registered ACG members for <?php echo $sqn_name; ?> with their
    ranks, callsigns, joining-dates, length of service and date of last promotion. Click on any 
    member to access a detailed profile.</p>
</div>
<div class="rosterSetup">
    <form id="rosterAircraft" onsubmit="return false;" >
        <table>
            <thead>
                <tr>
                    <th>Aircraft:</th>
                    <th>Assigned pilot:</th>
                </tr>
            </thead>
            <?php
            foreach ($hangar_array as $aircraftID => $aircraftInfo) {
            ?>
            <tbody>
                <tr>
                    <td><?php echo $aircraftInfo["designation"];?></td>
                    <td><?php createSelectFormWithName("aircraft", $aircraftID, $memberIDArray, $aircraftInfo["memberID"]);?></td>
                </tr>    
            </tbody>
            <?php } ?>
        </table>
        <button id="saveAircraftBtn" >Save changes</button>
        <span id="saveAircraftStatus" ></span>        
    </form>
</div>
<?php
    if($faction == "RAF"){

    } elseif ($faction == "LW") {
        if($sqn == 11){
            include(dirname(__FILE__).'/squadronRosterLWStab.php');
        } else {
            include(dirname(__FILE__).'/squadronRosterLW.php');
        }
    }
?>
<div>
<?php 
switch ($sqn) {
    case 2:
        $link = "../squadrons/no501.php";
        break;
    case 3:
        $link = "../squadrons/no64.php";
        break;
    case 4:
        $link = "../squadrons/no615.php";
        break;
    case 5:
        $link = "../squadrons/4jg26.php";
        break;
    case 6:
        $link = "../squadrons/5jg26.php";
        break;
    case 7:
        $link = "../squadrons/6jg26.php";
        break;
    case 8:
        $link = "../squadrons/7jg26.php";
        break;
    case 9:
        $link = "../squadrons/6zg76.php";
        break;
    case 10:
        $link = "../squadrons/no111.php";
        break;
    case 11:
        $link = "../squadrons/stabjg26.php";
        break;
    case 12:
        $link = "../squadrons/no610.php";
        break;
    case 13:
        $link = "../squadrons/8jg26.php";
        break;
    case 14:
        $link = "../squadrons/no32.php";
        break;
    case 15:
        $link = "../squadrons/no302.php";
        break;
    case 16:
        $link = "../squadrons/9jg26.php";
        break;
}
?>
    <a href="<?php echo $link;?>">TEST SETUP HERE!</a>
</div>
<?php include(dirname(__FILE__).'/footer.php');