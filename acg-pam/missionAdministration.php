<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 10;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT missions.id, missions.name AS miName, missions.realdate, missions.histdate, ".
           "missionstatus.status, campaigns.name ".
           "FROM missions LEFT JOIN campaigns ON missions.campaignid=campaigns.id ".
           "LEFT JOIN missionstatus ON missionstatus.id = missions.missionstatus ". 
           "ORDER BY id DESC LIMIT $start_from, $n_entries";
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM missions";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
    
    // Get all campaigns for seletion form.
    $sql = "SELECT * FROM campaigns ORDER BY id ASC";
    $c_result = mysqli_query($dbx, $sql);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/missionAdminLogic.js"></script>
<script type="text/javascript">
function addMissionLINK(){
    window.location = "addMission.php";
}   

function editMission(id){
    window.location = "editMission.php?mi_id="+id;
} 

window.onload = function(){
    gebid("submitBtn").addEventListener("click", addMission, false);

};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-400.1</p>
<h3>Missions Administration:</h3>
<div>
    <p>These pages list all ACG missions with their name, real- and historic date, 
    associated campaign and status. The real date is the date at which the mission
    was performed as part of a ACG campaign. The historic date is the date at
    which the mission took place in an historic context. Each mission is part of 
    a ACG campaign. The status of the mission has implications on the accessibility
    of mission briefings and After Action Reports (AAR's).</p>
    <p>The mission statuses are:
    <ul>
        <li><b>Briefing:</b> Before mission start. Briefings can only be seen by 
        the associated faction. AARs can not be created or edited.</li>
        <li><b>Debriefing:</b> After mission has ended. Briefings can be seen by 
        all factions. AARs can be created or edited.</li>
        <li><b>Closed:</b> After debriefing period has ended (usually one week after
        the mission has ended). Breifings can be seen by all factions. AARs can 
        not be created or edited.</li>
    </ul>
    </p>
    <p>The EDIT form provides tools for editing the mission parameters, deleting
    the mission and changing the mission status.</p>        
    <hr>
    <h3>Create new mission</h3>
    <form id="newMissionForm" onsubmit="return false;" >
        <div>
            <b>Name:</b>
            <input type="text" id="missionName" name="missionName" size="50" maxlength="50" >
        </div>
        
        <div class='middlePageStandard'>
            <b>Campaign:</b>
            <select id="campaign" name="campaign">
                <?php createSelectOptions($c_result, 1); ?> 
            </select>
        </div>

        <div class='middlePageStandard'>
            <b>Real date (YYYY-MM-DD):</b>
            <input type="text" id="rdateY" name="rdateY" size="4" maxlength="4" >-
            <input type="text" id="rdateM" name="rdateM" size="2" maxlength="2" >-
            <input type="text" id="rdateD" name="rdateD" size="2" maxlength="2" >
        </div>

        <div class='middlePageStandard'>
            <b>Historic date (YYYY-MM-DD hh:mm):</b>
            <input type="text" id="hdateY" name="hdateY" size="4" maxlength="4" >-
            <input type="text" id="hdateM" name="hdateM" size="2" maxlength="2" >-
            <input type="text" id="hdateD" name="hdateD" size="2" maxlength="2" >
            <input type="text" id="hdateH" name="hdateH" size="2" maxlength="2" >:
            <input type="text" id="hdateMM" name="hdateMM" size="2" maxlength="2" >
        </div>

        <div class='middlePageStandard'>
            <button id="submitBtn">Create mission</button>
            <span id="submitStatus">&nbsp;</span>
        </div>

    </form>
    <hr>
</div>
<div>
    <h3>ACG-Missions:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>Name:</th>
            <th>Date:</th>
            <th>Historic Date:</th>
            <th>Campaign:</th>
            <th>Status:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($result)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["miName"];?></td>
            <td><?php echo $row["realdate"];?></td>
            <td><?php echo date("Y-m-d H:i", strtotime($row["histdate"]));?></td>
            <td><?php echo $row["name"];?></td>
            <td><?php echo $row["status"];?></td>
            <td><button onclick="editMission(<?php echo $row['id']; ?>)">EDIT</button></td>
        </tr>
        <?php } ?>
    </table>
</div>

<div class='pageSelect'>
    <?php createPageSelect($n_pages, $page, 'missionAdministration.php?'); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');