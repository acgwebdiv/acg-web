<?php
    include(dirname(__FILE__).'/header0.php');
    include(dirname(__FILE__).'/squadronHeader.php');
    
    $missionDates = array();
    $attArray = array();
    $sql = "SELECT missions.id, missions.realDate FROM missions ".
           "ORDER BY missions.realDate DESC LIMIT 5";
    $result = mysqli_query($dbx, $sql);
    while($row = mysqli_fetch_assoc($result)){
        $missionDates[$row["id"]] = $row["realDate"];
    }
    
    foreach(array_keys($memberArray) as $memberId) {
            
        $sql = "SELECT COUNT(*) FROM reports ".
              "RIGHT JOIN careercharacters ON reports.authorid = careercharacters.id ".
              "RIGHT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
              "WHERE acgmembers.id=$memberId AND reports.accepted=1";
        $result = mysqli_query($dbx, $sql);
        $row = mysqli_fetch_array($result);
        $nreports = $row[0];

        $joiningDate = $memberArray[$memberId]["joiningdate"];
        $sql = "SELECT COUNT(*) FROM missions ".
               "WHERE missions.realDate >= '$joiningDate'";
        $result = mysqli_query($dbx, $sql);
        $row = mysqli_fetch_array($result);
        if($row[0]>0){
            $memberArray[$memberId]["reportsOnDuty"] = $nreports." (".number_format($nreports/$row[0]*100, 0)."%)";
        } else {
            $memberArray[$memberId]["reportsOnDuty"] = $nreports." (0%)";
        }
        
        $sql = "SELECT missions.id, me.name FROM missions ".
               "LEFT JOIN (SELECT acgmembers.id, reports.missionID, squadrons.name ".
               "FROM reports LEFT JOIN squadrons ON reports.squadronID = squadrons.id ".
               "LEFT JOIN careercharacters ON reports.authorID = careercharacters.id ".
               "LEFT JOIN acgmembers ON careercharacters.personifiedBy = acgmembers.id ".
               "WHERE acgmembers.id = $memberId) AS me ".
               "ON missions.id = me.missionID ORDER BY missions.realDate DESC LIMIT 5"; 
        $result = mysqli_query($dbx, $sql);
        while($row = mysqli_fetch_assoc($result)){
            $attArray[$memberId][$row["id"]] = $row["name"];
        }
    }
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">
    
    function editMember(id){
        window.location = "editMember.php?m_id="+id;
    }

</script>
<?php include(dirname(__FILE__).'/squadronMenu.php'); ?> 
<p class="form_id">ACG-PAM/500-301.1</p>
<h3><?php echo $sqn_name; ?>-Members:</h3>
<div>
    <p>This page lists all registered ACG members for <?php echo $sqn_name; ?> with their
    ranks, callsigns, number of sorties (percentage of attended mission while on
    duty) and an overview of each member's attendance of the last 5 missions. 
    Click on any member to access a detailed profile.</p>
</div>
<div>
    <table>
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
        <?php 
            foreach (array_reverse($missionDates, true) as $missionID => $dates) {
        ?>
                <th width="98"><?php echo $missionID;?></th>
        <?php } ?>
            </tr>
            <tr>
                <th>Rank:</th>
                <th>Callsign:</th>
                <th>Sorties:</th>
        <?php 
            foreach (array_reverse($missionDates, true) as $dates) {
        ?>
                <th width="98"><?php echo $dates;?></th>
        <?php } ?>
            </tr>
        </thead>
        <?php
            foreach ($memberArray as $memberID => $memberInfo) {
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["rankAbreviation"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["callsign"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["reportsOnDuty"];?></a></td>
        <?php 
            foreach (array_reverse($missionDates, true) as $miId => $dates) {
        ?>
                <td align="center"><?php echo $attArray[$memberID][$miId];?></td>
        <?php } ?>    
                <td><button onclick="editMember(<?php echo $memberID; ?>)">EDIT</button></td>
            </tr>    
        </tbody>

        <?php } ?>
    </table>
</div>
<div>
    <?php if(count($memberArrayRNR)>0){ ?>
    <p><b>Members on leave:</b></p>
    <table>
        <thead>
            <tr>
                <th>Rank:</th>
                <th>Callsign:</th>
                <th>Joining Date:</th>
                <th>Length of service:</th>
                <th>On leave since:</th>
            </tr>
        </thead>
        <?php
        foreach ($memberArrayRNR as $memberID => $memberInfo) {
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["rankAbreviation"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["callsign"];?></a></td>
                <td><a href="<?php echo($memberInfo["link"]);?>"><?php echo $memberInfo["joiningdate"];?></a></td>
                <td><?php echo lengthOfServiceByMemberID($memberID, $dbx);?></td>
                <td><?php echo lengthOfServiceByDates($memberInfo["lastStatus"],null);?></td>
                <td><button onclick="editMember(<?php echo $memberID; ?>)">EDIT</button></td>
            </tr>    
        </tbody>

        <?php } ?>
    </table>
    <?php } ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');