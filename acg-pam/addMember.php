<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    $dbx = getDBx();
    $sql = "SELECT * FROM squadrons";
    $s_query = mysqli_query($dbx, $sql);
    
    //Loading Osprey's and Pitti's callsign and squadron
    $sql = "SELECT acgmembers.callsign, squadrons.name FROM acgmembers ".
           "LEFT JOIN ".
                "(SELECT transfers.memberid, transfers.squadronid FROM transfers ".
                "JOIN ".
                    "(SELECT transfers.memberid, MAX(UNIX_TIMESTAMP(transfers.transferdate)) AS tdate ".
                    "FROM transfers GROUP BY memberid) AS currentsqu ".
                "ON (currentsqu.memberid, currentsqu.tdate) = ".
                "(transfers.memberid, UNIX_TIMESTAMP(transfers.transferdate))) AS currentsqu2 ".
           "ON currentsqu2.memberid = acgmembers.id ".
           "LEFT JOIN squadrons ON currentsqu2.squadronid = squadrons.id ".
           "WHERE acgmembers.id = 2 OR acgmembers.id = 36";
    $result = mysqli_query($dbx, $sql);
    $hc_result = mysqli_fetch_all($result);
    $ospreyStr = $hc_result[0][1]."_".$hc_result[0][0];
    $pittiStr = $hc_result[1][1]."_".$hc_result[1][0];
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/memberAdminLogic.js"></script>
<script type="text/javascript">

window.onload = function(){
    gebid("membername").addEventListener("change", checkDBforMembername, false);
    gebid("callsign").addEventListener("change", checkDBforCallsign, false);
    gebid("addMemberButton").addEventListener("click", addMember, false);
};

</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?>
<p class="form_id">ACG-PAM/400-110.1</p>

<div>
    <h3>ACG enlistment:</h3>
    <p>Enlistment form for new ACG-members. All fields have to be filled in beforehand
    submission. Member name and callsign will be checked against the database for 
    availability. No duplicate member and/or callsigns are allowed. The member name
    has to be the same as used in the ACG-Forums since the login functions of the forum
    are used for login on the database. The callsign is used in the After Action Reports
    and should be the same as used ingame. Admin rights will grant the member rights
    to create, edit and delete members, campaigns, and missions, comment on and accept 
    After Action Reports, as well as other administrative functions.</p>

    <hr>
    <form id="addNewMemberForm" onsubmit="return false;" >
    <div class="middlePageStandard">
        <b>Member name:</b>
        <input type="text" id="membername" name="membername">
        <span id="mnamestatus" ></span>
        <p>This has to be the same name as used in the ACG-Forums.
        Allowed symbols are letters, numbers and whitespaces.</p>
    </div>
   
    <div class="middlePageStandard">
        <b>Member callsign:</b>
        <input type="text" id="callsign" name="callsign" >
        <span id="callsignstatus" ></span>
        <p>Shorter name for usage in After Action Reports. It should be
        the same callsign as used ingame. Allowed symbols are letters
        and numbers.</p>
    </div>
        
    <div class="middlePageStandard">
        <b>Joining date (YYYY-MM-DD):</b>
        <input type="text" id="jdateY" name="jdateY" size="4" maxlength="4" >-
        <input type="text" id="jdateM" name="jdateM" size="2" maxlength="2" >-
        <input type="text" id="jdateD" name="jdateD" size="2" maxlength="2" >
    </div>
    
    <div class="middlePageStandard">
        <b>Admin rights:</b>
        <input id="admin" name="admin" type="checkbox" value="on" >
        <p>This will grant the member rights to add/edit/edit members, missions,
        etc.</p>
    </div>
    
    <div class="middlePageStandard">
        <b>Assigned squadron/Staffel:</b>
        <select id="squadron" name="squadron">
            <?php
                while($row = mysqli_fetch_assoc($s_query)){
            ?>
            <option value="<?php echo $row["id"];?>"><?php echo $row["faction"]." - ".$row["name"];?></option>
            <?php } ?>   
        </select>
    </div>
    
    <div>
        <b>Joining checklist:</b>
        <table>
            <tr>
                <td><input id="aboutUS" type="checkbox" onchange="jChecklist()"></td>
                <td>Make sure member has read the "about us" thread in the forum,  
                    <i><a href="http://aircombatgroup.co.uk/forum/viewtopic.php?p=10975#p10975">
                    link</a></i>.
                </td>
            </tr>
            <tr>
                <td><input id="commitment" type="checkbox" onchange="jChecklist()"></td>
                <td>Make sure member understands commitments towards ACG, 
                    <i><a href="https://docs.google.com/document/d/1l6QHOXpKnWMia_MkUpEjRCEI0dTfSSDlGyZ-dXiHUDg/pub#h.gx4oq2jvsmag">
                    see Field Book Chapter 2</a></i>. No competition, regular Sunday attendance,
                    AAR submission, information on periods of absence.
                </td>
            </tr>
            <tr>
                <td><input id="informHC" type="checkbox" onchange="jChecklist()"></td>
                <td>New member has to add <?php echo $ospreyStr;?> and <?php echo $pittiStr;?> as friends in Steam.</td>
            </tr>
            <tr>
                <td><input id="changeSteamName" type="checkbox" onchange="jChecklist()"></td>
                <td>Instruct member to change his name to SQN_CALLSIGN in TeamSpeak and Steam (i.e. No.1_Adam).</td>
            </tr>
            <tr>
                <td><input id="fieldBook" type="checkbox" onchange="jChecklist()"></td>
                <td>Give member the link to the ACG Field Book, 
                    <i><a href="https://docs.google.com/document/d/1l6QHOXpKnWMia_MkUpEjRCEI0dTfSSDlGyZ-dXiHUDg/pub">
                    link</a></i>. He should read especially the parts about the campaign rules and procedures, as well
                    as the instructions for the AAR's and claiming rules.
                </td>
            </tr>
            <tr>
                <td><input id="informCO" type="checkbox" onchange="jChecklist()"></td>
                <td>Inform the OC of the squadron about the new member and write a welcome post in the forum,
                    best in the ACG-Briefing room.
                </td>
            </tr>
        </table>
        

    </div>
    <div id="submitStatus"  class="middlePageStandard">&nbsp;</div>
    <button type=button id="addMemberButton" disabled="true">Add Member</button>
</form>

</div>
<?php include(dirname(__FILE__).'/footer.php'); ?>