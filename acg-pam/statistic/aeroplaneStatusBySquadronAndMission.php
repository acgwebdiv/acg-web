<?php

    // Choosing the status to display: 1=OK, 2=damaged, 3=lost
//    if(filter_has_var(INPUT_GET, "aStatus")) {
//        $aStatus = filter_input(INPUT_GET, "aStatus");
//    } else {
//        header("location: statisticsList.php");
//    }
    
    switch($selectedSubject){
        case 2:
            $aStatus = 1;
            $claimString = "returned undamaged";
            $claimString2 = "undamaged";
            break;
        case 3:
            $aStatus = 2;
            $claimString = "returned damaged";
            $claimString2 = "damaged";
            break;
        case 4:
            $aStatus = 3;
            $claimString = "were lost";
            $claimString2 = "lost";
            break;

    }

?>

<h3>Number of aircraft that <?php echo $claimString; ?> per squadron:</h3>
<div>
    <p>This page shows the number of aircraft that <?php echo $claimString; ?> for each 
    squadron and mission as well as total numbers of aircraft that <?php echo $claimString; ?>
    for each squadron and campaign and the overall sum of aircraft that <?php echo $claimString; ?>
    for each squadron. The number in brackets indicates the percentage of <?php echo $claimString2; ?>
    aircraft in relation to number of sorties for each mission, campaign and total
    number of sorties respectively.</p>
    <p>Click on the number of <?php echo $claimString; ?> aircraft for any squadron 
    in the tables to get to the report overview page for the particular mission 
    and squadron. Click on the mission name in any table to get to the mission details
    page of that particular mission.</p>
</div>
<?php

$sql = "SELECT campaigns.id, campaigns.name FROM `campaigns` WHERE 1 ORDER BY id ASC";
$cresult = mysqli_query($dbx, $sql);
$totalSorties = array();
$totalStatus = array();

while($crow = mysqli_fetch_assoc($cresult)){
    
$campaignID = $crow["id"];
$cname = $crow["name"];

$sql = "SELECT squadrons.id AS sid, squadrons.name, ".
       "missionID, missions.name AS mname, ".
       "missions.campaignID, count(reports.id) AS sorties, sum( `aeroplaneStatus` = $aStatus) AS sCount ". 
       "FROM `reports` ".
       "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ". 
       "LEFT JOIN missions ON reports.missionID = missions.id ".
       "LEFT JOIN campaigns ON missions.campaignID = campaigns.id ".
       "WHERE reports.accepted = 1 AND missions.campaignID = $campaignID ". 
       "GROUP BY squadrons.name, missionID ".
       "ORDER BY missionID ASC, squadrons.faction DESC, squadrons.rank ASC";
$result = mysqli_query($dbx, $sql);

$totalCampaignSorties = array();
$totalCampaignStatus = array();
$campaignStatus = array();

while($row = mysqli_fetch_assoc($result)){

    $sname = $row["name"];
    $s_id = $row["sid"];
    $mname = $row["mname"];
    $m_id = $row["missionID"];
    $sorties = $row["sorties"];
    $sCount = $row["sCount"];
    
    if(!array_key_exists($sname, $totalCampaignSorties)){
        $totalCampaignSorties[$sname] = 0;
        $totalCampaignStatus[$sname] = 0;
    }
    if(!array_key_exists($sname, $totalSorties)){
        $totalSorties[$sname] = 0;
        $totalStatus[$sname] = 0;
    }
    
    $totalCampaignSorties[$sname] += $sorties;
    $totalCampaignStatus[$sname] += $sCount;
    $campaignStatus[$mname][$sname] = "<a href='missionReports.php?m_id=$m_id&sqn=$s_id'>"
            . "$sCount (".number_format($sCount/$sorties,2)*100 ."%)</a>";
    $totalSorties[$sname] += $sorties;
    $totalStatus[$sname] += $sCount;

}

$sql = "SELECT DISTINCT squadrons.name ".
       "FROM `reports` ".
       "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ". 
       "LEFT JOIN missions ON reports.missionID = missions.id ".
       "WHERE reports.accepted = 1 AND missions.campaignID = $campaignID ". 
       "ORDER BY squadrons.faction DESC, squadrons.rank ASC";
$sresult = mysqli_query($dbx, $sql);
$sarray = mysqli_fetch_all($sresult);

$sql = "SELECT DISTINCT missions.id, missions.name, missions.realDate ". 
       "FROM `reports` ".
       "LEFT JOIN missions ON reports.missionID = missions.id ".
       "WHERE reports.accepted = 1 AND missions.campaignID = $campaignID ". 
       "ORDER BY missionID ASC";
$mresult = mysqli_query($dbx, $sql);

?>
<script type="text/javascript">

    $(document).ready( function () {
        $('#statsTable' + "<?php echo $campaignID; ?>").DataTable({
            scrollX: "900px",
            columnDefs: [
            { width: "200px", "targets": 1 }
            ],
            fixedColumns: {
                leftColumns: 2
            } 
        });
    } );
</script>
<h3><?php echo $cname;?></h3>
<div>
    <table id="<?php echo("statsTable".$campaignID);?>">
        <thead>
            <td>Date</td>
            <td>Mission name</td>
            <?php foreach($sarray as $sname){ ?>
            <td><?php echo $sname[0]; ?></td>
            <?php } ?>
        </thead>
        <?php // foreach($marray as $mname) { ?>
        <?php while($row = mysqli_fetch_assoc($mresult)){ 
            $m_id = $row["id"];
            $m_name = $row["name"];
            $m_link = "<a href='missionDetails.php?m_id=$m_id'>$m_name</a>";
        ?>
        <tr>
            <td><?php echo $row["realDate"]; ?></td>
            <td><?php echo $m_link; ?></td>
            <?php foreach($sarray as $sname){ 
                    if(!array_key_exists($sname[0], $campaignStatus[$row["name"]])){
            ?>
                <td>-</td>
            <?php
                    }else{
            ?>
                <td><?php echo $campaignStatus[$row["name"]][$sname[0]]; ?></td>
            <?php 
                    }
            }
         } ?>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(document).ready( function () {
        $('#totalsTable' + "<?php echo $campaignID; ?>").DataTable({
            scrollX: "900px",
            paging: false,
            searching: false,
            ordering: false
        });
    } );
    
</script>  
<div>
    <h3><?php echo($cname." total sorties");?></h3>
    <table id="<?php echo("totalsTable".$campaignID);?>">
        <thead>
            <?php foreach($sarray as $sname){ ?>
            <td><?php echo $sname[0]; ?></td>
            <?php } ?>
        </thead>
        <tr>
            <?php foreach($sarray as $sname){ 
                    if(!array_key_exists($sname[0], $totalCampaignStatus)){
            ?>
                <td>-</td>
            <?php
                    }else{
            ?>
                <td style="white-space: nowrap;">
                    <?php 
                    $sorties = $totalCampaignSorties[$sname[0]];
                    $sCount = $totalCampaignStatus[$sname[0]];
                    echo($sCount." (".number_format($sCount/$sorties,2)*100 ."%)");
                    ?>
                </td>
            <?php 
                    }
            }?>
        </tr>
    </table>
</div>
&nbsp;
<hr>
<?php } ?>
<script type="text/javascript">
    $(document).ready( function () {
        $('#totalsTable').DataTable({
            scrollX: "900px",
            paging: false,
            searching: false,
            ordering: false
        });
    } );
    
</script>  
<div>
    <h3><?php echo("Total sorties");?></h3>
    <table id="<?php echo("totalsTable");?>">
        <thead>
            <?php foreach($sarray as $sname){ ?>
            <td><?php echo $sname[0]; ?></td>
            <?php } ?>
        </thead>
        <tr>
            <?php foreach($sarray as $sname){ 
                    if(!array_key_exists($sname[0], $totalSorties)){
            ?>
                <td>-</td>
            <?php
                    }else{
            ?>
                <td style="white-space: nowrap;">
                    <?php 
                    $sorties = $totalSorties[$sname[0]];
                    $sCount = $totalStatus[$sname[0]];
                    echo($sCount." (".number_format($sCount/$sorties,2)*100 ."%)");
                    ?>
                </td>
            <?php 
                    }
            }?>
        </tr>
    </table>
</div>