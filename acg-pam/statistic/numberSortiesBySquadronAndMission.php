<h3>Number of sorties per squadron/Staffel:</h3>
<div>
    <p>This page shows the number of sorties for each squadron and mission as well
    as total numbers of sorties for each squadron and campaign and the overall
    sum of sorties for each squadron.</p>
    <p>Click on the number of sorties for any squadron in the tables to get to the
    report overview page for the particular mission and squadron. Click on the mission
    name in any table to get to the mission details page of that particular mission.</p>
</div>
<?php

$sql = "SELECT campaigns.id, campaigns.name FROM `campaigns` WHERE 1 ORDER BY id ASC";
$cresult = mysqli_query($dbx, $sql);
$totalSorties = array();

while($crow = mysqli_fetch_assoc($cresult)){
    
$campaignID = $crow["id"];
$cname = $crow["name"];

$sql = "SELECT squadrons.id AS sid, squadrons.name, squadrons.faction, ".
       "missionID, missions.name AS mname, ".
       "missions.campaignID, count(reports.id) AS sorties ". 
       "FROM `reports` ".
       "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ". 
       "LEFT JOIN missions ON reports.missionID = missions.id ".
       "LEFT JOIN campaigns ON missions.campaignID = campaigns.id ".
       "WHERE reports.accepted = 1 AND missions.campaignID = $campaignID ". 
       "GROUP BY squadrons.name, missionID ".
       "ORDER BY missionID ASC, squadrons.faction DESC, squadrons.rank ASC";
$result = mysqli_query($dbx, $sql);

$totalCampaignSorties = array();
$campaignSorties = array();


while($row = mysqli_fetch_assoc($result)){

    $sname = $row["name"];
    $s_id = $row["sid"];
    $mname = $row["mname"];
    $m_id = $row["missionID"];
    $faction = $row["faction"];
    $sorties = $row["sorties"];
  
    
    if(!array_key_exists($sname, $totalCampaignSorties)){
        $totalCampaignSorties[$sname] = 0;
    }
    if(!array_key_exists($faction, $totalCampaignSorties)){
        $totalCampaignSorties[$faction] = 0;
    }
    if(!array_key_exists($sname, $totalSorties)){
        $totalSorties[$sname] = 0;
    }
    if(!array_key_exists($faction, $totalSorties)){
        $totalSorties[$faction] = 0;
    }
        
    $totalCampaignSorties[$sname] += $sorties;
    $campaignSorties[$mname][$sname] = "<a href='missionReports.php?m_id=$m_id&sqn=$s_id'>$sorties</a>";
    $totalSorties[$sname] += $sorties;
    
    if(!array_key_exists($faction, $campaignSorties[$mname])){
        $campaignSorties[$mname][$faction] = 0;
    }
    
    $totalCampaignSorties[$faction] += $sorties;
    $campaignSorties[$mname][$faction] += $sorties;
    $totalSorties[$faction] += $sorties;

}

$sql = "SELECT DISTINCT squadrons.name ".
       "FROM `reports` ".
       "LEFT JOIN squadrons ON reports.squadronID = squadrons.id ". 
       "LEFT JOIN missions ON reports.missionID = missions.id ".
       "WHERE reports.accepted = 1 AND missions.campaignID = $campaignID ". 
       "ORDER BY squadrons.faction DESC, squadrons.rank ASC";
$sresult = mysqli_query($dbx, $sql);
$sarray = mysqli_fetch_all($sresult);

$sql = "SELECT DISTINCT missions.id, missions.name, missions.realDate ". 
       "FROM `reports` ".
       "LEFT JOIN missions ON reports.missionID = missions.id ".
       "WHERE reports.accepted = 1 AND missions.campaignID = $campaignID ". 
       "ORDER BY missionID ASC";
$mresult = mysqli_query($dbx, $sql);

?>
<script type="text/javascript">

    $(document).ready( function () {
        $('#statsTable' + "<?php echo $campaignID; ?>").DataTable({
            scrollX: "900px",
            
            columnDefs: [
            { width: "200px", "targets": 1 }
            ],
            fixedColumns: {
                leftColumns: 2
            }
        });
    } );
</script>
<h3><?php echo $cname;?></h3>
<div>
    <table id="<?php echo("statsTable".$campaignID);?>">
        <thead>
            <td>Date</td>
            <td>Mission name</td>
            <td>RAF</td>
            <td>LW</td>
            <?php foreach($sarray as $sname){ ?>
            <td><?php echo $sname[0]; ?></td>
            <?php } ?>
        </thead>
        <?php // foreach($marray as $mname) { ?>
        <?php while($row = mysqli_fetch_assoc($mresult)){ 
            $m_id = $row["id"];
            $m_name = $row["name"];
            $m_link = "<a href='missionDetails.php?m_id=$m_id'>$m_name</a>";
        ?>
        <tr>
            <td><?php echo $row["realDate"]; ?></td>
            <td><?php echo $m_link; ?></td>
            <td><?php echo $campaignSorties[$row["name"]]["RAF"]; ?></td>
            <td><?php echo $campaignSorties[$row["name"]]["LW"]; ?></td>
            <?php foreach($sarray as $sname){ 
                    if(!array_key_exists($sname[0], $campaignSorties[$row["name"]])){
            ?>
                <td>-</td>
            <?php
                    }else{
            ?>
                <td><?php echo $campaignSorties[$row["name"]][$sname[0]]; ?></td>
            <?php 
                    }
            }
         } ?>
        </tr>
    </table>
</div>
<script type="text/javascript">
    $(document).ready( function () {
        $('#totalsTable' + "<?php echo $campaignID; ?>").DataTable({
            scrollX: "900px",
            paging: false,
            searching: false,
            ordering: false
        });
    } );
    
</script>  
<div>
    <h3><?php echo($cname." total sorties");?></h3>
    <table id="<?php echo("totalsTable".$campaignID);?>">
        <thead>
            <td>RAF</td>
            <td>LW</td>
            <?php foreach($sarray as $sname){ ?>
            <td><?php echo $sname[0]; ?></td>
            <?php } ?>
        </thead>
        <tr>
            <td><?php echo $totalCampaignSorties["RAF"]; ?></td>
            <td><?php echo $totalCampaignSorties["LW"]; ?></td>
            <?php foreach($sarray as $sname){ 
                    if(!array_key_exists($sname[0], $totalCampaignSorties)){
            ?>
                <td>-</td>
            <?php
                    }else{
            ?>
                <td><?php echo $totalCampaignSorties[$sname[0]]; ?></td>
            <?php 
                    }
            }?>
        </tr>
    </table>
</div>
&nbsp;
<hr>
<?php } ?>
<script type="text/javascript">
    $(document).ready( function () {
        $('#totalsTable').DataTable({
            scrollX: "900px",
            paging: false,
            searching: false,
            ordering: false
        });
    } );
    
</script>  
<div>
    <h3><?php echo("Total sorties");?></h3>
    <table id="<?php echo("totalsTable");?>">
        <thead>
            <td>RAF</td>
            <td>LW</td>
            <?php foreach($sarray as $sname){ ?>
            <td><?php echo $sname[0]; ?></td>
            <?php } ?>
        </thead>
        <tr>
            <td><?php echo $totalSorties["RAF"]; ?></td>
            <td><?php echo $totalSorties["LW"]; ?></td>
            <?php foreach($sarray as $sname){ 
                    if(!array_key_exists($sname[0], $totalSorties)){
            ?>
                <td>0</td>
            <?php
                    }else{
            ?>
                <td><?php echo $totalSorties[$sname[0]]; ?></td>
            <?php 
                    }
            }?>
        </tr>
    </table>
</div>