<?php
    include(dirname(__FILE__).'/header0.php');
    include(dirname(__FILE__).'/includes/characterDBFunctions.php');

    $dbx = getDBx();
    
    // Check if existing user otherwise redirect
    if(!isset($_SESSION["userID"])){
        header("location: message.php?m=1");
        exit();
    } else {
        $user_id = $_SESSION["userID"];
    }
    
    if(isset($_SESSION["designatedCharacter"])){
        $c_id = $_SESSION["designatedCharacter"];
    } else {
        header("location: message.php?m=4");
        exit();
    }
    
    if(filter_has_var(INPUT_GET, "mi_id")){
        $mi_id = filter_input(INPUT_GET, "mi_id");
    } else {
        header("location: missionList.php");
        exit();
    }
    
    if(filter_has_var(INPUT_GET, "isReport")){
        $isReport = filter_input(INPUT_GET, "isReport");
    } else {
        header("location: missionList.php");
        exit();
    }
    
    $sqn = 0;
    if(filter_has_var(INPUT_GET, "s")){
        $sqn = filter_input(INPUT_GET, "s");
    }
    

    $sql = "SELECT firstname, lastname FROM careercharacters ".
           "WHERE id = $c_id";
    $query = mysqli_query($dbx, $sql);
    $c_result = mysqli_fetch_assoc($query);
    $firstName = $c_result["firstname"];
    $lastName = $c_result["lastname"];
    
    $faction = getFaction($user_id, $dbx);
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/characterLogic.js"></script>
<script src="jscript/reportLogic.js"></script>
<script src="jscript/dispersalLogic.js"></script>
<script type="text/javascript">
    
    
    
window.onload = function(){
    
};

</script>
<?php include(dirname(__FILE__).'/emptyMenu.php'); ?>
<p class="form_id">ACG-PAM/300-210.1</p>
<div class="query">
    Your current character, <?php echo($firstName." ".$lastName); ?>, flew already
    in a previous campaign. Do you want to continue with this character in this 
    campaign? <br>
    <?php echo($firstName." ".$lastName); ?> will be set to dismissed, if you select
    to create a new character.
</div>
<div class="query">
    <form id="afterActionReportRAF" onsubmit="return false;" >
        <span><button onclick="newCharacterQueryAnswer(<?php echo('1, '.$user_id.', '.$c_id.', '.$mi_id.', '.$isReport.
                ', '.$sqn.', \''.$faction.'\''); ?>)">Create a new character</button></span>
        <span><button onclick="newCharacterQueryAnswer(<?php echo('0, '.$user_id.', '.$c_id.', '.$mi_id.', '.$isReport.
                ', '.$sqn.', \''.$faction.'\''); ?>)">Continue with <?php echo($firstName." ".$lastName); ?></button></span>
    </form>
</div>
<div id="submitStatus">&nbsp;</div>
<?php include(dirname(__FILE__).'/footer.php');
