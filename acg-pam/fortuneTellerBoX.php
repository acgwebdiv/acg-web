<?php
    include(dirname(__FILE__).'/header0.php'); 
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');

    $dbx = getDBx();
    
    if(filter_has_var(INPUT_GET, "m_id")){
        $mi_id = filter_input(INPUT_GET, "m_id");
    } else {
        header("location: missionList.php");
        exit();
    }
    
    // Check if existing user otherwise redirect
    if(!isset($_SESSION["userID"])){
        header("location: message.php?m=1");
        exit();
    }
    $user_id = $_SESSION["userID"];
    $faction = getFaction($user_id, $dbx);
    
    // Get mission and game start times:
    $sql = "SELECT * FROM BoX_Character_Status WHERE id = $mi_id";
    $result = mysqli_query($dbx, $sql);
    
    
   
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script type="text/javascript">
</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>

<p class="form_id">ACG-PAM/100-001.1</p>
<div id="frontPageContainer">
    
    <p>This list gives an overview over the fate of all pilots that took part in 
    this mission.
    A pilot that chrashlands or parachutes into enemy territory will try to walk
    home to frindly lines on the shortest distance. He will walk a distance of appr.
    5 km per hour. About every 12 minutes a check is performed that determines if
    the pilot died or was captures.
    The probability to make it to friendly lines is appr. 50/50 for a distance of
    30 km. Injury and cold weather decrease the chances.
    Capture is determined on how close the pilot is to enemy troups. The closer
    enemy troups, the higher the chances for capture. A distance of 2 km to enemy
    troups will resulst in a 50/50 chance. Night hours increase the chances
    for escape.
    VVS troops that escape enemy territory have an additional risk of being send
    to the Gulag being suspected a german spy.
    Although the fate of the pilots are calculated instantaneously, the results will
    be displayed with a real life delay. A pilot still trying to escape will show
    "Alive and walking". For example: Should a pilot need to walk 5 hours to reach
    friendly lines, his status will show "Alive and walking" for those 5 hours. Unless
    he dies or is captured along the way.</p>
    
    <table class="wideTable">
        <tr>
            <th>Member:</th>
            <th>Flew as:</th>
            <th>Flight Number:</th>
            <th>Status:</th>
        </tr>
        <?php
            $username = "";
            $printname = "";
            while($row = mysqli_fetch_assoc($result)) {
                
                if($row["username"] == $username){
                    $printname = " ";
                } else {
                    $username = $row["username"];
                    $printname = $username;
                }
        ?>
        <tr>
            <td><?php echo $printname;?></td>
            <td><?php echo $row["servername"];?></td>
            <td><?php echo $row["FlightNumber"];?></td>
            <td><?php echo $row["ch_status"];?></td>
        </tr>
        <?php } ?>
    </table>    
    
</div>       
<?php include(dirname(__FILE__).'/footer.php');