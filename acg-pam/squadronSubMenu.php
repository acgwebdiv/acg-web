<?php
$containerWidth = max(array(900, mysqli_num_rows($smenu_result)*95));
?>
<div id="squadronScrollMenu" class='flexcroll'>
    <div class="squadronScrollContainter" style="width: <?php echo $containerWidth; ?>px;">
        <ul class='squadronMenu'>
        <?php
            while($s_row = mysqli_fetch_assoc($smenu_result)) { ?>
            <li><a href="<?php echo($link);?>sqn=<?php echo($s_row["id"]); ?>">
            <?php echo($s_row["name"]); ?></a></li>
        <?php  }?>
        </ul>
    </div>
</div>