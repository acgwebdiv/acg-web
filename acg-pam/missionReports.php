<?php
    include(dirname(__FILE__).'/header0.php');
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")){
        $mi_id = filter_input(INPUT_GET, "m_id");  
    }
    if(filter_has_var(INPUT_GET, "sqn")){
        $sqn_id = filter_input(INPUT_GET, "sqn");  
    }
    $sql = "SELECT missions.name AS m_name, missions.histdate, missions.realdate, ".
           "missions.missionstatus, campaigns.name AS c_name, missionstatus.status AS statusstr ".
           "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ".
           "LEFT JOIN missionstatus ON missionstatus.id = missions.missionstatus ". 
           "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_status = $m_row["missionstatus"];
    $m_faction = getFaction($_SESSION["userID"], $dbx);
    $mi_realDate = $m_row["realdate"];
        
    //Report information about author
    $sql = "SELECT reports.id AS report_id, reports.squadronid, squadrons.name AS s_name, ".
           "squadrons.faction, reports.type, reports.datesubmitted, ranks.abreviation, ranks.value, ".
           "careercharacters.firstname, careercharacters.lastname, acgmembers.callsign, ".
           "adjutants.accepted, adjutants.callsign AS adjutant ". 
           "FROM reports LEFT JOIN careercharacters ON reports.authorid = careercharacters.id ".
           "LEFT JOIN acgmembers ON careercharacters.personifiedby = acgmembers.id ".
           "LEFT JOIN squadrons ON squadrons.id = reports.squadronid ".
            "LEFT JOIN ".
                "(SELECT promotions.memberid, promotions.value FROM promotions ".
                "JOIN ".
                    "(SELECT promotions.memberid, promotions.date, MAX(UNIX_TIMESTAMP(promotions.date)) AS pdate ".
                    "FROM promotions WHERE date <= '$mi_realDate' GROUP BY memberid) AS currentrank ".
                "ON (currentrank.memberid, currentrank.pdate) = ".
                "(promotions.memberid, UNIX_TIMESTAMP(promotions.date))) AS currentrank2 ".
           "ON currentrank2.memberid = acgmembers.id ".
           "LEFT JOIN ranks ON (currentrank2.value, squadrons.faction) = (ranks.value, ranks.faction) ".
           "LEFT JOIN ".
                "(SELECT reports.id, reports.accepted, acgmembers.callsign ".
                "FROM reports LEFT JOIN acgmembers ON acgmembers.id = reports.acceptedby) AS adjutants ".
           "ON adjutants.id = reports.id ".
           "WHERE reports.missionid = $mi_id AND reports.squadronid = $sqn_id ".
           "ORDER BY ranks.value DESC";
    $r_result = mysqli_query($dbx, $sql);
    
    //Report information about accetping adjutant
//    $sql = "SELECT reports.id, reports.accepted, acgmembers.callsign ".
//           "FROM reports LEFT JOIN acgmembers ON acgmembers.id = reports.acceptedby ".
//           "WHERE reports.missionid = $mi_id AND reports.squadronid = $sqn_id ".
//           "ORDER BY id ASC";
////    echo $sql;
//    $a_result = mysqli_query($dbx, $sql);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script src="jscript/dispersalLogic.js"></script>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>
<p class="form_id">ACG-PAM/300-200.1</p>
<div>
    <h3><?php echo($m_row["m_name"]." (".$m_row["histdate"].")"); ?></h3>
    <p>
        <?php echo($m_row["m_name"]." (".$m_row["histdate"].")"); ?>,
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date:
         <?php echo($m_row["realdate"]); ?>.
    </p>
    <p>
        Current mission status: <?php echo($m_row["statusstr"]); ?>
    </p>
</div>
<!--<div>-->
<!--    <form onsubmit="return false;" >-->
<!--        <button -->
<!--            onclick="enterDispersal(<?php echo($mi_id.", ".$sqn_id); ?>)">-->
<!--            View <?php //echo $s_row["name"];?> dispersal.</button>-->
<!--    </form>-->
<!--</div>-->
<div>
    <h3>After action reports:</h3>
    <table>
        <tr>
            <th></th>
            <th>Pilot:</th>
            <th>Submission date:</th>
            <th>Submission status:</th>
            <th>Approved by:</th>
        </tr>
        <?php
            while($r_row = mysqli_fetch_assoc($r_result)) {
//                $a_row = mysqli_fetch_assoc($a_result);
                if($r_row["type"]==1) {
                    $link = "reportRAF.php?r_id=";
                } elseif ($r_row["type"]==2) {
                    $link = "reportLW.php?r_id=";
                } elseif ($r_row["type"]==3) {
                    $link = "reportVVS.php?r_id=";
                } else {
                    $link = "#?r_id=";
                }
        ?>
        <tr>           
            <td><a href="<?php echo($link.$r_row["report_id"]);?>"><?php echo($r_row["abreviation"]);?></a></td>
            <td><a href="<?php echo($link.$r_row["report_id"]);?>"><?php echo($r_row["firstname"]." '".$r_row["callsign"]."' ".$r_row["lastname"]);?></a></td>
            <td><a href="<?php echo($link.$r_row["report_id"]);?>"><?php echo $r_row["datesubmitted"];?></a></td>
            <td><a href="<?php echo($link.$r_row["report_id"]);?>"><?php echo($r_row["accepted"] ? "Approved" : "Pending");?></a></td>
            <td><a href="<?php echo($link.$r_row["report_id"]);?>"><?php echo $r_row["adjutant"];?></a></td>
        </tr>
        <?php } ?>
    </table>
</div>
<?php include(dirname(__FILE__).'/footer.php');