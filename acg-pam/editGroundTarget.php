<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Accessing ground target data from database for editing.
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "t_id")) {
        
        $edit_id = filter_input(INPUT_GET, "t_id");
        $_SESSION["edit_id"] = $edit_id;
        $sql = "SELECT name FROM groundtargets WHERE id = $edit_id";
        $query = mysqli_query($dbx, $sql);
        $result = mysqli_fetch_assoc($query);
        $name = $result["name"];
    } 
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/targetAdminLogic.js"></script>
<script type="text/javascript">

window.onload = function(){
    gebid("changeGroundTarget").addEventListener("click", changeGroundTarget, false);
    gebid("deleteGroundTarget").addEventListener("click", deleteGroundTarget, false);
};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-510.1</p>
<h3>Edit ground target ( ID: <?php echo $_SESSION["edit_id"]; ?> ): </h3>
<form id="editGroundTarget" onsubmit="return false;" >
    <div class='middlePageStandard'>
        <b>Ground target name:</b>
        <input type="text" id="groundTargetName" name="groundTargetName" value="<?php echo $name; ?>" size="50" maxlength="50">
        <p>The name of the ground target.</p>
    </div>
    
    <div class='middlePageStandard'>
        <button id="changeGroundTarget">Save Change</button><br>
        <span id="gtChangeStatus" ></span>
    </div>
    
    <div class='middlePageStandard'>
        <p><button id="deleteGroundTarget">Delete</button> Deletes ground targeted
        from database. Only ground targets that have not been selected in claims 
        should be deleted from the database for consistency.</p>
    </div>
</form>
<?php include(dirname(__FILE__).'/footer.php');
