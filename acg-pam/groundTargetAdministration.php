<?php
    include(dirname(__FILE__).'/header0.php');
    
    if(isset($_SESSION["admin"])){
        if(!$_SESSION["admin"]){
            echo $_SESSION["admin"];
            header("location: message.php?m=1");
            exit();
        }
    } else {
        
        header("location: message.php?m=1");
            exit();
    }
    
    // Setting up indices for spreading conten over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 10;
    $start_from = ($page - 1)*$n_entries;
    $dbx = getDBx();
    $sql = "SELECT * FROM groundtargets ORDER BY name ASC ".
           "LIMIT $start_from, $n_entries";
    $result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT COUNT(id) FROM groundtargets";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/targetAdminLogic.js"></script>
<script type="text/javascript">

function editTarget(id){
    window.location = "editGroundTarget.php?t_id="+id;
} 

window.onload = function(){
    gebid("addGroundTargetButton").addEventListener("click", addGroundTarget, false);

};   
</script>
<?php include(dirname(__FILE__).'/adminMenu.php'); ?> 
<p class="form_id">ACG-PAM/400-500.1</p>

<h2>Ground target administration:</h2>
<div>
    <p>These pages list all ground targets that can be claimed in the AARs.</p>
    <p>The EDIT form provides tools for editing the name of the ground target.</p>        
    <hr>
    <h3>Create new ground target:</h3>
    <form id="addnewGroundTargetForm" onsubmit="return false;">
        <div class='middlePageStandard'>
            <b>Ground target:</b>
            <input type="text" id="groundTargetName" name="groundTargetName" size="50" maxlength="50">
            <p>The name of the ground target.</p>
        </div>
        
        <div class='middlePageStandard'>
            <button id="addGroundTargetButton">Create Ground Target</button>
            <span id="addGroundTargetStatus" ></span>
        </div>
    </form>
    <hr>
</div>

<div>
    <h3>Ground targets:</h3>
    <table>
        <tr>
            <th>ID:</th>
            <th>Name:</th>
        </tr>
        <?php
            while($row = mysqli_fetch_assoc($result)) { 
        ?>
        <tr>
            <td><?php echo $row["id"];?></td>
            <td><?php echo $row["name"];?></td>
            <td><button onclick="editTarget(<?php echo $row['id']; ?>)">EDIT</button></td>
        </tr>
        <?php } ?>
    </table>
</div>

<div class="pageSelect">
    <?php createPageSelect($n_pages, $page, 'groundTargetAdministration.php?'); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');