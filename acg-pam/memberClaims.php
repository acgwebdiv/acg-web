<?php include(dirname(__FILE__).'/header0.php'); 

    $member_id = filter_input(INPUT_GET, "m_id");
    $dbx = getDBx();
    
    // Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
    } else {
        $page = 1;
    }
    $n_entries = 20;
    $start_from = ($page - 1)*$n_entries;
    
    $sql = "SELECT COUNT(cltable.id) FROM ".
           "((SELECT id, reportid FROM claimslw WHERE claimslw.accepted = 1) ".
           "UNION ".
           "(SELECT id, reportid FROM claimsvvs WHERE claimsvvs.accepted = 1) ".
           "UNION ".
           "(SELECT id, reportid FROM claimsraf WHERE claimsraf.accepted = 1))".
           "AS cltable ".
           "LEFT JOIN reports ON reports.id = cltable.reportid ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id  = $member_id AND reports.accepted=1";
    $n_ID_result = mysqli_query($dbx, $sql);
    $n_ID_row = mysqli_fetch_row($n_ID_result);
    $n_ID = $n_ID_row[0];
    $n_pages = ceil($n_ID / $n_entries);
    
    
    $sql = "SELECT cltable.id, cltable.reportid, cltable.clstatus, aeroplanes.name, ".
           "reports.missionid, reports.type, cltable.shared, cltable.groupcl ".
           "FROM ".
           "((SELECT claimslw.id, claimslw.reportid, claimslw.aeroplane, claimslw.confirmed AS clstatus, ".
           " 0 AS shared, 0 AS groupcl FROM claimslw WHERE claimslw.accepted = 1) ".
           "UNION ".
           "(SELECT claimsvvs.id, claimsvvs.reportid, claimsvvs.aeroplane, claimsvvs.confirmed AS clstatus, ".
           " 0 AS shared, claimsvvs.groupClaim AS groupcl FROM claimsvvs WHERE claimsvvs.accepted = 1) ".
           "UNION ".
           "(SELECT claimsraf.id, claimsraf.reportid, claimsraf.aeroplane, claimstatusraf.status AS clstatus, ".
           " claimsraf.shared AS shared, 0 AS groupcl ".
           " FROM claimsraf LEFT JOIN claimstatusraf ON claimstatusraf.id = enemystatus ".
           " WHERE claimsraf.accepted = 1)) ".
           "AS cltable ".
           "LEFT JOIN aeroplanes ON aeroplanes.id = cltable.aeroplane ".
           "LEFT JOIN reports ON reports.id = cltable.reportid ".
           "LEFT JOIN careercharacters ON careercharacters.id = reports.authorid ".
           "LEFT JOIN acgmembers ON acgmembers.id = careercharacters.personifiedby ".
           "WHERE acgmembers.id  = $member_id AND reports.accepted=1 ".
           "ORDER BY reportid, clstatus ASC LIMIT $start_from, $n_entries";
    // echo $sql;
    $cl_result = mysqli_query($dbx, $sql);
    
    $sql = "SELECT callsign FROM acgmembers WHERE id = $member_id";
    $cresult = mysqli_query($dbx, $sql);
    $crow = mysqli_fetch_row($cresult);
    $callsign = $crow[0];
?>    
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/memberMenu.php'); ?> 
<p class="form_id">ACG-PAM/200-221.1</p>
<h3>Member profile:</h3>
<div>
    <p>These pages show aerial victory claims submitted by <?php echo $callsign;?>.
    Click on any claim to access the full detailed After Action Report containing the claim.
    The displayed information is based on submitted and approved After Action Reports.</p>
    
    <h3>Claims:</h3>
    <table>
        <thead>
            <tr>
                <th>ID:</th>
                <th>Mission:</th>
                <th>Aeroplane:</th>
                <th>Status:</th>
            </tr>
        </thead>
        <?php
            while($row = mysqli_fetch_assoc($cl_result)) {
                if($row["type"]==1){
                    $link = "reportRAF.php?r_id=".$row["reportid"];
                    if($row["shared"]==1){
                        $shared = " shared";
                    } else {
                        $shared ="";
                    }
                } elseif($row["type"]==2){
                    $link = "reportLW.php?r_id=".$row["reportid"];
                    
                } elseif($row["type"]==3){
                    $link = "reportVVS.php?r_id=".$row["reportid"];
                    if($row["groupcl"]==1){
                        $shared = " - Group claim";
                    } else {
                        $shared = " - Personal claim";
                    }
                }
                
                if($row["clstatus"]=="0"){
                    $clstatus = "Unconfirmed";
                } elseif($row["clstatus"]=="1"){
                    $clstatus = "Confirmed";
                } else {
                    $clstatus = $row["clstatus"];
                }
                
                
                
                
                
        ?>
        <tbody>
            <tr>
                <td><a href="<?php echo($link);?>"><?php echo $row["id"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["missionid"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo $row["name"];?></a></td>
                <td><a href="<?php echo($link);?>"><?php echo($clstatus.$shared);?></a></td>
            </tr>
        </tbody>
        
        <?php } ?>
    </table>
</div>
<div class='pageSelect'>
    <?php createPageSelect($n_pages, $page, "memberClaims.php?m_id=".$member_id."&"); ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');
