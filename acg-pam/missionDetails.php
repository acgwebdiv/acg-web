<?php
    include(dirname(__FILE__).'/header0.php');
    include_once(dirname(__FILE__).'/includes/characterDBFunctions.php');
    
    if(isset($_SESSION['userID'])){
        $userID = $_SESSION['userID'];
    } else {
        $userID = 0;
    }
    
    $dbx = getDBx();
    if(filter_has_var(INPUT_GET, "m_id")){
        $mi_id = filter_input(INPUT_GET, "m_id");  
    }
    $sql = "SELECT missions.name AS miName, missions.histdate, missions.realdate, ".
           "missions.missionstatus, campaigns.name AS c_name ".
           "FROM missions LEFT JOIN campaigns ON missions.campaignid = campaigns.id ". 
           "WHERE missions.id = $mi_id";
    $m_result = mysqli_query($dbx, $sql);
    $m_row = mysqli_fetch_assoc($m_result);
    $mi_name = $m_row["miName"];
    $mi_status = $m_row["missionstatus"];
    $m_faction = getFaction($_SESSION["userID"], $dbx);
    
?>
<?php include(dirname(__FILE__).'/header1.php'); ?>
<script src="jscript/main.js"></script>
<script src="jscript/ajax.js"></script>
<script src="jscript/reportLogic.js"></script>
<script src="jscript/dispersalLogic.js"></script>
<script type="text/javascript">

</script>
<?php include(dirname(__FILE__).'/missionMenu.php'); ?>
<p class="form_id">ACG-PAM/300-210.1</p>
<div>
    <h3><?php echo($mi_name." (".$m_row["histdate"].")"); ?></h3>
    <p>
        <?php echo($mi_name." (".$m_row["histdate"].")"); ?>, 
        ACG-Campaign "<?php echo($m_row["c_name"]); ?>", mission date:
         <?php echo($m_row["realdate"]); ?>.
    </p>
    <?php 
    switch($m_row["missionstatus"]){
        case 1:
            include(dirname(__FILE__).'/missionLobby.php');               
            break;

        case 2:
            $closeDate = date('Y-m-d', strtotime($m_row["realdate"]."+1 week"));
    ?>
    <p>The mission's current status is: Debriefing</p>
    <p>Pilots are advised write and submit their After Action Report until <?php echo($closeDate); ?>.
    <p>In case the mission was flown without it's status being updated. Please
    contact the campaign administration.</p>
    <?php
        break;
        case 3:
            include(dirname(__FILE__).'/missionSynopsis.php');
            break;
    
    } ?>
</div>
<?php include(dirname(__FILE__).'/footer.php');