<?php
	include("../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../includes/header2.inc.php");
	include('flight_school.lang.'.$ac_lang.'.php');

	$section = -1;

	if ( isset( $_GET["section"] ) )
	{
		$section = $_GET["section"];
	}
?>
		<h1><?php echo $lang['FLIGHT_SCHOOL_HEADER'];?></h1>

		<div class="contentText">

			<img src="../includes/images/flight_school/flight_school_header.png">
			<p><?php echo $lang['FLIGHT_SCHOOL_P1'];?></p>

		</div>

		<br>

		<div >
			<a name="nav"></a>
			<ul class="flightSchoolNavigation">
<?php
				$array = array("General", "Royal Air Force", "Luftwaffe");

				foreach ($array as $i => $value) 
				{
					$pic = "";
					if ( $i == 0 )
					{
						$pic = "general.png";
					}
					else if ( $i == 1 )
					{
						$pic = "raf.png";
					}
					else if ( $i == 2 )
					{
						$pic = "luftwaffe.png";
					}

					if ( $i == $section )
					{
	    			echo "
	    			<li>
	    				<a href=\"./flight_school.php?section=$i#nav\" class=\"current\">
	    					<img src=\"../includes/images/flight_school/$pic\">
	    					$value
	    				</a>
	    			</li>";
	    		}
	    		else
	    		{
	    			echo "
	    			<li>
	    				<a href=\"./flight_school.php?section=$i#nav\">
	    					<img src=\"../includes/images/flight_school/$pic\">
	    					$value
	    				</a>
	    			</li>";
	    		}
	     	}
?>
			</ul>

			<div class="flightSchoolSubNavigation">
<?php

			$file = "";

			if ($section == 0)
				$file = file('flight_school_general_'.$ac_lang.'.csv');
			else if ($section == 1)
				$file = file('flight_school_raf_'.$ac_lang.'.csv');
			else if ($section == 2)
				$file = file('flight_school_lw_'.$ac_lang.'.csv');

			if (!empty($file))
			{

				echo "<ul>";

				foreach($file AS $line)
				{
					$data = explode("|", $line);

					if ($data[0] == "~") 
					{
						echo "
						</ul>
							<h3>$data[1]</h3>
						<ul>";
					}
					else
					{
						echo "<li>
							 			<a href=\"$data[1]\">
							 			<img src=\"../includes/images/flight_school/navigation/$data[2]\">
							 			$data[0]
							 			<p>
							 				$data[3]
							 			</p>
							 		</a>
							 	</li>";
					}
				}

				echo "</ul>";
			}
?>
			</div>

		</div>	
		
<?php
	include("../includes/footer.inc.php");
?>