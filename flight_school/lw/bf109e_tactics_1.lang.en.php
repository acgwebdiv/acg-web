<?php

//advanced_nav.php

$lang['BF109E_TACTICS_1_HEADER1'] = "Flight School";
$lang['BF109E_TACTICS_1_HEADER2'] = "Bf 109 E - Fighter Tactics #1";

$lang['BF109E_TACTICS_1_P1'] = "Brief tutorial by Royraiden showing the importance 
    of flying with a wingman during online combat missions. Not only does it make 
    it easier to get air victories, flying effectively as a team is the key to survival.";