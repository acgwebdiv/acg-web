<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Luftwaffe Ranks and Appointments</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
	include('Luftwaffe_rank.lang.'.$ac_lang.'.php');
?>

		<div class="contentText">
		<center><h1><?php echo $lang['LUFTWAFFE_RANK_HEADER1'];?></h1></center>
		<center><h3><?php echo $lang['LUFTWAFFE_RANK_HEADER2'];?></h3></center>
		<center><h4><?php echo $lang['LUFTWAFFE_RANK_HEADER3'];?></h4></center>		
		<p><?php echo $lang['LUFTWAFFE_RANK_P1'];?></p>
		<ul>
			<li><?php echo $lang['LUFTWAFFE_RANK_L1'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L2'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L3'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L4'];?></li>
		</ul>
		<center><h4><?php echo $lang['LUFTWAFFE_RANK_HEADER4'];?></h4></center>		
		<p><?php echo $lang['LUFTWAFFE_RANK_P2'];?></p>
		<ul>
			<li><?php echo $lang['LUFTWAFFE_RANK_L5'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L6'];?></li>
		</ul>
		<p><?php echo $lang['LUFTWAFFE_RANK_P3'];?></p>
		<ul>
			<li><?php echo $lang['LUFTWAFFE_RANK_L7'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L8'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L9'];?></li>
		</ul>		
		<center><h4><?php echo $lang['LUFTWAFFE_RANK_HEADER5'];?></h4></center>
		<p><?php echo $lang['LUFTWAFFE_RANK_P4'];?></p>
		<ul>
			<li><?php echo $lang['LUFTWAFFE_RANK_L10'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L11'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L12'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L13'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L14'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L15'];?></li>		
		</ul>
		<p><?php echo $lang['LUFTWAFFE_RANK_P5'];?></p>
		<ul>
			<li><?php echo $lang['LUFTWAFFE_RANK_L16'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L17'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L18'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L19'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L20'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L21'];?></li>
		</ul>
		<center><h4><?php echo $lang['LUFTWAFFE_RANK_HEADER6'];?></h4></center>
		<p><?php echo $lang['LUFTWAFFE_RANK_P6'];?></p>
		<p><?php echo $lang['LUFTWAFFE_RANK_P7'];?></p>
		<p><?php echo $lang['LUFTWAFFE_RANK_P8'];?></p>
		<p><?php echo $lang['LUFTWAFFE_RANK_P9'];?></p>
		<p><?php echo $lang['LUFTWAFFE_RANK_P10'];?></p>
		<center><h3><?php echo $lang['LUFTWAFFE_RANK_HEADER7'];?></h3></center>
		<p><?php echo $lang['LUFTWAFFE_RANK_P11'];?></p>
		<ul>
			<li><?php echo $lang['LUFTWAFFE_RANK_L22'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L23'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L24'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L25'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L26'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L27'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L28'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L29'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L30'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L31'];?></li>
			<li><?php echo $lang['LUFTWAFFE_RANK_L32'];?></li>
		</ul>
		</p>

		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>