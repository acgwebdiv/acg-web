<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Luftwaffe Radio Codes</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
?>
		<h1>Flight School</h1>
		<h2>Luftwaffe Radio Codes</h2>

		<div class="contentText">
			<table class="rosterTable" border="1">
				<tr class="rosterTableHeader">	
				<tr>
					<th>Command</th>
					<th>Meaning (Eng)</th>
					<th>Meaning (Ger)</th>
				</tr>
				<tr>
					<td>Victor</td>
					<td>Understood (Roger/Copy)</td>
					<td>Verstanden</td>
				</tr>
				<tr>
					<td>Vitamine</td>
					<td>Confirm</td>
					<td>Best&auml;tige</td>
				</tr>
				<tr>
					<td>Ricardus</td>
					<td>Didn't understand, please repeat</td>
					<td>Nicht verstanden, bitte wiederholen</td>
				</tr>
				<tr>
					<td>Rakete</td>
					<td>Takeoff</td>
					<td>Start</td>
				</tr>
				<tr>
					<td>Gartenzaun</td>
					<td>(Home) Airbase</td>
					<td>(Heimat) Flugplatz</td>
				</tr>
				<tr>
					<td>Indianer</td>
					<td>Enemy fighter</td>
					<td>Feindliche J&auml;ger</td>
				</tr>
				<tr>
					<td>Fragezeichen</td>
					<td>Unknown airplane</td>
					<td>Unbekannts Flugzeug</td>
				</tr>
				<tr>
					<td>Pauke-Pauke</td>
					<td>Attack</td>
					<td>Angreifen</td>
				</tr>
				<tr>
					<td>Rolf</td>
					<td>Right</td>
					<td>Rechts</td>
				</tr>
				<tr>
					<td>Lisa</td>
					<td>Left</td>
					<td>Links</td>
				</tr>
				<tr>
					<td>Rolf-Lisa</td>
					<td>Waggle your wings (for identification)</td>
					<td>Mit den Fl&uuml;geln wackeln (zur Erkennung)</td>
				</tr>
				<tr>
					<td>Kirchturm / Halo</td>
					<td>Altitude</td>
					<td>H&ouml;he</td>
				</tr>
				<tr>
					<td>Caruso</td>
					<td>Course</td>
					<td>Kurs</td>
				</tr>
				<tr>
					<td>Quelle</td>
					<td>Position</td>
					<td>Standort</td>
				</tr>
				<tr>
					<td>Ich bin blind</td>
					<td>I can't see target (blind)</td>
					<td>Ziel nicht gesehen (verloren)</td>
				</tr>
				<tr>
					<td>Ich ber&uuml;hre</td>
					<td>I can see the target</td>
					<td>Ich habe Ziel gesehen</td>
				</tr>
				<tr>
					<td>Halten</td>
					<td>Fly slower (throttle back)</td>
					<td>Fliegen Sie langsamer</td>
				</tr>
				<tr>
					<td>Expre&szlig;</td>
					<td>Fly faster (throttle up)</td>
					<td>Fliegen Sie schneller</td>
				</tr>
				<tr>
					<td>Expre&szlig;-Expre&szlig;</td>
					<td>Maximum speed (emergency power)</td>
					<td>Maximal Geschwindigkeit (Notleistung)</td>
				</tr>
				<tr>
					<td>Eile</td>
					<td>Own speed</td>
					<td>Eigene Geschwindigkeit</td>
				</tr>
				<tr>
					<td>Lucie-Anton</td>
					<td>Landing</td>
					<td>Landung</td>
				</tr>
				<tr>
					<td>Havanna</td>
					<td>Instant landing</td>
					<td>Sofort landen</td>
				</tr>
				<tr>
					<td>Reise ...</td>
					<td>Fly to ...</td>
					<td>Nach ... fliegen</td>
				</tr>
				<tr>
					<td>&Auml;quator</td>
					<td>In the clouds</td>
					<td>In den Wolken</td>
				</tr>
				<tr>
					<td>Nordpol</td>
					<td>Over the clouds</td>
					<td>&Uuml;ber den Wolken</td>
				</tr>
				<tr>
					<td>S&uuml;dpol</td>
					<td>Below the clouds</td>
					<td>Unter den Wolken</td>
				</tr>
			</table>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>