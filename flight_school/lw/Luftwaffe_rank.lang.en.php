<?php

//Luftwaffe_rank.php
//GERMAN NOUNS DO NOT NEED TO BE TRANSLATED.

$lang['LUFTWAFFE_RANK_HEADER1'] = "Luftwaffe Ranks and Appointments";
$lang['LUFTWAFFE_RANK_HEADER2'] = "Ranks";
$lang['LUFTWAFFE_RANK_HEADER3'] = "Enlisted men";

$lang['LUFTWAFFE_RANK_P1'] = "<b>Mannschaften</b> These are the lowest ranks of 
    the Luftwaffe and carry out basic duties. A rough translation for command is 
    unter (under), ober (over), haupt (lead) and stab (staff).";
    
$lang['LUFTWAFFE_RANK_L1'] = "Flieger, Kanonier, Funker.  The basic rank of a rekrut. 
    Usually described his function and is similar to the type of his unit. E.g. 
    a Kanonier was the operator or gunner of a (flak)cannon and based in a flak 
    unit.";
$lang['LUFTWAFFE_RANK_L2'] = "Gefreiter";
$lang['LUFTWAFFE_RANK_L3'] = "Obergefreiter";
$lang['LUFTWAFFE_RANK_L4'] = "Hauptgefreiter. This rank was replaced with Stabsgefreiter
    in 1944.";
    
$lang['LUFTWAFFE_RANK_HEADER4'] = "Non-Commissioned Officers";

$lang['LUFTWAFFE_RANK_P2'] = "<b>Unteroffiziere</b>	The junior Non-Commissioned 
    ranks were 'Unteroffiziere ohne Portepee' which translates as 'Underofficers 
    without swordknot' and refers to when senior German NCOs would carry a sword 
    side arm with the officers swordknot made from gold or silver lace.";

$lang['LUFTWAFFE_RANK_L5'] = "Unteroffizier";
$lang['LUFTWAFFE_RANK_L6'] = "Unterfeldwebel";

$lang['LUFTWAFFE_RANK_P3'] = "The senior Non-Commissioned ranks were 'Unteroffiziere 
    mit Portepee' which translates as 'Underofficers with swordknot'.";

$lang['LUFTWAFFE_RANK_L7'] = "Feldwebel";
$lang['LUFTWAFFE_RANK_L8'] = "Oberfeldwebel";
$lang['LUFTWAFFE_RANK_L9'] = "Hauptfeldwebel";

$lang['LUFTWAFFE_RANK_HEADER5'] = "Commissioned Officers";

$lang['LUFTWAFFE_RANK_P4'] = "<b>Offiziere</b> These are the Commissioned ranks 
    and equivalent to RAF ranks starting at Pilot Officer.";

$lang['LUFTWAFFE_RANK_L10'] = "Leutnant";
$lang['LUFTWAFFE_RANK_L11'] = "Oberleutnant";
$lang['LUFTWAFFE_RANK_L12'] = "Hauptmann";
$lang['LUFTWAFFE_RANK_L13'] = "Major";
$lang['LUFTWAFFE_RANK_L14'] = "Oberstleutnant";
$lang['LUFTWAFFE_RANK_L15'] = "Oberst";

$lang['LUFTWAFFE_RANK_P5'] = "<b>Generale</b> These are the High Command ranks and 
    equivalent to RAF ranks starting at Air Commodore.";

$lang['LUFTWAFFE_RANK_L16'] = "Generalmajor";
$lang['LUFTWAFFE_RANK_L17'] = "Generalleutnant";
$lang['LUFTWAFFE_RANK_L18'] = "General";
$lang['LUFTWAFFE_RANK_L19'] = "Generaloberst";
$lang['LUFTWAFFE_RANK_L20'] = "Generalfeldmarschall";
$lang['LUFTWAFFE_RANK_L21'] = "Reichsmarschall. The fat one himself, this was set 
    higher than equivalent ranks in Allied forces.";

$lang['LUFTWAFFE_RANK_HEADER6'] = "Appointments (these are NOT ranks)";

$lang['LUFTWAFFE_RANK_P6'] = "Generally Luftwaffe appointments were given to those 
    of a lower rank than the RAF. For example, an RAF officer commanding a squadron 
    in battle would usually be a Squadron Leader but in the Luftwaffe this role 
    would be undertaken by the equivalent of a Flight Lieutenant.";
$lang['LUFTWAFFE_RANK_P7'] = "<b>Staffelkapit&auml;n</b><br/> Staffelkapit&auml;n 
    is a position in the flying units of the German Luftwaffe. During the battle 
    of Britain era the Staffel was usually lead by a Oberleutnant or Hauptmann.";
$lang['LUFTWAFFE_RANK_P8'] = "<b>Gruppenkommendeur</b><br/> Gruppenkommandeur is 
    a position that identifies the commander of a Gruppe which if you recall in 
    early war is three Staffeln. A Gruppe was usually commanded by a Major but sometimes 
    Hauptmann or Oberstleutnant.";
$lang['LUFTWAFFE_RANK_P9'] = "<b>Geschwader Ia</b><br/>	Leading assistant to the 
    commander, usually an Oberstleutnant or Major i.G. (im Generalstab). A Geschwader 
    would also have a <b>Technischer Offizier</b> on the staff.";
$lang['LUFTWAFFE_RANK_P10'] = "<b>Geschwaderkommodore</b><br/> In the Luftwaffe, 
    the appointment Kommodore was given to the commanding officer of a Geschwader 
    or other self-dependent flying unit. The appointment was irrespective of the 
    actual rank, which could vary from Hauptmann to Oberst.";

$lang['LUFTWAFFE_RANK_HEADER7'] = "Fuselage Badges and Tactical Markings";

$lang['LUFTWAFFE_RANK_P11'] = "The following markings behind the balkankreuz on 
    the fuselage indicated an appointment within the Geschwader.";

$lang['LUFTWAFFE_RANK_L22'] = "Geschwaderkommodore: <| + or <<| + or <<- +";
$lang['LUFTWAFFE_RANK_L23'] = "Geschwaderadjutant: -| + or <| +";
$lang['LUFTWAFFE_RANK_L24'] = "Geschwader Ia: <- +";
$lang['LUFTWAFFE_RANK_L25'] = "Geschwader Technischer Offizier: <|O + or <|. + or 
    <|| +";
$lang['LUFTWAFFE_RANK_L26'] = "Gruppenkommandeur: << +";
$lang['LUFTWAFFE_RANK_L27'] = "Gruppenadjutant: < +";
$lang['LUFTWAFFE_RANK_L28'] = "Gruppen Technischer Offizier: <O +";
$lang['LUFTWAFFE_RANK_L29'] = "Major beim Stab: <1 +";
$lang['LUFTWAFFE_RANK_L30'] = "Staffelkapit&auml;n: No extra sign, formal and in 
    pre war time it should be the no. '1' of a Staffel however during the war, this 
    was not usual.";
$lang['LUFTWAFFE_RANK_L31'] = "Gruppe: Sign after the Balkenkreuz";
$lang['LUFTWAFFE_RANK_L32'] = "Staffel: Combination of color and Gruppe-sign";
