<?php

//teamplay_1.php
//GERMAN NOUNS DO NOT NEED TO BE TRANSLATED.

$lang['TEAMPLAY_1_HEADER1'] = "Flight School";
$lang['TEAMPLAY_1_HEADER2'] = "Teamplay &amp; Formation #1";

$lang['TEAMPLAY_1_P1'] = "Tutorial by 6./JG26_Ape and other 6./JG26 members 
    showing the basics of formation flying.";
