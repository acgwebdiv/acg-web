<?php

//advanced_nav.php

$lang['BF110_TUTORIAL_2_HEADER1'] = "Flight School";
$lang['BF110_TUTORIAL_2_HEADER2'] = "Understanding fuel operations in the Bf 110";

$lang['BF110_TUTORIAL_2_P1'] = "In order to maximise your sortie time you should 
    understand the fuel system in the Bf110. 6./ZG76_Von Archie from our explains 
    exactly how it works.";