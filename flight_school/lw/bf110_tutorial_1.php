<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Bf 110 C - Basic Tutorial</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
	include('bf110_tutorial_1.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['BF110_TUTORIAL_1_HEADER1'];?></h1>
		<h2><?php echo $lang['BF110_TUTORIAL_1_HEADER2'];?></h2>

		<div class="contentText">
			<p><?php echo $lang['BF110_TUTORIAL_1_P1'];?></p>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/8DhS9kiTPDw" frameborder="0" allowfullscreen></iframe>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>