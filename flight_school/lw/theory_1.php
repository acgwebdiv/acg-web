<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Basic Jagdwaffe Organisation and Formations</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
	include('theory_1.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['TEAMPLAY_1_HEADER1'];?></h1>
		<h2><?php echo $lang['TEAMPLAY_1_HEADER2'];?></h2>

    <h3><?php echo $lang['TEAMPLAY_1_HEADER3'];?></h3>
		<div class="contentText">
			<center><img src="../../includes/images/flight_school/lw/theory_1_1.png"></center>
			<p><?php echo $lang['TEAMPLAY_1_P1'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P2'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P3'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P4'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P5'];?></p>
		</div>

		<h3>Unit Identification</h3>
		<div class="contentText">
			<p><?php echo $lang['TEAMPLAY_1_P6'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P7'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P8'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P9'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P10'];?></p>
			<p><?php echo $lang['TEAMPLAY_1_P11'];?></p>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>