<?php

//theory_1.php
//GERMAN NOUNS DO NOT NEED TO BE TRANSLATED.

$lang['THEORY_1_HEADER1'] = "Flight School";
$lang['THEORY_1_HEADER2'] = "Basic Jagdwaffe Organisation and Formations";
$lang['THEORY_1_HEADER3'] = "Jagdwaffe Formations";

$lang['THEORY_1_P1'] = "<b>Rotte</b><br>The Rotte (pair) is the basic fighting unit 
    for all Jagdwaffe formations. It consits of the Rottenf&uuml;her (pair leader) 
    and the Katschmarek (wingman). The Rottenf&uuml;her is responsible for making 
    the kills and the Katschmarek is simply following the Rottenf&uuml;hrer and 
    protecting his tail.";
$lang['THEORY_1_P2'] = "<b>Schwarm</b><br>Two Rotten make up a Schwarm (flight). 
    Typically flying in the finger four formation where the leading Rotte is flying 
    to one side and slightly ahead of the other.";
$lang['THEORY_1_P3'] = "<b>Staffel</b><br>A Staffel (squadron) formation comprises 
    three Schw&auml;rme (flights) either stepped up in line astern or in line abreast.";
$lang['THEORY_1_P4'] = "<b>Gruppe</b><br>Following the tripartite system, a Gruppe 
    (group) consists of three Staffeln (squadrons).";
$lang['THEORY_1_P5'] = "<b>Geschwader</b><br>The Geschwader (wing) is the largest, 
    mobile homogeneous Luftwaffe flying unit and usually contains three our four 
    Gruppen (groups).";
    
$lang['THEORY_1_HEADER4'] = "Unit Identification";

$lang['THEORY_1_P6'] = "A Jagdgeschwader (fighting wing) is designated by an Arabic 
    numeral which is prefixed by the abbreviation JG. So Jagdgeschwader 26 is simply 
    written as JG 26. The Gruppen are identified by a Roman numeral in front of 
    the Geschwader designation, separated by a slash. This means the second Gruppe 
    of the Jagdgeschwader 26 is written as II./JG 26. Arabic numerals are used for 
    the Staffeln: 3./JG26 is the third Staffel of JG 26.";
$lang['THEORY_1_P7'] = "In identifying it can not be mistaken since every Gruppe 
    contains certain Staffeln. Once you understand the system, you can immediately 
    identify which Staffel belongs to which Gruppe of a Geschwader. The 1., 2. and 
    3. Staffel belong to the I. Gruppe, the 4., 5. and 6. to the II. Gruppe, the 
    7., 8. and 9. to the III. Gruppe and the 10., 11. and 12 to the IV. Gruppe.";
$lang['THEORY_1_P8'] = "For an easy and quick identification of the planes within 
    a Staffel a combination of numbers and colors is used. The planes of a Staffel 
    are numbered from 1 to 12. The 1., 4., 7. and 10. Staffel has white, 2., 5., 
    8. and 11. has black (sometimes red) and 3., 6., 9. and 12. has yellow numbers.";
$lang['THEORY_1_P9'] = "Likewise every Gruppe has it's own identification marking 
    which is placed near the tail of the aircraft. The I. Gruppe does not have a 
    marking, the II. has a crossbar, the III. has a wavy line (early) or a vertical 
    beam (later) and the IV. Gruppe has a dot (early) or a little cross (later).";
$lang['THEORY_1_P10'] = "With knowledge of this system you can identify at first 
    glance which pilot from which Staffel and which Gruppe is flying the plane.";
$lang['THEORY_1_P11'] = "Let's take the white 3 with a vertical beam as an example: 
    It is the plane with the number 3 of the 7. Staffel of the III. Gruppe. Since 
    we know that the 7., 8. and 9. Staffel are belonging to the III. Gruppe but 
    only the 7. Staffel has white numbers.";
