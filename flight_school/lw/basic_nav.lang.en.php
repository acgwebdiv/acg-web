<?php

//advanced_nav.php

$lang['BASIC_NAV_HEADER1'] = "Flight School";
$lang['BASIC_NAV_HEADER2'] = "Navigation for the beginner";

$lang['BASIC_NAV_P1'] = "This walk-through from 5./JG26_Grey shows you how 
    to get to somewhere using the instruments and a little maths, without magnetic 
    deviation (see the advanced navigation)";
    
$lang['BASIC_NAV_P2'] = "Step 1: Plot your course. See how many cities you come across. 
    In this case one large town; Bruya La Buissere. Keep this in mind to do adjustments 
    left or right during the flight. Here I'm going from Merville Calonne to St-Pol-Sur-Ternoise 
    it's 36km and an overall direction SW.";

$lang['BASIC_NAV_P3'] = "Step 2: SW on my compass rose gives me a heading of 225&deg;, 
    now if you use a ruler and point it over to the other side you'll notice that 
    you have your bearing back to the home location, in this case 45&deg;.";

$lang['BASIC_NAV_P4'] = "Step 3: Use a divers watch, stop-watch or grandmothers 
    clock, and check the time of take off.";

$lang['BASIC_NAV_P5'] = "Step 4: IMPORTANT!! Keep to roughly the same speed throughout 
    your flight and as soon as you get in to contact, or arrived at the battlefield, 
    stop the watch - in this case I'll say 8 minutes.";

$lang['BASIC_NAV_P6'] = "Step 5: To return simply head back in the opposite direction. 
    45&deg; on the rose. Now this is important, head back at the same speed you came 
    in, after 6 minutes you start looking around for your airfield.....and there 
    it is. If by any chance you had to make adjustments on your way in, calculate 
    an average. This is not rocket science so don't break your head over it, 
    just use the ruler over your compass rose and do an estimate.";

$lang['BASIC_NAV_P7'] = "Here are a couple of sample roses but you can find other 
    examples on the internet if you want to look for one you like.  Print out, laminate 
    and keep beside your virtual cockpit.  Happy navigating!";