<?php

//advanced_nav.php

$lang['BF109_TUTORIAL_2_DE_HEADER1'] = "Flight School";
$lang['BF109_TUTORIAL_2_DE_HEADER2'] = "Bf 109 E - Take Off and Landing - German";

$lang['BF109_TUTORIAL_2_P1'] = "Falcon explains how to master the startup, takeoff
    and landing procedure for the Bf109E fighter in this video (German language).";