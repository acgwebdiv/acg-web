<?php

//flight_school.php

$lang['FLIGHT_SCHOOL_HEADER'] = "Flight School";
$lang['FLIGHT_SCHOOL_P1'] = "Welcome to the ACG Flight School! Here you can find 
    various tutorials and other stuff which will help you to improve your flying 
    and fighting skills.";

//Additional text is contained in the files flight_school_general_LANG.csv, flight_school_lw_LANG.csv,
//flight_school_raf_LANG.csv, which have to be translated separately and stored 
//in their own files with LANG defining the language.
