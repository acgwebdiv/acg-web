<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | RAF formation flying</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
?>
		<h1>Flight School</h1>
		<h2>RAF formation flying</h2>

		<div class="contentText">
			<p>
				Allow No.501_Miki to explain the basics of formation flying and the art of staying with your leader through combat manoeuvres.  This skill can be equally applied to any type of aeroplane.
			</p>

			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/4J4kiMuYskg?rel=0" frameborder="0" allowfullscreen></iframe>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>