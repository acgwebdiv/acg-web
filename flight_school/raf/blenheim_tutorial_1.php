<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Hurricane Mk.I Rotol 100 Octane - Standard Landing Procedure</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
?>
		<h1>Flight School</h1>
		<h2>Blenheim Mk.IV - Flight and Bombing</h2>

		<div class="contentText">
			<p>
				Want to know how to operate the Blenheim and strike back at Jerry? Follow these tutorials by Sqn. Ldr Hawes of ACG's No.615 "County of Surrey" Squadron to find out how to turn those suicide attacks into milk runs!
			</p>
			
			<p>
				This opening video tells you how to set up the Blenheim and get her airbourne.
				<i>NB: There is a video issue with the end result on one of the texts, it basically says not to shift through the positions or you'll disable Ai gunner.
			</i></p>
			
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/24kE38Nn-nM" frameborder="0" allowfullscreen></iframe>
		
		<p>
		<img src="../../includes/images/flight_school/raf/Blenheim_cockpit.jpg" style="width:920px; border="0">		
		</p>
		<p>
		These are the main things to look for on the cockpit panel. <i>R-Click image and select Open in new window' for full screen picture.</i>
		</p>
		
		<p>
		Flying around is one thing, you need to be accurate in order to give Goering a firm kick in the shin.  Watch the next video to discover how.
		</p>
		<p>
		<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/uSc9TS6p54Q" frameborder="0" allowfullscreen></iframe>		
		</p>
		
		<p>
		Well we hope you've learned something with these tutorials today and hopefully you can put it into practice.  Download <u><a href="../../includes/files/Blenheim_mission.zip">these</a></u> practice missions in order to perfect your technique in a safe environment.<br />You may need 7zip from <a href="http://www.7-zip.org/download.html"><u>here</u></a> in order to unpack the files.
		</p>
		<p>
		If you have further questions or you wish to get involved in a campaign where your newly acquire skills will be needed then please do not hesitate to jump into our Teamspeak or post in our forums.  ACG does operate No.105 Squadron operating the mighty Blenheim as required by campaigns so there is every chance you can hand it back to the Hun yourself and help the Allies to victory.
		</p>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>