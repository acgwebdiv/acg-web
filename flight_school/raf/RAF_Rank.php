<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | RAF Ranks, Appointments and Organisation</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
?>

		<div class="contentText">
		<p>
		
		<center><h1>Flight School</h1></center>
		<center><h2>RAF Ranks, Appointments and Organisation</h2></center>
		The RAF is not always logical and can sound, to the unfamiliar, like the rules of cricket as described on a novelty tea towel, so here is a short aide memoire.<br/><br/>
		<center><h3>Organisation</h3></center>
		The basic unit in the RAF is the Squadron (Sqn), several of these make a Wing (Wg) and several Wgs, normally organized by task and area make a Group (Gp) and above that a Command. For example Kenley Wing is part of 11Gp (geographically gathered in the SE of England) and all the fighter Gps together come under Fighter Command. A further organisational complexity is the Station (Stn). This is the generic term for an RAF camp and not restricted to flying units or aerodromes. You could have any number of Sqns or even Wgs at a Stn. The Stn includes the administration, engineering and operations support for those stationed there.<br/><br/>
		<b>The Squadron</b><br/>
		Each Fighter Sqn is nominally 12 aircraft (ac) and 20 pilots. The Sqn is split into 2 Flights (Flts) and each Flt split into 2 Sections (Sect) of 3 ac each, also known as a "Vic" Squad is a US term as is an Element (2 ac).  Flights were normally referred to A and B, and colours applied to each section and then referred to as, for example, red leader, red one, red two.<br/>
		<center><img src="../../includes/images/flight_school/general/Squadron.jpg"></center><br/><br/>				
		<b>The Wing</b><br/>
		Each Wg is nominally 3 Sqn and 60 pilots. During the Battle of Britain a Wing would normally operate 2 of the squadrons out of the sector station and another on one of the sector satellite aerodromes.  For example, Kenley Wing had No.401 operating from Croydon and at the same time operate No.64 and No.615 from Kenley.<br/>
		<center><h4>Ranks</h4><br/></center>
		<b>Airman Ranks</b><br/>
		The lowest ranks in World War 2 were Aircraftsman (AC), Aircraftsman First Class (AC1) and Aircraftsman Second Class (AC2) collectively known as "Erks". These were tradesmen (eng, technical and general duties etc). The lowest NCO rank was Corporal (Cpl), there were no AC or JNCO flying duties.<br/><br/>
		These are followed by Sergeants (Sgt) and Flight Sergeant (FS). Sgt being the lowest rank which <b>flew</b> in the RAF. <br/><br/>
		<b>Warrant Officer</b><br/>
		This is slightly odd rank with the holder receiving a Kings Warrant rather than a Commission, the other ranks calling him Sir and officers calling him "Mr". He ate and bunked with the Sgts in the Sgt's Mess. <br/>
		In Fighter Command about a third of pilots were SNCOs or WO rather than officers.<br/><br/>

		<center><h4>Commissioned Officers</h4></center>
		Starting from the lowest rank<br/><br/>
		<b>Pilot Officer</b>, Plt Off or P/O (not PO that is a naval Petty Officer). Not all P/Os are pilots. The Stn Catering officer may well be one (See Stations). It is a rank, not a job.<br/><br/>
		<b>Flying Officer</b>, Fg Off or F/O. Not all F/Os are pilots. The Stn Dentist may well be one. It is a rank, not a job.<br/><br/>
		<b>Flight Lieutenant</b>, Flt Lt or F/L. Pronounced "Left-tenant" please gentlemen.<br/><br/>
		<b>Squadron Leader</b>, Sqn Ldr or S/L<br/><br/>
		<b>Wing Commander</b>, Wg Cdr or W/C<br/><br/>
		<b>Group Captain</b>, Gp Capt or G/C<br/><br/>
		<b>Air Commodore</b>, Air Cdre<br/><br/>
		<b>Air Vice Marshal</b>, AVM<br/>
		<center><h4>Appointments (these are NOT ranks)</h4></center>
		An Off in charge of a Flt is a <b>Flight Commander</b> (Flt Cdr). Could be a P/O, F/O, F/L or in today's RAF almost certainly a Sqn Ldr.<br/><br/>
		An Off in charge of a Sqn is a <b>Squadron Commander</b> (Sqn Cdr). <br/><br/>
		An Off in charge of a Wg (in Fighter Command in 1940) was a <b>Wing Leader</b>.  Flying Wgs do not really exist now, Wgs are more likely to be Eng, Ops or Admin and run by Wg Cdr Ops etc.<br/><br/>
		An officer in charge of a Stn is a <b>Station Commander</b> and he alone uses the term <b>Commanding Officer (CO)</b>. All the others are only <b>Officers Commanding (OC)</b>. The term is specific and has legal significance similar to a ship's captain.<br/><br/>
		The Air Officer (rank of General in US parlance) in Charge of 11Gp is the <b>Air Officer Commanding (AOC)</b>.<br/><br/>
		<center><h4>How Officers are addressed</h4></center>
		This is the confusing bit and also the important bit as if you get it wrong it sounds dreadful. In general, RAF officers are referred to by their post or appointment, not their rank. As in, AVM Parks AOC 11 Gp in known as "the AOC" not the AVM, so....<br/><br/>
		F/L Smith, Flt Cdr of A Flt would be known as <b>OC A Flt or OCA</b>.<br/><br/>
		S/L Smith, Sqn Cdr of 64 Sqn would be known as <b>OC 64 Sqn</b>, not S/L 64 Sqn nor CO 64 Sqn.<br/><br/>
		W/C Prickman in charge of the Kenley Wg is also Stn Cdr RAF Kenley and therefore known as the <b>CO</b>, the old man, the Wing Co or the "Staish" (as in stay-ssh), but <u>not</u> "Commander" that is a Naval rank, heaven forbid!<br/><br/>
		The term "Boss" refers to any officer to whom you are directly subordinate, as in your Boss.<br/><br/>
		The term <b>Executive Officer (XO)</b> which is a US term now used in Fighter Sqns in the RAF but not in 1940. Second in Command or Deputy Sqn Cdr (DSC) may have been used <i>(citation needed)</i>.  Regardless he would have been the senior Flt Cdr.<br/><br/>
		</p>

		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>