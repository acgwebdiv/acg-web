<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | RAF Vocabulary</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
?>
		<h1>Flight School</h1>
		<h2>RAF Vocabulary</h2>
		<p>Royal Air Force people, like any close-knit group, developed their own vocab, codewords and slang of the period.  Here is a list of the most common terms and meanings known and ACG pilots do try to use this language when out on missions.......for immersion purposes of course!
		<div class="contentText">
			<table class="rosterTable" border="1">
				<tr class="rosterTableHeader">		
<th>				
					<tr><td>ABC - Airborne Cigar - transmitter aboard aircraft which jammed German fighter control frequencies</tr></td>
					<tr><td>AC Plonk - an Aircraftman</tr></td>
					<tr><td>Abbeville Kids - a particularly aggressive bunch of Luftwaffe fighter units based at Abbeville</tr></td>
					<tr><td>Adj - Adjutant, the administrative assistant to the Squadron CO</tr></td>
					<tr><td>Admiral - officer i/c boats in the Air Sea Rescue Service</tr></td>
					<tr><td>Acc (Trolley Acc) - Accumulator (battery) - for starting engines on ground</tr></td>
					<tr><td>ace - pilot credited with bringing down five or more enemy aircraft, established in WWI</tr></td>
					<tr><td>ack - earlier phonetic alphabet for 'A' e.g. morning time a.m. was ack emma</tr></td>
					<tr><td>ack-ack - Anti Aircraft fire (these days, 'Triple A' - anti aircraft artillery)</tr></td>
					<tr><td>aiming point - during a bombing run the aircraft had to fly level and steady for aiming purpose before bomb release and for a short period afterwards to enable the camera on board to take a series of pictures (at night a photo flash flare was dropped) to provide evidence of where the bombs had been aimed - getting a photo showing the planned aiming point was highly regarded </tr></td>
					<tr><td>Air Commode - Air Commodore</tr></td>
					<tr><td>Air House - The Air Ministry</tr></td>
					<tr><td>aircrew - anyone who had a winged brevet, e.g. pilot, navigator, air gunner etc.</tr></td>
					<tr><td>airmaids - crew of an Air Sea Rescue boat</tr></td>
					<tr><td>airscrew - three or four propeller blades on a hub and with a spinner </tr></td>
					<tr><td>Album Leaf - improved kind of Oboe</tr></td>
					<tr><td>ammo - ball, De Wilde incendiary, tracer, armour piercing</tr></td>
					<tr><td>anchor - one who waits too long to drop by parachute</tr></td>
					<tr><td>angels - altitude in units of 1000 feet e.g. Angels 20 over Dover = 20,000 feet over Dover: see also grand</tr></td>
					<tr><td>APHRODITE - Use of aged aircraft as radio-controlled bombers</tr></td>
					<tr><td>apron - hangar tarmac</tr></td>
					<tr><td>Are you happy in/at your work? - facetious/ironic comment to someone doing some heavy/disagreeable/unsuitable task</tr></td>
					<tr><td>armourer - ground crew responsible for bombs, defensive ammunition, flares etc.</tr></td>
					<tr><td>arrival - clumsy or otherwise defective landing</tr></td>
					<tr><td>arse end charlie - the man who weaves backwards and forwards above and behind the fighter squadron to protect them from attack from the rear (not to be confused with tail-end Charlie)</tr></td>
					<tr><td>artic - see Queen Mary</tr></td>
					<tr><td>ASH - Narrow beam radar used for low-level operations</tr></td>
					<tr><td>arsy-tarsy - Aircrew Reception Centre</tr></td>
					<tr><td>Aspirin - jammer to counter the German Knickebein navigational aid</tr></td>
					<tr><td>attaboy - member of the Air Transport Auxiliary (ATA) many of whom were Americans</tr></td>
					<tr><td>aviate - showing off when flying a plane</tr></td>
					<tr><td>bag - collect/secure, possibly illegally</tr></td>
					<tr><td>bag - parachute</tr></td>
					<tr><td>bags of - a lot of as in "bags of swank"</tr></td>
					<tr><td>bags of swank - pride in a job</tr></td>
					<tr><td>bail out (or bale out) - to leave an aircraft by parachute: those doing so to save their lives qualified for a Caterpillar Club membership and badge: usually in forced circumstances as distinct from a practice drop</tr></td>
					<tr><td>Balbo - large formation of aircraft</tr></td>
					<tr><td>balloonatic - member of Balloon Command</tr></td>
					<tr><td>banana-boat - aircraft carrier</tr></td>
					<tr><td>bandit - enemy aircraft</tr></td>
					<tr><td>bandstand - cruet in Officers' Mess</tr></td>
					<tr><td>bang on - to be right on target - absolutely correct</tr></td>
					<tr><td>bar - second or more the award of the same decoration, also shown by an asterisk e.g. DFC* = DFC and bar</tr></td>
					<tr><td>base wallah - someone on HQ staff</tr></td>
					<tr><td>basher - man, chap, fellow in a particular trade e.g. stores basher</tr></td>
					<tr><td>battle dress blues - woollen working uniform</tr></td>
					<tr><td>beam (on the) - be right, understand (opposite of off (the) beam)</tr></td>
					<tr><td>beat up - to fly very low over those who are watching in celebration or to show off, sometimes with disciplinary action resulting or tragic consequences - also to attack</tr></td>
					<tr><td>Beau - Bristol Beaufighter aircraft</tr></td>
					<tr><td>Beer-Barrel - Brewster Buffalo aircraft</tr></td>
					<tr><td>beef - bore by talking very dry 'shop'</tr></td>
					<tr><td>beehive - very close formation of bombers (hive) with fighter escort (bees)</tr></td>
					<tr><td>beer-lever - joystick</tr></td>
					<tr><td>beetle-juice - Betelgeux, bright red star which with Sirius and Procyon form an equilateral triangle</tr></td>
					<tr><td>Belinda - frequent nickname of barrage balloons</tr></td>
					<tr><td>bell (rang the) - got good results</tr></td>
					<tr><td>BELLICOSE - first 'shuttle' bombing raid by Bomber Command June 20/21 1943, aircraft landed at Algiers and bombed Spezia on return to UK</tr></td>
					<tr><td>belly - underpart of aircraft fuselage</tr></td>
					<tr><td>belly landing/belly-flop - to land with the undercarriage retracted</tr></td>
					<tr><td>belt (to) - travel at a high speed or to hit target heavily</tr></td>
					<tr><td>belt up - be quiet</tr></td>
					<tr><td>best blues - parade uniform</tr></td>
					<tr><td>Benjamin - jammer to counter the German Y-Ger&auml;t bombing aid</tr></td>
					<tr><td>Bernhard - German ground-to-air communication system</tr></td>
					<tr><td>billed - briefed/detailed in Orders</tr></td>
					<tr><td>bind (in a) - people who obstruct or are a nuisance, as in "he's in a bind about my leave"</tr></td>
					<tr><td>bind (to) - complain excessively, be officious</tr></td>
					<tr><td>binder - someone who complains unduly</tr></td>
					<tr><td>binders - brakes of an aircraft</tr></td>
					<tr><td>binding - whinging about conditions</tr></td>
					<tr><td>Bishop - Padre </tr></td>
					<tr><td>black (a) - something reprehensible, e.g. "he's put up a black with the CO about the mess he made of the march-past"</tr></td>
					<tr><td>black box - instrument that enables bomb aimer to see through clouds or in the dark - see also gen box</tr></td>
					<tr><td>black-outs - WAAF knickers, navy-blue winter-weights</tr></td>
					<tr><td>Blenburgher - Bristol Blenheim aircraft</tr></td>
					<tr><td>Blighty - the UK</tr></td>
					<tr><td>blister - an enclosure/housing for a machine gun or cannon; a bulge in a perspex canopy to enable a better view</tr></td>
					<tr><td>blitz (A solid lump of) - large formation of close flying enemy aircraft</tr></td>
					<tr><td>blitz - the cleaning of barracks or buttons</tr></td>
					<tr><td>blitz buggy - an ambulance or very fast vehicle</tr></td>
					<tr><td>blitz time - time briefed for all aircraft to pass over target</tr></td>
					<tr><td>blonde job - young woman with fair hair especially a WAAF</tr></td>
					<tr><td>blood chit - ransom note carried by early fliers pre-war</tr></td>
					<tr><td>blood-wagon - ambulance</tr></td>
					<tr><td>bloody - at the time a fairly heavy duty profanity - often made milder as "ruddy"</tr></td>
					<tr><td>blotto - drunk as in "he was completely blotto"</tr></td>
					<tr><td>blower - aircraft supercharger; telephone</tr></td>
					<tr><td>blue (RAAF) - anything red</tr></td>
					<tr><td>Blue (The) - the desert</tr></td>
					<tr><td>bluebird - WAAF</tr></td>
					<tr><td>bobbing - currying of favour with a superior</tr></td>
					<tr><td>bods - squadron personnel</tr></td>
					<tr><td>body-snatcher - stretcher bearer</tr></td>
					<tr><td>boffins - scientific or technical types who worked on new aircraft or equipment developments</tr></td>
					<tr><td>bog - latrine - also "biffy"</tr></td>
					<tr><td>bogey - unidentified aircraft</tr></td>
					<tr><td>bogus - sham, spurious</tr></td>
					<tr><td>bolshie - a crewman who took a dim view of service bull</tr></td>
					<tr><td>bomber-boy - member of bomber aircrew</tr></td>
					<tr><td>bomb up - to load the bombs on to an aircraft</tr></td>
					<tr><td>bomphleteers - airmen engaged on the early pamphlet raids (later called Nickels)</tr></td>
					<tr><td>Bondu - expanse of land that we yomped across (RAF Regt)</tr></td>
					<tr><td>boob (to) - to make a mistake (boob)</tr></td>
					<tr><td>boomerang - returning early from an uncompleted operation because of alleged technical problems - see also DNCO</tr></td>
					<tr><td>boost - the amount of supercharging given to an engine to increase power (eg: +5 lb) </tr></td>
					<tr><td>Boozer - Radar receiver fitted to RAF bombers</tr></td>
					<tr><td>borrow - take, appropriate, purloin</tr></td>
					<tr><td>bought it (buy a packet) - be shot down, with connotations of carelessness or recklessness</tr></td>
					<tr><td>bounce - surprise attack</tr></td>
					<tr><td>bound rigid - see bind</tr></td>
					<tr><td>bowser - tanker truck or lorry used to refuel aircraft down the flights</tr></td>
					<tr><td>box clever - use of intelligence to avoid or wangle one's way out of a situation or duty</tr></td>
					<tr><td>Brains Trust - Central Trades Test Board, which examined candidates for a higher classification</tr></td>
					<tr><td>brass (the) or brasshats - Senior officers at the Wing or Group level - so called because of the amount of gold braid ("brass") found on their hats</tr></td>
					<tr><td>brassed off - some say this is less severe than "cheesed off" which in turn is less severe than "browned off" - unhappy</tr></td>
					<tr><td>break van - NAAFI or tea van</tr></td>
					<tr><td>brew up - prepare pot of tea</tr></td>
					<tr><td>Bricks/Bricks and Mortar - Air Ministry Works Department or Directorate (AMWD) -see Works and Bricks</tr></td>
					<tr><td>bride - chap's girl friend, not necessarily sexually intimate</tr></td>
					<tr><td>briefing - a meeting of all crews before they set out on an operation, to receive instructions for the op: some might have further specialist briefings e.g. navigators</tr></td>
					<tr><td>Brock's benefit - very intense display of flares, searchlights and AA fire (after the firework manufacturer who also supplied many of the flares used)</tr></td>
					<tr><td>brolly - parachute</tr></td>
					<tr><td>Bromide - jammer to counter the German X-Ger&auml;t bombing aid</tr></td>
					<tr><td>brown jobs - the Army - also "pongos" and "squaddies"</tr></td>
					<tr><td>brown (one's knees) - to have spent time in a posting with a hot climate - because of the heat the wearing of uniform KD shorts was necessary - "get your knees brown, sonny" putting (uppity) newcomers in their place</tr></td>
					<tr><td>brown - less severe error than a black</tr></td>
					<tr><td>browned off - fed up/angry: see brassed off</tr></td>
					<tr><td>bubble dancing - washing one's irons or plates</tr></td>
					<tr><td>buggers (play silly) - fool around, not take job seriously</tr></td>
					<tr><td>bull - formalities of the service (square bashing, saluting the King's Commission etc.) e.g. "he's full of bull"</tr></td>
					<tr><td>bullshit - longer version of bull</tr></td>
					<tr><td>Bully Beef - a canned meat product, consisting largely of fat - so called because of the Bull on the front of the Hereford Brand of corned beef</tr></td>
					<tr><td>bumf - useless paperwork, apparently bumph is not the correct version of this according to Partridge</tr></td>
					<tr><td>bumps - see circuits and bumps</tr></td>
					<tr><td>Bundoo (The) - the boondocks (see Blue) - somewhere far from civilisation</tr></td>
					<tr><td>bunk - small Corporals' Barrack Room with three bunks</tr></td>
					<tr><td>Burton - "Gone for a Burton" - killed in action - some say it comes from from old beer commercial for Burton Ale, but there are other explanations</tr></td>
					<tr><td>bus - an aircraft</tr></td>
					<tr><td>bus driver - bomber pilot</tr></td>
					<tr><td>Buster - as fast as possible</tr></td>
					<tr><td>buttoned up - job properly completed, all sorted out</tr></td>
					<tr><td>buy (to) - as in "to buy it" (see Burton) - "to buy the farm"; be killed or lost</tr></td>
					<tr><td>buzz - rumour</tr></td>
					<tr><td>cabbage - bomb </tr></td>
					<tr><td>Calvert Bar landing light system - A system of bars and intervening lights leading to the runway threshold</tr></td>
					<tr><td>Camp Comedian - Camp Commandant</tr></td>
					<tr><td>canteen cowboy - ladies' man</tr></td>
					<tr><td>Cas (the) - CAS, Chief of Air Staff</tr></td>
					<tr><td>Cat - Consolidated Catalina aircraft</tr></td>
					<tr><td>cat's eyes - particularly keen eyed pilot (esp. John Cunningham)</tr></td>
					<tr><td>Caterpillar Club - a club for those who had survived by using their parachutes - the pin was a small caterpillar (representing the insect that made silk) and was given by Irvin, the maker of parachutes</tr></td>
					<tr><td>Carpet operation - Supply dropping sortie to Resistance forces</tr></td>
					<tr><td>Chain Gang - aircrafthands, General Duties</tr></td>
					<tr><td>Chain Home - British early warning radar sytem</tr></td>
					<tr><td>chair-borne division - RAF personnel working in offices</tr></td>
					<tr><td>champagne glass - Handley Page Hampden aircraft</tr></td>
					<tr><td>Chance light - powerful light at end of runway which could be requested by a pilot in difficulty</tr></td>
					<tr><td>CHANNEL STOP - Attempt to close the English Channel to the passage of enemy shipping</tr></td>
					<tr><td>char - tea, from Hindustani borrowing from Chinese, tchai: "char and a wad"</tr></td>
					<tr><td>CHASTISE - attack on the Ruhr Dams by 617 Sqn, May 16/17 1943</tr></td>
					<tr><td>cheesed off - see brassed off</tr></td>
					<tr><td>Chiefy - Flight Sergeant</tr></td>
					<tr><td>chin food - a binder's talk</tr></td>
					<tr><td>chippy - carpenter</tr></td>
					<tr><td>chocks away - let's make a start</tr></td>
					<tr><td>chop, to get the - see Burton</tr></td>
					<tr><td>chuffed - happy, pleased</tr></td>
					<tr><td>chum - equivalent to the American "buddy"</tr></td>
					<tr><td>chute - a parachute</tr></td>
					<tr><td>Cigar - see ABC</tr></td>
					<tr><td>circuits and bumps - repeated touch and go landings in training a pilot training exercise in landing an aircraft and immediately taking off again</tr></td>
					<tr><td>Circus operations - Fighter-escorted daylight bombing attacks against short-range targets with the aim of bringing the enemy air force to battle</tr></td>
					<tr><td>civvies - civilian clothing</tr></td>
					<tr><td>civvy - a civilian</tr></td>
					<tr><td>civvy kip - bed in civilian life</tr></td>
					<tr><td>civvy street - the non military world outside the RAF</tr></td>
					<tr><td>clapped out - an aircraft or person nearing the end of its useful life - worn out, tired</tr></td>
					<tr><td>CLARION - American plan to disrupt German communications and morale by widespread bombing attacks</tr></td>
					<tr><td>clobber - flying gear it was necessary to wear in a wartime bomber</tr></td>
					<tr><td>close the hangar doors - stop talking shop (stop talking about RAF matters)</tr></td>
					<tr><td>clot - a person whose intelligence should be questioned</tr></td>
					<tr><td>club - propeller</tr></td>
					<tr><td>clueless - be ignorant of something</tr></td>
					<tr><td>cockup - a situation that has become extremely disorganised (from the term "cocked hat")</tr></td>
					<tr><td>collect a gong - receive a medal</tr></td>
					<tr><td>comb out - make an extensive ground target sweep with gunfire</tr></td>
					<tr><td>come up! - put some service in (those with less service are at the bottom of time-based promotion lists)</tr></td>
					<tr><td>coming to town - enemy aircraft approaching</tr></td>
					<tr><td>coming and going - applied to an aircraft fitted with a wireless set</tr></td>
					<tr><td>completely cheesed (off) - utterly fed up</tr></td>
					<tr><td>con course - conversion course e.g. when switching from one trade to another</tr></td>
					<tr><td>coned - when a master searchlight, often radar controlled, frequently described as having a blueish beam, picked up an aircraft, other searchlights in the area would swing onto the aircraft, thus coning it - then flak would be poured into the cone</tr></td>
					<tr><td>conservatory - cabin of a plane (from the perspex on three sides)</tr></td>
					<tr><td>Cookie - 4,000lb High Capacity blast bomb, usually dropped with incendiaries; also called a blockbuster (demolish a block of houses)</tr></td>
					<tr><td>cooler - guardroom</tr></td>
					<tr><td>cope - accomplish, deal with</tr></td>
					<tr><td>corker (a) - mine, also very attractive woman</tr></td>
					<tr><td>corkscrew - evasive manoeuvre when attacked by night fighter - sharp diving turn to port followed by sharp climbing turn to starboard: one of the gunners, watching the attack, would order the pilot "Corkscrew GO!"</tr></td>
					<tr><td>Corona - counterfeit orders to German fighters</tr></td>
					<tr><td>court a cat - take a girlfriend out</tr></td>
					<tr><td>COSSAC - collective title for Anglo-American staff groups which planned OVERLORD before General Eisenhower's appointment as Supreme Commander</tr></td>
					<tr><td>crab - Avro 504 training aircraft</tr></td>
					<tr><td>crabbing along - flying near the ground or water</tr></td>
					<tr><td>crack ( ) down - (it) when applied to an aircraft, to shoot it down: to crack down on the deck (in the drink) - to crash land on the ground (on the sea)</tr></td>
					<tr><td>crack-up - as a verb, to be showing signs of operational stress; otherwise an accident involving repairable damage</tr></td>
					<tr><td>crate - aircraft, particularly one which is obsolescent</tr></td>
					<tr><td>crib - verb, to complain about</tr></td>
					<tr><td>cricket - German night fighter plane</tr></td>
					<tr><td>CROSSBOW - attack on V-weapon launching sites</tr></td>
					<tr><td>crown (get your) - be promoted to Flight Sergeant, who wore a crown badge above the sergeant's three stripes</tr></td>
					<tr><td>curtains - killed</tr></td>
					<tr><td>Crump Dump - the Ruhr</tr></td>
					<tr><td>crump hole - bomb crater</tr></td>
					<tr><td>cu - cumulus cloud</tr></td>
					<tr><td>cu nim - cumulo nimbus cloud</tr></td>
					<tr><td>Daffy - Boulton Paul Defiant aircraft</tr></td>
					<tr><td>daisy cutter - faultless landing</tr></td>
					<tr><td>Dalton Computer - early mechanical hand held computer used in air navigation</tr></td>
					<tr><td>Darkie - system of homing at night on radio bearings provided by base when requested</tr></td>
					<tr><td>Day Ranger - Operation to engage air and ground targets within a wide but specified area, by day</tr></td>
					<tr><td>dead stick - engine failed - e.g. dead stick landing is a landing without engine power</tr></td>
					<tr><td>debriefing - interrogation over mugs of tea with the Intelligence staff to elicit what happened on the op</tr></td>
					<tr><td>deck - the ground : "crack down on the deck" - to "pancake" an aircraft</tr></td>
					<tr><td>desert lily - urinal made from tin can</tr></td>
					<tr><td>dicey do - a particularly hair-raising operation</tr></td>
					<tr><td>dickey flight - a training flight where a pilot not experienced on operations or a senior officer returning to operations would go on an op with an experienced crew as a "second dickey"</tr></td>
					<tr><td>dicky seat - the seat originally designed for a second pilot</tr></td>
					<tr><td>dim view (take a) - view with scepticism or disapproval</tr></td>
					<tr><td>Distil operation - Fighter operation to shoot down enemy aircraft minesweeping, usually off Denmark</tr></td>
					<tr><td>ditch - to force land on water</tr></td>
					<tr><td>dobhi - one's laundry</tr></td>
					<tr><td>DODGE - ferrying troops home from Italy</tr></td>
					<tr><td>dog-fight - aerial scrap</tr></td>
					<tr><td>doggo (lie) - remain quiet/hidden</tr></td>
					<tr><td>Domino - Jammer to counter the German X-Ger&auml;t bombing aid</tr></td>
					<tr><td>dope - nitrocelluloid liquid, similiar to nail polish, used to tighten and harden fabric (linen) airframe covering</tr></td>
					<tr><td>down the flights - the area on an airfield where the aircraft were serviced between ops</tr></td>
					<tr><td>DRAGOON - ultimate code name for the invasion of Southern France in August 1944 (previously named ANVIL)</tr></td>
					<tr><td>Drem lighting - System of outer markers and approach lights installed at many airfields in the early years of the war</tr></td>
					<tr><td>drill (the right) - correct way</tr></td>
					<tr><td>drink (in the) - to come down in the sea</tr></td>
					<tr><td>driver, airframe - pilot (play on RAF Quarter Master labelling of items)</tr></td>
					<tr><td>drome - (aerodrome) - an airfield</tr></td>
					<tr><td>dud - weather: when not fit to fly - bombs/ammunition: didn't go off</tr></td>
					<tr><td>duff - see u/s - also "not accurate/wrong" as in "duff gen" - the opposite of "pukka gen"</tr></td>
					<tr><td>D&uuml;ppel - German name for metal foil dropped to confuse radar</tr></td>
					<tr><td>dustbin - ventral gunner's position in aircraft</tr></td>
					<tr><td>dust-up - heated action/fight/altercation</tr></td>
					<tr><td>egg - a bomb or mine (lay eggs - lay mines)</tr></td>
					<tr><td>egg-whisk - an autogyro</tr></td>
					<tr><td>Elsan - chemical toilet carried on some aircraft (Elsan gen - unreliable information)</tr></td>
					<tr><td>embark leave - embarkation leave given when about to go overseas</tr></td>
					<tr><td>Engines - Engineering Officer</tr></td>
					<tr><td>Erb - any airman</tr></td>
					<tr><td>erk - junior ground crew - from the Cockney pronunciation of aircraftman</tr></td>
					<tr><td>Eureka - ground radio transmitter for guiding bombers to their target</tr></td>
					<tr><td>everything under control - all is well</tr></td>
					<tr><td>EXODUS - Ferrying troops and displaced persons to and from Europe</tr></td>
					<tr><td>Eyetie - an Italian (plane)</tr></td>
					<tr><td>Faithful Annie - Avro Anson</tr></td>
					<tr><td>fan - propeller</tr></td>
					<tr><td>Fighter Night patrol - Fighter patrol over area where anti-aircraft gunners were ordered not to fire, sometimes restricted to certain altitudes</tr></td>
					<tr><td>finger (to remove one's) - hurry up and/or pay attention</tr></td>
					<tr><td>fishheads - Navy</tr></td>
					<tr><td>fitter - ground crew responsible for engines and related controls</tr></td>
					<tr><td>Firebash sorties - sorties by Mosquitoes of 100 Group with the aircraft delivering incendiary or napalm loads on German airfields</tr></td>
					<tr><td>fireproof - invulnerable</tr></td>
					<tr><td>fireworks - heavy anti aircraft fire</tr></td>
					<tr><td>Fizzer - (disciplinary) charge</tr></td>
					<tr><td>Flak - Fliegerabwehrkanonen - anti-aircraft gun: in reports heavy/light flak referred to to the calibre observed not the intensity - "getting some flak" - being criticised</tr></td>
					<tr><td>flame float - small floating incendiary device thrown down the flare chute so that the rear gunner could center the "pip" on his reflector sight on the point of light and then read off the degree of deviation from a scale on his turret ring - thus providing the navigator with the degree of wind drift blowing the a/c off track</tr></td>
					<tr><td>flamer - aircraft shot down in flames</tr></td>
					<tr><td>flaming - mild, all purpose expletive</tr></td>
					<tr><td>flaming onions - anti aircraft tracer</tr></td>
					<tr><td>flannel - to avoid the truth, bluffing</tr></td>
					<tr><td>flap - ("there's a flap on") - rush to get something done - somewhat panicky activity e.g. resulting from the unexpected arrival of a very senior officer</tr></td>
					<tr><td>flare path - a row of lights (either kerosene gooseneck flares or permanent base electric lights) marking the boundary of the runway for taking off and landing</tr></td>
					<tr><td>flat out for (be) - support, be in favour of ("I'm flat out for his move")</tr></td>
					<tr><td>flicks - searchlights or cinema.</tr></td>
					<tr><td>flight - a bomber squadron was in one or more Flights - e.g. A and B each consisting of 6-8 aircraft and crews: each Flight was commanded by a Squadron Leader or Flight Lieutenant - A Flight aircraft were lettered A-N and B Flight from M-Z</tr></td>
					<tr><td>Flight - Flight Sergeant (Flight Louie - Flight Lieutenant: flight magician - flight mechanic)</tr></td>
					<tr><td>fling (or throw) one up - salute</tr></td>
					<tr><td>flip - short flight, especially when as a favour to a friend</tr></td>
					<tr><td>flying brevet - cloth insignia worn on all uniforms including battle dress to indicate aircrew trade - pilots' brevets were two winged - all other crew wore a single wing with their trade marked inside a circular area at the base of the wing: in the early part of the war a trade which later disappeared was Observer (Navigator/Bomb Aimer), they wore a winged O, which was fondly known as the flying arsehole</tr></td>
					<tr><td>flying log - every aircrew member was required to keep a flying logbook of every flight taken - including air tests, transport, training and operational flying - this was signed by the Flight Commander each month and by the CO. At the end of a tour the C/O and the Trade Leader would sign (eg: a Bomb Aimer's log would be signed by the Bombing Leader, The Gunner's by the Gunnery Leader etc.)</tr></td>
					<tr><td>Flying Pencil - Dornier bomber</tr></td>
					<tr><td>Flying Suitcase (Tadpole) - Handley Page Hampden</tr></td>
					<tr><td>Flying Tin-Opener - Hurricane in tank busting role</tr></td>
					<tr><td>fold up (to) - suddenly crash</tr></td>
					<tr><td>football feet - make excessive use of rudder</tr></td>
					<tr><td>fore-and-aft - RAF field service cap (now known as a 'chip bag') as distinct from dress service cap</tr></td>
					<tr><td>foreigner - article made of RAF material</tr></td>
					<tr><td>Form 78 - RAF form also called Aircraft Movement Card which followed the aircraft from the manufacturer to its final resting place</tr></td>
					<tr><td>Form 540 - pages of this form make up the Operations Record Books (ORB): the column headings are date, aircraft type and number, crew, duty, Time up, Time down, Details of sortie or flight, References</tr></td>
					<tr><td>Form 541 - pages of this form were used for the Appendices to the ORB</tr></td>
					<tr><td>Form 700 - form setting out the serviceability status of an aircraft, signed by the captain to signify taking over responsilbility for it from the ground crew: the Form had to list any defects</tr></td>
					<tr><td>Fort - Boeing B-17 Heavy Daylight Bomber</tr></td>
					<tr><td>fox (to) - to do something clever or rather cunning, bamboozle</tr></td>
					<tr><td>Freeman, Hardy &amp; Willis - or Pip, Squeak and Wilfred - 1914-15 Star, Overseas Service and Victory medals of the 1914-18 war (FHW was a chain of shoe shops, Pip etc. were comic strip characters). If the 1914-15 Star was missing the other two were nicknamed Mutt and Jeff from two early cinema cartoon characters</tr></td>
					<tr><td>Freya - German early warning radar</tr></td>
					<tr><td>frozen on the stick - paralysed with fear</tr></td>
					<tr><td>fruit salad - ribbons from medals ("gongs") wrapped around a thin bar and sewn together were worn under the flying brevet - the various colours of the ribbons would resemble fruit salad - applied to someone who had a large collection of awards; sometimes to Americans, who seemingly had more opportunities for awards than the RAF</tr></td>
					<tr><td>FULLER - counter measures agains the escape of the Scharnhorst and the Gneisenau from Brest</tr></td>
					<tr><td>full bore - flat out, at top speed</tr></td>
					<tr><td>Gardening - sea/coastal mine-laying by aircraft</tr></td>
					<tr><td>gate (through the) - apply maximum power</tr></td>
					<tr><td>Gee - Medium-range radio aid to navigation equipment employing ground transmitters and airborne receiver</tr></td>
					<tr><td>Giant W&uuml;rzburg - German fighter control radar</tr></td>
					<tr><td>Grand Slam - 22,000lb penetrating (earthquake) bomb</tr></td>
					<tr><td>groundcrew - RAF trade not wearing a flying brevet</tr></td>
					<tr><td>gen - a person on squadron who knew what he was doing - as in "a gen bod"</tr></td>
					<tr><td>gen - information (either good - see "pukka" or bad see "duff"): gen up - to swot for exams</tr></td>
					<tr><td>gen box - see black box</tr></td>
					<tr><td>George - automatic pilot</tr></td>
					<tr><td>Gerry or Jerry - German</tr></td>
					<tr><td>get cracking - get on with it, get going</tr></td>
					<tr><td>get some in - advice given to sprog crews who inadvisedly gave old lags on their opinion of operational flying (from "get some time in") - often paired with "chum" as in "Get some in chum, before you tell your grandmother how to suck eggs"</tr></td>
					<tr><td>gharry - originally a horse drawn cart - came to mean any form of wheeled transport</tr></td>
					<tr><td>get some flying hours in - get some sleep</tr></td>
					<tr><td>gippy tummy - "the screaming (h)ab dabs", dysentry, "the trots", at worst, gastroenteritis, associated with Egypt</tr></td>
					<tr><td>glasshouse - held in detention on camp</tr></td>
					<tr><td>goggled goblin - night fighter pilot</tr></td>
					<tr><td>going (around again) - repeating the bomb run to get a better result</tr></td>
					<tr><td>golden eagle sits on Friday/lays its eggs - next Friday is pay parade</tr></td>
					<tr><td>GOMORRAH - firestorm raid on Hamburg 1943</tr></td>
					<tr><td>gone for six - dead</tr></td>
					<tr><td>goner - fatally hit (aircraft or person) - killed/missing: see also Burton as in "gone for a Burton"</tr></td>
					<tr><td>gong - a medal, specifically a decoration, but now used to describe all service medals </tr></td>
					<tr><td>good show - excellent performance, well done</tr></td>
					<tr><td>goof - ladies man</tr></td>
					<tr><td>goolie chit - a scrap of paper or piece of cloth that when shown to the natives of a country over which you might be shot down offered a reward if they would return you alive and entire to the nearest Allied unit: arose from NW frontier and Iraq where the ladies of some tribes played nasty games with sharp knives</tr></td>
					<tr><td>goolies - a vital body part for aircrew - "I almost got my goolies shot off, last op"</tr></td>
					<tr><td>goon - fool, very stupind person: POW - German guards</tr></td>
					<tr><td>Gossage - barrage balloon (after AOC Balloon Command Sir Leslie Gossage)</tr></td>
					<tr><td>grab for altitude - try to gain altitude in flight: become very angry</tr></td>
					<tr><td>grand - unit of altitude, 1000 feet - see also the more often quoted angels</tr></td>
					<tr><td>gravel bashing - see square bashing</tr></td>
					<tr><td>gravel basher - Drill or Physical Training Instructor</tr></td>
					<tr><td>Gravy (the) - Atlantic: gravy - fuel</tr></td>
					<tr><td>green (in the) - all engine control gauges operating correctly - a needle which swung into the "red" indicated a malfunction</tr></td>
					<tr><td>green (to get the) - to receive permission to take off (expanded to get permission for anything) - the airfield control officer would signal with a morse code Aldis Lamp with a green lens to give an aircraft permission to take off - usually the message was the letter of the aircraft (eg: P for Peter - .. -)</tr></td>
					<tr><td>greenhouse - cockpit windows</tr></td>
					<tr><td>greens (when referring to undercarriage instrumentation) green lights referring to the retractable undercarriage legs, red lights indicate legs moving between up/locked and down/locked. When all up and locked there are no lights showing (makes bulbs last longer, most old indicators had two sets of green lamps switchable to cater for a blown bulb)</tr></td>
					<tr><td>gremlin - a mythical creature that lived on certain aircraft and caused it to go u/s at the most inconvenient times and then could not be located as the source of the problem when checks were made</tr></td>
					<tr><td>grief (to come to) - be destroyed or to get into trouble</tr></td>
					<tr><td>GR Navvy - general reconnaissance navigator</tr></td>
					<tr><td>grocer - Equipment Officer</tr></td>
					<tr><td>groceries - bombs see also cabbage, cookie, egg</tr></td>
					<tr><td>grope - ground operational exercise</tr></td>
					<tr><td>grounded - not permitted to fly: someone newly married (can't fly by night)</tr></td>
					<tr><td>ground wallah - an officer who did not fly</tr></td>
					<tr><td>ground-strafe - low flying attack</tr></td>
					<tr><td>Group - a formation of Wings</tr></td>
					<tr><td>Groupie - Group Captain - usual rank of officer who commanded a Wing or and RAF Station</tr></td>
					<tr><td>gubbins - equipment or needed material (eg: "has that kite got the gubbins for dropping a cookie?")</tr></td>
					<tr><td>Guinea Pig Club - after an incident where aircrew were extremely badly burned they would be sent to East Grinstead Hospital in the U.K. where some of the foremost plastic surgeons of the day performed "cutting edge" surgery - the term was made up by the patients themselves - many today proudly wear the maroon tie of the club </tr></td>
					<tr><td>Gussie - officer (from Augustus, a then typical 'tony' or posh name)</tr></td>
					<tr><td>hack (station) - a/c on squadron used for general communications duties or as the CO's private mount</tr></td>
					<tr><td>HADDOCK - code name for a force of Wellingtons sent to the South of France in 1940 to bomb Italy</tr></td>
					<tr><td>half-pint hero - a boaster</tr></td>
					<tr><td>half-section - mate, companion, even wife</tr></td>
					<tr><td>Halibag - Handley Page Halifax four engine bomber</tr></td>
					<tr><td>Ham-Bone - Handley Page Hampden</tr></td>
					<tr><td>Hangar doors closed! - see close the hangar doors</tr></td>
					<tr><td>Happy Valley - the Ruhr, much bombed and very heavily defended</tr></td>
					<tr><td>heavy - heavy bomb or bomber</tr></td>
					<tr><td>hedge-hopping - flying so low that the aircraft appears to hop over the hedges</tr></td>
					<tr><td>Herc - Bristol Hercules sleeve valve air cooled radial engine</tr></td>
					<tr><td>High Tea</tr></td>
					<tr><td>Himmelbett - German system of controlled night fighting</tr></td>
					<tr><td>hip-flask - revolver</tr></td>
					<tr><td>hit the deck - to land</tr></td>
					<tr><td>hockey stick - bomb loading jack or hoist</tr></td>
					<tr><td>hoick off - take off: move quickly to some place</tr></td>
					<tr><td>hold everything - stop what you are doing and wait for new instructions</tr></td>
					<tr><td>hoosh - land at great speed</tr></td>
					<tr><td>homework - sweetheart or girlfriend</tr></td>
					<tr><td>Hoover - fighter sweep</tr></td>
					<tr><td>hop the twig - crash fatally (Canadian)</tr></td>
					<tr><td>hot - right (bang) up to date: fast (USA)</tr></td>
					<tr><td>hours - on 24 hour clock - time - 2345 hours would be 12:45 p.m. ,also could be the amount of time in the air as calculated in your log book: night hours were usually written in red</tr></td>
					<tr><td>huffy - WAAF disdainful of approaches</tr></td>
					<tr><td>hulk - a severely damaged aircraft</tr></td>
					<tr><td>humdinger - very fast plane</tr></td>
					<tr><td>Hun - German</tr></td>
					<tr><td>Hurryback - Hurricane fighter</tr></td>
					<tr><td>Hurrybuster - see Flying Tin Opener</tr></td>
					<tr><td>illuminator - a crew tasked with dropping flares on a night target so that the following a/c could aim accurately</tr></td>
					<tr><td>intell - intelligence officer (I/O) or intelligence report</tr></td>
					<tr><td>intercom - the system by which the various crew members communicated with each other by voice in the aircraft</tr></td>
					<tr><td>iron lung - Nissen Hut</tr></td>
					<tr><td>Irvin Jacket - Standard RAF leather flying jacket lined with fleece</tr></td>
					<tr><td>jankers - to be put "on a charge" for a violation of service discipline</tr></td>
					<tr><td>Jerry - German (plane)</tr></td>
					<tr><td>Jerrycan - excellent German heavy duty portable can for holding water, fuel or other liquid - quickly replaced the leaky tins used by the RAF, was manufactured in England to the German pattern</tr></td>
					<tr><td>jink away - sharp manoeuvre, sudden evasive action of aircraft</tr></td>
					<tr><td>juice - aviation fuel (as in "we are low on juice") - also "gravy" - AVGAS was 100 Octane petrol</tr></td>
					<tr><td>K site - airfields with dummy aircraft for deception by day</tr></td>
					<tr><td>keen - eager or reliable - "keen as mustard "- pun on Kean's mustard powder</tr></td>
					<tr><td>khamsin - a desert dust storm</tr></td>
					<tr><td>king - NCO in charge of, e.g. bowser king</tr></td>
					<tr><td>Kipper Kite - Coastal Command aircraft which protected fishing fleets in the North and Irish Seas</tr></td>
					<tr><td>kit - ones belongings, both issue and personal - (hence kitbag) - also used to mean equipment as in "Does that erk have the kit to repair the hole in the starboard wing?" </tr></td>
					<tr><td>kite - an aircraft (in the USAAF also called a "ship)</tr></td>
					<tr><td>Knickebein - German navigational aid</tr></td>
					<tr><td>knot - measure of air or ground speed - one nautical mile per hour (1.150 statute miles per hour)</tr></td>
					<tr><td>Ladybird - WAAF Officer</tr></td>
					<tr><td>Lagoon - Shipping reconnaissance operation off the Dutch coast</tr></td>
					<tr><td>laid on (have) - provided, organised for you</tr></td>
					<tr><td>Lanc - Avro Lancaster bomber</tr></td>
					<tr><td>let down - descend through cloud</tr></td>
					<tr><td>let up - ease up on throttle</tr></td>
					<tr><td>Lib - Consolidated B-24 Liberator</tr></td>
					<tr><td>Lichtenstein - German night fighter radar</tr></td>
					<tr><td>Lindholme gear - equipment dropped from air-sea rescue aircraft to crews ditched in the sea, developed at RAF Lindholme</tr></td>
					<tr><td>line book - book kept in the Mess in which two or more officers could record a 'line shoot' by someone</tr></td>
					<tr><td>line shoot - shooting a line - exaggerating one's accomplishments - usually responded to by the line "there I was upside down, nothing on the clock but the makers name...."</tr></td>
					<tr><td>lose your wool - lose composure</tr></td>
					<tr><td>Lorenz system - Blind beam approach system</tr></td>
					<tr><td>low down (the) - inside information</tr></td>
					<tr><td>Mae West - inflatable life vest worn over flying suit (when inflated resembled the pigeon breasted movie star)</tr></td>
					<tr><td>Mahmoud sortie - night fighter sortie to specific point over enemy territory to engage his night fighters in that area</tr></td>
					<tr><td>mahogany Spitfire - desk - "flown" by penguins and ground wallahs</tr></td>
					<tr><td>main bit - major section of an aircraft</tr></td>
					<tr><td>Mandrel - Airborne radio swamping of the German early warning system, device used by 100 Group</tr></td>
					<tr><td>MANNA - delivery of food and supplies to Holland by air in 1945</tr></td>
					<tr><td>Meacon - stations set up to broadcast signals aimed at 'bending' or altering German navigation transmissions</tr></td>
					<tr><td>Meatbox - early versions of the Gloster Meteor, implied reference to coffin</tr></td>
					<tr><td>meat wagon - ambulance</tr></td>
					<tr><td>mepacrine - standard anti-malarial drug of the day</tr></td>
					<tr><td>mess - place assigned for other ranks, or (separately) NCOs and Officers to eat or relax - there was a protocol as to who could enter who's mess</tr></td>
					<tr><td>Met - Meteorology Officer or weather report</tr></td>
					<tr><td>Mickey Mouse - bomb-dropping controls - the bombing panel consisted of a clockwork distributor and selection switches (hence like a mickey mouse watch)</tr></td>
					<tr><td>MILLENNIUM - first 1000 bomber raid, on Cologne May30/31 1943</tr></td>
					<tr><td>milling around - aircraft forming defensive circle</tr></td>
					<tr><td>mixed death - various types of ammunition combined in a belt</tr></td>
					<tr><td>mob - Royal Air Force</tr></td>
					<tr><td>Monica - Radar fitted in rear of Bomber Command aircraft to provide some early warning of night fighters; in July 1944 it was found that Monica was being detected as a homing signal for the Luftwaffe</tr></td>
					<tr><td>muscle in (on) - to take advantage of something</tr></td>
					<tr><td>Mossie - De Haviland Mosquito aircraft</tr></td>
					<tr><td>Musical Paramatta - method of ground marking a target by coloured target indicators dropped blindly on Oboe</tr></td>
					<tr><td>Musical Wanganui - method of sky marking a target by parachute borne coloured markers dropped blindly on Oboe</tr></td>
					<tr><td>Naxos - German radar device enabling fighters to home on H2S transmissions of bombers</tr></td>
					<tr><td>Newhaven - Method of ground marking a target by flares or target indicators dropped blindly on HS followed by visual identification</tr></td>
					<tr><td>Nickel - Leaflet dropping operation</tr></td>
					<tr><td>Nickels - propaganda leaflets</tr></td>
					<tr><td>Night Ranger - Operation to engage air and ground targets within a wide but specified area, by night</tr></td>
					<tr><td>no joy - no enemy contact</tr></td>
					<tr><td>Nobby - all purpose nickname for anyone called Clark or Clarke. Originally clarks (which we now almost all spell "clerks", but in the UK still pronounced in the original fashion) wore top hats as a sign of their trade. The gentry, or "nobs" also wore top hats and thus the clarks came by the name "nobby" because of their "posh" hats</tr></td>
					<tr><td>Oasthouse</tr></td>
					<tr><td>Oboe - Ground-controlled radar system of blind bombing in which one station indicated track to be followed and another the bomb release point</tr></td>
					<tr><td>OCTAGON - second Quebec Conference, September 1944</tr></td>
					<tr><td>odd bod - crew member who had lost his crew or who had fallen behind the rest of his crew in number of operational trips, or whose crew could not fly because of illness etc., and who flew as a "spare" with another crew, "spare bod"</tr></td>
					<tr><td>office - cockpit of aircraft</tr></td>
					<tr><td>old lag - experienced airman - often Regular Airforce</tr></td>
					<tr><td>Old Man (the) - Squadron CO</tr></td>
					<tr><td>on the beam - some stations were equipped with a landing beam which told the pilot he was on the correct glide slope for landing - if he flew too high he would hear a series of morse dots and if too low a series of morse dashes - the idea was to keep a steady tone in one's earphones - also showed up in some aircraft as a set of lights showing that one was on the correct beam or too high or low - also used for flying on a navigation beam such as Gee or Oboe - generally translated to being on the right course of action about anything as in "I think the Wingco's on the beam about not flying over the Alps again." </tr></td>
					<tr><td>Oggin -sea, ocean</tr></td>
					<tr><td>op (operation) - an attack on the enemy (USAAF term - "mission")</tr></td>
					<tr><td>opsum - Operational Summary - prepared by the Intelligence Officer from debriefing notes recording the results of an operation</tr></td>
					<tr><td>oranges - Vitamin C tablets</tr></td>
					<tr><td>organise - to "win" a wanted article</tr></td>
					<tr><td>other ranks - "ORs" - those below officer level</tr></td>
					<tr><td>overload tanks - extra fuel tanks required when for example a Wellington was operated at its extreme range - two could be fitted in the bomb bays and one could be fitted on the rest cot in the fuselage</tr></td>
					<tr><td>OVERLORD - Allied invasion of northern France in June 1944</tr></td>
					<tr><td>pack up - cease to function - "My port engine packed up coming out of the target area"</tr></td>
					<tr><td>packet (catch a) - be on the receiving end of offensive fire (as in "I heard Nobby caught a packet over Verona last night")</tr></td>
					<tr><td>PAMPA - Long-range weather reporting sortie</tr></td>
					<tr><td>pancake - to land</tr></td>
					<tr><td>pansy - effeminate</tr></td>
					<tr><td>party - air battle</tr></td>
					<tr><td>passion killers - WAAF blackouts (see blackouts)</tr></td>
					<tr><td>passion wagon - WAAF transport</tr></td>
					<tr><td>Pebble Monkeys - trainee Rock Apes</tr></td>
					<tr><td>peel off (to) - break formation to engage enemy</tr></td>
					<tr><td>penguin - for ground officers with no operational experience: for mebers of the RAF Regiment (Rock Apes) a Penguin was anyone who wore a Blue uniform </tr></td>
					<tr><td>perch - to land</tr></td>
					<tr><td>pickled - drunk</tr></td>
					<tr><td>pie eyed - drunk</tr></td>
					<tr><td>piece of cake - an easy target with little opposition - anything easily done</tr></td>
					<tr><td>piece of nice - any pleasant entertainment</tr></td>
					<tr><td>plaster - to bomb heavily and accurately</tr></td>
					<tr><td>Plastic Sgt - newly unsubstantive Sgt Aircrew</tr></td>
					<tr><td>play pussy - hide in the clouds.</tr></td>
					<tr><td>pleep - A squeak, rather like a high note klaxon.</tr></td>
					<tr><td>plonk - cheap wine, also "AC plonk" - A/C 2 - the lowest rank in the RAF</tr></td>
					<tr><td>plug away - continue to fire, keep going for target</tr></td>
					<tr><td>plumber - Armourer</tr></td>
					<tr><td>Pom - Australian term for the British - also "Pommy" used adjectivally as in "What a typical Pommy cockup.."</tr></td>
					<tr><td>POINTBLANK - directive for the Combined Bomber Offensive, June 1943</tr></td>
					<tr><td>Pond life - lowest of the low</tr></td>
					<tr><td>poop off - open fire</tr></td>
					<tr><td>popsie - young lady</tr></td>
					<tr><td>port - the left side of a/c as seen from pilots seat</tr></td>
					<tr><td>posted - orders sending a crewman to another station or responsibility</tr></td>
					<tr><td>prang - to crash an a/c or to hit a target well</tr></td>
					<tr><td>PRATA - Weather reconnaissance flight</tr></td>
					<tr><td>press on regardless - unofficial motto of RAF - meant to show keeness to fly through adversity to the target</tr></td>
					<tr><td>Prune, P/O - fictional officer in the RAF training manuals who demonstrated all of the things that could go wrong if procedures were not followed correctly</tr></td>
					<tr><td>pukka - genuine as in "pukka gen"</tr></td>
					<tr><td>pulpit - cockpit of aircraft</tr></td>
					<tr><td>Pundit - a flashing light which signalled the airfield identity in order to assist navigation - RAF ones flash red, Civil ones flash green</tr></td>
					<tr><td>Pull the tit - the act of operating the Boost Cut-out Control (Boost Override) on fighter aircraft in order to increase engine power in emergency situations - also "through the gate"</tr></td>
					<tr><td>QUADRANT - first Quebec Conference, August, 1943</tr></td>
					<tr><td>Q-site - site flashing lights to represent a mock airfield to attract enemy attention at night</tr></td>
					<tr><td>Queen Bee - WAAF Officer in charge of the WAAF in a unit or camp</tr></td>
					<tr><td>Queen Mary - an articulated "semi" trailer used to transport aircraft or aircraft parts by ground to Maintenance Units for service or refurbishment </tr></td>
					<tr><td>quick squirt - short sharp burst of machine-gun fire - "quickie"</tr></td>
					<tr><td>Ramrod - bomber raid escorted by fighters aimed at destruction of a specific target in daylight</tr></td>
					<tr><td>Ranger - usually a deep penetration flight to a specified area, to engage targets of opportunity</tr></td>
					<tr><td>Rhombus - weather reporting flight</tr></td>
					<tr><td>Rhubarb - low-level strike operation mounted in cloudy condition against enemy targets in Occupied Countries</tr></td>
					<tr><td>rigger - ground crew responsible for airframe (specialists might include "instrument bashers" and "sparks" to look after instruments and electrical systems)</tr></td>
					<tr><td>rings - rank designation on officer's cuffs e.g. half ringer = Pilot Officer, one ringer = Flying Officer, two ringer = Flight Lieutenant, two and a half = Squadron Leader, three = Wing Commander</tr></td>
					<tr><td>Roadstead - fighter operation mounted against shipping</tr></td>
					<tr><td>ROBINSON - daylight Lancaster raid on Le Creusot, October 17th 1942</tr></td>
					<tr><td>rocket - a reprimand</tr></td>
					<tr><td>Rodeo - Fighter sweep</tr></td>
					<tr><td>Rodney(s) - officers of any rank RAF Regt</tr></td>
					<tr><td>roddie or rodded bombs - bomb fitted with a rod in the nose so that it would explode above the ground - used in antipersonnel ops</tr></td>
					<tr><td>ropey - uncomplimentary adjective "A ropey landing", "A ropey type", "A ropey evening", etc.</tr></td>
					<tr><td>round - one cartridge of .303 ammunition, ammunition was measured in number of rounds carried</tr></td>
					<tr><td>Rover - Coastal Command armed patrol to search for enemy shipping</tr></td>
					<tr><td>R/T - Radio Telecoms</tr></td>
					<tr><td>runup - to test engines for magneto drop before taking off - also the route taken into the target area before the bomb dropping point was reached</tr></td>
					<tr><td>salvo - bomb selection which released all bombs at the same time</tr></td>
					<tr><td>sardine-tin - torpedo carrying plane</tr></td>
					<tr><td>Sashlite bulb - Photo-flash bulb used for training and experimental purposes</tr></td>
					<tr><td>Sasso - SASO - Senior Air Staff Officer</tr></td>
					<tr><td>sawn-off - short in stature</tr></td>
					<tr><td>scapathy - feeling of depression said to be found among some of those posted to the Orkneys</tr></td>
					<tr><td>scarecrow - crews reported aircraft blowing up without evidence of attacks (e.g. tracer), and the story arose that the Germans were firing scarecrow shells to simulate stricken aircraft, so as to demoralise crews. Probably actually the result of Schr&auml;ge Musik attacks, but less easy to explain in daylight reports.</tr></td>
					<tr><td>scarlet slugs - Bofors tracer fire</tr></td>
					<tr><td>schooly - Education Officer</tr></td>
					<tr><td>Schr&auml;ge Musik - devastatingly successful upward firing gun fighter attack using tracerless ammunition against bombers used by expert Luftwaffe pilots, used in WWI by Albert Ball</tr></td>
					<tr><td>Scopie - Air Traffic Controller</tr></td>
					<tr><td>scramble - immediate operational take off</tr></td>
					<tr><td>scrambled egg - gold braid on Group Captain or higher officers' hats</tr></td>
					<tr><td>scrap - fight</tr></td>
					<tr><td>scraper ring - half (middle) ring on a Squadron Leader's tunic cuffs (technical origin: from pistons)</tr></td>
					<tr><td>scream downhill - execute a power dive</tr></td>
					<tr><td>screamer - bomb that makes a whistling sound as it comes down</tr></td>
					<tr><td>screened - a period after completing a tour when aircrew could not be called on to do operational flying</tr></td>
					<tr><td>screw - propeller</tr></td>
					<tr><td>scrounge - obtain illicitly</tr></td>
					<tr><td>scrub - to cancel an op</tr></td>
					<tr><td>scrub around - take evasive action</tr></td>
					<tr><td>SEA LION - German plan for the invasion of England</tr></td>
					<tr><td>second dickey - additional pilot usually aboard for experience on an operation before starting ops with his own crew: the first pilot is however not called a first dickey</tr></td>
					<tr><td>senior scribe - NCO i/c O/R - NCO in charge of the Orderly Room</tr></td>
					<tr><td>Serrate sortie - Operation to locate and destroy enemy night fighters and combined with night bomber raids. Made use of airborne radar</tr></td>
					<tr><td>SEXTANT - Cairo Conference, November-December 1943</tr></td>
					<tr><td>Shagbat - Supermarine Walrus aircraft</tr></td>
					<tr><td>shakey do - see "dicey do"</tr></td>
					<tr><td>shed - hangar</tr></td>
					<tr><td>ships that pass in the night - those serving only for the duration (of the war)</tr></td>
					<tr><td>shit - very bad weather</tr></td>
					<tr><td>shooting a line - see line shoot: recorded in line book or shooting gallery</tr></td>
					<tr><td>shot down in flames - crossed in love - severely reprimanded</tr></td>
					<tr><td>shot up - very drunk</tr></td>
					<tr><td>shot to ribbons - totally incapable through drink</tr></td>
					<tr><td>show - performance or situation - ("that was a good show over Budapest" or "he put on a bad show")</tr></td>
					<tr><td>shufti (have a) - take a look</tr></td>
					<tr><td>Sid Walker gang - crash salvage party: Cockney comedian of that name, song of his was 'Day after day, I'm on my way, Any rags, bottles or bones?'</tr></td>
					<tr><td>silver sausage - barrage balloon</tr></td>
					<tr><td>six (hit for a) - to score maximum points - to put on a very good show (from cricket) </tr></td>
					<tr><td>skipper - the captain of the aircraft and crew leader - in the air his rule was law regardless of his rank</tr></td>
					<tr><td>skirt patrol - search for a female companion</tr></td>
					<tr><td>sky-piece - smoke trails</tr></td>
					<tr><td>sky pilot - padre</tr></td>
					<tr><td>smashing job - excellent (aircraft, girl)</tr></td>
					<tr><td>snake - lively or noisy party</tr></td>
					<tr><td>snake about - evasive action when chased by enemy fighters or searchlights or AA fire</tr></td>
					<tr><td>snake-charmers - dance band</tr></td>
					<tr><td>snappers - enemy fighters</tr></td>
					<tr><td>snargasher - training aircraft (Canadian usage)</tr></td>
					<tr><td>snoop - Service Policeman; SP about his duties</tr></td>
					<tr><td>Snow Drops - RAF/Military Police</tr></td>
					<tr><td>soggy - aircraft with unresponsive controls; soggy type - dull or uninteresting person</tr></td>
					<tr><td>sortie - one aircraft doing one trip to target and back</tr></td>
					<tr><td>soup - fog; "tangled in the soup" - astray in the fog</tr></td>
					<tr><td>Spam - canned meat product - produced by Hormel in the US - substitute for real meat (see Bully Beef)</tr></td>
					<tr><td>spam can - B-24 Liberator</tr></td>
					<tr><td>spam medal - 1939-43 Star (also Naffy gong)</tr></td>
					<tr><td>sparks - term for either the ground crew who looked after the electrical systems or the aircrew wireless operator</tr></td>
					<tr><td>spawny - very lucky</tr></td>
					<tr><td>Spit - Supermarine Spitfire aircraft</tr></td>
					<tr><td>spit-and-polish parade - parade inspection by CO or an ACO</tr></td>
					<tr><td>split-arse - daring, reckless: (rude: a WRAF)</tr></td>
					<tr><td>spoof - a diversionary raid or operation</tr></td>
					<tr><td>sprog - a "new boy" fresh from training - inexperienced (also a "sprog crew") - US "rookie"</tr></td>
					<tr><td>spun in - bad mistake, analogy from an aircraft spinning out of control into the ground</tr></td>
					<tr><td>Spartan - exercise to establish methods of making entire airfield formation mobile</tr></td>
					<tr><td>Squadron Leader - rank of officer who usually led a Flight: also squadron bleeder</tr></td>
					<tr><td>Squadron Leader Swill - Admin Officer, whose duties included disposal of swill</tr></td>
					<tr><td>squadron vic - V shaped formation of aircraft</tr></td>
					<tr><td>square bashing - (marching) drill on the square or parade ground</tr></td>
					<tr><td>squirt - to fire a short burst from machine guns - as in "the R/G gave him a squirt before we went into the corkscrew"</tr></td>
					<tr><td>squo - Squadron Officer - WAAF equivalent to RAF Squadron Leader</tr></td>
					<tr><td>stand down - no operations planned</tr></td>
					<tr><td>starboard - the right side of the a/c as seen from pilot's seat </tr></td>
					<tr><td>Starfish - UK decoy sites with various fire and illumination effects simulating targets to lure Axis bomber formations e.g. Langstone Harbour for Portsmouth</tr></td>
					<tr><td>STARKEY - large scale spoof invasion of the Pas de Calais in September 1943, mounted to assess German reaction and bring his fighters to battle, which brought about the unplanned destruction of the French town of Le Portel</tr></td>
					<tr><td>Stationmaster - CO of Station</tr></td>
					<tr><td>Stationmaster's meetings - meetings run by Station Commander</tr></td>
					<tr><td>steam - work hard and effectively</tr></td>
					<tr><td>stick - bomb selection so that bombs would be released at timed intervals from their carriers in the bomb bay (also to release only a part of bomb load, going around a second time to drop the rest)</tr></td>
					<tr><td>stiffener - someone who binds you rigid</tr></td>
					<tr><td>stooge - deputy, i.e. second pilot or any assistant</tr></td>
					<tr><td>stooging (around) - flying around waiting for something to happen - flying slowly over an area, patrolling</tr></td>
					<tr><td>Store Basher's Bible - Air Publication No.830: vol I Equipment Regulations; vol II Storage and Packing; vol III Scales of RAF Equipment</tr></td>
					<tr><td>strip (tear off a) - to be severely reprimanded by a superior - reflecting the tearing off of rank badges when someone was demoted</tr></td>
					<tr><td>Sun - Short Sunderland aircraft</tr></td>
					<tr><td>Sunray - Overseas training flight for bomber crews</tr></td>
					<tr><td>Sunray - code for any CO</tr></td>
					<tr><td>supercharged - in a drunken state</tr></td>
					<tr><td>swede - green recruit/countrified</tr></td>
					<tr><td>sweep - systematic operation to flush out targets in a given area e.g. fighter sweep</tr></td>
					<tr><td>swindle - cunning piece of work</tr></td>
					<tr><td>synthetic - not the real thing e.g. flying simulators</tr></td>
					<tr><td>Tallboy - 12,0001b penetrating (earthquake) bomb</tr></td>
					<tr><td>Tally-Ho! - Exclamation made by pilots, particularly leaders, upon beginning an attack on enemy aircraft. This term originates from fox hunting, a traditional sport of the British ruling classes and shouted upon sighting the quarry</tr></td>
					<tr><td>tail end Charlie - rear gunner or rear aircraft of a formation</tr></td>
					<tr><td>take it on - (aeroplane) climb rapidly</tr></td>
					<tr><td>Tame Sow/Zahme Sau - German tactics, designed to bring night fighters in contact with bombers moving to and from target, using co-ordinated groups</tr></td>
					<tr><td>tapes (get) - become a corporal; get your third tape - become a sergeant</tr></td>
					<tr><td>tap in - have a good time (France 1940); "enemy aircraft ahead, let's tap in"</tr></td>
					<tr><td>taps - controls, indicators in the cockpit or cabin of an aircraft</tr></td>
					<tr><td>target for tonight - girl friend</tr></td>
					<tr><td>tatered - depressed by unexciting patrols</tr></td>
					<tr><td>taters (sack of) - load of bombs delivered to same address</tr></td>
					<tr><td>taxi - plane carrying small number of passengers</tr></td>
					<tr><td>taxidermist (go and see) - go and get stuffed</tr></td>
					<tr><td>taxi-driver - instructor at a school of air navigation</tr></td>
					<tr><td>tear off a strip - reprimand, "take down a peg"</tr></td>
					<tr><td>teased out - exhausted or very tired after long duty on a patrol or raid</tr></td>
					<tr><td>teat - electric button which fires the guns (or tit, also for dropping bombs)</tr></td>
					<tr><td>Tee Emm - RAF Magazine (as in Training Manual), post war retitled Happy Landings</tr></td>
					<tr><td>tee up - prepare a job, to get ready.</tr></td>
					<tr><td>ten-tenths - no visibility because of total cloud cover - also 10/10ths flak - very heavy concentration</tr></td>
					<tr><td>Theatre or Theatre of Operations - geographic area where combat was taking place - eg: The Mediterranean Theatre, The Far East Theatre etc. : US - ETO European Theater of Operations</tr></td>
					<tr><td>three months bumps - 3 month course of training in flying</tr></td>
					<tr><td>three pointer - excellent landing (on all three wheels)</tr></td>
					<tr><td>Through the gate - pushing the throttle forwards past a gate or kink or through a wire stopper in the throttle mechanism to apply maximum boosted power. The broken wire indicates to maintenance crew that maximum boost was used during the flight</tr></td>
					<tr><td>Thum - Weather reporting flight</tr></td>
					<tr><td>THUNDERCLAP - Plan to deliver a sudden, catastrophic blow by bombing Berlin and other cities with a view to bringing about German surrender</tr></td>
					<tr><td>ticket - pilot's certificate</tr></td>
					<tr><td>tiggerty-boo (tickety-boo) - all in order (tiggerty from the Hindustani teega)</tr></td>
					<tr><td>tin basher - metal worker</tr></td>
					<tr><td>tin fish - torpedo</tr></td>
					<tr><td>Tinsel - metallic foil dropped from bombers on raids to spoil German radar</tr></td>
					<tr><td>titfa/titfer - cap, steel hat (rhyming slang tit-for-tat)</tr></td>
					<tr><td>Tommy - after Tommy Atkins (Kipling) - originally used to denote the British infantryman - later to be used by the Germans as "Tommi" as their equivalent to "Jerry" : US equivalent - "G.I."</tr></td>
					<tr><td>tool along - fly aimlessly</tr></td>
					<tr><td>totem-pole - airfield lighting equipment of this shape</tr></td>
					<tr><td>TORCH - Allied invasion of French North Africa in 1942</tr></td>
					<tr><td>touch bottom - crash</tr></td>
					<tr><td>touch down - to land</tr></td>
					<tr><td>tour (of operations) - initially a period of time, later a number of ops, that a bomber crewman had to complete before being screened </tr></td>
					<tr><td>toy - a training aircraft or Link Trainer</tr></td>
					<tr><td>toys - mechanical parts of a plane beloved of the armourers and mechanics</tr></td>
					<tr><td>tracer - a type of machine gun round which glowed as it moved showing the way to the target and allowing for adjustments in sighting - unfortunately also gave away the firer's position - usually every fourth round was a tracer </tr></td>
					<tr><td>train (driving the) - leading more than one squadron into battle</tr></td>
					<tr><td>Trenchard Brat - RAF Boy Apprentice: to be a Brat was highly regarded as the training was tough and the standards demanded were high; many achieved distinction</tr></td>
					<tr><td>trip - an op</tr></td>
					<tr><td>TRIDENT- The third Washington Conference, May 1943</tr></td>
					<tr><td>trousers - fairing of the undercarriage legs of certain types of aircraft, see also spats</tr></td>
					<tr><td>tug - plane which tows gliders or targets</tr></td>
					<tr><td>turn up the wick - open the throttle</tr></td>
					<tr><td>turnip bashing - drill on the square or in fields</tr></td>
					<tr><td>twilights - Summer-weight lighter coloured underwear of the WAAF</tr></td>
					<tr><td>twit - see clot</tr></td>
					<tr><td>twitch - body tremors developed by aircrew after a number of operations - "he's got the twitch" - sign of operational stress</tr></td>
					<tr><td>two-six (2-6) - general base call "down the flights", all personnel needed on a job</tr></td>
					<tr><td>type - a kind of person ( as in: "he's a ropey type" or "he's a bolshie type")</tr></td>
					<tr><td>umbrella - parachute</tr></td>
					<tr><td>undercart - the undercarriage of an aircraft - two main wheels and a tail wheel in the case of "taildraggers" like the Wellington - two main and a nosewheel for "tricycle" aircraft like the B-24 - attempting a landing with the 'cart "up" was considered a "putting up a large black" for the pilot</tr></td>
					<tr><td>upstairs - in the air</tr></td>
					<tr><td>up top - flying high</tr></td>
					<tr><td>UP - projector for firing Z rockets</tr></td>
					<tr><td>VARSITY - Airborne operation to facilitate the crossing of the River Rhine in March 1945</tr></td>
					<tr><td>Vees - a brand of wartime cigarette</tr></td>
					<tr><td>vegetables - acoustic or magnetic mines sowed on gardening expeditions to various "beds"</tr></td>
					<tr><td>V Force - the RAF's strategic jet bomber fleet of Valiants, Victors and Vulcans</tr></td>
					<tr><td>vic - aircraft formation in the shape of a "vee" usually three aircraft but could be more</tr></td>
					<tr><td>Vic - Vickers Victoria or Vickers Valencia aircraft</tr></td>
					<tr><td>view - RAF personnel always take a "view" of things: good, poor, dim, long distance view, lean view, outside view, "Ropey" view</tr></td>
					<tr><td>visiting card - bomb</tr></td>
					<tr><td>waafise - the substitution of WAAF for male members of a unit</tr></td>
					<tr><td>wad - cake or bun or scone "char and a wad"</tr></td>
					<tr><td>waffle/waffling - out of control, losing height; or cruising along unconcernedly and indecisively</tr></td>
					<tr><td>wallah - chap or fellow</tr></td>
					<tr><td>wallop - beer</tr></td>
					<tr><td>wanks - strong liquor</tr></td>
					<tr><td>washed out - scrubbed - to fail as a student pilot - bomb aimers or navigators in particular were often scrubbed pilots</tr></td>
					<tr><td>Wassermann - German early warning radar</tr></td>
					<tr><td>wear (it) - tolerate, accept, permit</tr></td>
					<tr><td>weaver - the proven to be poor tactic of using a "tail end Charlie" weave from side to side behind the squadron formation as a spotter whilst the other pilots stay in formation. This poor chap was frequently picked off without a word of warning on the R/T</tr></td>
					<tr><td>weaving - a gentle form of corkscrew - evasive manoeuvre to allow gunners maximum view around aircraft, especially below</tr></td>
					<tr><td>weaving (get) - get going, hurry up</tr></td>
					<tr><td>Week-End Air Force - Auxiliary Air Force, 1925 to outbreak of war</tr></td>
					<tr><td>wheels down - get ready to leave a form of transportation</tr></td>
					<tr><td>whiff - Oxygen</tr></td>
					<tr><td>Whirligig - Westland Whirlwind aircraft</tr></td>
					<tr><td>Whispering Death - Beaufighter</tr></td>
					<tr><td>whistle (to) - depart hurriedly e.g. scramble</tr></td>
					<tr><td>whistled - state of intoxication</tr></td>
					<tr><td>whistler - high explosive bomb as it comes down</tr></td>
					<tr><td>wild - (is or was, of an aircraft) - faster, performs more energetically than expected</tr></td>
					<tr><td>Wild Sow/Wilde Sau - German tactics to engage bombers over the target by individual fighters</tr></td>
					<tr><td>Wimpey - Vickers Armstrong's Wellington Bomber - after J. Wellington Wimpey from Popeye comic strip</tr></td>
					<tr><td>windmill - see egg-whisk; motion of an unpowered propeller</tr></td>
					<tr><td>Window - Tinfoil strips dropped by bombers to disrupt enemy radar system</tr></td>
					<tr><td>Wing - unit made up of two or sometimes three squadrons </tr></td>
					<tr><td>Wingco - Wing Commander</tr></td>
					<tr><td>wizard/wizzo - really first class, superlative, attractive, ingenious</tr></td>
					<tr><td>wog - coloured person, native (westernised oriental gentleman)</tr></td>
					<tr><td>wop - Wireless Operator</tr></td>
					<tr><td>wopag - Wireless Operator/Air Gunner</tr></td>
					<tr><td>wom - Wireless Operator/Mechanic</tr></td>
					<tr><td>woof - to open the throttle very quickly</tr></td>
					<tr><td>Works and Bricks - Air Ministry Works Directorate AMWD responsible for the construction and maintenance of aerodromes, hangars, offices etc.</tr></td>
					<tr><td>wrap up - crash</tr></td>
					<tr><td>wrapped-up - under perfect control</tr></td>
					<tr><td>write-off - something damaged beyond repair</tr></td>
					<tr><td>W&uuml;rzburg - German radar used to direct A.A. guns, searchlights, and briefly, night fighters</tr></td>
					<tr><td>X-Ger&auml;t - German bombing aid</tr></td>
					<tr><td>Y-control - method of controlling night fighters using modified Y-Ger&auml;t equipment</tr></td>
					<tr><td>Y-Ger&auml;t - German bombing aid</tr></td>
					<tr><td>Y Service - ground based signals monitoring service, using German or Italian speaking operators</tr></td>
					<tr><td>yellow doughnut - collapsible dinghy carried on aircraft</tr></td>
					<tr><td>yellow peril - training aircraft</tr></td>
					</th>
			</table>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>