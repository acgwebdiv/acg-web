<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Hurricane Mk.I Rotol 100 Octane - Hurricane Startup Procedure</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
?>
		<h1>Flight School</h1>
		<h2>Hurricane Mk.I Rotol 100 Octane - Hurricane Startup Procedure</h2>

		<div class="contentText">
			<p>
				Wing Co Spud walks through your pre-flight for the Hawker Hurricane Mk.Ia (100 oct) methodically.
			</p>

			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/Dv4lWBIjkq8" frameborder="0" allowfullscreen></iframe>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>