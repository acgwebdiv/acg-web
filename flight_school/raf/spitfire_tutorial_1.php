<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Spitfire Mk.Ia 100 Octane - Landing Procedure</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
?>
		<h1>Flight School</h1>
		<h2>Spitfire Mk.Ia 100 Octane (CSP version)- Standard Landing Procedure</h2>

		<div class="contentText">
			<p>
				Want to know how to put the Spitfire down gently? Follow this tutorial from Wing. Co. Osprey to find out how not to prang your precious kite!
			</p>

			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/OI1qi-tGi7E" frameborder="0" allowfullscreen></iframe>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>