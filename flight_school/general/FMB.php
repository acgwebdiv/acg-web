<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Full Mission Builder</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
	include('FMB.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['FMB_HEADER1'];?></h1>
		<h2><?php echo $lang['FMB_HEADER2'];?></h2>

		<div class="contentText">
			<p><?php echo $lang['FMB_P1'];?></p>
			<p><b><?php echo $lang['FMB_P2'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/p07ymuw0wOw" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P3'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/ujdobZAtQHE" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P4'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/I9-cfFkScBw" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P5'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/jwvfjtelaRU" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P6'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/A4_nZHmdk9I" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P7'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/FiFG32ZfU-E" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P8'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/bWSuTQjfFBE" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P9'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/r1dXtAwJDOI" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P10'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/v9asaOZXXmc" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P11'];?></b><p/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/sl9cMJwDnNg" frameborder="0" allowfullscreen></iframe>
			<p><b><?php echo $lang['FMB_P12'];?></b><p/>
			</div>
		
<?php
	include("../../includes/footer.inc.php");
?>