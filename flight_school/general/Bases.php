<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Aerodromes and Airfields</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
	include('Bases.lang.'.$ac_lang.'.php');
?>
			<h1><?php echo $lang['BASES_HEADER1'];?></h1>
			<h2><?php echo $lang['BASES_HEADER2'];?></h2>

			<div class="contentText">			
				<p><?php echo $lang['BASES_P1'];?></p>
				<p></p><?php echo $lang['BASES_P2'];?></p>
				<a href="http://www.pcpilot.hu/dokumentumtar/il-2-sturmovik/cliffs-of-dover/2928-repuloter-terkepek-airbase-maps.html"><img src="../../includes/images/flight_school/general/AFBs_of_Clod_poster2.jpg"></a>
				
				
				<center><h2><?php echo $lang['BASES_HEADER3'];?></h2></center>
				<p><?php echo $lang['BASES_P3'];?></p>
				<ul>
				<li><?php echo $lang['BASES_LI1'];?></li>
				<li><?php echo $lang['BASES_LI2'];?></li>
				<li><?php echo $lang['BASES_LI3'];?></li>
				<li><?php echo $lang['BASES_LI4'];?></li>
				</ul>
				<center><h3><?php echo $lang['BASES_HEADER4'];?></h3></center>
				<ul>
				<li><?php echo $lang['BASES_LI5'];?></li>
				<li><?php echo $lang['BASES_LI6'];?></li>
				<li><?php echo $lang['BASES_LI7'];?></li>
				<li><?php echo $lang['BASES_LI8'];?></li>
				<li><?php echo $lang['BASES_LI9'];?></li>
				<li><?php echo $lang['BASES_LI10'];?></li>
				<li><?php echo $lang['BASES_LI11'];?></li>
				</ul>

				<center><h2><?php echo $lang['BASES_HEADER5'];?></h2></center>
				<center><h3><?php echo $lang['BASES_HEADER6'];?></h3></center>
				<ul>
				<li><?php echo $lang['BASES_LI12'];?></li>
				<li><?php echo $lang['BASES_LI13'];?></li>
				<li><?php echo $lang['BASES_LI14'];?></li>
				</ul>

				<center><h3><?php echo $lang['BASES_HEADER7'];?></h3></center>
				<ul>
				<li><?php echo $lang['BASES_LI15'];?></li>
				<li><?php echo $lang['BASES_LI16'];?></li>
				</ul>
				
				<center><h3><?php echo $lang['BASES_HEADER7'];?></h3></center>
				<p><?php echo $lang['BASES_P4'];?></p>				

				</div>				
<?php
	include("../../includes/footer.inc.php");
?>