<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Advanced Navigation</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
	include('advanced_nav.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['ADV_NAV_HEADER'];?></h1>
		<h2><?php echo $lang['ADV_NAV_HEADER2'];?></h2>

		<div class="contentText">
			<p><?php echo $lang['ADV_NAV_P1'];?></p>
			<b><?php echo $lang['ADV_NAV_P2'];?></b><br/>
			<p><?php echo $lang['ADV_NAV_P3'];?></p>
			<center><img src="../../includes/images/flight_school/navigation/advanced_nav01.jpg" style="width:920px;"></center>
			<p><?php echo $lang['ADV_NAV_P4'];?></p>
			<p><?php echo $lang['ADV_NAV_P5'];?></p>
			<p><?php echo $lang['ADV_NAV_P6'];?></p>
			<center><img src="../../includes/images/flight_school/general/basic_nav05.jpg"></center>
			<p><?php echo $lang['ADV_NAV_P7'];?></p>
			<b><?php echo $lang['ADV_NAV_P8'];?></b><br/>
			<p><?php echo $lang['ADV_NAV_P9'];?></p>
			<p><?php echo $lang['ADV_NAV_P10'];?></p>
			<b><?php echo $lang['ADV_NAV_P11'];?></b><br/>
			<p><?php echo $lang['ADV_NAV_P12'];?></p>
			<center><img src="../../includes/images/flight_school/navigation/advanced_nav03.jpg"></center>
			<p><?php echo $lang['ADV_NAV_P13'];?></p>
			<b><?php echo $lang['ADV_NAV_P14'];?></b><br/>
			<p><?php echo $lang['ADV_NAV_P15'];?></p>
			<b><?php echo $lang['ADV_NAV_P16'];?></b><br/>
			<center><img src="../../includes/images/flight_school/navigation/advanced_nav04.jpg"></center><br/><br/>
			<p><?php echo $lang['ADV_NAV_P17'];?></p>
			<p><?php echo $lang['ADV_NAV_P18'];?></p>
			<p><?php echo $lang['ADV_NAV_P19'];?></p>
			<p><?php echo $lang['ADV_NAV_P20'];?></p>
			<b><?php echo $lang['ADV_NAV_P21'];?></b><br/>
			<p><?php echo $lang['ADV_NAV_P22'];?></p>
			<p><?php echo $lang['ADV_NAV_P23'];?></p>
			<p><?php echo $lang['ADV_NAV_P24'];?></p>
			<p><?php echo $lang['ADV_NAV_P25'];?></p>
			<img src="../../includes/images/flight_school/navigation/advanced_nav05.jpg"><br/><br/>
			<p><?php echo $lang['ADV_NAV_P26'];?></p>
			<img src="../../includes/images/flight_school/navigation/advanced_nav06.jpg"><br/><br/>			
			<p><?php echo $lang['ADV_NAV_P27'];?></p>
			<img src="../../includes/images/flight_school/navigation/advanced_nav07.jpg"><br/><br/>
			<p><?php echo $lang['ADV_NAV_P28'];?></p>
			<img src="../../includes/images/flight_school/navigation/advanced_nav08.jpg"><br/><br/>
			<p><?php echo $lang['ADV_NAV_P29'];?></p>
			<img src="../../includes/images/flight_school/navigation/advanced_nav09.jpg"><br/><br/>
			<p><?php echo $lang['ADV_NAV_P30'];?></p>
			<b><?php echo $lang['ADV_NAV_P31'];?></b><br/>
			<p><?php echo $lang['ADV_NAV_P32'];?></p>
			<center><img src="../../includes/images/flight_school/navigation/advanced_nav10.jpg"></center><br/><br/>
			<p><?php echo $lang['ADV_NAV_P33'];?></p>
			<p><?php echo $lang['ADV_NAV_P34'];?></p>
			<b><?php echo $lang['ADV_NAV_P35'];?></b><br/>
			<p><?php echo $lang['ADV_NAV_P36'];?></p>
			<center><img src="../../includes/images/flight_school/navigation/advanced_nav11.jpg" style="width:920px;"></center><br/><br/>
			<p><?php echo $lang['ADV_NAV_P37'];?></p>
			<p><?php echo $lang['ADV_NAV_P38'];?></p>
			<p><?php echo $lang['ADV_NAV_P39'];?></p>
			<p><?php echo $lang['ADV_NAV_P40'];?></p>
			<p><?php echo $lang['ADV_NAV_P41'];?></p>
			<center><img src="../../includes/images/flight_school/navigation/advanced_nav12.jpg" style="width:920px;"></center><br/><br/>
			<p><?php echo $lang['ADV_NAV_P42'];?></p>
			<p><?php echo $lang['ADV_NAV_P43'];?></p>
			<p><?php echo $lang['ADV_NAV_P44'];?></p>
			<p><?php echo $lang['ADV_NAV_P45'];?></p>
			<center><img src="../../includes/images/flight_school/navigation/advanced_nav13.jpg" style="width:920px;"></center><br/><br/>
			<p><?php echo $lang['ADV_NAV_P46'];?></p>
			<p><?php echo $lang['ADV_NAV_P47'];?></p>
			<p><?php echo $lang['ADV_NAV_P48'];?></p>
			<p><?php echo $lang['ADV_NAV_P49'];?></p>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/NQWZEVaoFKQ" frameborder="0" allowfullscreen></iframe>
			<br/><br/>
			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/C6oGa1bqe1U" frameborder="0" allowfullscreen></iframe>			
			
			
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>