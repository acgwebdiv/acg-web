<?php

//advanced_nav.php

$lang['FORMATION_HEADER1'] = "Flight School";
$lang['FORMATION_HEADER2'] = "Formation Flying";

$lang['FORMATION_P1'] = "Allow No.501_Miki to explain the basics of formation flying 
    and the art of staying with your leader through combat manoeuvres.  Although 
    this is demonstrated in the Hawker Hurricane this skill can be equally applied 
    to any type of aeroplane. It is best suitable for a fighting pair however applies 
    to any echelon formation.";