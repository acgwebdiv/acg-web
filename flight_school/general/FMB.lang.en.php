<?php

//advanced_nav.php

$lang['FMB_HEADER1'] = "Flight School";
$lang['FMB_HEADER2'] = "IL2 Cliffs of Dover Full Mission Builder tutorials";

$lang['FMB_P1'] = "5./JG26_Boyezee shows you the basics of the FMB in order 
    that you can build your own missions.";
$lang['FMB_P2'] = "I. Airspawning AI aircraft";
$lang['FMB_P3'] = "II. Airspawning enemy raid";
$lang['FMB_P4'] = "III. Using basic triggers";
$lang['FMB_P5'] = "IV. Using Group Destroy triggers";
$lang['FMB_P6'] = "V. Creating surface AI units";
$lang['FMB_P7'] = "VI. Using the Pass Through trigger";
$lang['FMB_P8'] = "VII. Creating custom position airfield spawns";
$lang['FMB_P9'] = "VIII. Creating multiplayer airspawns";
$lang['FMB_P10'] = "IX. Placing flak guns on static ships";
$lang['FMB_P11'] = "X. Placing placing and moving ground objects";
$lang['FMB_P12'] = "If you have any questions for Boyezee, or need any help at all 
    please post your question in our forums <a href=http://www.aircombatgroup.co.uk/forum/viewforum.php?f=48><u>here</u>";
