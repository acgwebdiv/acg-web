<?php

//advanced_nav.php

$lang['STALLS_AND_SPINS_HEADER1'] = "Flight School";
$lang['STALLS_AND_SPINS_HEADER2'] = "Stalls and Spins";

$lang['STALLS_AND_SPINS_P1'] = "Stalls and spins can be daunting for new or inexperienced 
    pilots.  Spud explains the causes for these events, and how to get out of them 
    safely.";