<?php

//advanced_nav.php

$lang['ADV_NAV_HEADER'] = "Flight School";
$lang['ADV_NAV_HEADER2'] = "Advanced Navigation for Experienced Pilots";

$lang['ADV_NAV_P1'] = "This lesson from No.501_Robo teaches navigation as it was 
    typically done by fighter pilots during the war.  Although this lesson focusses 
    on the Hawker Hurricane, the methods can equally be applied to the Luftwaffe 
    aircraft.";

$lang['ADV_NAV_P2'] = "I. Introduction:";
$lang['ADV_NAV_P3'] = "One of the essential skills of a RAF fighter pilot is navigation. 
    You would need to get the basics long before you set your unworthy feet into 
    a Hurricane or Spitfire - countless cross country training flights in Tiger 
    Moths and Harvards and even night flying would make your pilot wings well earned. 
    You would know how to plan your flight, how to calculate the actual legs and 
    how to check your position against the map precisely. You would be very familiar 
    with all these:";

$lang['ADV_NAV_P4'] = "Now it seems lots of us know how to turn with an enemy but 
    somehow we skip the essential pilot skills like this because it's just a game 
    and all this theory is boring. Navigation in terms of visual recognition of 
    where we are approximately (in geographical sense) is satisfactory - especially 
    when we always fly in good weather and we can see the terra firma underneath 
    us at all times. With just a bit of flying in this sim you will see easily if 
    you're in England, France or over the water, which means you are somewhere in-between. 
    You will also learn the notorious landmarks and major towns and cities, bays 
    and rivers, and that is of course pretty much enough for the very beginning. 
    But for advanced flying or flying in formation we should really understand at 
    least the very basics of what the real pilots of the era must have known if 
    they wanted to survive the flight (not necessarily the fight). You would be 
    surprised how many good pilots got killed by making a silly navigational mistake 
    when flying through the cloud, or crash landed as they run out of fuel after 
    getting very very lost, which was deadly when over desert or sea.";

$lang['ADV_NAV_P5'] = "For us in this game, it's not nearly as difficult, but if 
    we ever attempt any proper formation flying, knowing our heading (HDG) is absolutely 
    crucial. The Flight Commander (it might be a pair or vic or the whole squadron) 
    refers to HDG in degrees at every change of direction.";

$lang['ADV_NAV_P6'] = "If you haven't got a compass rose already here is a simple 
    compass rose showing the 360&deg;circle with actual magnetic directions which 
    you can print out and laminate for your virtual cockpit:";

$lang['ADV_NAV_P7'] = "You need to know that: <br/><br/> 
    HDG 0 (or 360) is North <br/>
	HDG 180 is South <br/> 
	HDG 270 is West<br/> 
	HDG 90 is East <br/><br/> 
	Okay, enough of that basic stuff!";

$lang['ADV_NAV_P8'] = "II. Commands:";

$lang['ADV_NAV_P9'] = "We announce the heading as follows: 'Red 2, Heading One-Eight-Zero'. 
    The leader normally counts three seconds in his head after he says the 'Zero' 
    bit and only then starts the actual turn (one-two-three-turn). This depends 
    on how we want to do this and R/T protocol, but there must be a way of giving 
    the others some time to plan and predict the action and this is how we used 
    to do it without polluting the R/T too much. Everybody in the flight must be 
    aware of what the current HDG is and where and how much we will be turning, 
    e.g. 90&deg; to the left.";

$lang['ADV_NAV_P10'] = "We start practicing this in flights of two, first including 
    the crossing of the path in 90 degree turns while flying in line abreast formation 
    (see our lesson in Formation Flying). At first, we will be announcing the direction 
    of the turn and how many degrees we turn, But as we get better at this it won't 
    be necessary.";
    
$lang['ADV_NAV_P11'] = "III. The Map:";

$lang['ADV_NAV_P12'] = "I am very sure that all of us can read the map. The in-game 
    map is a bit awkward to use and for proper navigation you would need a proper 
    paper map, this is not necessary unless you are interested in navigation and 
    logging flight plans. The flights are always planned beforehand by the Squadron 
    Commander and the pilots only use their map to compare the landscape with where 
    he thinks he is.<br/><br/>
    You need to know that:<br/><br/>
	1. Every grid on the map represents a 10km x 10km square. That is cca 6.2 x 6.2 
	miles. NB: Diagonally it's cca 14.1km = 8.8 miles)<br/><br/>
	2. The difference between Geographical North (on the map) and Magnetic North 
	(as shown by your magnetic compass) was a hefty 10&deg;W in 1940, it is quite 
	different to that today, a mild 2.4&deg;E and you will be pleased to know that 
	1940 is actually modelled in game rather than today. For better understanding 
	see following picture:<br/>";
	
$lang['ADV_NAV_P13'] = "For our flying, we always refer to magnetic HDG, as shown 
    on the compass, but you want to know that if your compass shows HDG 0, you're 
    actually flying HDG 350 when referring to the map.";

$lang['ADV_NAV_P14'] = "IV. Instruments:";

$lang['ADV_NAV_P15'] = "The most important part is understanding the relevant cockpit 
    instruments and using them correctly. Yes, this is where the navigation gets 
    the bad reputation - the instruments used by the RAF are rather awkward to use 
    in the game and not very straightforward in real life either:";

$lang['ADV_NAV_P16'] = "P8 Compass";

$lang['ADV_NAV_P17'] = "Originally designed for naval use and therefore not exactly 
    a precise instrument for a fighter aircraft. It is just what it looks like it 
    is - a bowl of spirit with magnetised piece of metal in it. The outside part 
    (the rim with the scale and glass top with yellow lines on it) is movable and 
    the pilot would rotate it by hand. You should map 2 keys to control it - Increase 
    / Decrease Course setter.";

$lang['ADV_NAV_P18'] = "The needle in the bowl is + shaped. One of the bit of this
    + looks like a letter 'T' and this is always pointing to magnetic north. If 
    you turn too sharply or do some manoeuvres you will find that the compass will 
    drift a bit and settle down eventually which is expected from a bowl of liquid 
    placed in your aircraft. You will need to fly nice and level with both arrows 
    on your turn-slip gauge vertically to get a precise reading. The good news is 
    though that you can always tell where your magnetic North is and that will need 
    to be good enough for us to navigate.";

$lang['ADV_NAV_P19'] = "How did they use in in real life? A pilot would rotate the 
    rim to get the desired HDG on the 12 o'clock position of the instrument, in 
    our case that would be HDG 245. The exact value will be displayed on the HUD 
    as you rotate the compass for the Hurricane if you have this enabled - bad luck 
    Spitfire pilots, you'll need to zoom in and look at the top of the bevel for 
    your reading, or mouse over for a tool-tip.  Next he would flip the small lever 
    on the side to lock the P-8 compass and prevent it from drifting as much as 
    possible, this feature is unfortunately not modelled in game. As he flies he 
    would turn the aircraft until the yellow lines match with the T - needle, almost 
    like shown on the above picture. The Spitfire and Hurricane P8 model do not have 
    a yellow T but rather two bars. The pilot must turn his machine until these 
    are parallel with the needle, but be careful, the 'T' on the needle must meet 
    the red North on the bevel or you will be flying in the opposite direction!";

$lang['ADV_NAV_P20'] = "You will find that I already added the -10° compensation 
    to fly on course based geographical north (the map). In flight, I would announce 
    'HDG 245' because that is what the instrument shows us and that is what we refer 
    to at all times.";

$lang['ADV_NAV_P21'] = "Directional Gyro or 'DI'";

$lang['ADV_NAV_P22'] = "Both Hurricane and Spitfire are equipped with more advanced
    Directional Gyro instrument. It is always placed on the main panel where your
    main 6 gauges are for night flying, right there in the middle. The view is also
    obstructed by control stick, but you can get used to that. Basically, we use
    the P-8 compass to set up our Gyro and keep cross-checking the correct setting
    with the compass during the whole flight.";
    
$lang['ADV_NAV_P23'] = "NB: Your DI will not work while the engine is off, the gyro
    rotation is propelled by your engine.";
    
$lang['ADV_NAV_P24'] = "Normally, as a part of your pre-flight routine you would 
    set-up your P-8 compass (by rotating it until the N on the rim matches with 
    the T - needle) and by doing that you get your actual HDG (the number on the 
    12 o'clock of the rim), then you would set up your gyro to that value and you're 
    all set. Pretty awkward as you need to fiddle with 2 imprecise instruments that 
    like to drift. Also, you need to look around your stick as you try to get a 
    correct reading. Considering that you will always join the game with your P-8 
    compass set to HDG 0, you can keep it like that and get the actual reading with 
    a simple technique by mirroring the reading of where the T- needle is pointing.";
    
$lang['ADV_NAV_P25'] = "Let's say you spawn like this:";

$lang['ADV_NAV_P26'] = "North is HDG 50, you would need to rotate the compass 50&deg;
    to get the correct reading for your gyro. Or you can simply deduct 50 from 360
    giving you the actual HDG of 310. You might prefer a simple and quick visual
    aid and mirror the value onto the other side of the compass:";

$lang['ADV_NAV_P27'] = "50&deg; = 310&deg;";

$lang['ADV_NAV_P28'] = "140&deg; = 220&deg;";

$lang['ADV_NAV_P29'] = "285&deg; = 75&deg;";

$lang['ADV_NAV_P30'] = "You simply adjust your gyro based on this reading every 
    now and then. You will get pretty fast at it with some practice and it will 
    become natural after a while.";

$lang['ADV_NAV_P31'] = "V. Navigation Exercise:";

$lang['ADV_NAV_P32'] = "I have prepared a training mission based on real life practice; 
    an aspiring pilot would have to fly a prescribed route, calculate the times 
    and headings for each leg and then follow this path in actual flight, reading 
    certain signs spread in the terrain by the crew en route to prove that he's 
    been where he was supposed to be.  Hopefully by now you will have understood 
    the basic navigation lesson in our flight school and know all about timing your 
    flight.";
    
$lang['ADV_NAV_P33'] = "Unzip <a href='../../includes/files/navigation.zip'><u>THIS 
    MISSION</u></a> into your ../Documents/1C SoftClub/il-2 sturmovik cliffs of 
    dover - MOD/missions/Single and play as a single mission. Feel free to use external
    views and pause your game and check your gyro settings, time, position vs. the 
    map, but don't enable the minimap path because, well, it's just cheating isn't 
    it! Try to fly precisely and focus on the instrument usage and your flying technique 
    - keep constant speed, constant altitude, don't drift off course and look out 
    for the letters and for landmarks to cross-check your position! I deliberately 
    chose an area inland we're not really familiar with, but there are some bigger 
    cities and rivers to help with navigation. There is no wind in this mission. 
    The signs you're supposed to be looking for are almost always placed near some 
    major landmarks, small town on direct route, you won't miss them unless you 
    get lost. There are 6 letters in total, one at each turning point and one somewhere 
    in the middle of the leg.";

$lang['ADV_NAV_P34'] = "Please read the briefing, and a bonus giveaway is the lovely 
    Hawker Hurricane 6 OTU skin courtesy of Air Combat Group.  Go on,  keep it and 
    use it in our server later, we have skins enabled.";

$lang['ADV_NAV_P35'] = "VI. Preparation:";

$lang['ADV_NAV_P36'] = "Your goal is to take off from Reading aerodrome and fly 
    the triangle: Reading (T/O) - Winchester - Swindon - Reading (L).";

$lang['ADV_NAV_P37'] = "he whole path is shown on the picture above - we place our 
    compass on the map to calculate our heading for leg 1 - Reading to Winchester. 
    We can see the heading is 215. Then we measure the straight line distance between 
    these two places, see that the red line has got a small blue dot every 10 miles. 
    Distance from Reading to Winchester is 34 miles. Flying at 260 mph IAS at some 
    3000 feet (I suggest you keep this altitude, for you can see the landscape well 
    enough from here) this will take you 7 minutes 30 seconds.";
    
$lang['ADV_NAV_P38'] = "NB: - 260mph IAS at 3000ft is pretty much what you get when 
    flying with correctly trimmed aircraft at full boost (+6.25lbs.) 2600rpm and 
    radiator normal (50&#37;) in the Hawker Hurricane.";
    
$lang['ADV_NAV_P39'] = "Remember we are converting IAS to TAS, your gauge reads 
    260 mph at the altitude 3000 feet (that is your Indicated Air Speed, IAS), but 
    you will need to add 1&#37; every 600ft to get your True Air Speed (TAS), e.g. 
    the speed you're actually flying against the ground. At 3000 feet, the difference 
    will make cca 5&#37;, that is 13mph, so when your airspeed indicator reads 260 
    mph you are actually flying at 273 mph ground speed at that given altitude. 
    This does not make much of a difference on this practice flight, but it will 
    considerably add up on longer and higher flight. At 260 mph IAS you will fly 
    the distance of 4,5 miles every minute.";
    
$lang['ADV_NAV_P40'] = "Also, do not forget your Magnetic heading correction. To 
    recap, if you obtain your HDG from the map (215 in our case), you will need 
    to subtract 10 degrees to calculate the heading to use on your compass. According 
    to your gyro you will be flying HDG 205 to get from Reading to Winchester.";

$lang['ADV_NAV_P41'] = "Leg 2:";

$lang['ADV_NAV_P42'] = "Winchester - Swindon, HDG 332 for 42 miles.";

$lang['ADV_NAV_P43'] = "For our flight that's 322&deg; magnetic for 9 minutes 15 
    seconds, flying at 260 mph IAS, at 3000 ft.";

$lang['ADV_NAV_P44'] = "Leg 3:";

$lang['ADV_NAV_P45'] = "Swindon - Reading, HDG 112 for 40 miles.";

$lang['ADV_NAV_P46'] = "And that's 102&deg; magnetic for 8 minutes 5 seconds, flying 
    at 260 mph IAS, at 3000 ft.";

$lang['ADV_NAV_P47'] = "Can you pass this test?  See how good you are against those 
    early war pilots rushed into battle during the Summer of 1940.  If you can find 
    at least 5 letters you've done very well. 4 out of 6 is a pass in my book as 
    long as you can rtb within 40 minutes. You can record a track if you wish or 
    you can PM me, Robo, in the ACG forums with letters you have spotted and I can 
    confirm if they're right. Be aware that I may have added some extra letters 
    around the area off course so it's not a case of just finding them!";

$lang['ADV_NAV_P48'] = "<a href='http://il2map.info/map?type=clod_channel'><u>Here</u></a> 
    is an interactive online navigation tool for the Cliffs of Dover map which you 
    may find useful.";
    
$lang['ADV_NAV_P49'] = "And finally, here are some official RAF videos which are 
    quite similar and cover the navigation using real terrain.";
    
    