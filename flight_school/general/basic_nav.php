<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Navigation for Beginners</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("../../includes/header2.inc.php");
	include('basic_nav.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['BASIC_NAV_HEADER1'];?></h1>
		<h2><?php echo $lang['BASIC_NAV_HEADER2'];?></h2>

		<div class="contentText">
			<p><?php echo $lang['BASIC_NAV_P1'];?></p>
			<p><?php echo $lang['BASIC_NAV_P2'];?></p>
			<img src="../../includes/images/flight_school/general/basic_nav01.jpg">
			<p><?php echo $lang['BASIC_NAV_P3'];?></p>
			<img src="../../includes/images/flight_school/general/basic_nav02.jpg"><br/><br/>
			<img src="../../includes/images/flight_school/general/basic_nav03.jpg"><br/><br/>
			<img src="../../includes/images/flight_school/general/basic_nav04.jpg"><br/><br/>
			<p><?php echo $lang['BASIC_NAV_P4'];?></p>
			<p><?php echo $lang['BASIC_NAV_P5'];?></p>
			<p><?php echo $lang['BASIC_NAV_P6'];?></p>
			<p><?php echo $lang['BASIC_NAV_P7'];?></p>
			<img src="../../includes/images/flight_school/general/basic_nav05.jpg">
		</div>
<?php
	include("../../includes/footer.inc.php");
?>