<?php
	include("../../includes/header1.inc.php");
?>
	<title>Air Combat Group | Flight School | Stalls and Spins</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation, spins, stalling, stalls, spin recovery"/>
<?php
	include("../../includes/header2.inc.php");
	include('Stalls_and_spins.lang.'.$ac_lang.'.php');
?>
		<h1><?php echo $lang['STALLS_AND_SPINS_HEADER1'];?></h1>
		<h2><?php echo $lang['STALLS_AND_SPINS_HEADER2'];?></h2>

		<div class="contentText">
			<p><?php echo $lang['STALLS_AND_SPINS_P1'];?></p>

			<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/lL4tKIddW5s" frameborder="0" allowfullscreen></iframe>
		</div>
		
<?php
	include("../../includes/footer.inc.php");
?>