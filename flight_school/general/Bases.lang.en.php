<?php

//advanced_nav.php

$lang['BASES_HEADER1'] = "Aerodromes and Airfields";
$lang['BASES_HEADER2'] = "Tom's Airforce bases of Cliffs of Dover - v2";

$lang['BASES_P1'] = "Air Combat Group are pleased to promote VO101_Tom's excellent 
    work on documenting the aerodromes and airfields on the Channel map with essential 
    flight information from the period.";
$lang['BASES_P2'] = "Simply click on the image to reach the download site.";

$lang['BASES_HEADER3'] = "Summary";
$lang['BASES_P3'] = "These AFBs of CloD 2 contains the following documents (The 
    whole pack is 747MB):";
    
$lang['BASES_LI1'] = "High resolution pdf of all Airfields, for printing. NB: Only 
    100 MB per file, therefore the high-res pdf files are sliced into three parts. 
    ALL three parts are needed to properly extract.";
$lang['BASES_LI2'] = "Medium resolution pdf of all Airfields, for PC use.";
$lang['BASES_LI3'] = "High resolution jpg pack of all Airfields.";
$lang['BASES_LI4'] = "High resolution overview map. Based of original maps of the 
    English Channel region in A1 size, 300 dpi resolution, for printing.";
    
$lang['BASES_HEADER4'] = "Content";
$lang['BASES_LI5'] = "The document contains all of the 121 airports of CloD.";
$lang['BASES_LI6'] = "The taxiways were also marked with letters, so you can determine 
    your location more precisely.";
$lang['BASES_LI7'] = "The map legends shows the changes of the AFB icons. You can 
    distinguish the civil and military airports, the type of the runaway, and if 
    the runaway is permanent or temporary.";
$lang['BASES_LI8'] = "I made a separate section of the pilot notes. There is various 
    information relating to airports.";
$lang['BASES_LI9'] = "I deleted the runaway symbol from the airfields which are 
    not concrete. This is also mentioned in the pilot notes. I drew the most likely 
    runaway direction and length so if there is no other directive you can use this 
    data.";
$lang['BASES_LI10'] = "The pack contains an overview map which can be printed in 
    A1 size at 300 dpi. It was made in two versions, one with a better visibility 
    of the airport and the other covers less of the original map. Print and use 
    whichever you prefer.";
$lang['BASES_LI11'] = "Finally, there is an additional 50% sized overview map designed 
    for PC use.";

$lang['BASES_HEADER5'] = "Further Information";
$lang['BASES_HEADER6'] = "Technical Changes";
$lang['BASES_LI12'] = "I attached most of the images as jpg, not as a vector graphic. 
    This improves memory usage.";
$lang['BASES_LI13'] = "The airport list is the beginning of the document, you can 
    jump to any page with a single click.";
$lang['BASES_LI14'] = "I made a .jpg package, so if you want a custom selection 
    then use this. I recommend to use these files on tablet devices or mobile phones. 
    Unfortunately there were several problems with pdf's.";
    
$lang['BASES_HEADER7'] = "Known Bugs";
$lang['BASES_LI15'] = "The 4th page of the medium-res .pdf is blurry. I can't do 
    anything. The 'overview map 1' is the same jpg.";
$lang['BASES_LI16'] = "The overview map and the CloD have some inaccuracies (mostly 
    with the blue, 'CloD grid'). The blue vectors are hand made vectors  which is 
    the best which can be done. The other reason is the original map also contains 
    minor inaccuracies, for example city's, rivers, etc. As a result the coordinates 
    of airports slip a little (the first four characters of the 'location' is accurate, 
    these errors affect the last number of the coordinate). It is not very significant, 
    but I had to mention it.";

$lang['BASES_HEADER8'] = "Special Thanks";
$lang['BASES_P4'] = "VO101_Tom, the creator, plus SC/JG_IvanK, 5./JG27.Farber, bongodriver, 
    JVM and Artist who helped with suggestions, opinions and data.";