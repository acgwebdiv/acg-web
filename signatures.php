<?php
	include("includes/header1.inc.php");
?>
	<title>Air Combat Group | Signatures</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("includes/header2.inc.php");
?>
				<h1>Squadron Signatures</h1>

				
				<div class="contentText">
				
					<p>
						Open yours in new tab and grab the URL for your signature in the forums.
					</p>

					<?php

					$files = scandir('includes/images/signatures');

					foreach ($files as $file) 
					{
						if ($file != "." && $file != "..")
		        {
		            echo "<img src=\"includes/images/signatures/$file\">\n";
		        }
					}
					
					?>

				</div>
				
<?php	
	include("includes/footer.inc.php");
?>