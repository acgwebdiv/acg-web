<?php
	include("includes/header1.inc.php");
?>
	<title>Air Combat Group | ACG Ranks</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("includes/header2.inc.php");
?>

		<h1>List of ACG Ranks</h1>
		<div id="rankList">
			<h2>Non-Commissioned Rank</h2>
			<table class="rankTable" id="nonCommissionedRanks">
				<tr>
					<th>Royal Air Force</th>
					<th></th>
					<th>Soviet Air Forces</th>
					<th></th>
					<th>Luftwaffe</th>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Leading Aircraftman.png"></td>
					<td></td>
					<td></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Gefreiter.png"></td>
				</tr>
				<tr>
					<td>Leading Aircraftman<br />(Not in use)</td>
					<th></th>
					<td>No Equivalent</td>
					<th></th>
					<td>Gefreiter<br /> </td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Senior Aircraftman.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankKrasnoarmeyets.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Obergefreiter.png"></td>
				</tr>
				<tr>
					<td>Senior Aircraftman<br />(Not in use)</td>
					<th></th>
					<td>Рядово́й Aвиа́ции<br>(Private)</td>
					<th></th>
					<td>Obergefreiter<br /> </td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Technician.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankEfreitor.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Hauptgefreiter.png"></td>
				</tr>
				<tr>
					<td>Technician<br />(Not in use)</td>
					<th></th>
					<td>Eфре́йтор Aвиа́ции<br>(Efraitor)</td>
					<th></th>
					<td>Hauptgefreiter<br /> </td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Corporal.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankMladshiySerzhant.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Unteroffizier.png"></td>
				</tr>
				<tr>
					<td>Corporal<br />(Not in use)</td>
					<th></th>
					<td>Mла́дший Cержа́нт Aвиа́ции<br>(Junior Sergeant)</td>
					<th></th>
					<td>Unteroffizier<br /> </td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Sergeant.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankSerzhant.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Unterfeldwebel.png"></td>
				</tr>
				<tr>
					<td>Sergeant Pilot</td>
					<th></th>
					<td>Cержа́нт Aвиа́ции<br>(Sergeant)</td>
					<th></th>
					<td>Unterfeldwebel</td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Flight Sergeant.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankStarshiySerzhant.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Feldwebel.png"></td>
				</tr>
				<tr>
					<td>Flight Sergeant</td>
					<th></th>
					<td>Cта́рший Cержа́нт Aвиа́ции<br>(Senior Sergeant)</td>
					<th></th>
					<td>Feldwebel</td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Warrant Officer.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankStarshina.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Oberfeldwebel.png"></td>
				</tr>
				<tr>
					<td>Warrant Officer</td>
					<th></th>
					<td>Cтаршина́ Aвиа́ции<br>(Sergeant Major)</td>
					<th></th>
					<td>Oberfeldwebel</td>
				</tr>
			</table>
			<br/>
			<h2>Commissioned Ranks</h2>
			<table class="rankTable" id="commissionedRanks">
				<tr>
					<th>Royal Air Force</th>
					<th></th>
					<th>Soviet Air Forces</th>
					<th></th>
					<th>Luftwaffe</th>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Pilot Officer.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankLeytenant.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Leutnant.png"></td>
				</tr>
				<tr>
					<td>Pilot Officer</td>
					<th></th>
					<td>Лейтена́нт Авиа́ции<br>(Lieutenant)</td>
					<th></th>
					<td>Leutnant</td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Flying Officer.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankStarshiyLeytenant.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Oberleutnant.png"></td>
				</tr>
				<tr>
					<td>Flying Officer</td>
					<th></th>
					<td>Ста́рший Лейтена́нт Авиа́ции<br>(Senior Lieutenant)</td>
					<th></th>
					<td>Oberleutnant</td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Flight Lieutenant.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankKapitan.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Hauptmann.png"></td>
				</tr>
				<tr>
					<td>Flight Lieutenant</td>
					<th></th>
					<td>Капита́н Авиа́ции<br>(Captain)</td>
					<th></th>
					<td>Hauptmann</td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Squadron Leader.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankMayor.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Major.png"></td>
				</tr>
				<tr>
					<td>Squadron Leader</td>
					<th></th>
					<td>Mайо́р Aвиа́ции<br>(Major)</td>
					<th></th>
					<td>Major</td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Wing Commander.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankPodpolkovnik.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Oberstleutnant.png"></td>
				</tr>
				<tr>
					<td>Wing Commander</td>
					<th></th>
					<td>Подполко́вник Авиа́ции<br>(Lieutenant Colonel)</td>
					<th></th>
					<td>Oberstleutnant</td>
				</tr>
				<tr>
					<td><img src = "/forum/images/ranks/Group Captain.png"></td>
					<td></td>
					<td><img src="/forum/images/ranks/RankPolkovnik.png"></td>
					<td></td>
					<td><img src = "/forum/images/ranks/Oberst.png"></td>
				</tr>
				<tr>
					<td>Group Captain</td>
					<th></th>
					<td>Полко́вник Авиа́ции<br>(Colonel)</td>
					<th></th>
					<td>Oberst</td>
				</tr>				
			</table>
		</div>
<?php
	include("includes/footer.inc.php");
?>