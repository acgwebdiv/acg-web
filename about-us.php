<?php
	include("includes/header1.inc.php");
?>
	<title>Air Combat Group | About Us</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include("includes/header2.inc.php");
	include('about-us.lang.'.$ac_lang.'.php');
?>
				<h1><?php echo $lang["ABOUT_US_HEADER"]; ?></h1>
				<div class="contentText">
					<p><?php echo $lang["ABOUT_US_P1"]; ?></p>
					<p><?php echo $lang["ABOUT_US_P2"]; ?></p>				</div>
				
				<h2><?php echo $lang["ABOUT_US_HEADER2"]; ?></h2>				
				
				<div class="contentText">
				<p><?php echo $lang["ABOUT_US_P3"]; ?></p>
				<p><?php echo $lang["ABOUT_US_P4"]; ?></p>
				<p><?php echo $lang["ABOUT_US_P5"]; ?></p>
				<p><?php echo $lang["ABOUT_US_P6"]; ?></p>
				<p><?php echo $lang["ABOUT_US_P7"]; ?></p>
				<ul>
					<li><?php echo $lang["ABOUT_US_LI1"]; ?></li>
					<li><?php echo $lang["ABOUT_US_LI2"]; ?></li>
					<li><?php echo $lang["ABOUT_US_LI3"]; ?></li>
					<li><?php echo $lang["ABOUT_US_LI4"]; ?></li>
				</ul>	
				<p><?php echo $lang["ABOUT_US_P8"]; ?></p>
				<ul>
					<li><?php echo $lang["ABOUT_US_LI5"]; ?></li>
					<li><?php echo $lang["ABOUT_US_LI6"]; ?></li>
					<li><?php echo $lang["ABOUT_US_LI7"]; ?></li>
					<li><?php echo $lang["ABOUT_US_LI8"]; ?></li>
				</ul>
				<p><?php echo $lang["ABOUT_US_P9"]; ?></p>

				</div>
				
				<h2><?php echo $lang["ABOUT_US_HEADER3"]; ?></h2>
				<div class="contentText">
					<p><?php echo $lang["ABOUT_US_P10"]; ?></p>
					<p><?php echo $lang["ABOUT_US_P11"]; ?></p>
					<p><?php echo $lang["ABOUT_US_P12"]; ?></p>
				</div>
				
				<h3><?php echo $lang["ABOUT_US_HEADER4"]; ?></h3>
				<div class="contentText">
				<b><?php echo $lang["ABOUT_US_P13"]; ?></b><br/>
				<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/fWW9dHsPy0c" frameborder="0" allowfullscreen></iframe>
				</div><br/>
				
				<div class="contentText">
				<b><?php echo $lang["ABOUT_US_P14"]; ?></b><br/>
				<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/qhe-G4rDLFE" frameborder="0" allowfullscreen></iframe>
				</div>				
				
				<h3><?php echo $lang["ABOUT_US_HEADER5"]; ?></h3>
				<div class="contentText">
				<b><?php echo $lang["ABOUT_US_P15"]; ?></b><br/>
				<iframe width="100%" height="520" src="//www.youtube-nocookie.com/embed/kmCpybuV0dc" frameborder="0" allowfullscreen></iframe>
				</div>				
				
				<h3><?php echo $lang["ABOUT_US_HEADER6"]; ?></h3></h3>
				<div class="contentText">
				<p><?php echo $lang["ABOUT_US_P16"]; ?></p>
				</div>
					
				<h2><?php echo $lang["ABOUT_US_HEADER7"]; ?></h2>
				<div class="contentText">				
					<p><?php echo $lang["ABOUT_US_P17"]; ?></p>
					<ul>
						<li><?php echo $lang["ABOUT_US_LI9"]; ?></li>
							<ul>
								<li><?php echo $lang["ABOUT_US_LI10"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI11"]; ?></li>
							</ul>
						<li><?php echo $lang["ABOUT_US_LI12"]; ?></li>
							<ul>
								<li><?php echo $lang["ABOUT_US_LI13"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI14"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI15"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI16"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI17"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI18"]; ?></li>
							</ul>
						<li><?php echo $lang["ABOUT_US_LI19"]; ?></li>
							<ul>
								<li><?php echo $lang["ABOUT_US_LI20"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI21"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI22"]; ?></li>
								<li><?php echo $lang["ABOUT_US_LI23"]; ?></li>
							</ul>
					</ul>

					<p><?php echo $lang["ABOUT_US_P18"]; ?></p>
				</div>
				
				<h3><?php echo $lang["ABOUT_US_HEADER7"]; ?></h3>
				<div class="contentText">
					<p><?php echo $lang["ABOUT_US_P19"]; ?></p>
				</div>
<?php
	include("includes/footer.inc.php");
?>