<?php
	include(dirname(dirname(__FILE__))."/includes/header1.inc.php");
	include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/db_connect.php');
    include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/functions.php');

	// Setting up indices to spread content over several pages.
    if(filter_has_var(INPUT_GET, "index")) {
        $indexLetter = filter_input(INPUT_GET, "index");
    } else {
        $indexLetter = '%';
    }

    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
        $pagePost = "page=$page";
    } else {
        $page = 1;
        $pagePost = "";
    }
    $n_entries = 20;
    $start_from = ($page - 1)*$n_entries;
    
    $dbx = getDBx();
    $sql = "CALL `PS_getNumberOfPilots` ('$indexLetter%');";
	$n_pilots_result = mysqli_query($dbx, $sql);
    $n_pilots_row = mysqli_fetch_row($n_pilots_result);
    $n_pilots = $n_pilots_row[0];
    $n_pages = ceil($n_pilots / $n_entries);
    mysqli_free_result($n_pilots_result);
    mysqli_next_result($dbx);
    
    $sql = "CALL `PS_getPilotList` ('$indexLetter%', $start_from, $n_entries);";
    // echo $sql;
 	$result = mysqli_query($dbx, $sql) or die;

?>
	<title>Air Combat Group | Public Server Pilots</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include(dirname(dirname(__FILE__))."/includes/header2.inc.php");
	include("pilotList.lang.".$ac_lang.".php");
?>
	
	<h3><?php echo $lang['PILOTLIST_HEADER1'];?></h3>
<div class="contentText">
    <p><?php echo $lang['PILOTLIST_P1'];?></p>

<div class="psTable indexSelect">
    <?php createPageIndexExt($indexLetter, "pilotList.php?&"); ?>
</div>
<div class="psTable">
    <table >
        <thead>
            <tr>
                <?php $filterLink = "pilotList.php?&".$indexPost."&".$pagePost; ?>
                <th><a class="psTable" href="<?php echo($filterLink."&filter=rank");?>"><?php echo $lang['PILOTLIST_TH1'];?></a></th>
                <th><a class="psTable"href="<?php echo($filterLink."&filter=name");?>"><?php echo $lang['PILOTLIST_TH2'];?></a></th>
                <th><a class="psTable"href="<?php echo($filterLink."&filter=name");?>"><?php echo $lang['PILOTLIST_TH3'];?></a></th>
                <th><a class="psTable"href="<?php echo($filterLink."&filter=name");?>"><?php echo $lang['PILOTLIST_TH4'];?></a></th>
            </tr>
            <tr>
                <td colspan="4"><hr></td>
            </tr>
        </thead>
        <?php
            while($row = mysqli_fetch_assoc($result)) {
                $link = "pilotDetails.php?pilot=".urlencode($row["PilotName"]);
        ?>
        <tbody>
            <tr>
                <td><a class="psTable" href="<?php echo($link);?>"><?php echo $row["PilotName"];?></a></td>
                <td><?php echo $row["value"];?></td>
                <td><?php echo $row["LWsorties"];?></td>
                <td><?php echo $row["RAFsorties"];?></td>
            </tr>    
        </tbody>
        
        <?php } ?>
    </table>
</div>
<div class="psTable indexSelect">
    <?php  createPageSelect($n_pages, $page, "pilotList.php?&index=$indexLetter&"); ?>
</div>
</div>
	
<?php
	include(dirname(dirname(__FILE__))."/includes/footer.inc.php");
?>