<?php

$lang['PILOTDETAILS_HEADER1'] = "Public server pilot profile:";
$lang['PILOTDETAILS_P1'] = "NOTE: These pages are still under construction. Parts ".
    "are still missing, the content and format might change.";

$lang['PILOTDETAILS_HEADER2'] = "General information:";
$lang['PILOTDETAILS_TH1'] = "Name:";

$lang['PILOTDETAILS_SORTIE_HEADER'] = "Sorties overview:";
$lang['PILOTDETAILS_SORTIE_LUFTWAFFE'] = "Luftwaffe";
$lang['PILOTDETAILS_SORTIE_RAF'] = "Royal Air Force";
$lang['PILOTDETAILS_TH3'] = "Total:";
$lang['PILOTDETAILS_TH4'] = "Pilot status:";
$lang['PILOTDETAILS_TH5'] = "Aircraft status:";

$lang['PILOTDETAILS_PS1'] = "OK:";
$lang['PILOTDETAILS_PS2'] = "Wounded:";
$lang['PILOTDETAILS_PS3'] = "POW:";
$lang['PILOTDETAILS_PS4'] = "KIA:";

$lang['PILOTDETAILS_AS1'] = "OK:";
$lang['PILOTDETAILS_AS2'] = "Damaged:";
$lang['PILOTDETAILS_AS3'] = "Lost:";

$lang['PILOTDETAILS_MEDALS_HEADER'] = "Awarded medals:";

$lang['PILOTDETAILS_VICTORIES_HEADER'] = "Victories overview:";
$lang['PILOTDETAILS_V1'] = "Victories:";
$lang['PILOTDETAILS_V2'] = "Victories Shared:";
$lang['PILOTDETAILS_V3'] = "Fighters:";
$lang['PILOTDETAILS_V4'] = "Bombers:";

$lang['PILOTDETAILS_PROMOTIONS_HEADER'] = "Promotion/Demotion overview:";
$lang['PILOTDETAILS_PR1'] = "Date:";
$lang['PILOTDETAILS_PR2'] = "Level:";

?>