<?php


$lang['PILOTLIST_HEADER1'] = "Public Server Pilots:";
$lang['PILOTLIST_P1'] = "These pages list all pilots that flew on the ACG Public ".
    "Server with their corresponding names, rank levels and number of Sorties. Please ".
    "refer to the public server documentation for a more detailed explanation of ".
    "the pilot statistics. <b>NOTE:</b> These pages are still under construction. Parts are ".
    "still missing, the content and format might change.";

$lang['PILOTLIST_TH1'] = "Name";
$lang['PILOTLIST_TH2'] = "Rank Level";
$lang['PILOTLIST_TH3'] = "LW sorties";
$lang['PILOTLIST_TH4'] = "RAF sorties";

?>