<?php

function ps_pilot_awards($pilot, $size, $faction){
    
    $dbx = getDBx();
    $sql = "SELECT awards.abreviation, awards.image, PS_decorations.missionStartTime ".
           "FROM PS_decorations ".
           "LEFT JOIN awards ON PS_decorations.awardID = awards.id ".
           "LEFT JOIN PS_characters ON PS_decorations.characterID = PS_characters.id ".
           "WHERE PS_characters.PilotName = '$pilot' AND PS_characters.Status < 3 ".
           "AND PS_characters.Faction = '$faction' ".
           "ORDER BY missionStartTime ASC;";
    $querry = mysqli_query($dbx, $sql);
    
    while($row = mysqli_fetch_assoc($querry)){
       
        $imageStr = $row["image"];
        $medalAbr = $row["abreviation"];
        if($medalAbr == "AB" | $medalAbr == "FBA" | $medalAbr == "FBAgd"){
            continue;
        }
        $image = "../acg-pam/imgsource/medals-$size/".$imageStr;
        if($medalAbr == "DFM*"){
            $medalAbr = "DFM";
        } else if($medalAbr == "DFC*"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DFC**"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DSO*"){
            $medalAbr = "DSO";
        } else if($medalAbr == "RK I"){
            $medalAbr = "RK II";
        }
        
        $imageArray[$medalAbr] = $image;
   }

    if(count($imageArray)>0){
        
        foreach($imageArray as $image) {
            
            echo("<img src=$image>");

        }

    }

}

function ps_character_awards($character, $size){
    
    $dbx = getDBx();
    $sql = "SELECT awards.abreviation, awards.image, PS_decorations.missionStartTime ".
           "FROM PS_decorations ".
           "LEFT JOIN awards ON PS_decorations.awardID = awards.id ".
           "LEFT JOIN PS_characters ON PS_decorations.characterID = PS_characters.id ".
           "WHERE PS_characters.id = $character ".
           "ORDER BY missionStartTime ASC;";
    $querry = mysqli_query($dbx, $sql);
    
    while($row = mysqli_fetch_assoc($querry)){
       
        $imageStr = $row["image"];
        $medalAbr = $row["abreviation"];
        if($medalAbr == "AB" | $medalAbr == "FBA" | $medalAbr == "FBAgd"){
            continue;
        }
        $image = "../acg-pam/imgsource/medals-$size/".$imageStr;
        if($medalAbr == "DFM*"){
            $medalAbr = "DFM";
        } else if($medalAbr == "DFC*"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DFC**"){
            $medalAbr = "DFC";
        } else if($medalAbr == "DSO*"){
            $medalAbr = "DSO";
        } else if($medalAbr == "RK I"){
            $medalAbr = "RK II";
        }
        
        $imageArray[$medalAbr] = $image;
   }

    if(count($imageArray)>0){
        
        foreach($imageArray as $image) {
            
            echo("<img src=$image>");

        }

    }

}