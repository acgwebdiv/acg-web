<?php
	include(dirname(dirname(__FILE__))."/includes/header1.inc.php");
	include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/db_connect.php');
    include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/functions.php');
    include_once(dirname(__FILE__).'/ps_renderFunctions.php');

    if(filter_has_var(INPUT_GET, "pilot")) {
        $pilot = urldecode(filter_input(INPUT_GET, "pilot"));
    } else {
        // header("location: pilotList.php");
        // exit();
    }
    
    if(filter_has_var(INPUT_GET, "page")) {
        $page = filter_input(INPUT_GET, "page");
        $pagePost = "page=$page";
    } else {
        $page = 1;
        $pagePost = "";
    }
    $n_entries = 20;
    $start_from = ($page - 1)*$n_entries;
    
    $dbx = getDBx();
    
    $sql = "SELECT COUNT(PS_characters.id) ".
           "FROM PS_characters ".    
           "WHERE PS_characters.PilotName = '$pilot'";
    $n_characters_result = mysqli_query($dbx, $sql);
    $n_characters_row = mysqli_fetch_row($n_characters_result);
    $n_characters = $n_characters_row[0];
    $n_pages = ceil($n_characters / $n_entries);

    //Get all character id's of member
    $sql = "SELECT PS_characters.id, FirstName, LastName, characterstatus.status, dateTable.mdate, sortieTable.sorties, Faction, ". 
           "IFNULL(fvictoryTable.fvictories, 0), IFNULL(fvictoryTable.fvictoriesSH, 0), ".
           "IFNULL(bvictoryTable.bvictories, 0), IFNULL(bvictoryTable.bvictoriesSH, 0) ".
           "FROM PS_characters ".
           "LEFT JOIN ".
                "(SELECT characterID, MAX(CAST(STR_TO_DATE(MissionStartTime,'%Y%m%d%H%i%S') AS DATE)) AS mdate ".
                "FROM RP_Flight ".
            "GROUP BY characterID) AS dateTable ".
            "ON dateTable.characterID = PS_characters.id ".
            "LEFT JOIN ".
                "(SELECT characterID, COUNT(RP_Flight.characterID) AS sorties ".
                "FROM RP_Flight ".
            "GROUP BY characterID) AS sortieTable ".
            "ON sortieTable.characterID = PS_characters.id ".
            "LEFT JOIN ".
                "characterstatus ".
            "ON PS_getCharacterStatus(PS_characters.id) = characterstatus.id ".
            "LEFT JOIN ".
	            "(SELECT RP_Flight.characterID, ".
                "SUM( RP_KillDetail.Shared =0 ) AS fvictories, ".
                "SUM( RP_KillDetail.Shared =1 ) AS fvictoriesSH ".
                "FROM RP_Flight ".
                    "LEFT JOIN RP_KillDetail ON ". 
                    "(RP_Flight.PilotName, RP_Flight.MissionStartTime, RP_Flight.Flight) ".
                    "= ( RP_KillDetail.PilotName, RP_KillDetail.MissionStartTime, RP_KillDetail.Flight ) ".
	            "WHERE RP_Flight.PAMstatus <=3 AND RP_KillDetail.KillType = 'Aircraft - Fighter' ".
	            "GROUP BY RP_Flight.characterID) AS fvictoryTable ".
            "ON fvictoryTable.characterID = PS_characters.id ".
            "LEFT JOIN ".
	            "(SELECT RP_Flight.characterID, ".
	            "SUM( RP_KillDetail.Shared =0 ) AS bvictories, ". 
                "SUM( RP_KillDetail.Shared =1 ) AS bvictoriesSH ".
                "FROM RP_Flight ".
                    "LEFT JOIN RP_KillDetail ON ".
                    "(RP_Flight.PilotName, RP_Flight.MissionStartTime, RP_Flight.Flight) ".
                    " = ( RP_KillDetail.PilotName, RP_KillDetail.MissionStartTime, RP_KillDetail.Flight ) ".
                "WHERE RP_Flight.PAMstatus <=3 AND RP_KillDetail.KillType = 'Aircraft - Bomber' ".
                "GROUP BY RP_Flight.characterID) AS bvictoryTable ".
            "ON bvictoryTable.characterID = PS_characters.id ".
            "WHERE PS_characters.PilotName = '$pilot' ORDER BY id ASC ".
            "LIMIT $start_from, $n_entries";
    // echo $sql;
    $cresult = mysqli_query($dbx, $sql);
    $c_id_array = mysqli_fetch_all($cresult);
    $character_array = array();

    for($n = 0; $n < count($c_id_array); $n++){

        $c_id = $c_id_array[$n][0];
        $character_array[$c_id]["firstName"] = $c_id_array[$n][1];
        $character_array[$c_id]["lastName"] = $c_id_array[$n][2];
        $character_array[$c_id]["status"] = $c_id_array[$n][3];
        $mxrdate = $c_id_array[$n][4];
        $character_array[$c_id]["sorties"] = $c_id_array[$n][5];
        $faction = $c_id_array[$n][6];
        $character_array[$c_id]["faction"] = $faction;
        $character_array[$c_id]["fvictories"] = $c_id_array[$n][7];
        $character_array[$c_id]["fvictoriesSH"] = $c_id_array[$n][8];
        $character_array[$c_id]["bvictories"] = $c_id_array[$n][9];
        $character_array[$c_id]["bvictoriesSH"] = $c_id_array[$n][10];
        
        // echo var_dump($character_array[$c_id]);
        // Get last rank
        $sql = "SELECT ranks.abreviation, ranks.image, PS_promotions.value ".
               "FROM ranks ".
               "RIGHT JOIN PS_promotions ".
               "ON PS_promotions.value = ranks.value ".
               "WHERE PS_promotions.PilotName = '$pilot' AND ranks.faction = '$faction' ".
               "AND PS_promotions.date <= '$mxrdate' ORDER BY date DESC LIMIT 1";
                // echo $sql;
        $presult = mysqli_query($dbx, $sql);
        $promotiondates = mysqli_fetch_all($presult);
        if(count($promotiondates)>0){
            $character_array[$c_id]["rank"] = $promotiondates[0][0];
            $character_array[$c_id]["rankImage"] = $promotiondates[0][1];
            $character_array[$c_id]["rankValue"] = $promotiondates[0][2];
            if($character_array[$c_id]["faction"] == "RAF" && 
            $character_array[$c_id]["rankValue"] < 5 ){
                $character_array[$c_id]["rankValue"] = 5;
                $character_array[$c_id]["rank"] = "Sgt";
            }
        }
    }
?>

<title>Air Combat Group | Public Server <?php echo $pilot;?> Characters</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include(dirname(dirname(__FILE__))."/includes/header2.inc.php");
	include("pilotCharacterList.lang.".$ac_lang.".php");
?>
<div class="contentText">
	<h3><?php echo $lang['PILOTLIST_HEADER1'];?></h3>
<div>
    <p><?php echo $lang['PILOTLIST_P1'];?></p>
</div>

    <?php 
    for($n = 0; $n < count($c_id_array); $n++){
        
        $c_id = $c_id_array[$n][0];
        $link = "characterDetails.php?c_id=".$c_id;
    ?>
    <div class="psListButton">
    <a href=<?php echo $link;?>>
    <?php if($character_array[$c_id]["faction"] == "RAF"){ ?>
        <div class="psListButtonRAF">
        <img src="../includes/images/navigation/raf_small.png">
        <p><?php echo($character_array[$c_id]["rank"]." ".
        $character_array[$c_id]["firstName"]." ".$character_array[$c_id]["lastName"]);?></p>
        <p>Sorties: <?php echo $character_array[$c_id]["sorties"];?> 
        - Status: <?php echo $character_array[$c_id]["status"];?></p>
    <?php } else if($character_array[$c_id]["faction"] == "LW"){ ?>
        <div class="psListButtonLW" >
        <img src="../includes/images/navigation/luftwaffe_small.png">
        <p><?php echo($character_array[$c_id]["rank"]." ".
        $character_array[$c_id]["firstName"]." ".$character_array[$c_id]["lastName"]);?></p>
        <p>Sorties: <?php echo $character_array[$c_id]["sorties"];?> 
        - Status: <?php echo $character_array[$c_id]["status"];?></p>
    <?php } ?>
        <div class="characterListMedalContainer">
        <?php ps_character_awards($c_id, "small");?>
        </div>
        </div>
    </a>
    </div>
    <?php } ?>
    
</div>
<div class="psTable indexSelect">
    <?php  createPageSelect($n_pages, $page, "pilotCharacterList.php?pilot=$pilot&index=$indexLetter&"); ?>
</div>
<?php
	include(dirname(dirname(__FILE__))."/includes/footer.inc.php");
?>