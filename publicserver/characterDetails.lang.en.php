<?php

$lang['CHARACTERDETAILS_HEADER1'] = "Public server character profile - $firstName '$pilot' $lastName";
$lang['CHARACTERDETAILS_P1'] = "NOTE: These pages are still under construction. Parts ".
    " are still missing, the content and format might change.";

$lang['CHARACTERDETAILS_BUTTON1'] = "Missions";
$lang['CHARACTERDETAILS_HEADER2'] = "General information:";

$lang['CHARACTERDETAILS_GI1'] = "Name:";
$lang['CHARACTERDETAILS_GI2'] = "Pilot:";
$lang['CHARACTERDETAILS_GI3'] = "Status:";

$lang['CHARACTERDETAILS_SORTIE_HEADER'] = "Sorties & victories overview:";
$lang['CHARACTERDETAILS_SORTIE_1'] = "Total:";
$lang['CHARACTERDETAILS_SORTIE_2'] = "Pilot status:";
$lang['CHARACTERDETAILS_SORTIE_3'] = "Aircraft status:";
$lang['CHARACTERDETAILS_SORTIE_4'] = "OK:";
$lang['CHARACTERDETAILS_SORTIE_5'] = "Wounded:";
$lang['CHARACTERDETAILS_SORTIE_6'] = "POW:";
$lang['CHARACTERDETAILS_SORTIE_7'] = "KIA:";
$lang['CHARACTERDETAILS_SORTIE_8'] = "OK:";
$lang['CHARACTERDETAILS_SORTIE_9'] = "Damaged:";
$lang['CHARACTERDETAILS_SORTIE_10'] = "Lost:";

$lang['CHARACTERDETAILS_VICTORIES_1'] = "Victories:";
$lang['CHARACTERDETAILS_VICTORIES_2'] = "Victories Shared:";
$lang['CHARACTERDETAILS_VICTORIES_3'] = "Fighters:";
$lang['CHARACTERDETAILS_VICTORIES_4'] = "Bombers:";

$lang['CHARACTERDETAILS_MEDALS_HEADER'] = "Awarded medals:";

$lang['CHARACTERDETAILS_PROMOTIONS_HEADER'] = "Promotion/Demotion overview:";
$lang['CHARACTERDETAILS_PROMOTIONS_1'] = "Date:";
$lang['CHARACTERDETAILS_PROMOTIONS_2'] = "Rank:";

?>