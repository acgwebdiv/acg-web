<?php
    include(dirname(dirname(__FILE__))."/includes/header1.inc.php");
	include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/db_connect.php');
	include_once(dirname(__FILE__).'/ps_renderFunctions.php');
    
    $c_id = filter_input(INPUT_GET, "c_id");
    $dbx = getDBx();
    
    
    // Get general information of character
    $sql = "SELECT FirstName, LastName, characterstatus.status, dateTable.mndate, ".
           "dateTable.mxdate, Faction, PilotName ". 
           "FROM PS_characters ".
           "LEFT JOIN ".
                "(SELECT characterID, MAX(CAST(STR_TO_DATE(MissionStartTime,'%Y%m%d%H%i%S') AS DATE)) AS mxdate, ".
                "MIN(CAST(STR_TO_DATE(MissionStartTime,'%Y%m%d%H%i%S') AS DATE)) AS mndate ".
                "FROM RP_Flight ".
                "GROUP BY characterID) AS dateTable ".
            "ON dateTable.characterID = PS_characters.id ".
            "LEFT JOIN ".
                "characterstatus ".
            "ON PS_getCharacterStatus($c_id) = characterstatus.id ".
            "WHERE PS_characters.id = $c_id";
    // echo $sql;
    $cresult = mysqli_query($dbx, $sql);
    $crow = mysqli_fetch_assoc($cresult);
    
    $pilot = $crow["PilotName"];
    $firstName = $crow["FirstName"];
    $status = $crow["status"];
    $lastName = $crow["LastName"];
    $faction = $crow["Faction"];
    $mxrdate = $crow["mxdate"];
    $mnrdate = $crow["mndate"]; 
 
    if($faction == "RAF"){
        // Get promotion info of character
        $sql = "(SELECT ranks.abreviation, ranks.image, PS_promotions.value, ".
               "PS_promotions.date, ranks.name ".
               "FROM ranks ".
               "RIGHT JOIN PS_promotions ".
               "ON PS_promotions.value = ranks.value ".
               "WHERE PS_promotions.PilotName = '$pilot' AND ranks.faction = '$faction' ".
               "AND PS_promotions.date <= '$mxrdate' AND PS_promotions.date > '$mnrdate' ".
               "AND PS_promotions.value > 5) ".
               "UNION ".
               "(SELECT ranks.abreviation, ranks.image, PS_promotions.value, ".
               "PS_promotions.date, ranks.name ".
               "FROM ranks ".
               "RIGHT JOIN PS_promotions ".
               "ON PS_promotions.value = ranks.value ".
               "WHERE PS_promotions.PilotName = '$pilot' AND ranks.faction = '$faction' ".
               "AND PS_promotions.value > 5 AND PS_promotions.date <= '$mnrdate' ".
               "ORDER BY date DESC LIMIT 1)".
               "ORDER BY date ASC";
        // echo $sql;
        $presult = mysqli_query($dbx, $sql);
        $promotiondates = mysqli_fetch_all($presult);
        if(count($promotiondates)>0){
        
            $promotiondates[count($promotiondates)-1][3] = $mnrdate;
            $currentRankAbr = $promotiondates[count($promotiondates)-1][0];
            $currentRankImage =  $promotiondates[count($promotiondates)-1][1];
            $currentRankValue = $promotiondates[count($promotiondates)-1][2];
        } else {
            $currentRankAbr = "Sgt";
            $currentRankImage = "RankSeargentPilot.png";
            $currentRankValue = 5;
            $promotiondates[0][0] = "Sgt";
            $promotiondates[0][1] = "RankSeargentPilot.png";
            $promotiondates[0][2] = 5;
            $promotiondates[0][3] = $mnrdate;
            $promotiondates[0][4] = "Seargent";
        }
    } else if($faction == "LW"){
        // Get promotion info of character
        $sql = "(SELECT ranks.abreviation, ranks.image, PS_promotions.value, ".
               "PS_promotions.date, ranks.name ".
               "FROM ranks ".
               "RIGHT JOIN PS_promotions ".
               "ON PS_promotions.value = ranks.value ".
               "WHERE PS_promotions.PilotName = '$pilot' AND ranks.faction = '$faction' ".
               "AND PS_promotions.date <= '$mxrdate' AND PS_promotions.date > '$mnrdate' )".
               "UNION ".
               "(SELECT ranks.abreviation, ranks.image, PS_promotions.value, ".
               "PS_promotions.date, ranks.name ".
               "FROM ranks ".
               "RIGHT JOIN PS_promotions ".
               "ON PS_promotions.value = ranks.value ".
               "WHERE PS_promotions.PilotName = '$pilot' AND ranks.faction = '$faction' ".
               "AND PS_promotions.date <= '$mnrdate' ORDER BY date DESC LIMIT 1)".
               "ORDER BY date ASC";
        // echo $sql;
        $presult = mysqli_query($dbx, $sql);
        $promotiondates = mysqli_fetch_all($presult);
        if(count($promotiondates)>0){
        
            $promotiondates[count($promotiondates)-1][3] = $mnrdate;
            $currentRankAbr = $promotiondates[count($promotiondates)-1][0];
            $currentRankImage =  $promotiondates[count($promotiondates)-1][1];
            $currentRankValue = $promotiondates[count($promotiondates)-1][2];
        }   
    }

    $sql = "CALL `PS_getPCStatusesVictories` ('', $c_id, '$faction');";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);

    $sorties = $row["sorties"];
    $pilotOK = $row["pilotOK"];
    $pilotWND = $row["pilotWND"];
    $pilotPOW = $row["pilotPOW"];
    $pilotKIA = $row["pilotKIA"];
    $aircraftOK = $row["aircraftOK"];
    $aircraftDMG = $row["aircraftDMG"];
    $aircraftLOST = $row["aircraftLOST"];
    $victoriesF = $row["victoriesF"];
    $victoriesSHF = $row["victoriesSHF"];
    $victoriesB = $row["victoriesB"];
    $victoriesSHB = $row["victoriesSHB"];
    
    mysqli_free_result($result);
    mysqli_next_result($dbx);
    
    //Check if member has received his wings
    if($faction == "RAF"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN PS_decorations ON awards.id = PS_decorations.awardID ".
               "WHERE PS_decorations.characterID = $c_id AND awards.abreviation = 'AB'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            if($currentRankValue<8){
                $wingsImage = $wingsrow["image"]."Fabric.png";
            } else {
                $wingsImage = $wingsrow["image"]."Brass.png";
            }
        } else {
            $wingsImage = "";
        }
    } else if($faction == "LW"){
        $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN PS_decorations ON awards.id = PS_decorations.awardID ".
               "WHERE PS_decorations.characterID = $c_id AND awards.abreviation = 'FBAgd'";
        $wingsQuery = mysqli_query($dbx, $sql);
        if(mysqli_num_rows($wingsQuery)>0){
            $wingsrow = mysqli_fetch_assoc($wingsQuery);
            $wingsImage = $wingsrow["image"];
        } else {
            $sql = "SELECT awards.abreviation, awards.image FROM awards ".
               "RIGHT JOIN PS_decorations ON awards.id = PS_decorations.awardID ".
               "WHERE PS_decorations.characterID = $c_id AND awards.abreviation = 'FBA'";
            $wingsQuery = mysqli_query($dbx, $sql);
            if(mysqli_num_rows($wingsQuery)>0){
                $wingsrow = mysqli_fetch_assoc($wingsQuery);
                $wingsImage = $wingsrow["image"];
            } else {
                $wingsImage = "";
            }
        }
    }
?>
	<title>Air Combat Group | Public server pilot profile</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include(dirname(dirname(__FILE__))."/includes/header2.inc.php");
	include("characterDetails.lang.".$ac_lang.".php");
?>
<div class="contentText">	
	<h3><?php echo $lang['CHARACTERDETAILS_HEADER1'];?></h3>
    <p><?php echo $lang['CHARACTERDETAILS_P1'];?></p>
</div>
<div class="contentText">
    <hr>
    <!--<a class="buttonLink" href="missionList.php"><?php echo $lang['CHARACTERDETAILS_BUTTON1'];?></a>-->
</div>
<div class="contentText">
    <h3><?php echo $lang['CHARACTERDETAILS_HEADER2'];?></h3>
    <div class="uniform">
        <?php if($faction == "RAF"){ ?>
        <img src="../acg-pam/imgsource/RAF-ranks/RAFUniform.png">
        <?php } else if($faction == "LW"){ ?>
        <img src="../acg-pam/imgsource/LW-ranks/LWUniform.png">
        <?php } ?>
    </div>

    <div class="uniformRank">
        <?php if($faction == "RAF"){ ?>
        <img src="../acg-pam/imgsource/RAF-ranks/<?php echo $currentRankImage; ?>">
        <?php } else if($faction == "LW"){ ?>
        <img src="../acg-pam/imgsource/LW-ranks/<?php echo $currentRankImage; ?>">
        <?php } ?>
    </div>
    
    <div class="uniformWings">
        <?php if($wingsImage != ""){ ?>
        <img src="../acg-pam/imgsource/medals-small/<?php echo $wingsImage; ?>">
        <?php } ?>
    </div>
    
    <div class="memberGeneralInformation">
        <table>
            <tr><td><?php echo $lang['CHARACTERDETAILS_GI1'];?></td><td><?php echo($lastName.", ".$firstName);?></td></tr>
            <tr><td><?php echo $lang['CHARACTERDETAILS_GI2'];?></td><td><?php echo $pilot;?></td></tr>
            <tr><td><?php echo $lang['CHARACTERDETAILS_GI3'];?></td><td><?php echo $status;?></td></tr>
        </table>
    </div>
</div>
<div class="contentText">
    <div class="psTable">
    <h3><?php echo $lang['CHARACTERDETAILS_SORTIE_HEADER'];?></h3>
        <table>
            <tr>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_1'];?></td>
                <td><?php echo $sorties;?></td>
            </tr>
            <tr>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_2'];?></td>
                <td colspan="1"></td>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_3'];?></td>
                <td colspan="3"></td>
                <td><?php echo $lang["CHARACTERDETAILS_VICTORIES_3"];?></td>
                <td><?php echo $lang["CHARACTERDETAILS_VICTORIES_4"];?></td>
            </tr>
            <tr>
                <td colspan=4><hr/></td>
                <td></td>
                <td colspan=3><hr/></td>
            </tr>
            <tr>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_4']; ?></td>
                <td><?php echo $pilotOK; ?></td>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_8']; ?></td>
                <td><?php echo $aircraftOK; ?></td>
                <td></td>
                <td><?php echo $lang["CHARACTERDETAILS_VICTORIES_1"];?></td>
                <td><?php echo $row['victoriesF'];?></td>
                <td><?php echo $row['victoriesB'];?></td>
            </tr>
            <tr>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_5']; ?></td>
                <td><?php echo $pilotWND; ?></td>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_9']; ?></td>
                <td><?php echo $aircraftDMG; ?></td>
                <td></td>
                <td><?php echo $lang["CHARACTERDETAILS_VICTORIES_2"];?></td>
                <td><?php echo $row['victoriesSHF'];?></td>
                <td><?php echo $row['victoriesSHB'];?></td>
            </tr>
            <tr>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_6']; ?></td>
                <td><?php echo $pilotPOW; ?></td>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_10']; ?></td>
                <td><?php echo $aircraftLOST; ?></td>
            </tr>
            <tr>
                <td><?php echo $lang['CHARACTERDETAILS_SORTIE_7']; ?></td>
                <td><?php echo $pilotKIA; ?></td>
                <td colspan="2"></td>
            </tr>
        </table>    
    </div>
</div>
<div class="contentText">
    <h3><?php echo $lang['CHARACTERDETAILS_MEDALS_HEADER'];?></h3>
    <div><?php ps_character_awards($c_id, "big");?></div>
</div>
<div class="contentText">
    <div class="psTable">
    <h3><?php echo $lang['CHARACTERDETAILS_PROMOTIONS_HEADER']; ?></h3>
    <table style="width: 50%; float: left" >
        <tr>
            <th><?php echo $lang['CHARACTERDETAILS_PROMOTIONS_1']; ?></th>
            <th><?php echo $lang['CHARACTERDETAILS_PROMOTIONS_2']; ?></th>
        </tr>
        <td colspan=2><hr/></td>
        <?php
            for($n = 0; $n < count($promotiondates); $n++){ 
        ?>
        <tr>
            <td><?php echo $promotiondates[$n][3];?></td>
            <td><?php echo $promotiondates[$n][4];?></td>
            <td>
                <?php if($faction == "RAF"){ ?>
                <img src="../acg-pam/imgsource/RAF-ranks/<?php echo $promotiondates[$n][1]; ?>"
                height="50%">
                <?php } else if($faction == "LW"){ ?>
                <img src="../acg-pam/imgsource/LW-ranks/<?php echo $promotiondates[$n][1]; ?>"
                height="50%">
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
    </table>
    </div>
</div>
<?php
	include(dirname(dirname(__FILE__))."/includes/footer.inc.php");
?>
