<?php
	include(dirname(dirname(__FILE__))."/includes/header1.inc.php");
	include_once(dirname(dirname(__FILE__)).'/acg-pam/includes/db_connect.php');
	include_once(dirname(__FILE__).'/ps_renderFunctions.php');

    if(filter_has_var(INPUT_GET, "pilot")) {
        $pilot = urldecode(filter_input(INPUT_GET, "pilot"));
    } else {
        // header("location: pilotList.php");
        // exit();
    }
    
    $dbx = getDBx();
    $sql = "CALL `PS_getPCStatusesVictories` ('$pilot', '', 'LW');";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);

    $LWsorties = $row["sorties"];
    $LWpilotOK = $row["pilotOK"];
    $LWpilotWND = $row["pilotWND"];
    $LWpilotPOW = $row["pilotPOW"];
    $LWpilotKIA = $row["pilotKIA"];
    $LWaircraftOK = $row["aircraftOK"];
    $LWaircraftDMG = $row["aircraftDMG"];
    $LWaircraftLOST = $row["aircraftLOST"];
    $LWvictoriesF = $row["victoriesF"];
    $LWvictoriesSHF = $row["victoriesSHF"];
    $LWvictoriesB = $row["victoriesB"];
    $LWvictoriesSHB = $row["victoriesSHB"];
    
    mysqli_free_result($result);
    mysqli_next_result($dbx);
    
    $sql = "CALL `PS_getPCStatusesVictories` ('$pilot', '', 'RAF');";
    $result = mysqli_query($dbx, $sql);
    $row = mysqli_fetch_assoc($result);
    
    $RAFsorties = $row["sorties"];
    $RAFpilotOK = $row["pilotOK"];
    $RAFpilotWND = $row["pilotWND"];
    $RAFpilotPOW = $row["pilotPOW"];
    $RAFpilotKIA = $row["pilotKIA"];
    $RAFaircraftOK = $row["aircraftOK"];
    $RAFaircraftDMG = $row["aircraftDMG"];
    $RAFaircraftLOST = $row["aircraftLOST"];
    $RAFvictoriesF = $row["victoriesF"];
    $RAFvictoriesSHF = $row["victoriesSHF"];
    $RAFvictoriesB = $row["victoriesB"];
    $RAFvictoriesSHB = $row["victoriesSHB"];
    
    mysqli_free_result($result);
    mysqli_next_result($dbx);
    
?>
	<title>Air Combat Group | Public server pilot profile</title>
	<meta name="description" content="Air Combat Group is a 'full real' IL2 Cliffs of Dover squad with a focus on teamwork and tactics to provide a great environment in which to enjoy simulating various historically accurate moments of the Battle of Britain and other Theatres of Operation."/>
	<meta name="keywords" content="IL2, Cliffs of Dover, ACG, Full Real, multiplayer, teamspeak, historical, Battle of Britain, flight sim, simulation"/>
<?php
	include(dirname(dirname(__FILE__))."/includes/header2.inc.php");
	include("pilotDetails.lang.".$ac_lang.".php");
?>
<div class="contentText">	
	<h3><?php echo $lang['PILOTDETAILS_HEADER1'];?></h3>
    <p><?php echo $lang['PILOTDETAILS_P1'];?></p>
</div>
<div class="contentText">
    <hr>
    <!--<a class="buttonLink" href="missionList.php">Missions</a>-->
    <a class="buttonLink" href="pilotCharacterList.php?pilot=<?php echo $pilot;?>">Characters</a>
</div>
<div>
    <h3><?php echo $lang['PILOTDETAILS_HEADER2'];?></h3>
    
    <div style="display: inline-block; width: 50%;">
        <table>
            <tr><td><?php echo $lang['PILOTDETAILS_TH1'];?></td><td><?php echo $pilot;?></td></tr>
        </table>
    </div>
    <div style="display: inline-block;">
        <table>
            <tr><td><?php echo $lang['PILOTDETAILS_TH1'];?></td><td><?php echo $pilot;?></td></tr>
        </table>
    </div>
</div>
<div>
    <h3><?php echo $lang['PILOTDETAILS_SORTIE_HEADER'];?></h3>
    <div class="psTable">
        <table>
            <tr>
                <td></td>
                <td colspan="1"><?php echo $lang['PILOTDETAILS_SORTIE_LUFTWAFFE'];?></td>
                <td colspan="1"><?php echo $lang['PILOTDETAILS_SORTIE_RAF'];?></td>
                <td></td>
                <td colspan="1"><?php echo $lang['PILOTDETAILS_SORTIE_LUFTWAFFE'];?></td>
                <td colspan="1"><?php echo $lang['PILOTDETAILS_SORTIE_RAF'];?></td>
            </tr>
            <tr><td colspan="100%"><hr/></td></tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_TH3'];?></td>
                <td><?php echo $LWsorties;?></td>
                <td><?php echo $RAFsorties;?></td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_TH4'];?></td>
                <td colspan="2"></td>
                <td><?php echo $lang['PILOTDETAILS_TH6'];?></td>
                <td colspan="2"></td>
            </tr>
            <tr><td colspan="100%"><hr/></td></tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_PS1']; ?></td>
                <td><?php echo $LWpilotOK; ?></td>
                <td><?php echo $RAFpilotOK; ?></td>
                <td><?php echo $lang['PILOTDETAILS_AS1']; ?></td>
                <td><?php echo $LWaircraftOK; ?></td>
                <td><?php echo $RAFaircraftOK; ?></td>
            </tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_PS2']; ?></td>
                <td><?php echo $LWpilotWND; ?></td>
                <td><?php echo $RAFpilotWND; ?></td>
                <td><?php echo $lang['PILOTDETAILS_AS2']; ?></td>
                <td><?php echo $LWaircraftDMG; ?></td>
                <td><?php echo $RAFaircraftDMG; ?></td>
            </tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_PS3']; ?></td>
                <td><?php echo $LWpilotPOW; ?></td>
                <td><?php echo $RAFpilotPOW; ?></td>
                <td><?php echo $lang['PILOTDETAILS_AS3']; ?></td>
                <td><?php echo $LWaircraftLOST; ?></td>
                <td><?php echo $RAFaircraftLOST; ?></td>
            </tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_PS4']; ?></td>
                <td><?php echo $LWpilotKIA; ?></td>
                <td><?php echo $RAFpilotKIA; ?></td>
                <td colspan="3"></td>
            </tr>
        </table>    
    </div>
</div>
<div class="contentText">
    <h3><?php echo $lang['PILOTDETAILS_MEDALS_HEADER'];?></h3>
    <div><?php ps_pilot_awards($pilot, "big", "LW");?></div>
    <div><?php ps_pilot_awards($pilot, "big", "RAF");?></div>
</div>
<div>
    <h3><?php echo $lang['PILOTDETAILS_VICTORIES_HEADER'];?></h3>
    <div class="psTable">
        <table>
            <tr>
                <td colspan="2"></td>
                <td colspan="1"><?php echo $lang['PILOTDETAILS_SORTIE_LUFTWAFFE'];?></td>
                <td colspan="1"><?php echo $lang['PILOTDETAILS_SORTIE_RAF'];?></td>
           </tr>
            <tr><td colspan="100%"><hr/></td></tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_V1'];?></td>
                <td></td>
                <td><?php echo $LWvictoriesF + $LWvictoriesB;?></td>
                <td><?php echo $RAFvictoriesF + $RAFvictoriesB;?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo $lang["PILOTDETAILS_V3"];?></td>
                <td><?php echo $LWvictoriesF;?></td>
                <td><?php echo $RAFvictoriesF;?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo $lang["PILOTDETAILS_V4"];?></td>
                <td><?php echo $LWvictoriesB;?></td>
                <td><?php echo $RAFvictoriesB;?></td>
            </tr>
            <tr><td colspan="100%"><hr/></td></tr>
            <tr>
                <td><?php echo $lang['PILOTDETAILS_V2'];?></td>
                <td></td>
                <td><?php echo $LWvictoriesSHF + $LWvictoriesSHB;?></td>
                <td><?php echo $RAFvictoriesSHF + $RAFvictoriesSHB;?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo $lang["PILOTDETAILS_V3"];?></td>
                <td><?php echo $LWvictoriesSHF;?></td>
                <td><?php echo $RAFvictoriesSHF;?></td>
            </tr>
            <tr>
                <td></td>
                <td><?php echo $lang["PILOTDETAILS_V4"];?></td>
                <td><?php echo $LWvictoriesSHB;?></td>
                <td><?php echo $RAFvictoriesSHB;?></td>
            </tr>
        </table>    
    </div>
    <?php
    
    $sql = "CALL `PS_getPilotPromotions` ('$pilot');";
    $result = mysqli_query($dbx, $sql);

    { ?>
    <h3><?php echo $lang['PILOTDETAILS_PROMOTIONS_HEADER'];?></h3>
    <div class="psTable" style="margin: auto; width: 50%;">
        <table>    
            <tr>
                <td><?php echo $lang['PILOTDETAILS_PR1'];?></td>
                <td><?php echo $lang['PILOTDETAILS_PR2'];?></td>
            </tr>
            <?php while($row = mysqli_fetch_row($result)){?>
            <tr>
                <td><?php echo $row[1]; ?></td>
                <td><?php echo $row[0]; ?></td>
            </tr>
            <?php } ?>
        </table>    
    </div>
</div>
    <?php }    ?>
<?php
	include(dirname(dirname(__FILE__))."/includes/footer.inc.php");
?>